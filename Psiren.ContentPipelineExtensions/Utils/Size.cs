﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;

namespace Psiren.ContentPipelineExtensions.Utils
{
    public struct  Size
    {
        /// <summary>
        ///     Gets a Size structure that has a Height and Width value of 0
        /// </summary>
        public static readonly Size Empty = new Size(0, 0);

        /// <summary>
        ///     Tests whether this Size structure has widht and height of 0
        /// </summary>
        /// <returns>
        ///     This property returns ture when this Size structure has both a 
        ///     width and height of 0; otherwise false
        /// </returns>
        public bool IsEmpty {
            get
            {
                return this.Equals(Empty);
            }
        }

        /// <summary>
        ///     Gets or sets the horizontal compoent of this Size structure
        /// </summary>
        /// <returns>
        ///     The horizontal component of this Size structure, typically measured in pixels
        /// </returns>
        [JsonProperty("w")]
        public int Width { get; set; }

        /// <summary>
        ///     Gets or sets the vertical component of this Size structure
        /// </summary>
        /// <returns>
        ///     The vertical component of this Size structure, typically measured in pixels
        /// </returns>
        [JsonProperty("h")]
        public int Height { get; set; }


        /// <summary>
        ///     Initilizes a new instance of the Size structure from the specified Point structure
        /// </summary>
        /// <param name="point">The <see cref="Point"/> structrue from which to initialize this Size structure</param>
        public Size(Point point)
        {
            this.Width = point.X;
            this.Height = point.Y;
        }

        /// <summary>
        ///     Initilizes a new instance of the Size structure from the specified dimensions
        /// </summary>
        /// <param name="width">The width component of the new size</param>
        /// <param name="height">The height component of the new size</param>
        public Size(int width, int height)
        {
            this.Width = width;
            this.Height = height;
        }

        /// <summary>
        ///     Adds the width and height of on Size structure to the width and height
        ///     of another Size structure
        /// </summary>
        /// <param name="sz1">The first Size structure to add</param>
        /// <param name="sz2">The second Size structure to add</param>
        /// <returns>
        ///     A Size structure that is the result of the addition operation
        /// </returns>
        public static Size Add(Size sz1, Size sz2)
        {
            return sz1 + sz2;
        }

        /// <summary>
        ///     Converts the specified SizeF structure to a Size
        ///     structure by rounding the values of the Size structure to the
        ///     next higher integer values.
        /// </summary>
        /// <param name="value">The SizeF structure to convert.</param>
        /// <returns>
        ///     The Size structure this method converts to.
        /// </returns>
        public static Size Ceiling(SizeF value)
        {
            return new Size((int)Math.Ceiling(value.Width), (int)Math.Ceiling(value.Height));
        }

        /// <summary>
        ///     Converts the specified SizeF structure to a Size
        ///     structure by rounding the values of the SizeF structure to the
        ///     nearest integer values.
        /// </summary>
        /// <param name="value">The SizeF structure to convert.</param>
        /// <returns>
        ///     The Size structure this method converts to.
        /// </returns>
        public static Size Round(SizeF value)
        {
            return new Size((int)Math.Round(value.Width), (int)Math.Round(value.Height));
        }

        /// <summary>
        ///     Subtracts the width and height of one Size structure from the
        ///     width and height of another Size structure
        /// </summary>
        /// <param name="sz1">The Size structure on the left side of the subraction operation</param>
        /// <param name="sz2">The Size structure on the right side of the subtraction operation</param>
        /// <returns>
        ///     A Size structure that is a result of the subtraction operation
        /// </returns>
        public static Size Subtract(Size sz1, Size sz2)
        {
            return sz1 - sz2;
        }


        /// <summary>
        ///     Converts the specified SizeF structure to a Size
        ///     structure by truncating the values of the SizeF structure to the
        ///     next lower integer values.
        /// </summary>
        /// <param name="value">The SizeF structure to convert.</param>
        /// <returns>
        ///     The Size structure this method converts to.
        /// </returns>
        public static Size Trunicate(SizeF value)
        {
            return new Size((int)Math.Floor(value.Width), (int)Math.Floor(value.Height));
        }

        /// <summary>
        ///     Tests to see whether the specified object is a Size structure
        ///     witht he same dimensions as  this Size strcture
        /// </summary>
        /// <param name="obj">The object to test</param>
        /// <returns>
        ///     tru if obj is a Size structure and has the same widht and height as this
        ///     Size structure; otherwise false
        /// </returns>
        public override bool Equals(object obj)
        {
            if(obj is Size other)
            {
                return this.Width == other.Width && this.Height == other.Height;
            }
            return false;   
        }

        /// <summary>
        ///     Returns a hash code for this Size structure
        /// </summary>
        /// <returns>
        ///     An integer value that specifies a hash value for this Size structure
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        ///     Creates a human-readable string that represents this Size structure
        /// </summary>
        /// <returns>
        ///     A string that represents this size structure
        /// </returns>
        public override string ToString()
        {
            return $"(W: {this.Width}, H: {this.Height})";
        }


        /// <summary>
        ///     Adds the width and height of one Size structure to the width and height of
        ///     another Size structure
        /// </summary>
        /// <param name="sz1">The first Size to add</param>
        /// <param name="sz2">The second Size to add</param>
        /// <returns>
        ///     A Size structure that is the result of the addition operation
        /// </returns>
        public static Size operator +(Size sz1, Size sz2)
        {
            return new Size(sz1.Width + sz2.Width, sz1.Height + sz2.Height);
        }


        /// <summary>
        ///     Subtracts the width and height of one Size structure from the
        ///     width and height of another Size structure.
        /// </summary>
        /// <param name="sz1">The Size structure on the left side of the subtraction operator.</param>
        /// <param name="sz2">The Size structure on the right side of the subtraction operator.</param>
        /// <returns>
        ///     A Size structure that is the result of the subtraction operation.
        /// </returns>
        public static Size operator -(Size sz1, Size sz2)
        {
            return new Size(sz1.Width - sz2.Width, sz1.Height - sz2.Height);
        }

        /// <summary>
        ///     Tests whether two Size structures are equal.
        /// </summary>
        /// <param name="sz1">The Size structure on the left side of the equality operator.</param>
        /// <param name="sz2">The Size structure on the right of the equality operator.</param>
        /// <returns>
        ///     true if sz1 and sz2 have equal width and height; otherwise, false.
        /// </returns>
        public static bool operator ==(Size sz1, Size sz2)
        {
            return sz1.Equals(sz2);
        }

        /// <summary>
        ///     Tests whether two Size structures are different.
        /// </summary>
        /// <param name="sz1">The Size structure on the left of the inequality operator.</param>
        /// <param name="sz2">The Size structure on the right of the inequality operator.</param>
        /// <returns>
        ///     true if sz1 and sz2 differ either in width or height; false if sz1 and sz2 are
        ///     equal.
        /// </returns>
        public static bool operator !=(Size sz1, Size sz2)
        {
            return !sz2.Equals(sz2);
        }

        /// <summary>
        ///     Converts the specified Size structure to a SizeF
        ///     structure.
        /// </summary>
        /// <param name="p">The Size structure to convert.</param>
        /// <returns>
        ///     The SizeF structure to which this operator converts.
        /// </returns>
        public static implicit operator SizeF(Size p)
        {
            return new SizeF(p.Width, p.Height);
        }

        /// <summary>
        ///     Converts the specified Size structure to a System.Drawing.Point
        ///     structure.
        /// </summary>
        /// <param name="size">The Size structure to convert.</param>
        /// <returns>
        ///     The System.Drawing.Point structure to which this operator converts.
        /// </returns>
        public static explicit operator Point(Size size)
        {
            return new Point(size.Width, size.Height);
        }




    }
}
