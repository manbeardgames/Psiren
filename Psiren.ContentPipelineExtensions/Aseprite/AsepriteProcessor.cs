﻿using Microsoft.Xna.Framework.Content.Pipeline;

using TInput = System.String;
using TOutput = Psiren.ContentPipelineExtensions.Aseprite.Models.AsepriteModel;

namespace Psiren.ContentPipelineExtensions.Aseprite
{
    [ContentProcessor(DisplayName = "Aseprite Animation Processor")]
    public class AsepriteProcessor : ContentProcessor<TInput, TOutput>
    {
        public override TOutput Process(TInput input, ContentProcessorContext context)
        {
            //  Convert the input json string to anAsepriteModel
            context.Logger.LogMessage($"Input Length: {input.Length}");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Psiren.ContentPipelineExtensions.Aseprite.Models.AsepriteModel>(input);
        }
    }
}
