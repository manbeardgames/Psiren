﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Psiren.ContentReaders;
using TWrite = Psiren.ContentPipelineExtensions.Aseprite.Models.AsepriteModel;

namespace Psiren.ContentPipelineExtensions.Aseprite
{
    [ContentTypeWriter]
    public class AsepriteWriter : ContentTypeWriter<TWrite>
    {

        protected override void Write(ContentWriter output, TWrite value)
        {
            //  Write how many frames there are in total
            output.Write(value.frames.Count);

            //  Write the data about the frames
            for (int i = 0; i < value.frames.Count; i++)
            {
                //  filename
                output.Write(value.frames[i].filename);

                //  frame-x
                output.Write(value.frames[i].frame.x);

                //  frame-y
                output.Write(value.frames[i].frame.y);

                //  frame-width
                output.Write(value.frames[i].frame.w);

                //  frame-height
                output.Write(value.frames[i].frame.h);

                //  rotated
                output.Write(value.frames[i].rotated);

                //  trimmed
                output.Write(value.frames[i].trimmed);

                //  spriteSourceSize-x
                output.Write(value.frames[i].spriteSourceSize.x);

                //  sprieSourceSize-y
                output.Write(value.frames[i].spriteSourceSize.y);

                //  spriteSourceSize-width
                output.Write(value.frames[i].spriteSourceSize.w);

                //  spriteSourceSize-height
                output.Write(value.frames[i].spriteSourceSize.h);

                //  sourceSize-width
                output.Write(value.frames[i].sourceSize.Width);

                //  sourceSize-height
                output.Write(value.frames[i].sourceSize.Height);

                //  duration 
                output.Write(value.frames[i].duration);
            }


            //  Write how many animations there are in total
            output.Write(value.meta.frameTags.Count);

            //  write the dta about the aniamtions
            for (int i = 0; i < value.meta.frameTags.Count; i++)
            {
                //  name
                output.Write(value.meta.frameTags[i].name);

                //  from
                output.Write(value.meta.frameTags[i].from);

                //  to
                output.Write(value.meta.frameTags[i].to);

                //  direction
                output.Write(value.meta.frameTags[i].direction);
            }

        }

        public override string GetRuntimeType(TargetPlatform targetPlatform)
        {
            return typeof(AsepritePackageReader).AssemblyQualifiedName;
        }


        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Psiren.ContentReaders.AsepritePackageReader, Psiren";
        }
    }

}
