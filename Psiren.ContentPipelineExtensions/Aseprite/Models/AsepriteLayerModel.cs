﻿namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteLayerModel
    {
        public string name { get; set; }
        public int opacity { get; set; }
        public string blendmode { get; set; }
    }
}
