﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteModel
    {
        public List<AsepriteFrameModel> frames { get; set; }
        public AsepriteMetaModel meta { get; set; }
    }
}
