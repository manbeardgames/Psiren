﻿
using Newtonsoft.Json;
using Psiren.ContentPipelineExtensions.Converters;
using Psiren.ContentPipelineExtensions.Utils;

namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteFrameModel
    {
        public string filename { get; set; }

        //[JsonConverter(typeof(RectangleConverter))]
        public Rectangle frame { get; set; }

        public bool rotated { get; set; }
        public bool trimmed { get; set; }

        //[JsonConverter(typeof(RectangleConverter))]
        public Rectangle spriteSourceSize { get; set; }

        //[JsonConverter(typeof(SizeConverter))]
        public Size sourceSize { get; set; }
        public int duration { get; set; }
    }
}
