﻿namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteFrameTagModel
    {
        public string name { get; set; }
        public int from { get; set; }
        public int to { get; set; }
        public string direction { get; set; }
    }
}
