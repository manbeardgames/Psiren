﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Psiren.ContentPipelineExtensions.Converters;

namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteSliceKeyModel
    {
        public int frame { get; set; }

        //[JsonConverter(typeof(RectangleConverter))]
        public Rectangle bounds { get; set; }
    }
}
