﻿using System.Collections.Generic;

namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteSliceModel
    {
        public string name { get; set; }
        public string color { get; set; }
        public List<AsepriteSliceKeyModel> keys { get; set; }
    }
}
