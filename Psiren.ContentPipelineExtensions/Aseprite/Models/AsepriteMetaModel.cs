﻿using Newtonsoft.Json;
using Psiren.ContentPipelineExtensions.Converters;
using Psiren.ContentPipelineExtensions.Utils;
using System.Collections.Generic;

namespace Psiren.ContentPipelineExtensions.Aseprite.Models
{
    public class AsepriteMetaModel
    {
        public string app { get; set; }
        public string version { get; set; }
        public string format { get; set; }

        //[JsonConverter(typeof(SizeConverter))]
        public Size size { get; set; }

        public float scale { get; set; }
        public List<AsepriteFrameTagModel> frameTags { get; set; }
        public List<AsepriteLayerModel> layers { get; set; }
        public List<AsepriteSliceModel> slices { get; set; }
    }
}
