﻿using Microsoft.Xna.Framework.Content.Pipeline;
using System.IO;

using TInput = System.String;

namespace Psiren.ContentPipelineExtensions.Aseprite
{
    [ContentImporter(".json", DisplayName = "Aseprite Animation Importer", DefaultProcessor = "AsepriteProcessor")]
    public class AsepriteImporter : ContentImporter<TInput>
    {
        public override TInput Import(string filename, ContentImporterContext context)
        {
            //  Read the json from the file and return it
            return File.ReadAllText(filename);
        }
    }
}
