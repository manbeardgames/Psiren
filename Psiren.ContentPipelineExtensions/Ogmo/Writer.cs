﻿using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TWrite = Psiren.ContentPipelineExtensions.Ogmo.Models.LevelWriterModel;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    [ContentTypeWriter]
    public class Writer : ContentTypeWriter<TWrite>
    {
        protected override void Write(ContentWriter output, TWrite value)
        {
            output.Write(value.Json);
        }

        public override string GetRuntimeReader(TargetPlatform targetPlatform)
        {
            return "Psiren.Ogmo.LevelReader, Psiren.Ogmo";
        }
    }
}
