﻿using Microsoft.Xna.Framework.Content.Pipeline;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TImport = Psiren.ContentPipelineExtensions.Ogmo.Models.LevelImportModel;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    [ContentImporter(".pl", DisplayName = "Psiren (OGMO) Level Importer", DefaultProcessor = "Processor")]
    public class Importer : ContentImporter<TImport>
    {
        public override TImport Import(string filename, ContentImporterContext context)
        {
            //  Import the contents of the file
            context.Logger.LogMessage("Reading file contents");
            string json = File.ReadAllText(filename);

            TImport model = new TImport();
            model.Json = json;
            return model;
        }
    }
}
