﻿//////////--------------------------------------------------------------------------------
//////////  Processor
//////////
//////////  This is the ContentProcessor for Level objects
//////////
//////////-----------------------------Psiren Engine License-----------------------------

//////////    Copyright(c) 2018   Chris Whitley
//////////
//////////    Permission is hereby granted, free of charge, to any person obtaining a copy
//////////    of this software and associated documentation files (the "Software"), to deal
//////////    in the Software without restriction, including without limitation the rights
//////////    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//////////    copies of the Software, and to permit persons to whom the Software is
//////////    furnished to do so, subject to the following conditions:

//////////    The above copyright notice and this permission notice shall be included in
//////////    all copies or substantial portions of the Software.

//////////    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//////////    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//////////    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//////////    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//////////    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//////////    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//////////    THE SOFTWARE.
//////////--------------------------------------------------------------------------------
////////using Microsoft.Xna.Framework.Content.Pipeline;
////////using Psiren.Ogmo.Project;
////////using System;
////////using System.Collections.Generic;
////////using System.Xml;
////////using System.Linq;

////////using TInput = Psiren.ContentPipelineExtensions.Ogmo.Level.LevelImportModel;
//////////using TOutput = Psiren.Ogmo.Level.OgmoLevel;
////////using TOutput = Psiren.ContentPipelineExtensions.Ogmo.Level.LevelWriterModel;
////////using Psiren.Ogmo.Level;

////////namespace Psiren.ContentPipelineExtensions.Ogmo.Level
////////{
////////    [ContentProcessor(DisplayName = "Ogmo Level Processor")]
////////    public class Processor : ContentProcessor<TInput, TOutput>
////////    {
////////        public override TOutput Process(TInput input, ContentProcessorContext context)
////////        {

////////            //  Create the OGMO Level Object (TOutput)
////////            Psiren.Ogmo.Level.OgmoLevel level = new OgmoLevel();

////////            //  Create the XmlDocument to step through
////////            XmlDocument doc = new XmlDocument();
////////            doc.LoadXml(input.LevelXml);

////////            //  Get the Width and Height
////////            XmlNode levelNode = doc.SelectSingleNode("/level");
////////            level.Width = System.Int32.Parse(levelNode.Attributes["width"].Value);
////////            level.Height = System.Int32.Parse(levelNode.Attributes["height"].Value);



////////            //  Go through each of the child nodes of the level node
////////            //  These children are the actual layers
////////            level.EntityLayers = new List<Psiren.Ogmo.Level.EntityLayer>();
////////            level.TileLayers = new List<Psiren.Ogmo.Level.TileLayer>();
////////            level.GridLayers = new List<Psiren.Ogmo.Level.GridLayer>();
////////            foreach (XmlNode layerNode in levelNode.ChildNodes)
////////            {
////////                //  Look for this node in the OGMO Project to determine what type
////////                //  of layer it is for processsing
////////                if (input.Project.TileLayerDefinitions.Any(tileLayer => tileLayer.Name == layerNode.Name))
////////                {
////////                    //  Ensure the TileLayer is set to exportmode XML.
////////                    //  This is a requirement, we do not import otherwise
////////                    if (layerNode.Attributes["exportMode"].Value != "XML")
////////                    {
////////                        throw new Exception($"Tile Layer named {layerNode.Name} is not set to XML export mode. This is a requirement for import");
////////                    }

////////                    TileLayer tileLayer = new TileLayer();
////////                    tileLayer.Name = layerNode.Name;
////////                    tileLayer.Order = level.TileLayers.Count;

////////                    //  Get the name of the tileset to use
////////                    tileLayer.TileSet = layerNode.Attributes["tileset"].Value.ToLower();
////////                    context.Logger.LogMessage("TILE SET NAME: " + tileLayer.TileSet);

////////                    //  Get all the tiles
////////                    tileLayer.Tiles = new List<Tile>();
////////                    foreach (XmlNode tileNode in layerNode.ChildNodes)
////////                    {
////////                        Tile tile = new Tile();
////////                        tile.X = Int32.Parse(tileNode.Attributes["x"].Value);
////////                        tile.Y = Int32.Parse(tileNode.Attributes["y"].Value);
////////                        tile.ID = Int32.Parse(tileNode.Attributes["id"].Value);
////////                        tileLayer.Tiles.Add(tile);
////////                    }

////////                    level.TileLayers.Add(tileLayer);
////////                }
////////                else if (input.Project.EntityLayerDefinitions.Any(entityLayer => entityLayer.Name == layerNode.Name))
////////                {
////////                    //  Create a new EntityLayer object
////////                    EntityLayer entityLayer = new EntityLayer();

////////                    //  Get the name from the naem of the layer node
////////                    entityLayer.Name = layerNode.Name;

////////                    //  Set the order
////////                    entityLayer.Order = level.EntityLayers.Count;
                    
////////                    //  Get all the entities
////////                    entityLayer.Entities = new List<OgmoEntity>();
////////                    foreach(XmlNode entityNode in layerNode.ChildNodes)
////////                    {
////////                        //  Create new OgoEntity object
////////                        OgmoEntity entity = new OgmoEntity();

////////                        //  Get the name from the entity node
////////                        entity.Name = entityNode.Name;
////////                        entity.ID = Int32.Parse(entityNode.Attributes["id"].Value);
////////                        entity.X = Int32.Parse(entityNode.Attributes["x"].Value);
////////                        entity.Y = Int32.Parse(entityNode.Attributes["y"].Value);
////////                        entityLayer.Entities.Add(entity);
////////                    }

////////                    level.EntityLayers.Add(entityLayer);
////////                }
////////                else if (input.Project.GridLayerDefinitions.Any(gridlayer => gridlayer.Name == layerNode.Name))
////////                {
////////                    //  Ensure that the GridLayer is set to exportmode Rectangles.
////////                    //  This is a requirement, we do not import otherwise
////////                    if(layerNode.Attributes["exportMode"].Value != "Rectangles")
////////                    {
////////                        throw new Exception($"Grid Layer named {layerNode.Name} is not set to Rectangles export mode.  This is a requirement to import");
////////                    }

////////                    //  Create new GridLayer object
////////                    GridLayer gridLayer = new GridLayer();

////////                    //  Get the name from the layerNode name
////////                    gridLayer.Name = layerNode.Name;

////////                    //  Set the order
////////                    gridLayer.Order = level.GridLayers.Count;

////////                    //  Get all the rectangels
////////                    foreach(XmlNode rectNode in layerNode.ChildNodes)
////////                    {
////////                        //  Create new GridRect object
////////                        GridRect rect = new GridRect();

////////                        rect.X = Int32.Parse(rectNode.Attributes["x"].Value);
////////                        rect.Y = Int32.Parse(rectNode.Attributes["y"].Value);
////////                        rect.Width = Int32.Parse(rectNode.Attributes["w"].Value);
////////                        rect.Height = Int32.Parse(rectNode.Attributes["h"].Value);
////////                    }

////////                    level.GridLayers.Add(gridLayer);
                    
////////                }
////////                else
////////                {
////////                    //  Layer could not be found in the tile, entity, or grid layer definitions
////////                    //  in the OGMO Project. Throw an error as we need the information from the
////////                    //  project
////////                    throw new Exception($"No layer with name {layerNode.Name} could be found in the OGMO Project File (.oep) in the TIleLayerDefinitions, EntityLayerDefinitions, or the GridLayerDefinitions.  Please ensure you are using the correction OGMO Project File (.oep)");
////////                }

////////            }

////////            //  Assign the OGMO Project to the level
////////            level.OgmoProject = input.Project;

////////            //  Assign the name ot the level
////////            level.Name = input.LevelName;


////////            LevelWriterModel model = new LevelWriterModel()
////////            {
////////                Level = level,
////////                TileSetBytes = input.TileSetBytes
////////            };

////////            return model;
////////        }
////////    }
////////}
