﻿////////////--------------------------------------------------------------------------------
////////////  Writer
////////////
////////////  This is the ContentTypeWriter for Level objects
////////////
////////////-----------------------------Psiren Engine License-----------------------------

////////////    Copyright(c) 2018   Chris Whitley
////////////
////////////    Permission is hereby granted, free of charge, to any person obtaining a copy
////////////    of this software and associated documentation files (the "Software"), to deal
////////////    in the Software without restriction, including without limitation the rights
////////////    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
////////////    copies of the Software, and to permit persons to whom the Software is
////////////    furnished to do so, subject to the following conditions:

////////////    The above copyright notice and this permission notice shall be included in
////////////    all copies or substantial portions of the Software.

////////////    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
////////////    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
////////////    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
////////////    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
////////////    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
////////////    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
////////////    THE SOFTWARE.
////////////--------------------------------------------------------------------------------
//////////using Microsoft.Xna.Framework.Content.Pipeline;
//////////using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Compiler;

////////////using TWrite = Psiren.Ogmo.Level.OgmoLevel;
//////////using TWrite = Psiren.ContentPipelineExtensions.Ogmo.Level.LevelWriterModel;

//////////namespace Psiren.ContentPipelineExtensions.Ogmo.Level
//////////{
//////////    [ContentTypeWriter]
//////////    public class Writer : ContentTypeWriter<TWrite>
//////////    {
//////////        protected override void Write(ContentWriter output, TWrite value)
//////////        {

//////////            //  First write the level string
//////////            string json = Newtonsoft.Json.JsonConvert.SerializeObject(value.Level);
//////////            output.Write(json);

//////////            //  Next write int of number of tilesets
//////////            output.Write(value.TileSetBytes.Count);

//////////            foreach (var kvp in value.TileSetBytes)
//////////            {
//////////                //  Write the name of the tileset as a string
//////////                output.Write(kvp.Key);

//////////                //  Write the number of bytes in the array
//////////                output.Write(kvp.Value.Length);

//////////                //  Write the byte[] 
//////////                output.Write(kvp.Value);
//////////            }


//////////        }

//////////        public override string GetRuntimeReader(TargetPlatform targetPlatform)
//////////        {
//////////            return "Psiren.Ogmo.Level.LevelReader, Psiren.Ogmo";
//////////        }
//////////    }
//////////}
