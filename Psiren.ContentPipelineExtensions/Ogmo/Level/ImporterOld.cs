﻿//////////--------------------------------------------------------------------------------
//////////  Importer
//////////
//////////  This is the ContentImporter for Level objects
//////////
//////////-----------------------------Psiren Engine License-----------------------------

//////////    Copyright(c) 2018   Chris Whitley
//////////
//////////    Permission is hereby granted, free of charge, to any person obtaining a copy
//////////    of this software and associated documentation files (the "Software"), to deal
//////////    in the Software without restriction, including without limitation the rights
//////////    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//////////    copies of the Software, and to permit persons to whom the Software is
//////////    furnished to do so, subject to the following conditions:

//////////    The above copyright notice and this permission notice shall be included in
//////////    all copies or substantial portions of the Software.

//////////    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//////////    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//////////    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//////////    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//////////    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//////////    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//////////    THE SOFTWARE.
//////////--------------------------------------------------------------------------------
////////using Microsoft.Xna.Framework.Content.Pipeline;
////////using Psiren.Ogmo.Project;
////////using System.Collections.Generic;
////////using System.Linq;
////////using TImport = Psiren.ContentPipelineExtensions.Ogmo.Level.LevelImportModel;

////////namespace Psiren.ContentPipelineExtensions.Ogmo.Level
////////{
////////    [ContentImporter(".oel", DisplayName = "Ogmo Level Importer", DefaultProcessor = "Processor")]
////////    public class Importer : ContentImporter<TImport>
////////    {
////////        public override TImport Import(string filename, ContentImporterContext context)
////////        {

////////            //  First we need to ensure there is a OGMO Project File (.oep) in the same directory
////////            var directoryPath = System.IO.Path.GetDirectoryName(filename);
////////            var files = System.IO.Directory.GetFiles(directoryPath, "*.oep");

////////            //  Ensure we have a .oep file and only one .oep file
////////            if (files.Length == 0)
////////            {
////////                throw new System.Exception("No OGMO Project File (.oep) located in the same directory as this level file");
////////            }
////////            else if(files.Length > 1)
////////            {
////////                throw new System.Exception($"There should be only one OGMO Project File (.oep) located in the same directory as this level file.  Detected {files.Length} OGMO Project (.oep) files");
////////            }

////////            //  Get the XML of the OGMO Project file
////////            string projectXml = System.IO.File.ReadAllText(files[0]);

////////            //  Create project object from XML
////////            OgmoProject project = OgmoProject.FromXmlString(projectXml);

////////            //  Ensure that we have all the tileset files
////////            foreach(var tileSet in project.TileSets)
////////            {
////////                if(!System.IO.File.Exists(System.IO.Path.Combine(directoryPath, tileSet.FileName)))
////////                {
////////                    throw new System.Exception($"TileSet file {tileSet.FileName} could not be found.  Please ensure it is in the same directory as the OGMO Level File (.oel)");
////////                }
////////            }

////////            //  Get the xml of the level
////////            string levelXml =  System.IO.File.ReadAllText(filename);

////////            //  Get the name of the level from the filename without the extension
////////            string name = System.IO.Path.GetFileNameWithoutExtension(filename);

////////            //  tempFile contins the full file path
////////            var tempFiles = System.IO.Directory.GetFiles(directoryPath, "*", System.IO.SearchOption.TopDirectoryOnly)
////////                .Where(fn => !fn.EndsWith(".oep", System.StringComparison.InvariantCultureIgnoreCase) && !fn.EndsWith(".oel", System.StringComparison.InvariantCultureIgnoreCase))
////////                .ToList<string>();

////////            Dictionary<string, byte[]> TileSetBitties = new Dictionary<string, byte[]>();
////////            List<string> additionalFiles = new List<string>();
////////            foreach (string file in tempFiles)
////////            {
////////                //  Need full file path to read file (obvs)
////////                byte[] imagebitties = System.IO.File.ReadAllBytes(System.IO.Path.Combine(directoryPath, file));

////////                string fileNameNoExtension = System.IO.Path.GetFileNameWithoutExtension(file);
////////                additionalFiles.Add(fileNameNoExtension);
////////                TileSetBitties.Add(fileNameNoExtension, imagebitties);
////////            }

////////            foreach(string file in additionalFiles)
////////            {
////////                context.Logger.LogMessage($"File Name: {file}");
////////            }

////////            return new TImport() { LevelName = name, LevelXml = levelXml, Project = project, AdditionalFiles = additionalFiles.ToArray(), TileSetBytes = TileSetBitties };
////////        }
////////    }
////////}
