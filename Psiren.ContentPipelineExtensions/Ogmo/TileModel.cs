﻿namespace Psiren.ContentPipelineExtensions.Ogmo
{
    public class TileModel
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int ColorA { get; set; }
        public int ColorR { get; set; }
        public int ColorG { get; set; }
        public int ColorB { get; set; }

        public string TileSetName { get; set; }
        public int TileSheetX { get; set; }
        public int TileSheetY { get; set; }
        public int TileSheetWidth { get; set; }
        public int TileSheetHeight { get; set; }
        public int TileSheetSep { get; set; }
        public float ScaleX { get; set; }
        public float ScaleY { get; set; }
    }
}
