﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    public class OgmoLevelImportModel
    {
        public string FileName { get; set; }
        public Dictionary<string, TilesheetModel> Tilesheets { get; set; }

        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Dictionary<string, string> LevelValues { get; set; }


        public Dictionary<string, LayerDefinitionModel> LayerDefinitions { get; set; }
        public List<LayerModel> Layers { get; set; }

        public Dictionary<string, byte[]> TileSetBytes { get; set; }

    }
}
