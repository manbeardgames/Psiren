﻿using Microsoft.Xna.Framework.Content.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Psiren.Ogmo;
using Microsoft.Xna.Framework;
using System.IO;


using TInput = Psiren.ContentPipelineExtensions.Ogmo.Models.LevelImportModel;
using TOutput = Psiren.ContentPipelineExtensions.Ogmo.Models.LevelWriterModel;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    [ContentProcessor(DisplayName = "Psiren (OGMO) Level Processor")]
    public class Processor : ContentProcessor<TInput, TOutput>
    {
        public override TOutput Process(TInput input, ContentProcessorContext context)
        {
            //  Deserialize the json to an OgmoLevelImportModel object
            context.Logger.LogMessage("Deserializing to OgmoLevelImportModel");
            OgmoLevelImportModel model = JsonConvert.DeserializeObject<OgmoLevelImportModel>(input.Json);

            //------------------------------------------------------------
            //  Convert the OgmoLevelImportModel to an OgmoLevelExportModel
            //------------------------------------------------------------
            OgmoLevel level = new OgmoLevel();

            //  Get the level name;
            context.Logger.LogMessage("Getting Name");
            level.Name = model.Name;

            //  Get the level size
            context.Logger.LogMessage("Getting Size");
            level.Size = new Vector2(model.Width, model.Height);

            //  Get the level values
            context.Logger.LogMessage("Getting Level Values");
            level.LevelValues = new Dictionary<string, string>();
            if (model.LevelValues != null)
            {
                foreach (var levelValue in model.LevelValues)
                {
                    context.Logger.LogMessage($"Adding Name = '${levelValue.Key}' with Value = '${levelValue.Value}'");
                    level.LevelValues.Add(levelValue.Key, levelValue.Value);
                }
            }

            //  Get the layer definitions
            context.Logger.LogMessage("Getting Layer Definitions");
            level.LayerDefinitions = new Dictionary<string, OgmoLayerDefinition>();
            foreach (var definition in model.LayerDefinitions)
            {
                OgmoLayerDefinition old = new OgmoLayerDefinition();
                old.Name = definition.Value.Name;
                old.ScrollFactor = new Vector2(definition.Value.ScrollFactorX, definition.Value.ScrollFactorY);
                old.Grid = new Vector2(definition.Value.GridWidth, definition.Value.GridHeight);
                context.Logger.LogMessage($"Adding Layer Definition Name = '${old.Name}'");
                level.LayerDefinitions.Add(old.Name, old);
            }

            //  Get the layers
            context.Logger.LogMessage("Getting Layers");
            level.Layers = new List<OgmoLayer>();
            foreach (var oLayer in model.Layers)
            {
                OgmoLayer layer = new OgmoLayer();
                layer.Name = oLayer.Name;
                layer.Order = oLayer.Order;
                layer.Tiles = new List<OgmoTile>();
                foreach (var oTile in oLayer.Tiles)
                {
                    OgmoTile tile = new OgmoTile();
                    tile.Color = new Color(oTile.ColorR, oTile.ColorG, oTile.ColorB, oTile.ColorA);
                    tile.Position = new Vector2(oTile.X, oTile.Y);
                    tile.Size = new Vector2(oTile.Width, oTile.Height);
                    tile.TileSetName = oTile.TileSetName;
                    tile.TileSheetPosition = new Vector2(oTile.TileSheetX, oTile.TileSheetY);
                    tile.TileSheetTileSize = new Vector2(oTile.TileSheetWidth, oTile.TileSheetHeight);
                    tile.TileSheetSep = oTile.TileSheetSep;
                    tile.Scale = new Vector2(oTile.ScaleX, oTile.ScaleY);
                    layer.Tiles.Add(tile);
                }
                context.Logger.LogMessage($"Adding Layer Name = '${layer.Name}'");
                level.Layers.Add(layer);
            }


            //------------------------------------------------------------
            //  Import the tilesheets
            //------------------------------------------------------------
            context.Logger.LogMessage("Getting Tileset Bytes");
            level.TileSetBytes = new Dictionary<string, byte[]>();
            foreach(var file in model.Tilesheets)
            {
                string name = file.Key;
                byte[] bitties = File.ReadAllBytes(file.Value.FilePath);
                context.Logger.LogMessage($"Adding Tileset Name = '${name}'");
                level.TileSetBytes.Add(name, bitties);
            }

            //  Now serialize the level object
            context.Logger.LogMessage("Serializing level object");
            string outputJson = JsonConvert.SerializeObject(level);
            context.Logger.LogMessage("Serilize successfull");

            TOutput outputModel = new TOutput();
            outputModel.Json = outputJson;

            return outputModel;
        }
    }
}
