﻿using System.Collections.Generic;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    public class LayerModel
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public List<TileModel> Tiles { get; set; }
    }
}
