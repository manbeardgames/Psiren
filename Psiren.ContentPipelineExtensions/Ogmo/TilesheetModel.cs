﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    public class TilesheetModel
    {
        public string Name { get; set; }
        public int TileWidth { get; set; }
        public int TileHeight { get; set; }
        public int TileSep { get; set; }
        public string FilePath { get; set; }
    }
}
