﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.ContentPipelineExtensions.Ogmo
{
    public class LayerDefinitionModel
    {
        public string Name { get; set; }
        public int GridWidth { get; set; }
        public int GridHeight { get; set; }
        public float ScrollFactorX { get; set; }
        public float ScrollFactorY { get; set; }
    }
}
