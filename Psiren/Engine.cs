﻿//--------------------------------------------------------------------------------
//  Engine
//
//  This is the main engine that your Game should inherit from
//
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Audio;
using Psiren.Input;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime;

namespace Psiren
{
    public class Engine : Game
    {
        /// <summary>
        ///     The title of the game window
        /// </summary>
        public string Title;

        /// <summary>
        ///     The game version
        /// </summary>
        public Version Version;

        //----------------------------------------------
        //  References
        //----------------------------------------------
        public static Engine Instance { get; private set; }
        public static GraphicsDeviceManager Graphics { get; private set; }
        public static Commands Commands { get; private set; }
        public static Pooler Pooler { get; private set; }
        public static Action OverloadGameLoop;

        //----------------------------------------------
        //  Screen Size
        //----------------------------------------------
        /// <summary>
        ///     The width of the screen
        /// </summary>
        public static int Width { get; private set; }

        /// <summary>
        ///     The height of the screen
        /// </summary>
        public static int Height { get; private set; }

        /// <summary>
        ///     The width of the view
        /// </summary>
        public static int ViewWidth { get; private set; }

        /// <summary>
        ///     The height of the view
        /// </summary>
        public static int ViewHeight { get; private set; }

        /// <summary>
        ///     The amount of padding to give to the view
        /// </summary>
        public static int ViewPadding
        {
            get { return _viewPadding; }
            set
            {
                _viewPadding = value;
                Instance.UpdateView();
            }
        }
        private static int _viewPadding = 0;

        /// <summary>
        ///     Is the window currently resizing
        /// </summary>
        private static bool _resizing;


        //----------------------------------------------
        //  Time
        //----------------------------------------------
        /// <summary>
        ///     The current frame GameTime provided through the Update method
        /// </summary>
        public static GameTime GameTime { get; private set; }

        /// <summary>
        ///     The delta time
        /// </summary>
        public static float DeltaTime { get; private set; }

        /// <summary>
        ///     The raw delta time
        /// </summary>
        public static float RawDeltaTime { get; private set; }

        /// <summary>
        ///     The time rate that is applied to delta time
        /// </summary>
        public static float TimeRate = 1.0f;

        /// <summary>
        ///     Timer used to freeze time for a certain period
        /// </summary>
        public static float FreezeTimer;

        /// <summary>
        ///     The current FPS
        /// </summary>
        public static int FPS;

        /// <summary>
        ///     Elapsed counter
        /// </summary>
        public TimeSpan counterElapsed = TimeSpan.Zero;

        /// <summary>
        ///     number of fps samples taken
        /// </summary>
        private int fpsCounter = 0;


        //----------------------------------------------
        //  Content Directory
        //----------------------------------------------
        /// <summary>
        ///     The directory that this assembly is located in
        /// </summary>
        private static string AssemblyDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        /// <summary>
        ///     The directory for the Content folder
        /// </summary>
        public static string ContentDirectory
        {
            get { return Path.Combine(AssemblyDirectory, Instance.Content.RootDirectory); }
        }


        //----------------------------------------------
        //  Util
        //----------------------------------------------
        /// <summary>
        ///     Clear color to use when rendering
        /// </summary>
        public static Color ClearColor;

        /// <summary>
        ///     Should the game exit (close) when escape is pressed
        /// </summary>
        public static bool ExitOnEscapeKeypress;

        public static SpriteFont DebugFont;

        //----------------------------------------------
        //  Scene
        //----------------------------------------------
        /// <summary>
        ///     The current <see cref="Psiren.Scene"/>
        /// </summary>
        private Scene _scene;

        /// <summary>
        ///     The next <see cref="Psiren.Scene"/> to load
        /// </summary>
        private Scene _nextScene;

        /// <summary>
        ///     The <see cref="IInputProfile"/> for the Engine to reference
        /// </summary>
        public static Dictionary<PlayerNumber, IInputProfile> InputProfiles = new Dictionary<PlayerNumber, IInputProfile>();
        public static IInputProfile Input
        {
            get
            {
                return InputProfiles[PlayerNumber.One];
            }
        }


        public static IInputProfile PlayerOneInput { get => InputProfiles[PlayerNumber.One]; }

        /// <summary>
        ///     Creates a new <see cref="Engine"/> instance
        /// </summary>
        /// <param name="width">The virtual width</param>
        /// <param name="height">The virtual height</param>
        /// <param name="windowWidth">The window actual width</param>
        /// <param name="windowHeight">The window actual height</param>
        /// <param name="windowTitle">The title of the window</param>
        /// <param name="fullscreen">Start in fullscreen?</param>
        public Engine(int width, int height, int windowWidth, int windowHeight, string windowTitle, bool fullscreen)
        {
            Instance = this;

            Title = Window.Title = windowTitle;
            Width = width;
            Height = height;
            ClearColor = Color.Black;

            Graphics = new GraphicsDeviceManager(this);
            Graphics.DeviceReset += OnGraphicsReset;
            Graphics.DeviceCreated += OnGraphicsCreated;
            Graphics.SynchronizeWithVerticalRetrace = true;
            Graphics.PreferMultiSampling = false;
            Graphics.GraphicsProfile = GraphicsProfile.HiDef;
            Graphics.PreferredBackBufferFormat = SurfaceFormat.Color;
            Graphics.PreferredDepthStencilFormat = DepthFormat.Depth24Stencil8;

            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += OnClientSizeChanged;

            //fullscreen = true;
            if (fullscreen)
            {
                Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                Graphics.IsFullScreen = true;
            }
            else
            {
                Graphics.PreferredBackBufferWidth = windowWidth;
                Graphics.PreferredBackBufferHeight = windowHeight;
                Graphics.IsFullScreen = false;
            }

            Content.RootDirectory = "Content";

            IsMouseVisible = false;
            IsFixedTimeStep = false;
            ExitOnEscapeKeypress = true;



            GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;
        }

        /// <summary>
        ///     Called when the game client size is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnClientSizeChanged(object sender, EventArgs e)
        {
            if (Window.ClientBounds.Width > 0 && Window.ClientBounds.Height > 0 && !_resizing)
            {
                _resizing = true;

                Graphics.PreferredBackBufferWidth = Window.ClientBounds.Width;
                Graphics.PreferredBackBufferHeight = Window.ClientBounds.Height;
                UpdateView();

                _resizing = false;
            }
        }

        /// <summary>
        ///     Called when the GraphicsDevice graphics are reset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnGraphicsReset(object sender, EventArgs e)
        {
            UpdateView();

            if (_scene != null)
            {
                _scene.HandleGraphicsReset();
            }

            if (_nextScene != null && _nextScene != _scene)
            {
                _nextScene.HandleGraphicsReset();
            }
        }

        /// <summary>
        ///     Called when the GraphicsDevice is created
        /// </summary>
        /// <param name="senter"></param>
        /// <param name="e"></param>
        protected virtual void OnGraphicsCreated(object senter, EventArgs e)
        {
            UpdateView();

            if (_scene != null)
            {
                _scene.HandleGraphicsCreate();
            }

            if (_nextScene != null && _nextScene != _scene)
            {
                _nextScene.HandleGraphicsCreate();
            }
        }

        /// <summary>
        ///     Called when activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected override void OnActivated(object sender, EventArgs args)
        {
            base.OnActivated(sender, args);

            if (_scene != null)
            {
                _scene.GainFocus();
            }
        }

        /// <summary>
        ///     Called when dectivated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected override void OnDeactivated(object sender, EventArgs args)
        {
            base.OnDeactivated(sender, args);

            if (_scene != null)
            {
                _scene.LostFocus();
            }
        }

        /// <summary>
        ///     Initializes the <see cref="Engine"/> instance
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            //  Input initialize
            MInput.Initialize();
            Tracker.Initialize();
            Pooler = new Pooler();
            Commands = new Commands();
            Commands.Enabled = true;


        }



        /// <summary>
        ///     Loads content needed
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();

            Psiren.Utils.Draw.Initialize(GraphicsDevice);
            Engine.DebugFont = Engine.Instance.Content.Load<SpriteFont>("fonts/default");
        }

        /// <summary>
        ///     Updates the <see cref="Engine"/>
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            Engine.GameTime = gameTime;
            RawDeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            DeltaTime = RawDeltaTime * TimeRate;

            //  Update input
            MInput.Update();

            if (ExitOnEscapeKeypress) { }

            if (OverloadGameLoop != null)
            {
                OverloadGameLoop();
                base.Update(gameTime);
                return;
            }

            if (FreezeTimer > 0)
            {
                FreezeTimer = Math.Max(FreezeTimer - RawDeltaTime, 0);
            }
            else if (_scene != null)
            {
                _scene.BeforeUpdate();
                _scene.Update();
                _scene.AfterUpdate();
            }

            if (Commands.Open)
            {
                Commands.UpdateOpen();
            }
            else if (Commands.Enabled)
            {
#if DEBUG
                Commands.UpdateClosed();
#endif
            }

            if (_scene != _nextScene)
            {
                var lastScene = _scene;
                if (_scene != null)
                {
                    _scene.End();
                }
                _scene = _nextScene;
                OnSceneTransition(lastScene, _nextScene);
                if (_scene != null)
                {
                    _scene.Begin();
                }
            }
        }

        /// <summary>
        ///     Draw entry point
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Draw(GameTime gameTime)
        {
            RenderCore();

            base.Draw(gameTime);

            if (Commands.Open)
            {
                Commands.Render();
            }

            fpsCounter++;
            counterElapsed += gameTime.ElapsedGameTime;
            if (counterElapsed >= TimeSpan.FromSeconds(1))
            {
#if DEBUG
                Window.Title = $"{Title} {1 / (float)gameTime.ElapsedGameTime.TotalSeconds} fps - {(GC.GetTotalMemory(false) / 1048576.0f).ToString("F")} MB - TotalTime: {gameTime.TotalGameTime:hh\\:mm\\:ss}";
                string mbString = (GC.GetTotalMemory(false) / 1048576.0f).ToString("F") + " MB";
#endif
                FPS = fpsCounter;
                fpsCounter = 0;
                counterElapsed -= TimeSpan.FromSeconds(1);
            }
        }

        /// <summary>
        ///     Renders the current <see cref="Psiren.Scene"/>
        /// </summary>
        protected virtual void RenderCore()
        {
            if (_scene != null)
            {
                _scene.BeforeRender();
            }

            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Viewport = Viewport;
            GraphicsDevice.Clear(ClearColor);

            if (_scene != null)
            {
                _scene.Render();
                _scene.AfterRender();
            }
        }

        /// <summary>
        ///     Called when the game client is exiting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
            //  Shutdown input
            MInput.Shutdown();



        }

        /// <summary>
        ///     Called when a <see cref="Psiren.Scene"/> is transition from current to next
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        protected virtual void OnSceneTransition(Scene from, Scene to)
        {
            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            TimeRate = 1.0f;
        }

        /// <summary>
        ///     The current <see cref="Psiren.Scene"/>
        /// </summary>
        public static Scene Scene
        {
            get { return Instance._scene; }
            set { Instance._nextScene = value; }
        }

        /// <summary>
        ///     The Viewport used
        /// </summary>
        public static Viewport Viewport { get; private set; }

        /// <summary>
        ///     The current screen matrix to use for scaling
        /// </summary>
        public static Matrix ScreenMatrix;

        /// <summary>
        ///     Sets the game client as windowed
        /// </summary>
        /// <param name="width">The width of the window</param>
        /// <param name="height">The height of the window</param>
        public static void SetWindowed(int width, int height)
        {
            if (width > 0 && height > 0)
            {
                _resizing = true;
                Graphics.PreferredBackBufferWidth = width;
                Graphics.PreferredBackBufferHeight = height;
                Graphics.IsFullScreen = false;
                Graphics.ApplyChanges();
                Console.WriteLine($"Window-{width}x{height}");
                _resizing = false;
            }
        }

        /// <summary>
        ///     Sets the game client as fullscreen
        /// </summary>
        public static void SetFullscreen()
        {
            _resizing = true;
            Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            Graphics.IsFullScreen = true;
            Graphics.ApplyChanges();
            Console.WriteLine("FULLSCREEN");
            _resizing = false;
        }

        /// <summary>
        ///     Updates the client view and view information
        /// </summary>
        private void UpdateView()
        {
            float screenWidth = GraphicsDevice.PresentationParameters.BackBufferWidth;
            float screenHeight = GraphicsDevice.PresentationParameters.BackBufferHeight;

            //  Get View Size
            if (screenWidth / Width > screenHeight / Height)
            {
                ViewWidth = (int)(screenHeight / Height * Width);
                ViewHeight = (int)screenHeight;
            }
            else
            {
                ViewWidth = (int)screenWidth;
                ViewHeight = (int)(screenWidth / Width * Height);
            }

            //  apply view padding
            var aspect = ViewHeight / (float)ViewWidth;
            ViewWidth -= ViewPadding * 2;
            ViewHeight -= (int)(aspect * ViewPadding * 2);

            //  Update screen matrix
            ScreenMatrix = Matrix.CreateScale(ViewWidth / (float)Width);

            //  Update Viewport
            Viewport = new Viewport
            {
                X = (int)(screenWidth / 2 - ViewWidth / 2),
                Y = (int)(screenHeight / 2 - ViewHeight / 2),
                Width = ViewWidth,
                Height = ViewHeight,
                MinDepth = 0,
                MaxDepth = 1
            };
        }

        /// <summary>
        ///     Exist the game
        /// </summary>
        public void Quit()
        {
            Exit();
        }


    }
}
