﻿//--------------------------------------------------------------------------------
//  BitTag
//
//  Used as a way of creating Tags for Entities
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace Psiren.Utils
{
    public class BitTag
    {
        /// <summary>
        ///     The total number of tags created
        /// </summary>
        internal static int TotalTags = 0;
        /// <summary>
        ///     The collection of BitTags created, allow retreiving them by index
        /// </summary>
        internal static BitTag[] byID = new BitTag[32];

        /// <summary>
        ///     A collection of the <see cref="BitTag"/> created, retrivable by name
        /// </summary>
        private static Dictionary<string, BitTag> _byName = new Dictionary<string, BitTag>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        ///     Gets a <see cref="BitTag"/> with the given name
        /// </summary>
        /// <param name="name">The name of the <see cref="BitTag"/></param>
        /// <returns></returns>
        public static BitTag Get(string name)
        {
#if DEBUG
            if (!_byName.ContainsKey(name))
            {
                throw new Exception($"No tag with the name {name}' has been defined");
            }
#endif
            return _byName[name];
        }

        /// <summary>
        ///     The ID of the <see cref="BitTag"/>
        /// </summary>
        public int ID;

        /// <summary>
        ///     The Value of the <see cref="BitTag"/>
        /// </summary>
        public int Value;

        /// <summary>
        ///     The name of the <see cref="BitTag"/>
        /// </summary>
        public string Name;

        /// <summary>
        ///     Creates a new <see cref="BitTag"/> instance
        /// </summary>
        /// <param name="name">The name of the <see cref="BitTag"/></param>
        /// <exception cref="Exception">If the total number of <see cref="BitTag"/> created exceeds 32 totals</exception>
        /// <exception cref="Exception">If the name given already exists</exception>
        public BitTag(string name)
        {
#if DEBUG
            if (TotalTags >= 32)
            {
                throw new Exception("Maximum tag limit of 32 exceeded");
            }

            if (_byName.ContainsKey(name))
            {
                throw new Exception($"Two tags defined wit h the same name: '{name}'");
            }
#endif

            ID = TotalTags;
            Value = 1 << TotalTags;
            Name = name;

            byID[ID] = this;
            _byName[name] = this;

            TotalTags++;
        }

        /// <summary>
        ///     Implicitly converts the <see cref="BitTag"/> to an int based on the <see cref="Value"/>
        /// </summary>
        /// <param name="tag"></param>
        public static implicit operator int(BitTag tag)
        {
            return tag.Value;
        }
    }
}
