﻿namespace Psiren.Util
{
    public struct SizeF
    {

        /// <summary>
        ///     Gets a SizeF structure that has a SizeF.Height
        ///     and SizeF.Width value of 0.
        /// </summary>
        /// <returns>
        ///     A SizeF structure that has a SizeF.Height and SizeF.Width
        ///     value of 0.
        /// </returns>
        public static readonly SizeF Empty = new SizeF(0.0f, 0.0f);

        /// <summary>
        ///     Gets a value that indicates whether this SizeF structure has zero
        ///     width and height.
        /// </summary>
        /// <returns>
        ///     This property returns true when this SizeF structure has both
        ///     a width and height of zero; otherwise, false.
        /// </returns>
        public bool IsEmpty
        {
            get
            {
                return Equals(Empty);
            }
        }

        /// <summary>
        ///     Gets or sets the horizontal component of this SizeF structure.
        /// </summary>
        /// <returns>
        ///     The horizontal component of this SizeF structure, typically measured
        ///     in pixels.
        /// </returns>
        public float Width { get; set; }

        /// <summary>
        ///     Gets or sets the vertical component of this SizeF structure.
        ///
        /// <returns>
        ///     The vertical component of this SizeF structure, typically measured
        ///     in pixels.
        /// </returns>
        public float Height { get; set; }


        /// <summary>
        ///     Initializes a new instance of the SizeF structure from the specified
        ///     existing SizeF structure.
        /// </summary>
        /// <param name="size">The SizeF structure from which to create the new SizeF structure.</param>
        public SizeF(SizeF size)
        {
            Height = size.Height;
            Width = size.Width;
        }

        /// <summary>
        ///     Initializes a new instance of the SizeF structure from the specified
        ///     System.Drawing.PointF structure.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF structure from which to initialize this SizeFstructure.</param>
        /// </returns>
        public SizeF(PointF pt)
        {
            Width = pt.X;
            Height = pt.Y;
        }

        /// <summary>
        ///     Initializes a new instance of the SizeF structure from the specified
        ///     dimensions.
        /// </summary>
        /// <param name="width">The width component of the new SizeF structure.</param>
        /// <param name="height">The height component of the new SizeF structure.</param>
        /// </returns>
        public SizeF(float width, float height)
        {
            Width = width;
            Height = height;
        }

        /// <summary>
        ///     Adds the width and height of one SizeF structure to the width
        ///     and height of another SizeF structure.
        /// </summary>
        /// Parameters:
        /// <param name="sz1">The first SizeF structure to add.</param>
        /// <param name="sz2">The second SizeF structure to add.</param>
        /// <returns>
        ///     A SizeF structure that is the result of the addition operation.
        /// </returns>
        public static SizeF Add(SizeF sz1, SizeF sz2)
        {
            return sz1 + sz2;
        }

        /// <summary>
        ///     Subtracts the width and height of one SizeF structure from the
        ///     width and height of another SizeF structure.
        /// </summary>
        /// <param name="sz1">The SizeF structure on the left side of the subtraction operator.</param>
        /// <param name="sz2">The SizeF structure on the right side of the subtraction operator.</param>
        /// <returns>
        ///     A SizeF structure that is a result of the subtraction operation.
        /// </returns>
        public static SizeF Subtract(SizeF sz1, SizeF sz2)
        {
            return sz1 - sz2;
        }


        /// <summary>
        ///     Tests to see whether the specified object is a SizeF structure
        ///     with the same dimensions as this SizeF structure.
        /// </summary>
        /// <param name="obj">The System.Object to test.</param>
        /// <returns>
        ///     This method returns true if obj is a SizeF and has the same width
        ///     and height as this SizeF; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is SizeF other)
            {
                return Width == other.Width && Height == other.Height;
            }
            return false;
        }


        /// <summary>
        ///     Returns a hash code for this Size structure.
        /// </summary>
        /// <returns>
        ///     An integer value that specifies a hash value for this Size structure.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        ///     Converts a SizeF structure to a System.Drawing.PointF structure.
        /// </summary>
        /// <returns>
        ///     Returns a System.Drawing.PointF structure.
        /// </returns>
        public PointF ToPointF()
        {
            return new PointF(Width, Height);
        }

        /// <summary>
        ///     Converts a SizeF structure to a Size structure.
        /// </summary>
        /// <returns>
        ///     Returns a Size structure.
        /// </returns>
        public Size ToSize()
        {
            return Size.Round(this);
        }

        /// <summary>
        ///     Creates a human-readable string that represents this SizeF structure.
        /// </summary>
        /// <returns>
        ///     A string that represents this SizeF structure.
        /// </returns>
        public override string ToString()
        {
            return $"(W: {Width}, H: {Height})";
        }

        /// <summary>
        ///     Adds the width and height of one SizeF structure to the width
        ///     and height of another SizeF structure.
        /// </summary>
        /// <param name="sz1">The first SizeF structure to add.</param>        
        /// <param name="sz2">The second SizeF structure to add.</param>
        /// <returns>
        ///     A Size structure that is the result of the addition operation.
        /// </returns>
        public static SizeF operator +(SizeF sz1, SizeF sz2)
        {
            return new SizeF(sz1.Width + sz2.Width, sz1.Height + sz2.Height);
        }


        /// <summary>
        ///     Subtracts the width and height of one SizeF structure from the
        ///     width and height of another SizeF structure.
        /// </summary>
        /// <param name="sz1">The SizeF structure on the left side of the subtraction operator.</param>
        /// <param name="sz2">The SizeF structure on the right side of the subtraction operator.</param>
        /// <returns>
        ///     A SizeF that is the result of the subtraction operation.
        /// </returns>
        public static SizeF operator -(SizeF sz1, SizeF sz2)
        {
            return new SizeF(sz1.Width - sz2.Width, sz1.Height - sz2.Height);
        }


        /// <summary>
        ///     Tests whether two SizeF structures are equal.
        /// </summary>
        /// <param name="sz1">The SizeF structure on the left side of the equality operator.</param>
        /// <param name="sz2">The SizeF structure on the right of the equality operator.</param>
        /// <returns>
        ///     This operator returns true if sz1 and sz2 have equal width and height; otherwise,
        ///     false.
        /// </returns>
        public static bool operator ==(SizeF sz1, SizeF sz2)
        {
            return sz1.Equals(sz2);
        }


        /// <summary>
        ///     Tests whether two SizeF structures are different.
        /// </summary>
        /// <param name="sz1">The SizeF structure on the left of the inequality operator.</param>
        /// <param name="sz2">The SizeF structure on the right of the inequality operator.</param>
        /// <returns>
        ///     This operator returns true if sz1 and sz2 differ either in width or height; false
        ///     if sz1 and sz2 are equal.
        /// </returns>
        public static bool operator !=(SizeF sz1, SizeF sz2)
        {
            return !sz1.Equals(sz2);
        }

        /// <summary>
        ///     Converts the specified SizeF structure to a System.Drawing.PointF
        ///     structure.
        /// </summary>
        /// <param name="size">The SizeF structure to be converted</param>
        /// <returns>
        ///     The System.Drawing.PointF structure to which this operator converts.
        /// </returns>
        public static explicit operator PointF(SizeF size)
        {
            return new PointF(size.Width, size.Height);
        }
    }
}
