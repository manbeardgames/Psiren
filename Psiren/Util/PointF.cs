﻿namespace Psiren.Util
{
    public struct PointF
    {

        /// <summary>
        ///     Represents a new instance of the System.Drawing.PointF class with member data
        ///     left uninitialized.
        /// </summary>
        public static readonly PointF Empty = new PointF(0.0f, 0.0f);

        /// <summary>
        ///     Gets a value indicating whether this System.Drawing.PointF is empty.
        /// </summary>
        ///
        /// <returns>
        ///     true if both System.Drawing.PointF.X and System.Drawing.PointF.Y are 0; otherwise,
        ///     false.
        /// </returns>
        public bool IsEmpty
        {
            get { return Equals(Empty); }
        }


        /// <summary>
        ///     Gets or sets the x-coordinate of this System.Drawing.PointF.
        /// </summary>
        ///
        /// <returns>
        ///     The x-coordinate of this System.Drawing.PointF.
        /// </returns>
        public float X { get; set; }


        /// <summary>
        ///     Gets or sets the y-coordinate of this System.Drawing.PointF.
        ///
        /// <returns>
        ///     The y-coordinate of this System.Drawing.PointF.
        /// </returns>
        public float Y { get; set; }


        /// <summary>
        ///     Initializes a new instance of the System.Drawing.PointF class with the specified
        ///     coordinates.
        /// </summary>
        /// <param name="x">The horizontal position of the point.</param>
        /// <param name="y">The vertical position of the point.</param>
        public PointF(float x, float y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        ///     Translates a given System.Drawing.PointF by a specified System.Drawing.SizeF.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.SizeF that specifies the numbers to add to the coordinates of pt.</param>
        ///
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF Add(PointF pt, SizeF sz)
        {
            return pt + sz;
        }


        /// <summary>
        ///     Translates a given System.Drawing.PointF by the specified System.Drawing.Size.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.Size that specifies the numbers to add to the coordinates of pt.</param>
        ///
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF Add(PointF pt, Size sz)
        {
            return pt + sz;
        }


        /// <summary>
        ///     Translates a System.Drawing.PointF by the negative of a specified size.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.Size that specifies the numbers to subtract from the coordinates of pt.</param>
        ///
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF Subtract(PointF pt, Size sz)
        {
            return pt - sz;
        }


        /// <summary>
        ///     Translates a System.Drawing.PointF by the negative of a specified size.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.SizeF that specifies the numbers to subtract from the coordinates of pt.</param>
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF Subtract(PointF pt, SizeF sz)
        {
            return pt - sz;
        }


        /// <summary>
        ///     Specifies whether this System.Drawing.PointF contains the same coordinates as
        ///     the specified System.Object.
        /// </summary>
        /// <param name="obj">The System.Object to test.</param>
        /// <returns>
        ///     This method returns true if obj is a System.Drawing.PointF and has the same coordinates
        ///     as this System.Drawing.Point.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is PointF other)
            {
                return other.X == X && other.Y == Y;
            }
            return false;
        }


        /// <summary>
        ///     Returns a hash code for this System.Drawing.PointF structure.
        /// </summary>
        ///
        /// <returns>
        ///     An integer value that specifies a hash value for this System.Drawing.PointF structure.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


        /// <summary>
        ///     Converts this System.Drawing.PointF to a human readable string.
        /// </summary>
        ///
        /// <returns>
        ///     A string that represents this System.Drawing.PointF.
        /// </returns>
        public override string ToString()
        {
            return $"(X: {X}, Y: {Y})";
        }



        /// <summary>
        ///     Translates a System.Drawing.PointF by a given System.Drawing.Size.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">A System.Drawing.Size that specifies the pair of numbers to add to the coordinates of pt.</param>
        ///
        /// <returns>
        ///     Returns the translated System.Drawing.PointF.
        /// </returns>
        public static PointF operator +(PointF pt, Size sz)
        {
            return new PointF(pt.X + sz.Width, pt.Y + sz.Height);
        }


        /// <summary>
        ///     Translates the System.Drawing.PointF by the specified System.Drawing.SizeF.
        /// </summary>
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.SizeF that specifies the numbers to add to the x- and y-coordinates of the System.Drawing.PointF.</param>
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF operator +(PointF pt, SizeF sz)
        {
            return new PointF(pt.X + sz.Width, pt.Y + sz.Height);
        }


        /// <summary>
        ///     Translates a System.Drawing.PointF by the negative of a specified System.Drawing.SizeF.
        /// </summary>
        /// 
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.SizeF that specifies the numbers to subtract from the coordinates of pt.</param>
        ///     
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF operator -(PointF pt, SizeF sz)
        {
            return new PointF(pt.X - sz.Width, pt.Y - sz.Height);
        }


        /// <summary>
        ///     Translates a System.Drawing.PointF by the negative of a given System.Drawing.Size.
        /// </summary>
        /// 
        /// <param name="pt">The System.Drawing.PointF to translate.</param>
        /// <param name="sz">The System.Drawing.Size that specifies the numbers to subtract from the coordinatesof pt.</param>
        ///
        /// <returns>
        ///     The translated System.Drawing.PointF.
        /// </returns>
        public static PointF operator -(PointF pt, Size sz)
        {
            return new PointF(pt.X - sz.Width, pt.Y - sz.Height);
        }


        /// <summary>
        ///     Compares two System.Drawing.PointF structures. The result specifies whether the
        ///     values of the System.Drawing.PointF.X and System.Drawing.PointF.Y properties
        ///     of the two System.Drawing.PointF structures are equal.
        /// </summary>
        ///
        /// <param name="left">A System.Drawing.PointF to compare.</param>
        /// <param name="right">A System.Drawing.PointF to compare.</param>
        ///
        /// <returns>
        ///     true if the System.Drawing.PointF.X and System.Drawing.PointF.Y values of the
        ///     left and right System.Drawing.PointF structures are equal; otherwise, false.
        public static bool operator ==(PointF left, PointF right)
        {
            return left.Equals(right);
        }


        /// <summary>
        ///     Determines whether the coordinates of the specified points are not equal.
        /// </summary>
        ///
        /// <param name="left">A System.Drawing.PointF to compare.</param>
        /// <param name="right">A System.Drawing.PointF to compare.</param>
        ///
        /// <returns>
        ///     true to indicate the System.Drawing.PointF.X and System.Drawing.PointF.Y values
        ///     of left and right are not equal; otherwise, false.
        /// </returns>
        public static bool operator !=(PointF left, PointF right)
        {
            return !left.Equals(right);
        }
    }
}
