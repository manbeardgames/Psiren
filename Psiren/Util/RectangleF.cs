﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Utils
{
    public class RectangleF
    {
        public float X { get; set; }
        public float Y { get; set; }


        public Vector2 Position => new Vector2(this.X, this.Y);

        public float Width { get; set; }
        public float Height { get; set; }

        public Vector2 CenterPosition => new Vector2(this.X + (this.Width * 0.5f), this.Y + (this.Height * 0.5f));

        //  MBGTODO: Add Below Properties at some point

        //  Bottom Right 
        //  Bottom Left
        //  Top Right
        //  Top Left
        //  Top
        //  Right
        //  Bottom
        //  Left


        public RectangleF()
        {

        }


        public RectangleF(float x, float y, float width, float height)
        {
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }

    }


    public static class RectangleFExtensions
    {
        public static Rectangle ToRectangle(this RectangleF rect)
        {
            return new Rectangle()
            {
                X = (int)rect.X,
                Y = (int)rect.Y,
                Width = (int)rect.Width,
                Height = (int)rect.Height
            };
        }
    }
}
