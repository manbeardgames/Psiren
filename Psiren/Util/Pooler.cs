﻿//--------------------------------------------------------------------------------
//  Pooler
//
//  Used to pool all entities marked as Pooled
//
//  TODO: Needs <summary> documentation
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Psiren.Entities;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Psiren.Utils
{
    public class Pooler
    {
        internal Dictionary<Type, Queue<Entity>> Pools { get; private set; }

        public Pooler()
        {
            Pools = new Dictionary<Type, Queue<Entity>>();

            foreach (var type in Assembly.GetEntryAssembly().GetTypes())
            {
                if (type.GetCustomAttributes(typeof(Pooled), false).Length > 0)
                {
                    if (!typeof(Entity).IsAssignableFrom(type))
                    {
                        throw new Exception($"Type '{type.Name}' cannot be Polled becuase it doesn't derive from Entity");
                    }
                    else if (type.GetConstructor(Type.EmptyTypes) == null)
                    {
                        throw new Exception($"Type '{type.Name}' cannot be Polled becuase it doesn't have a parameterless constructor");
                    }
                    else
                    {
                        Pools.Add(type, new Queue<Entity>());
                    }
                }
            }
        }

        public T Create<T>() where T : Entity, new()
        {
            if (!Pools.ContainsKey(typeof(T)))
            {
                return new T();
            }

            var queue = Pools[typeof(T)];
            if (queue.Count == 0)
            {
                return new T();
            }
            else
            {
                return queue.Dequeue() as T;
            }
        }

        internal void EntityRemoved(Entity entity)
        {
            var type = entity.GetType();
            if (Pools.ContainsKey(type))
            {
                Pools[type].Enqueue(entity);
            }
        }

        public void Log()
        {
            if (Pools.Count == 0)
                Engine.Commands.Log("No Entity types are marked as Pooled!");

            foreach (var kv in Pools)
            {
                string output = kv.Key.Name + " : " + kv.Value.Count;
                Engine.Commands.Log(output);
            }
        }
    }
}
