﻿//--------------------------------------------------------------------------------
//  Pnt
//
//  A Point with an int xy-coordinate
//
//  TODO: Write <summary> documentation
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

namespace Psiren.Utils
{
    public struct Pnt
    {
        public static readonly Pnt Zero = new Pnt(0, 0);
        public static readonly Pnt UnitX = new Pnt(1, 0);
        public static readonly Pnt UnitY = new Pnt(0, 1);
        public static readonly Pnt One = new Pnt(1, 1);

        public int X;
        public int Y;

        public Pnt(int x, int y)
        {
            X = x;
            Y = y;
        }

        #region Pnt operators

        public static bool operator ==(Pnt a, Pnt b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Pnt a, Pnt b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public static Pnt operator +(Pnt a, Pnt b)
        {
            return new Pnt(a.X + b.X, a.Y + b.Y);
        }

        public static Pnt operator -(Pnt a, Pnt b)
        {
            return new Pnt(a.X - b.X, a.Y - b.Y);
        }

        public static Pnt operator *(Pnt a, Pnt b)
        {
            return new Pnt(a.X * b.X, a.Y * b.Y);
        }

        public static Pnt operator /(Pnt a, Pnt b)
        {
            return new Pnt(a.X / b.X, a.Y / b.Y);
        }

        public static Pnt operator %(Pnt a, Pnt b)
        {
            return new Pnt(a.X % b.X, a.Y % b.Y);
        }

        #endregion

        #region int operators

        public static bool operator ==(Pnt a, int b)
        {
            return a.X == b && a.Y == b;
        }

        public static bool operator !=(Pnt a, int b)
        {
            return a.X != b || a.Y != b;
        }

        public static Pnt operator +(Pnt a, int b)
        {
            return new Pnt(a.X + b, a.Y + b);
        }

        public static Pnt operator -(Pnt a, int b)
        {
            return new Pnt(a.X - b, a.Y - b);
        }

        public static Pnt operator *(Pnt a, int b)
        {
            return new Pnt(a.X * b, a.Y * b);
        }

        public static Pnt operator /(Pnt a, int b)
        {
            return new Pnt(a.X / b, a.Y / b);
        }

        public static Pnt operator %(Pnt a, int b)
        {
            return new Pnt(a.X % b, a.Y % b);
        }

        #endregion

        public override bool Equals(object obj)
        {
            return false;
        }

        public override int GetHashCode()
        {
            return X * 10000 + Y;
        }

        public override string ToString()
        {
            return "{ X: " + X + ", Y: " + Y + " }";
        }
    }
}