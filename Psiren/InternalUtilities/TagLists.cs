﻿//--------------------------------------------------------------------------------
//  TagLists
//
//  Used to manage the Tags used within a scene
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Psiren.Entities;
using Psiren.Utils;
using System.Collections.Generic;

namespace Psiren.InternalUtilities
{
    public class TagLists
    {
        /// <summary>
        ///     Collection of <see cref="Entity"/> arrays
        /// </summary>
        private List<Entity>[] _lists;

        //  Unsorted array for the Entities
        private bool[] _unsorted;

        //  Used to flag if any entities are sorted
        private bool _areAnyUnsorted;


        /// <summary>
        ///     Creates a new <see cref="TagLists"/> instance
        /// </summary>
        internal TagLists()
        {
            _lists = new List<Entity>[BitTag.TotalTags];
            _unsorted = new bool[BitTag.TotalTags];
            for (int i = 0; i < _lists.Length; i++)
            {
                _lists[i] = new List<Entity>();
            }
        }

        /// <summary>
        ///     Retrieves a collection of <see cref="Entity"/> at the given index in this colleciton
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public List<Entity> this[int index]
        {
            get => _lists[index];
        }

        /// <summary>
        ///     Marks the collection of <see cref="Entity"/> as unsorted at the given index in this collection
        /// </summary>
        /// <param name="index"></param>
        internal void MarkUnsorted(int index)
        {
            _areAnyUnsorted = true;
            _unsorted[index] = true;
        }

        /// <summary>
        ///     Updates the lists for this collection
        /// </summary>
        internal void UpdateLists()
        {
            if (_areAnyUnsorted)
            {
                for (int i = 0; i < _lists.Length; i++)
                {
                    if (_unsorted[i])
                    {
                        _lists[i].Sort(EntityList.CompareDepth);
                        _unsorted[i] = false;
                    }
                }
                _areAnyUnsorted = false;
            }
        }

        /// <summary>
        ///     Called when a new <see cref="Entity"/> is added
        /// </summary>
        /// <param name="entity"></param>
        internal void EntityAdded(Entity entity)
        {
            for (int i = 0; i < BitTag.TotalTags; i++)
            {
                if (entity.TagCheck(1 << i))
                {
                    this[i].Add(entity);
                    _areAnyUnsorted = true;
                    _unsorted[i] = true;
                }
            }
        }

        /// <summary>
        ///     Called when an <see cref="Entity"/> is removed
        /// </summary>
        /// <param name="entity"></param>
        internal void EntityRemoved(Entity entity)
        {
            for (int i = 0; i < BitTag.TotalTags; i++)
            {
                if (entity.TagCheck(1 << i))
                {
                    _lists[i].Remove(entity);
                }
            }
        }
    }
}
