﻿//--------------------------------------------------------------------------------
//  RenderList
//
//  Used to manage all Renderers for a Scene
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Psiren.Renderers;
using Psiren.Scenes;
using Psiren.Utils;
using System.Collections.Generic;

namespace Psiren.InternalUtilities
{
    public class RendererList
    {
        /// <summary>
        ///     Collection of <see cref="Renderer"/>
        /// </summary>
        public List<Renderer> Renderers;

        /// <summary>
        ///     Collection of <see cref="Renderer"/> to add
        /// </summary>
        private List<Renderer> _adding;

        /// <summary>
        ///     Collection of <see cref="Renderer"/> to remove
        /// </summary>
        private List<Renderer> _removing;

        //  The Scene this colleciton belongs to
        private Scene _scene;

        /// <summary>
        ///     Creates a new <see cref="RendererList"/> instance
        /// </summary>
        /// <param name="scene">The <see cref="Scene"/> this <see cref="RendererList"/> belongs too</param>
        internal RendererList(Scene scene)
        {
            _scene = scene;

            Renderers = new List<Renderer>();
            _adding = new List<Renderer>();
            _removing = new List<Renderer>();
        }

        /// <summary>
        ///     Updates the collections
        /// </summary>
        internal void UpdateLists()
        {
            if (_adding.Count > 0)
            {
                foreach (var renderer in _adding)
                {
                    Renderers.Add(renderer);
                }
            }

            _adding.Clear();

            if (_removing.Count > 0)
            {
                foreach (var renderer in _removing)
                {
                    Renderers.Remove(renderer);
                }
            }

            _removing.Clear();
        }

        /// <summary>
        ///     Updates each <see cref="Renderer"/> in this colleciton
        /// </summary>
        internal void Update()
        {
            foreach (var renderer in Renderers)
            {
                renderer.Update(_scene);
            }
        }

        /// <summary>
        ///     Calls BeforeRender for each <see cref="Renderer"/> in this collection
        /// </summary>
        internal void BeforeRender()
        {
            for (int i = 0; i < Renderers.Count; i++)
            {
                if (!Renderers[i].Visible)
                {
                    continue;
                }

                Draw.Renderer = Renderers[i];
                Renderers[i].BeforeRender(_scene);
            }
        }

        /// <summary>
        ///     Calls Render for each <see cref="Renderer"/> in this collection
        /// </summary>
        internal void Render()
        {
            for (int i = 0; i < Renderers.Count; i++)
            {
                if (!Renderers[i].Visible)
                {
                    continue;
                }

                Draw.Renderer = Renderers[i];
                Renderers[i].Render(_scene);
            }
        }

        /// <summary>
        ///     Calls AfterRender for each <see cref="Renderer"/> in this collection
        /// </summary>
        internal void AfterRender()
        {
            for (int i = 0; i < Renderers.Count; i++)
            {
                if (!Renderers[i].Visible)
                {
                    continue;
                }
                Draw.Renderer = Renderers[i];
                Renderers[i].AfterRender(_scene);
            }
        }

        /// <summary>
        ///     Moves the given <see cref="Renderer"/> to the front of the collection
        /// </summary>
        /// <param name="renderer"></param>
        public void MoveToFront(Renderer renderer)
        {
            Renderers.Remove(renderer);
            Renderers.Add(renderer);
        }

        /// <summary>
        ///     Adds the given <see cref="Renderer"/> to the collection
        /// </summary>
        /// <param name="renderer"></param>
        public void Add(Renderer renderer)
        {
            _adding.Add(renderer);
        }

        /// <summary>
        ///     Removes the given <see cref="Renderer"/> from the collection
        /// </summary>
        /// <param name="renderer"></param>
        public void Remove(Renderer renderer)
        {
            _removing.Add(renderer);
        }

    }
}
