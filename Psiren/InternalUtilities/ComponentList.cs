﻿//--------------------------------------------------------------------------------
//  ComponentList
//
//  Used to manage the components for an Entity
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Psiren.Components;
using Psiren.Entities;
using Psiren.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Psiren.InternalUtilities
{
    public class ComponentList : IEnumerable<Component>, IEnumerable
    {
        /// <summary>
        ///     Defines the lock mode when working with the list of components
        /// </summary>
        public enum LockModes { Open, Locked, Error };

        /// <summary>
        ///     The <see cref="Psiren.Entity"/> which uses this <see cref="ComponentList"/>
        /// </summary>
        public Entity Entity { get; internal set; }

        /// <summary>
        ///     Collection of all components
        /// </summary>
        private List<Component> _components;

        /// <summary>
        ///     Collection of components that need to be added
        /// </summary>
        private List<Component> _toAdd;

        /// <summary>
        ///     Colleciton of components that need to be removed
        /// </summary>
        private List<Component> _toRemove;

        /// <summary>
        ///     HashSet collection of current components
        /// </summary>
        private HashSet<Component> _current;

        /// <summary>
        ///     HashSet collection of components that need to be added
        /// </summary>
        private HashSet<Component> _adding;

        /// <summary>
        ///     Hashset collection of components that need to be removed
        /// </summary>
        private HashSet<Component> _removing;

        /// <summary>
        ///     The count of components
        /// </summary>
        public int Count { get => _components.Count; }

        /// <summary>
        ///     THe current <see cref="LockModes"/>
        /// </summary>
        private LockModes _lockMode;

        /// <summary>
        ///     Gets or sets the current <see cref="LockModes"/>
        /// </summary>
        internal LockModes LockMode
        {
            get => _lockMode;
            set
            {
                _lockMode = value;
                if (_toAdd.Count > 0)
                {
                    foreach (var component in _toAdd)
                    {
                        if (!_current.Contains(component))
                        {
                            _current.Add(component);
                            _components.Add(component);
                            component.Added(Entity);
                        }
                    }

                    _adding.Clear();
                    _toAdd.Clear();
                }

                if (_toRemove.Count > 0)
                {
                    foreach (var component in _toRemove)
                    {
                        if (_current.Contains(component))
                        {
                            _current.Remove(component);
                            _components.Remove(component);
                            component.Removed(Entity);
                        }
                    }
                    _removing.Clear();
                    _toRemove.Clear();
                }
            }
        }

        /// <summary>
        ///     Creates a new <see cref="ComponentList"/> instance
        /// </summary>
        /// <param name="entity">The <see cref="Psiren.Entity"/> which uses this <see cref="ComponentList"/></param>
        internal ComponentList(Entity entity)
        {
            Entity = entity;

            _components = new List<Component>();
            _toAdd = new List<Component>();
            _toRemove = new List<Component>();
            _current = new HashSet<Component>();
            _adding = new HashSet<Component>();
            _removing = new HashSet<Component>();
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/> to the beginning of the colleciton
        /// </summary>
        /// <param name="component"></param>
        public void AddAtBeginning(Component component)
        {
            switch (_lockMode)
            {
                case LockModes.Open:
                    if (!_current.Contains(component))
                    {
                        _current.Add(component);
                        _components.Add(component);
                        component.Added(Entity);
                    }
                    break;
                case LockModes.Locked:
                    if (!_current.Contains(component) && !_adding.Contains(component))
                    {
                        _adding.Add(component);
                        _toAdd.Add(component);
                    }
                    break;

                case LockModes.Error:
                    throw new Exception("Cannot add or remove Entities at this time");
            }
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/> to the collection
        /// </summary>
        /// <param name="component"></param>
        public void Add(Component component)
        {
            switch (_lockMode)
            {
                case LockModes.Open:
                    if (!_current.Contains(component))
                    {
                        _current.Add(component);
                        _components.Add(component);
                        component.Added(Entity);
                    }
                    break;
                case LockModes.Locked:
                    if (!_current.Contains(component) && !_adding.Contains(component))
                    {
                        _adding.Add(component);
                        _toAdd.Add(component);
                    }
                    break;

                case LockModes.Error:
                    throw new Exception("Cannot add or remove Entities at this time");
            }
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/> collection to the collection
        /// </summary>
        /// <param name="components"></param>
        public void Add(IEnumerable<Component> components)
        {
            foreach (var component in components)
            {
                Add(component);
            }
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/>s to the collection
        /// </summary>
        /// <param name="components"></param>
        public void Add(params Component[] components)
        {
            foreach (var component in components)
                Add(component);
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/> from the collection
        /// </summary>
        /// <param name="component"></param>
        public void Remove(Component component)
        {
            switch (_lockMode)
            {
                case LockModes.Open:
                    if (_current.Contains(component))
                    {
                        _current.Remove(component);
                        _components.Remove(component);
                        component.Removed(Entity);
                    }
                    break;

                case LockModes.Locked:
                    if (_current.Contains(component) && !_removing.Contains(component))
                    {
                        _removing.Add(component);
                        _toRemove.Add(component);
                    }
                    break;

                case LockModes.Error:
                    throw new Exception("Cannot add or remove Entities at this time!");
            }
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/> collection from the collection
        /// </summary>
        /// <param name="components"></param>
        public void Remove(IEnumerable<Component> components)
        {
            foreach (var component in components)
            {
                Remove(component);
            }
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/>s from the collection
        /// </summary>
        /// <param name="components"></param>
        public void Remove(params Component[] components)
        {
            foreach (var component in components)
                Remove(component);
        }

        /// <summary>
        ///     Removes all <see cref="Component"/>s from the collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void RemoveAll<T>() where T : Component
        {
            Remove(GetAll<T>());
        }


        /// <summary>
        ///     Retrieves the <see cref="Component"/> at the given index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Component this[int index]
        {
            get
            {
                if (index < 0 || index >= _components.Count)
                    throw new IndexOutOfRangeException();
                else
                    return _components[index];
            }
        }


        public IEnumerator<Component> GetEnumerator()
        {
            return _components.GetEnumerator();
        }


        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        ///     Returns this <see cref="ComponentList"/> as an array of <see cref="Component"/>
        /// </summary>
        /// <returns></returns>
        public Component[] ToArray()
        {
            return _components.ToArray<Component>();
        }

        /// <summary>
        ///     Updates the each <see cref="Component"/> if it is active
        /// </summary>
        internal void Update()
        {
            LockMode = ComponentList.LockModes.Locked;
            foreach (var component in _components)
            {
                if (component.Active)
                {
                    component.Update();
                }
            }
            LockMode = ComponentList.LockModes.Open;
        }

        /// <summary>
        ///     Renders each <see cref="Component"/> if it is visible
        /// </summary>
        internal void Render()
        {
            LockMode = ComponentList.LockModes.Error;
            foreach (var component in _components)
                if (component.Visible)
                    component.Render();
            LockMode = ComponentList.LockModes.Open;
        }

        /// <summary>
        ///     Calls the DebugRender for each <see cref="Component"/>
        /// </summary>
        /// <param name="camera"></param>
        internal void DebugRender(Camera camera)
        {
            LockMode = ComponentList.LockModes.Error;
            foreach (var component in _components)
                component.DebugRender(camera);
            LockMode = ComponentList.LockModes.Open;
        }

        /// <summary>
        ///     Calls the HandleGraphicsReset for each <see cref="Component"/>
        /// </summary>
        internal void HandleGraphicsReset()
        {
            LockMode = ComponentList.LockModes.Error;
            foreach (var component in _components)
                component.HandleGraphicsReset();
            LockMode = ComponentList.LockModes.Open;
        }

        /// <summary>
        ///     Calls the HandleGraphicsCreate for each <see cref="Component"/>
        /// </summary>
        internal void HandleGraphicsCreate()
        {
            LockMode = ComponentList.LockModes.Error;
            foreach (var component in _components)
                component.HandleGraphicsCreate();
            LockMode = ComponentList.LockModes.Open;
        }

        /// <summary>
        ///     Gets the <see cref="Component"/> of type T
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Component"/> to get</typeparam>
        /// <returns></returns>
        public T Get<T>() where T : Component
        {
            foreach (var component in _components)
                if (component is T)
                    return component as T;
            return null;
        }

        /// <summary>
        ///     Gets all <see cref="Component"/> of type T
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Component"/> to get</typeparam>
        /// <returns></returns>
        public IEnumerable<T> GetAll<T>() where T : Component
        {
            foreach (var component in _components)
                if (component is T)
                    yield return component as T;
        }

    }
}
