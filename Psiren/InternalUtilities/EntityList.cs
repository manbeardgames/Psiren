﻿//--------------------------------------------------------------------------------
//  EntityList
//
//  Used to manage the entities for a Scene
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Psiren.Entities;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Psiren.InternalUtilities
{
    public class EntityList : IEnumerable<Entity>, IEnumerable
    {
        /// <summary>
        ///     The <see cref="Psiren.Scene"/> this <see cref="Psiren.Entity"/> is in
        /// </summary>
        public Scene Scene { get; private set; }

        /// <summary>
        ///     The collction of <see cref="Entity"/>s
        /// </summary>
        private List<Entity> _entities;

        /// <summary>
        ///     The collection of <see cref="Entity"/> to add   
        /// </summary>
        private List<Entity> _toAdd;

        /// <summary>
        ///     The collection of <see cref="Entity"/> that need to have Awake called
        /// </summary>
        private List<Entity> _toAwake;

        /// <summary>
        ///     The collection of <see cref="Entity"/> that need to be removed
        /// </summary>
        private List<Entity> _toRemove;

        /// <summary>
        ///     The HashSet collection of current <see cref="Entity"/>
        /// </summary>
        private HashSet<Entity> _current;

        /// <summary>
        ///     The HashSet collection of <see cref="Entity"/> that need to be added
        /// </summary>
        private HashSet<Entity> _adding;

        /// <summary>
        ///     The HashSet collection of <see cref="Entity"/> that need to be removed
        /// </summary>
        private HashSet<Entity> _removing;

        //  Is the collection unsorted
        private bool _unsorted;

        /// <summary>
        ///     The total number of <see cref="Entity"/> in the colleciton
        /// </summary>
        public int Count { get => _entities.Count; }

        /// <summary>
        ///     Creates a new <see cref="EntityList"/> instance
        /// </summary>
        /// <param name="scene">The <see cref="Psiren.Scene"/> this belongs to</param>
        internal EntityList(Scene scene)
        {
            Scene = scene;

            _entities = new List<Entity>();
            _toAdd = new List<Entity>();
            _toAwake = new List<Entity>();
            _toRemove = new List<Entity>();

            _current = new HashSet<Entity>();
            _adding = new HashSet<Entity>();
            _removing = new HashSet<Entity>();
        }

        /// <summary>
        ///     Marks the collection as unsorted
        /// </summary>
        internal void MarkUnsorted()
        {
            _unsorted = true;
        }

        /// <summary>
        ///     Updates the lists
        /// </summary>
        public void UpdateLists()
        {
            if (_toAdd.Count > 0)
            {
                for (int i = 0; i < _toAdd.Count; i++)
                {
                    var entity = _toAdd[i];
                    if (!_current.Contains(entity))
                    {
                        _current.Add(entity);
                        _entities.Add(entity);

                        if (Scene != null)
                        {
                            Scene.TagLists.EntityAdded(entity);
                            Scene.Tracker.EntityAdded(entity);
                            entity.Added(Scene);
                        }
                    }
                }

                _unsorted = true;
            }

            if (_toRemove.Count > 0)
            {
                for (int i = 0; i < _toRemove.Count; i++)
                {
                    var entity = _toRemove[i];
                    if (_entities.Contains(entity))
                    {
                        _current.Remove(entity);
                        _entities.Remove(entity);

                        if (Scene != null)
                        {
                            entity.Removed(Scene);
                            Scene.TagLists.EntityRemoved(entity);
                            Scene.Tracker.EntityRemoved(entity);
                            Engine.Pooler.EntityRemoved(entity);
                        }
                    }
                }

                _toRemove.Clear();
                _removing.Clear();
            }

            if (_unsorted)
            {
                _unsorted = false;
                _entities.Sort(CompareDepth);
            }

            if (_toAdd.Count > 0)
            {
                _toAwake.AddRange(_toAdd);
                _toAdd.Clear();
                _adding.Clear();

                foreach (var entity in _toAwake)
                {
                    if (entity.Scene == Scene)
                    {
                        entity.Awake(Scene);
                    }
                }
                _toAwake.Clear();
            }
        }

        /// <summary>
        ///     Adds the <see cref="Entity"/> to the collection
        /// </summary>
        /// <param name="entity">The <see cref="Entity"/> to add</param>
        public void Add(Entity entity)
        {
            if (!_adding.Contains(entity) && !_current.Contains(entity))
            {
                _adding.Add(entity);
                _toAdd.Add(entity);
            }
        }

        /// <summary>
        ///     Adds a colleciton of <see cref="Entity"/> to the collection
        /// </summary>
        /// <param name="entities"></param>
        public void Add(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
                Add(entity);
        }

        /// <summary>
        ///     Adds the given <see cref="Entity"/> to the collection
        /// </summary>
        /// <param name="entities"></param>
        public void Add(params Entity[] entities)
        {
            for (int i = 0; i < entities.Length; i++)
                Add(entities[i]);
        }

        /// <summary>
        ///     Removes the given <see cref="Entity"/> from the collection
        /// </summary>
        /// <param name="entity"></param>
        public void Remove(Entity entity)
        {
            if (!_removing.Contains(entity) && _current.Contains(entity))
            {
                _removing.Add(entity);
                _toRemove.Add(entity);
            }
        }

        /// <summary>
        ///     Removes the given <see cref="Entity"/> collection from the collection
        /// </summary>
        /// <param name="entities"></param>
        public void Remove(IEnumerable<Entity> entities)
        {
            foreach (var entity in entities)
                Remove(entity);
        }

        /// <summary>
        ///     Removes the given <see cref="Entity"/> from the collection
        /// </summary>
        /// <param name="entities"></param>
        public void Remove(params Entity[] entities)
        {
            for (int i = 0; i < entities.Length; i++)
                Remove(entities[i]);
        }


        /// <summary>
        ///     Retrives the <see cref="Entity"/> at the given index from this collection
        /// </summary>
        /// <param name="index">The index of the <see cref="Entity"/> to retrieve</param>
        /// <returns></returns>
        public Entity this[int index]
        {
            get
            {
                if (index < 0 || index >= _entities.Count)
                {
                    throw new IndexOutOfRangeException();
                }
                else
                {
                    return _entities[index];
                }
            }
        }

        /// <summary>
        ///     Returns the count of <see cref="Entity"/> of type T
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Entity"/></typeparam>
        /// <returns></returns>
        public int AmountOf<T>() where T : Entity
        {
            int count = 0;
            foreach (var e in _entities)
            {
                if (e is T)
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        ///     Returns the first instace of <see cref="Entity"/> of type T in this collection. 
        ///     If one is not found, returns null
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Entity"/></typeparam>
        /// <returns></returns>
        public T FindFirst<T>() where T : Entity
        {
            foreach (var e in _entities)
            {
                if (e is T)
                {
                    return e as T;
                }
            }
            return null;
        }

        /// <summary>
        ///     Returns all <see cref="Entity"/> of type T
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Entity"/></typeparam>
        /// <returns></returns>
        public List<T> FindAll<T>() where T : Entity
        {
            List<T> list = new List<T>();

            foreach (var e in _entities)
            {
                if (e is T)
                {
                    list.Add(e as T);
                }
            }

            return list;
        }

        /// <summary>
        ///     Perfoms the given Action on <see cref="Entity"/> of type T in this colleciton
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Entity"/></typeparam>
        /// <param name="action">The Action to perform</param>
        public void With<T>(Action<T> action) where T : Entity
        {
            foreach (var e in _entities)
            {
                if (e is T)
                {
                    action(e as T);
                }
            }
        }

        public IEnumerator<Entity> GetEnumerator()
        {
            return _entities.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        ///     Returns this <see cref="EntityList"/> collection as an <see cref="Entity"/> array   
        /// </summary>
        /// <returns></returns>
        public Entity[] ToArray()
        {
            return _entities.ToArray<Entity>();
        }

        /// <summary>
        ///     Checks if there are any <see cref="Entity"/> with the given tag int that are visible
        /// </summary>
        /// <param name="matchTags">The int of the <see cref="BitTag"/></param>
        /// <returns></returns>
        public bool HasVisibleEntities(int matchTags)
        {
            foreach (var entity in _entities)
            {
                if (entity.Visible && entity.TagCheck(matchTags))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Updates all <see cref="Entity"/> in this collection that are active
        /// </summary>
        internal void Update()
        {
            foreach (var entity in _entities)
            {
                if (entity.Active)
                {
                    entity.Update();
                }
            }
        }

        /// <summary>
        ///     Renders all <see cref="Entity"/> int his collection that are visible
        /// </summary>
        public void Render()
        {
            foreach (var entity in _entities)
            {
                if (entity.Visible)
                {
                    entity.Render();
                }
            }
        }

        /// <summary>
        ///     Renders only the <see cref="Entity"/> in this collection that have the matching
        ///     tag and are visible
        /// </summary>
        /// <param name="matchTags">The int of the <see cref="BitTag"/></param>
        public void RenderOnly(int matchTags)
        {
            foreach (var entity in _entities)
            {
                if (entity.Visible && entity.TagCheck(matchTags))
                {
                    entity.Render();
                }
            }
        }

        /// <summary>
        ///     Renders only the <see cref="Entity"/> in this colleciton that full match
        ///     the tag and are visible
        /// </summary>
        /// <param name="matchTags">The int of the <see cref="BitTag"/></param>
        public void RenderOnlyFullMatch(int matchTags)
        {
            foreach (var entity in _entities)
            {
                if (entity.Visible && entity.TagFullCheck(matchTags))
                {
                    entity.Render();
                }
            }
        }

        /// <summary>
        ///     Renders all <see cref="Entity"/> in this collection excpet those
        ///     with the matching tag
        /// </summary>
        /// <param name="excludeTags">The int of the <see cref="BitTag"/> to exclude</param>
        public void RenderExcept(int excludeTags)
        {
            foreach (var entity in _entities)
            {
                if (entity.Visible && !entity.TagCheck(excludeTags))
                {
                    entity.Render();
                }
            }
        }

        /// <summary>
        ///     Calls the DebugRender of all <see cref="Entity"/> in this collection
        /// </summary>
        /// <param name="camera"></param>
        public void DebugRender(Camera camera)
        {
            foreach (var entity in _entities)
            {
                entity.DebugRender(camera);
            }
        }

        /// <summary>
        ///     Calls HandleGraphicReset on all <see cref="Entity"/> in this collection
        /// </summary>
        internal void HandleGraphicsReset()
        {
            foreach (var entity in _entities)
            {
                entity.HandleGraphicsReset();
            }
        }

        /// <summary>
        ///     Calls HandleGraphicsCreate on all <see cref="Entity"/> in this collection
        /// </summary>
        internal void HandleGraphicsCreate()
        {
            foreach (var entity in _entities)
            {
                entity.HandleGraphicsCreate();
            }
        }

        /// <summary>
        ///     Compares the <see cref="Entity.Depth"/> value of the given <see cref="Entity"/>s
        /// </summary>
        public static Comparison<Entity> CompareDepth = (a, b) => { return Math.Sign(b._actualDepth - a._actualDepth); };

    }
}
