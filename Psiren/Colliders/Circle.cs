﻿//--------------------------------------------------------------------------------
//  
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Psiren.Utils;

namespace Psiren.Colliders
{
    public class Circle : Collider
    {
        /// <summary>
        ///     The radius (half diameter) fo the <see cref="Circle"/>
        /// </summary>
        public float Radius;

        /// <summary>
        ///     The full width (diameter) of the <see cref="Circle"/>
        /// </summary>
        /// <remarks>
        ///     This is the same as Height
        /// </remarks>
        public override float Width
        {
            get { return Radius * 2; }
            set { Radius = value / 2; }
        }

        /// <summary>
        ///     The full height (diameter) of the <see cref="Circle"/>
        /// </summary>
        /// <remarks>
        ///     This is the same as Width
        /// </remarks>
        public override float Height
        {
            get { return Radius * 2; }
            set { Radius = value / 2; }
        }

        /// <summary>
        ///     The top most y-coordinate position of the <see cref="Circle"/>
        /// </summary>
        public override float Top
        {
            get { return Position.Y - Radius; }
            set { Position.Y = value + Radius; }
        }

        /// <summary>
        ///     The bottom most y-coordinate position of the <see cref="Circle"/>
        /// </summary>
        public override float Bottom
        {
            get { return Position.Y + Radius; }
            set { Position.Y = value - Radius; }
        }

        /// <summary>
        ///     The left most x-coordinate position of the <see cref="Circle"/>
        /// </summary>
        public override float Left
        {
            get { return Position.X - Radius; }
            set { Position.X = value + Radius; }
        }

        /// <summary>
        ///     The right most x-coordinate position of the <see cref="Circle"/>
        /// </summary>
        public override float Right
        {
            get { return Position.X + Radius; }
            set { Position.X = value - Radius; }
        }


        /// <summary>
        ///     Creates a new Circle instance
        /// </summary>
        /// <param name="radius">The radius (half diameter) of the <see cref="Circle"/></param>
        /// <param name="x">The x-coordinate of the center of the <see cref="Circle"/></param>
        /// <param name="y">The y-coordinate of the center of the <see cref="Circle"/></param>
        public Circle(float radius, float x = 0, float y = 0)
        {
            Radius = radius;
            Position.X = x;
            Position.Y = y;
        }

        /// <summary>
        ///     Creates a new Circle instance
        /// </summary>
        /// <param name="radius">The radius (half diameter) of the <see cref="Circle"/></param>
        /// <param name="center">The xy-coordinate center of the <see cref="Circle"/></param>
        public Circle(float radius, Vector2 center)
        {
            Radius = radius;
            Position = center;
        }

        /// <summary>
        ///     Creates a clone of this <see cref="Circle"/>
        /// </summary>
        /// <returns></returns>
        public override Collider Clone() => new Circle(Radius, Position.X, Position.Y);

        /// <summary>
        ///     Draws the circle to the screen
        /// </summary>
        /// <param name="camera">The <see cref="Camera"/> used to draw the <see cref="Circle"/></param>
        /// <param name="color">The color of the <see cref="Circle"/>/></param>
        public override void Render(Camera camera, Color color)
        {
            Draw.Circle(AbsolutePosition, Radius, color, 4);
        }

        /// <summary>
        ///     Checks if the point given is contained within the <see cref="Circle"/>
        /// </summary>
        /// <param name="point">The xy-coordiante point to check</param>
        /// <returns></returns>
        public override bool Collide(Vector2 point)
        {
            return Psiren.Colliders.Collide.CircleToPoint(AbsolutePosition, Radius, point);
        }

        /// <summary>
        ///     Checks if the rectangle given is colliding with the <see cref="Circle"/>
        /// </summary>
        /// <param name="rect">The rectangle to check</param>
        /// <returns></returns>
        public override bool Collide(Rectangle rect)
        {
            return Psiren.Colliders.Collide.RectToCircle(rect, AbsolutePosition, Radius);
        }

        /// <summary>
        ///     Checks if a line drawn starting at xy-coordinate from and ending at xy-coordinate to
        ///     collides with the <see cref="Circle"/>
        /// </summary>
        /// <param name="from">The starting xy-coordinte of the line</param>
        /// <param name="to">The ending xy-coordinate of the line</param>
        /// <returns></returns>
        public override bool Collide(Vector2 from, Vector2 to)
        {
            return Psiren.Colliders.Collide.CircleToLine(AbsolutePosition, Radius, from, to);
        }

        /// <summary>
        ///     Checks if the given <see cref="Circle"/> is colliding with this <see cref="Circle"/>
        /// </summary>
        /// <param name="circle">The <see cref="Circle"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Circle circle)
        {
            return Vector2.DistanceSquared(AbsolutePosition, circle.AbsolutePosition) < (Radius + circle.Radius) * (Radius + circle.Radius);
        }

        /// <summary>
        ///     Checks if the given <see cref="Hitbox"/> is colliding with this <see cref="Circle"/>
        /// </summary>
        /// <param name="hitbox">The <see cref="Hitbox"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Hitbox hitbox)
        {
            return hitbox.Collide(this);
        }

        /// <summary>
        ///     Checks if the given <see cref="Grid"/> is colliding with this <see cref="Circle"/>
        /// </summary>
        /// <param name="grid">The <see cref="Grid"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Grid grid)
        {
            return grid.Collide(this);
        }

        /// <summary>
        ///     Checks if any colliders in the given <see cref="ColliderList"/> is colliding with this <see cref="Circle"/>
        /// </summary>
        /// <param name="list">The <see cref="ColliderList"/> of colliders to check</param>
        /// <returns></returns>
        public override bool Collide(ColliderList list)
        {
            return list.Collide(this);
        }

    }
}
