﻿//--------------------------------------------------------------------------------
//  ColliderList
//  A ColliderList is a list of colliders that are combined together
//  to act as one collider.  Think of it like hitboxes on a character
//  where you can define seperate hitboxes for like legs, arms, etc.
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.Entities;
using Psiren.Utils;
using System;
using System.Linq;

namespace Psiren.Colliders
{
    public class ColliderList : Collider
    {
        /// <summary>
        ///     The collective width
        /// </summary>
        public override float Width
        {
            get
            {
                return Right - Left;
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        ///     The collective height
        /// </summary>
        public override float Height
        {
            get
            {
                return Bottom - Top;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        ///     The top most y-coordinate position
        /// </summary>
        public override float Top
        {
            get
            {
                float top = colliders[0].Top;
                for (int i = 1; i < colliders.Length; i++)
                    if (colliders[i].Top < top)
                        top = colliders[i].Top;
                return top;
            }

            set
            {
                float changeY = value - Top;
                foreach (var c in colliders)
                    Position.Y += changeY;
            }
        }

        /// <summary>
        ///     The bottom most y-coordiate position
        /// </summary>
        public override float Bottom
        {
            get
            {
                float bottom = colliders[0].Bottom;
                for (int i = 1; i < colliders.Length; i++)
                    if (colliders[i].Bottom > bottom)
                        bottom = colliders[i].Bottom;
                return bottom;
            }

            set
            {
                float changeY = value - Bottom;
                foreach (var c in colliders)
                    Position.Y += changeY;
            }
        }

        /// <summary>
        ///     The left most x-coordiate position
        /// </summary>
        public override float Left
        {
            get
            {
                float left = colliders[0].Left;
                for (int i = 1; i < colliders.Length; i++)
                    if (colliders[i].Left < left)
                        left = colliders[i].Left;
                return left;
            }

            set
            {
                float changeX = value - Left;
                foreach (var c in colliders)
                    Position.X += changeX;
            }
        }

        /// <summary>
        ///     The right most x-coordinate position
        /// </summary>
        public override float Right
        {
            get
            {
                float right = colliders[0].Right;
                for (int i = 1; i < colliders.Length; i++)
                    if (colliders[i].Right > right)
                        right = colliders[i].Right;
                return right;
            }

            set
            {
                float changeX = value - Right;
                foreach (var c in colliders)
                    Position.X += changeX;
            }
        }


        /// <summary>
        ///     Array of <see cref="Collider"/> that this <see cref="ColliderList"/> represents
        /// </summary>
        public Collider[] colliders { get; private set; }

        /// <summary>
        ///     Creates a new <see cref="ColliderList"/> from the given <see cref="Collider"/>s
        /// </summary>
        /// <param name="colliders"></param>
        public ColliderList(params Collider[] colliders)
        {
#if DEBUG
            foreach (var c in colliders)
                if (c == null)
                    throw new Exception("Cannot add a null Collider to a ColliderList.");
#endif
            this.colliders = colliders;
        }

        /// <summary>
        ///     Adds the given <see cref="Collider"/> to this <see cref="ColliderList"/>
        /// </summary>
        /// <param name="toAdd">The <see cref="Collider"/>s to add</param>
        public void Add(params Collider[] toAdd)
        {
#if DEBUG
            foreach (var c in toAdd)
            {
                if (colliders.Contains(c))
                    throw new Exception("Adding a Collider to a ColliderList that already contains it!");
                else if (c == null)
                    throw new Exception("Cannot add a null Collider to a ColliderList.");
            }
#endif

            Collider[] newColliders = new Collider[colliders.Length + toAdd.Length];
            for (int i = 0; i < colliders.Length; i++)
                newColliders[i] = colliders[i];
            for (int i = 0; i < toAdd.Length; i++)
            {
                newColliders[i + colliders.Length] = toAdd[i];
                toAdd[i].Added(Entity);
            }
            colliders = newColliders;
        }


        /// <summary>
        ///     Removes the given <see cref="Collider"/>s from this <see cref="ColliderList"/>
        /// </summary>
        /// <param name="toRemove"></param>
        public void Remove(params Collider[] toRemove)
        {
#if DEBUG
            foreach (var c in toRemove)
            {
                if (!colliders.Contains(c))
                    throw new Exception("Removing a Collider from a ColliderList that does not contain it!");
                else if (c == null)
                    throw new Exception("Cannot remove a null Collider from a ColliderList.");
            }
#endif

            Collider[] newColliders = new Collider[colliders.Length - toRemove.Length];
            int at = 0;
            foreach (var c in colliders)
            {
                if (!toRemove.Contains(c))
                {
                    newColliders[at] = c;
                    at++;
                }
            }
            colliders = newColliders;
        }


        internal override void Added(Entity entity)
        {
            base.Added(entity);
            foreach (var c in colliders)
                c.Added(entity);
        }

        internal override void Removed()
        {
            base.Removed();
            foreach (var c in colliders)
                c.Removed();
        }




        /// <summary>
        ///     Creates a clone of this <see cref="ColliderList"/>
        /// </summary>
        /// <returns></returns>
        public override Collider Clone()
        {
            Collider[] clones = new Collider[colliders.Length];
            for (int i = 0; i < colliders.Length; i++)
                clones[i] = colliders[i].Clone();

            return new ColliderList(clones);
        }

        /// <summary>
        ///     Draws all of the <see cref="Collider"/>s that are part of this <see cref="ColliderList"/>
        /// </summary>
        /// <param name="camera">The <see cref="Camera"/> used for the render</param>
        /// <param name="color">The color to use when drawing</param>
        public override void Render(Camera camera, Color color)
        {
            foreach (var c in colliders)
                c.Render(camera, color);
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with the
        ///     given xy-coordinate point
        /// </summary>
        /// <param name="point">The xy-coordinate point</param>
        /// <returns></returns>
        public override bool Collide(Vector2 point)
        {
            foreach (var c in colliders)
                if (c.Collide(point))
                    return true;

            return false;
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with the
        ///     give Rectangle
        /// </summary>
        /// <param name="rect">The rectangle to check</param>
        /// <returns></returns>
        public override bool Collide(Rectangle rect)
        {
            foreach (var c in colliders)
                if (c.Collide(rect))
                    return true;

            return false;
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with
        ///     a line that starts at the xy-coodinate <paramref name="from"/> and ends at the
        ///     xy-coordinate <paramref name="to"/>
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public override bool Collide(Vector2 from, Vector2 to)
        {
            foreach (var c in colliders)
                if (c.Collide(from, to))
                    return true;

            return false;
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with
        ///     given <see cref="Hitbox"/>
        /// </summary>
        /// <param name="hitbox">The <see cref="Hitbox"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Hitbox hitbox)
        {
            foreach (var c in colliders)
                if (c.Collide(hitbox))
                    return true;

            return false;
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with
        ///     give <see cref="Grid"/>
        /// </summary>
        /// <param name="grid">The <see cref="Grid"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Grid grid)
        {
            foreach (var c in colliders)
                if (c.Collide(grid))
                    return true;

            return false;
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with
        ///     given <see cref="Circle"/>
        /// </summary>
        /// <param name="circle">The <see cref="Circle"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Circle circle)
        {
            foreach (var c in colliders)
                if (c.Collide(circle))
                    return true;

            return false;
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in this <see cref="ColliderList"/> collids with
        ///     any <see cref="Collider"/> in the given <see cref="ColliderList"/>
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public override bool Collide(ColliderList list)
        {
            foreach (var c in colliders)
                if (c.Collide(list))
                    return true;

            return false;
        }
    }
}