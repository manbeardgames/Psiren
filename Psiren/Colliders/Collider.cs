﻿//--------------------------------------------------------------------------------
//  Collider
//  Base abstract class that all colliders inherit from
//  Be sure to override the various abstract methods for collision checks
//  against other collider types when create a new collider type that
//  inherits from this
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Psiren.Entities;
using Psiren.Utils;
using System;

namespace Psiren.Colliders
{
    public abstract class Collider
    {
        /// <summary>
        ///     The entity that this <see cref="Collider"/> is attached to
        /// </summary>
        public Entity Entity { get; private set; }

        /// <summary>
        ///     The xy-coordinate position of this <see cref="Collider"/>
        /// </summary>
        public Vector2 Position;

        /// <summary>
        ///     Called from the <see cref="Entity"/> when this <see cref="Collider"/> is attached to it
        /// </summary>
        /// <param name="entity">The <see cref="Entity"/></param>
        internal virtual void Added(Entity entity) => Entity = entity;

        /// <summary>
        ///     Called from the <see cref="Entity"/> when this <see cref="Collider"/> is attached to it
        /// </summary>
        internal virtual void Removed() => Entity = null;

        /// <summary>
        ///     Creates a new <see cref="Collider"/> instance
        /// </summary>
        /// <param name="entity">The <see cref="Entity"/> this collider is attached to</param>
        /// <returns></returns>
        public bool Collide(Entity entity) => Collide(entity.Collider);

        /// <summary>
        ///     Checks if this <see cref="Collider"/> collides with another <see cref="Collider"/>
        /// </summary>
        /// <param name="collider">The <see cref="Collider"/> to check</param>
        /// <returns></returns>
        public bool Collide(Collider collider)
        {
            if (collider is Hitbox)
            {
                return Collide(collider as Hitbox);
            }
            else if (collider is Grid)
            {
                return Collide(collider as Grid);
            }
            else if (collider is ColliderList)
            {
                return Collide(collider as ColliderList);
            }
            else if (collider is Circle)
            {
                return Collide(collider as Circle);
            }
            else
            {
                throw new Exception("Collisions against the collider type are not implemented");
            }
        }

        public abstract bool Collide(Vector2 point);
        public abstract bool Collide(Rectangle rect);
        public abstract bool Collide(Vector2 from, Vector2 to);
        public abstract bool Collide(Hitbox hitbox);
        public abstract bool Collide(Grid grid);
        public abstract bool Collide(Circle circle);
        public abstract bool Collide(ColliderList list);
        public abstract Collider Clone();
        public abstract void Render(Camera camera, Color color);
        public abstract float Width { get; set; }
        public abstract float Height { get; set; }
        public abstract float Top { get; set; }
        public abstract float Bottom { get; set; }
        public abstract float Left { get; set; }
        public abstract float Right { get; set; }

        public void CenterOrigin()
        {
            Position.X = -Width / 2;
            Position.Y = -Height / 2;
        }
        public float CenterX
        {
            get => Left + Width / 2f;
            set => Left = value - Width / 2f;
        }
        public float CenterY
        {
            get => Top + Height / 2f;
            set => Top = value - Height / 2f;
        }
        public Vector2 TopLeft
        {
            get => new Vector2(Left, Top);
            set
            {
                Left = value.X;
                Top = value.Y;
            }
        }
        public Vector2 TopCenter
        {
            get => new Vector2(CenterX, Top);
            set
            {
                CenterX = value.X;
                Top = value.Y;
            }
        }
        public Vector2 TopRight
        {
            get => new Vector2(Right, Top);
            set
            {
                Right = value.X;
                Top = value.Y;
            }
        }
        public Vector2 CenterLeft
        {
            get => new Vector2(Left, CenterY);
            set
            {
                Left = value.X;
                CenterY = value.Y;
            }
        }
        public Vector2 Center
        {
            get => new Vector2(CenterX, CenterY);
            set
            {
                CenterX = value.X;
                CenterY = value.Y;
            }
        }
        public Vector2 CenterRight
        {
            get => new Vector2(Right, CenterY);
            set
            {
                Right = value.X;
                CenterY = value.Y;
            }
        }
        public Vector2 BottomLeft
        {
            get => new Vector2(Left, Bottom);
            set
            {
                Left = value.X;
                Bottom = value.Y;
            }
        }
        public Vector2 BottomCenter
        {
            get => new Vector2(CenterX, Bottom);
            set
            {
                CenterX = value.X;
                Bottom = value.Y;
            }
        }
        public Vector2 BottomRight
        {
            get => new Vector2(Right, Bottom);
            set
            {
                Right = value.X;
                Bottom = value.Y;
            }
        }

        public Vector2 Size => new Vector2(Width, Height);
        public Vector2 HalfSize => Size * 0.5f;

        public void Render(Camera camera)
        {
            Render(camera, Color.Red);
        }

        public Vector2 AbsolutePosition
        {
            get
            {
                if (Entity != null)
                {
                    return Entity.Position + Position;
                }
                else
                {
                    return Position;
                }
            }
        }
        public float AbsoluteX
        {
            get
            {
                if (Entity != null)
                {
                    return Entity.Position.X + Position.X;
                }
                else
                {
                    return Position.X;
                }
            }
        }
        public float AbsoluteY
        {
            get
            {
                if (Entity != null)
                {
                    return Entity.Position.Y + Position.Y;
                }
                else
                {
                    return Position.Y;
                }
            }
        }
        public float AbsoluteTop
        {
            get
            {
                if (Entity != null)
                {
                    return Top + Entity.Position.Y;
                }
                else
                {
                    return Top;
                }
            }
        }
        public float AbsoluteBottom
        {
            get
            {
                if (Entity != null)
                {
                    return Bottom + Entity.Position.Y;
                }
                else
                {
                    return Bottom;
                }
            }
        }
        public float AbsoluteLeft
        {
            get
            {
                if (Entity != null)
                {
                    return Left + Entity.Position.X;
                }
                else
                {
                    return Left;
                }
            }
        }
        public float AbsoluteRight
        {
            get
            {
                if (Entity != null)
                {
                    return Right + Entity.Position.X;
                }
                else
                {
                    return Right;
                }
            }
        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((int)AbsoluteLeft, (int)AbsoluteTop, (int)Width, (int)Height);
            }
        }


    }
}
