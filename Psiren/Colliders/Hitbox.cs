﻿//--------------------------------------------------------------------------------
//  Hitbox
//
//  This is a standard rectangle collider
//
//  Needs <summary> documentation on the Edge properties
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.Utils;

namespace Psiren.Colliders
{
    public class Hitbox : Collider
    {
        /// <summary>
        ///     The width
        /// </summary>
        public override float Width { get => _width; set => _width = value; }
        private float _width;

        /// <summary>
        ///     The height
        /// </summary>
        public override float Height { get => _height; set => _height = value; }
        private float _height;


        /// <summary>
        ///     The top most y-coordinate position
        /// </summary>
        public override float Top { get => Position.Y; set => Position.Y = value; }

        /// <summary>
        ///     The bottom most y-coordinate position
        /// </summary>
        public override float Bottom { get => Position.Y + Height; set => Position.Y = value - Height; }

        /// <summary>
        ///     The left most x-coordinate position
        /// </summary>
        public override float Left { get => Position.X; set => Position.X = value; }

        /// <summary>
        ///     The right most x-coordinate position
        /// </summary>
        public override float Right { get => Position.X + Width; set => Position.X = value - Width; }

        /// <summary>
        ///     Creates a new <see cref="Hitbox"/> instance
        /// </summary>
        /// <param name="width">The width</param>
        /// <param name="height">The height</param>
        /// <param name="x">The x-coordinate position (relative to top-left)</param>
        /// <param name="y">The y-coordinate position (relative to top-left)</param>
        public Hitbox(float width, float height, float x = 0, float y = 0)
        {
            _width = width;
            _height = height;

            Position.X = x;
            Position.Y = y;
        }

        public void GetTopEdge(out Vector2 from, out Vector2 to)
        {
            from.X = AbsoluteLeft;
            to.X = AbsoluteRight;
            from.Y = to.Y = AbsoluteTop;
        }

        public void GetBottomEdge(out Vector2 from, out Vector2 to)
        {
            from.X = AbsoluteLeft;
            to.X = AbsoluteRight;
            from.Y = to.Y = AbsoluteBottom;
        }

        public void GetLeftEdge(out Vector2 from, out Vector2 to)
        {
            from.Y = AbsoluteTop;
            to.Y = AbsoluteBottom;
            from.X = to.X = AbsoluteLeft;
        }

        public void GetRightEdge(out Vector2 from, out Vector2 to)
        {
            from.Y = AbsoluteTop;
            to.Y = AbsoluteBottom;
            from.X = to.X = AbsoluteRight;
        }

        /// <summary>
        ///     Checks if this <see cref="Hitbox"/> intersects with another <see cref="Hitbox"/>
        /// </summary>
        /// <param name="hitbox"></param>
        /// <returns></returns>
        public bool Intersects(Hitbox hitbox)
        {
            return AbsoluteLeft < hitbox.AbsoluteRight &&
                AbsoluteRight > hitbox.AbsoluteLeft &&
                AbsoluteBottom > hitbox.AbsoluteTop &&
                AbsoluteTop < hitbox.AbsoluteBottom;
        }

        /// <summary>
        ///     Checks if this <see cref="Hitbox"/> intersects with a virtually defined area
        /// </summary>
        /// <param name="x">The top-left x-coordinate of the virtual area</param>
        /// <param name="y">The top-left y-coordinate of the virtual area</param>
        /// <param name="width">The width of the virtual area</param>
        /// <param name="height">The height of the virtual area</param>
        /// <returns></returns>
        public bool Intersects(float x, float y, float width, float height)
        {
            return AbsoluteRight > x &&
                AbsoluteBottom > y &&
                AbsoluteLeft < x + width &&
                AbsoluteTop < y + height;
        }

        /// <summary>
        ///     Creates a clone of this <see cref="Hitbox"/>
        /// </summary>
        /// <returns></returns>
        public override Collider Clone()
        {
            return new Hitbox(_width, _height, Position.X, Position.Y);
        }

        /// <summary>
        ///     Draws this <see cref="Hitbox"/> to the screen
        /// </summary>
        /// <param name="camera">The <see cref="Camera"/> to use</param>
        /// <param name="color">The color to render at</param>
        public override void Render(Camera camera, Color color)
        {
            Utils.Draw.HollowRect(AbsoluteX, AbsoluteY, Width, Height, color);
        }

        /// <summary>
        ///     Sets the <see cref="Width"/>, <see cref="Height"/>, top-left xy-coordinate
        ///     poisition for this <see cref="Hitbox"/> based on the given Rectangle
        /// </summary>
        /// <param name="rect"></param>
        public void SetFromRectangle(Rectangle rect)
        {
            Position = new Vector2(rect.X, rect.Y);
            Width = rect.Width;
            Height = rect.Height;
        }

        /// <summary>
        ///     Set the <see cref="Width"/>, <see cref="Height"/>, and top-left xy-coordinate
        ///     based on the given values
        /// </summary>
        /// <param name="x">The top-left x-coordinate position</param>
        /// <param name="y">The top-left y-coordinate position</param>
        /// <param name="width">The width</param>
        /// <param name="height">The height</param>
        public void Set(float x, float y, float width, float height)
        {
            Position = new Vector2(x, y);
            Width = width;
            Height = height;
        }

        /// <summary>
        ///     Checks if the given xy-coordinate position is colliding with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="point">The xy-coordinate position to check</param>
        /// <returns></returns>
        public override bool Collide(Vector2 point)
        {
            return Psiren.Colliders.Collide.RectToPoint(AbsoluteLeft, AbsoluteTop, Width, Height, point);
        }

        /// <summary>
        ///     Checks if the given rectangle is colliding with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="rect">The rectangle to check</param>
        /// <returns></returns>
        public override bool Collide(Rectangle rect)
        {
            return AbsoluteRight > rect.Left && AbsoluteBottom > rect.Top && AbsoluteLeft < rect.Right && AbsoluteTop < rect.Bottom;
        }

        /// <summary>
        ///     Checks if a line that starts at the <paramref name="from"/> xy-coordinate position and ends
        ///     at the <paramref name="to"/> xy-coordinate position collides with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="from">The starting xy-coordinate</param>
        /// <param name="to">The ending xy-coordinate</param>
        /// <returns></returns>
        public override bool Collide(Vector2 from, Vector2 to)
        {
            return Psiren.Colliders.Collide.RectToLine(AbsoluteLeft, AbsoluteTop, Width, Height, from, to);
        }

        /// <summary>
        ///     Checks if the given <see cref="Hitbox"/> is colliding with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="hitbox">The <see cref="Hitbox"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Hitbox hitbox)
        {
            return Intersects(hitbox);
        }

        /// <summary>
        ///     Checks if the given <see cref="Grid"/> is colliding with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="grid">The <see cref="Grid"/> to check</param>
        /// <returns></returns>
        public override bool Collide(Grid grid)
        {
            return grid.Collide(Bounds);
        }

        /// <summary>
        ///     Checks if the given <see cref="Circle"/> is colliding with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="circle"></param>
        /// <returns></returns>
        public override bool Collide(Circle circle)
        {
            return Psiren.Colliders.Collide.RectToCircle(AbsoluteLeft, AbsoluteTop, Width, Height, circle.AbsolutePosition, circle.Radius);
        }

        /// <summary>
        ///     Checks if any <see cref="Collider"/> in the provided <see cref="ColliderList"/>
        ///     is colliding with this <see cref="Hitbox"/>
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public override bool Collide(ColliderList list)
        {
            return list.Collide(this);
        }


    }
}

