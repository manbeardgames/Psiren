﻿//-----------------------------------------------------------------
//  Padding
//  Defines the amount of padding to use when creating a NineSlice
//
//
//-----------------------------Psiren Engine License-----------------------------
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
namespace Psiren.Graphics
{
    public class Padding
    {
        //-----------------------------------------------------------------
        //  Used to get quick Padding types with preset values
        //-----------------------------------------------------------------
        public static readonly Padding Zero = new Padding(0);
        public static readonly Padding One = new Padding(1);
        public static readonly Padding Two = new Padding(2);
        public static readonly Padding Three = new Padding(3);
        public static readonly Padding Four = new Padding(4);
        public static readonly Padding Five = new Padding(5);
        public static readonly Padding Six = new Padding(6);
        public static readonly Padding Seven = new Padding(7);
        public static readonly Padding Eight = new Padding(8);
        public static readonly Padding Nine = new Padding(9);
        public static readonly Padding Ten = new Padding(10);


        //-----------------------------------------------------------------
        //  Padding Top, Bottom, Left, and Right properties
        //-----------------------------------------------------------------
        public int Top { get; set; }
        public int Bottom { get; set; }
        public int Left { get; set; }
        public int Right { get; set; }


        //-----------------------------------------------------------------
        //  Constructors
        //-----------------------------------------------------------------
        /// <summary>
        /// Creates a Padding instenace
        /// </summary>
        /// <param name="top">The amount of top padding</param>
        /// <param name="bottom">The amount of bottom padding</param>
        /// <param name="left">The amount of left padding</param>
        /// <param name="right">The amount of right padding</param>
        public Padding(int top, int bottom, int left, int right)
        {
            Top = top;
            Bottom = bottom;
            Left = left;
            Right = right;
        }

        /// <summary>
        /// Creates a new Padding instance where the top and bottom are equal
        /// and the left and right are equal
        /// </summary>
        /// <param name="topAndBottom">The amount of padding for the top and bottom</param>
        /// <param name="leftAndRight">The amount of padding for the left and right</param>
        public Padding(int topAndBottom, int leftAndRight)
            : this(topAndBottom, topAndBottom, leftAndRight, leftAndRight) { }


        /// <summary>
        /// Creats a new Padding instance where all sides are equal amounts
        /// </summary>
        /// <param name="topAndBottomAndLeftAndRight">The amount of padding equal for all side</param>
        public Padding(int topAndBottomAndLeftAndRight)
            : this(topAndBottomAndLeftAndRight, topAndBottomAndLeftAndRight, topAndBottomAndLeftAndRight, topAndBottomAndLeftAndRight) { }
    }
}
