﻿//--------------------------------------------------------------------------------
//  MTexture
//
//  Wrapper for texture2D for managing the texture and rendering it
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

namespace Psiren.Graphics
{
    public class Tileset
    {
        private MTexture[,] tiles;

        public Tileset(MTexture texture, int tileWidth, int tileHeight)
        {
            Texture = texture;
            TileWidth = tileWidth;
            TileHeight = TileHeight;

            tiles = new MTexture[Texture.Width / tileWidth, Texture.Height / tileHeight];
            for (int x = 0; x < Texture.Width / tileWidth; x++)
                for (int y = 0; y < Texture.Height / tileHeight; y++)
                    tiles[x, y] = new MTexture(Texture, x * tileWidth, y * tileHeight, tileWidth, tileHeight);
        }

        public MTexture Texture
        {
            get; private set;
        }

        public int TileWidth
        {
            get; private set;
        }

        public int TileHeight
        {
            get; private set;
        }

        public MTexture this[int x, int y]
        {
            get
            {
                return tiles[x, y];
            }
        }

        public MTexture this[int index]
        {
            get
            {
                if (index < 0)
                    return null;
                else
                    return tiles[index % tiles.GetLength(0), index / tiles.GetLength(0)];
            }
        }
    }
}
