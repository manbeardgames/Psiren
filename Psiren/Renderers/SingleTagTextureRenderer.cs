﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Renderers
{
    public class SingleTagTextureRenderer : SingleTagRenderer
    {
        private RenderTarget2D _renderTarget;

        public SingleTagTextureRenderer(BitTag tag, int width, int height) : base(tag)
        {
            _renderTarget = new RenderTarget2D(
                graphicsDevice: Engine.Graphics.GraphicsDevice,
                width: width,
                height: height,
                mipMap: false,
                preferredFormat: SurfaceFormat.Color,
                preferredDepthFormat: DepthFormat.Depth24Stencil8);
        }

        public override void BeforeRender(Scene scene)
        {
            base.BeforeRender(scene);

            Engine.Graphics.GraphicsDevice.SetRenderTarget(_renderTarget);
            Draw.SpriteBatch.Begin(SortMode, BlendState, SamplerState, DepthStencilState.None, RasterizerState.CullNone, null, Camera.Matrix * Engine.ScreenMatrix);
            foreach (var entity in scene[Tag])
            {
                if (entity.Visible)
                {
                    entity.Render();
                }
            }

            if (Engine.Commands.Open)
                foreach (var entity in scene[Tag])
                    entity.DebugRender(Camera);

            Draw.SpriteBatch.End();
            Engine.Graphics.GraphicsDevice.SetRenderTarget(null);
        }

        public override void Render(Scene scene)
        {
            base.Render(scene);
            Draw.SpriteBatch.Begin(SortMode, BlendState, SamplerState, DepthStencilState.None, RasterizerState.CullNone, Effect, Camera.Matrix * Engine.ScreenMatrix);

            if (Effect != null)
            {
                Effect.CurrentTechnique.Passes[0].Apply();
            }

            Draw.SpriteBatch.Draw(_renderTarget, new Rectangle(0, 0, Engine.Width, Engine.Height), Color.White);

            Draw.SpriteBatch.End();
        }
    }
}
