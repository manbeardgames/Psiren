﻿//--------------------------------------------------------------------------------
//  Renderer
//
//  Base abstract renderer class that all renderers derive from
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------


using Psiren.Scenes;
using Psiren.Utils;

namespace Psiren.Renderers
{
    public abstract class Renderer
    {
        /// <summary>
        ///     Is the <see cref="Renderer"/> visible
        /// </summary>
        public bool Visible = true;

        public Camera Camera;

        /// <summary>
        ///     Updates the <see cref="Renderer"/>
        /// </summary>
        /// <param name="scene"></param>
        public virtual void Update(Scene scene) { }

        /// <summary>
        ///     Called before <see cref="Render(Scene)"/> is called
        /// </summary>
        /// <param name="scene"></param>
        public virtual void BeforeRender(Scene scene) { }

        /// <summary>
        ///     Renders the <see cref="Renderer"/>
        /// </summary>
        /// <param name="scene"></param>
        public virtual void Render(Scene scene) { }

        /// <summary>
        ///     Called after <see cref="Render(Scene)"/> is called
        /// </summary>
        /// <param name="scene"></param>
        public virtual void AfterRender(Scene scene) { }

    }
}
