﻿using Microsoft.Xna.Framework.Graphics;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Renderers
{
    public class EverythingExceptRenderer : Renderer
    {

        public BlendState BlendState { get; set; }
        public SamplerState SamplerState { get; set; }
        public Effect Effect { get;  set; }
        //public Camera Camera { get; set; }
        public SpriteSortMode SpriteSortMode { get; set; }
        public RasterizerState RasterizerState { get; set; }
        public DepthStencilState DepthStencilState { get;  set; }
        public int TagToExclude { get;  set; }


        public override void BeforeRender(Scene scene)
        {

        }


        public EverythingExceptRenderer(int tagToExclude)
        {

            TagToExclude = tagToExclude;
            BlendState = BlendState.AlphaBlend;
            SamplerState = SamplerState.PointClamp;
            SpriteSortMode = SpriteSortMode.Deferred;
            RasterizerState = RasterizerState.CullNone;
            DepthStencilState = DepthStencilState.None;
            Camera = new Camera();
        }


        public override void Render(Scene scene)
        {
            Draw.SpriteBatch.Begin(
                sortMode: SpriteSortMode,
                blendState: BlendState,
                samplerState: SamplerState,
                depthStencilState: DepthStencilState,
                rasterizerState: RasterizerState,
                effect: Effect,
                transformMatrix: Camera.Matrix * Engine.ScreenMatrix);


            foreach(var entity in scene.Entities)
            {
                if(entity.Visible && (entity.Tag & TagToExclude) == 0)
                {
                    entity.Render();
                }
            }

            if(Engine.Commands.Open)
            {
                foreach(var entity in scene.Entities)
                {
                    if((entity.Tag & TagToExclude) == 0)
                    {
                        entity.DebugRender(Camera);
                    }
                }
            }

            Draw.SpriteBatch.End();
        }

        public override void AfterRender(Scene scene)
        {
        }


    }
}
