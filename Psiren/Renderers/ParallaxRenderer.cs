﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Renderers
{
    public class ParallaxRenderer : Renderer
    {
        public BlendState BlendState { get; set; }
        public SamplerState SamplerState { get; set; }
        public Effect Effect { get; set; }
        public SpriteSortMode SortMode { get; set; }

        private List<ParallaxLayer> _layers;

        public Vector2 Direction { get; set; }

        private float _rate;
        public float Rate
        {
            get { return _rate; }
            set
            {
                if (_rate == value) { return; }
                _layers?.ForEach(layer => layer.Rate = new Vector2(value, value));
            }
        }



        public ParallaxRenderer()
        {
            this.BlendState = BlendState.AlphaBlend;
            this.SamplerState = SamplerState.LinearWrap;
            this.SortMode = SpriteSortMode.Deferred;
            this.Effect = null;
            this.Camera = new Utils.Camera();
            this._layers = new List<ParallaxLayer>();
            this.Rate = 1.0f;
        }

        public void AddLayer(Texture2D texture, Vector2 speed)
        {
            this._layers.Add(new ParallaxLayer(texture, speed));
        }

        public override void Update(Scene scene)
        {
            foreach(var layer in this._layers)
            {
                layer.Update(this.Direction);
            }
        }

        public override void Render(Scene scene)
        {
            Draw.SpriteBatch.Begin(this.SortMode, this.BlendState, this.SamplerState, null, null, this.Effect, this.Camera.Matrix * Engine.ScreenMatrix);
            
            foreach(var layer in this._layers)
            {
                layer.Render(this.Camera.Viewport);
            }

            Draw.SpriteBatch.End();
        }
    }

    public class ParallaxLayer
    {
        public Texture2D Texture { get; set; }
        public Vector2 Speed { get; set; }
        public Vector2 Offset { get; set; }
        public Vector2 Rate { get; set; }


        public ParallaxLayer(Texture2D texture, Vector2 speed)
        {
            this.Texture = texture;
            this.Speed = speed;
            this.Rate = new Vector2(1.0f, 1.0f);
        }

        public void Update(Vector2 direction)
        {
            Vector2 distance = direction * this.Speed * this.Rate * Engine.DeltaTime;
            this.Offset += distance;
        }

        public void Render(Viewport viewport)
        {
            Rectangle rectangle = new Rectangle((int)this.Offset.X, (int)this.Offset.Y, (int)viewport.Width, (int)Texture.Height);
            Draw.SpriteBatch.Draw(this.Texture, new Vector2(viewport.X, viewport.Y), rectangle, Color.White, 0, Vector2.Zero, Vector2.One, SpriteEffects.None, 0);
        }

    }
}
