﻿//--------------------------------------------------------------------------------
//  EverythingRenderer
//
//  Renderer used to render all Entities within a Scene
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework.Graphics;
using Psiren.Scenes;
using Psiren.Utils;

namespace Psiren.Renderers
{
    public class EverythingRenderer : Renderer
    {
        /// <summary>
        ///     The BlendState to use
        /// </summary>
        public BlendState BlendState;

        /// <summary>
        ///     The SamplerState to use
        /// </summary>
        public SamplerState SamplerState;

        /// <summary>
        ///     The Effect (shader) to use
        /// </summary>
        public Effect Effect;

        /// <summary>
        ///     The SpriteSortMode to use
        /// </summary>
        public SpriteSortMode SortMode;

        /// <summary>
        ///     The <see cref="Camera"/> used by this renderer
        /// </summary>
        //public Camera Camera;

        /// <summary>
        ///     Creates a new <see cref="EverythingRenderer"/> instance
        /// </summary>
        public EverythingRenderer()
        {
            BlendState = BlendState.AlphaBlend;
            SamplerState = SamplerState.LinearClamp;
            SortMode = SpriteSortMode.Deferred;
            Camera = new Camera();
        }

        /// <summary>
        ///     Called before <see cref="Render(Scene)"/> is called
        /// </summary>
        /// <param name="scene"></param>
        public override void BeforeRender(Scene scene) { }

        /// <summary>
        ///     Renders the <see cref="Entity"/> collection of the <see cref="Scene"/>
        /// </summary>
        /// <param name="scene">The <see cref="Scene"/> to render</param>
        public override void Render(Scene scene)
        {
            Draw.SpriteBatch.Begin(SortMode, BlendState, SamplerState, DepthStencilState.None, RasterizerState.CullNone, Effect, Camera.Matrix * Engine.ScreenMatrix);

            scene.Entities.Render();
            if (Engine.Commands.Open)
                scene.Entities.DebugRender(Camera);

            Draw.SpriteBatch.End();
        }

        /// <summary>
        ///     Called After <see cref="Render(Scene)"/> is called
        /// </summary>
        /// <param name="scene"></param>
        public override void AfterRender(Scene scene) { }
    }
}
