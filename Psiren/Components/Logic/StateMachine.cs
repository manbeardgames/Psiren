﻿//--------------------------------------------------------------------------------
//  StateMachine
//
//  Needs <summary> documentation added
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Psiren.Entities;
using Psiren.Scenes;
using System;
using System.Collections;

namespace Psiren.Components.Logic
{
    public class StateMachine : Component
    {
        private int _state;
        private Action[] _begins;
        private Func<int>[] _updates;
        private Action[] _ends;
        private Func<IEnumerator>[] _coroutines;
        private Coroutine _currentCoroutine;

        public bool ChangedStates;
        public bool Log;
        public int PreviousState { get; private set; }
        public bool Locked;

        public StateMachine(int maxStates = 10)
            : base(true, false)
        {
            PreviousState = _state = -1;
            _begins = new Action[maxStates];
            _updates = new Func<int>[maxStates];
            _ends = new Action[maxStates];
            _coroutines = new Func<IEnumerator>[maxStates];

            _currentCoroutine = new Coroutine();
            _currentCoroutine.RemoveOnComplete = false;
        }

        public override void Added(Entity entity)
        {
            base.Added(entity);
            if (Entity.Scene != null && _state == -1)
            {
                State = 0;
            }
        }

        public override void EntityAdded(Scene scene)
        {
            base.EntityAdded(scene);
            if (_state == -1)
            {
                State = 0;
            }
        }

        public int State
        {
            get => _state;
            set
            {
#if DEBUG
                if (value >= _updates.Length || value < 0)
                {
                    throw new Exception("StateMachine out of range");
                }
#endif

                if (!Locked && _state != value)
                {
                    if (Log)
                    {
                        Calc.Log($"Enter State {value} (leaving {_state})");
                    }

                    ChangedStates = true;
                    PreviousState = _state;
                    _state = value;

                    if (PreviousState != -1 && _ends[PreviousState] != null)
                    {
                        if (Log)
                        {
                            Calc.Log($"Calling End {PreviousState}");
                        }
                        _ends[PreviousState]();
                    }

                    if (_begins[_state] != null)
                    {
                        if (Log)
                        {
                            Calc.Log($"Calling Being {_state}");
                        }
                        _begins[_state]();
                    }

                    if (_coroutines[_state] != null)
                    {
                        if (Log)
                        {
                            Calc.Log($"Starting Coroutine {_state}");
                        }
                        _currentCoroutine.Replace(_coroutines[_state]());
                    }
                    else
                    {
                        _currentCoroutine.Cancel();
                    }
                }
            }
        }

        public void ForceState(int toState)
        {
            if (_state != toState)
            {
                State = toState;
            }
            else
            {
                if (Log)
                {
                    Calc.Log($"Enter State {toState} (leaving {_state}");
                }

                ChangedStates = true;
                PreviousState = _state;
                _state = toState;


                if (PreviousState != -1 && _ends[PreviousState] != null)
                {
                    if (Log)
                    {
                        Calc.Log($"Calling End {_state}");
                    }
                    _ends[PreviousState]();
                }

                if (_begins[_state] != null)
                {
                    if (Log)
                    {
                        Calc.Log($"Calling Begin {_state}");
                    }
                    _begins[_state]();
                }

                if (_coroutines[_state] != null)
                {
                    if (Log)
                    {
                        Calc.Log($"Starting Coroutine {_state}");
                    }
                    _currentCoroutine.Replace(_coroutines[_state]());
                }
                else
                    _currentCoroutine.Cancel();
            }
        }

        public void SetCallbacks(int state, Func<int> onUpdate, Func<IEnumerator> coroutine = null, Action begin = null, Action end = null)
        {
            _updates[state] = onUpdate;
            _begins[state] = begin;
            _ends[state] = end;
            _coroutines[state] = coroutine;
        }

        public void ReflectState(Entity from, int index, string name)
        {
            _updates[index] = (Func<int>)Calc.GetMethod<Func<int>>(from, name + "Update");
            _begins[index] = (Action)Calc.GetMethod<Action>(from, name + "Begin");
            _ends[index] = (Action)Calc.GetMethod<Action>(from, name + "End");
            _coroutines[index] = (Func<IEnumerator>)Calc.GetMethod<Func<IEnumerator>>(from, name + "Coroutine");
        }

        public override void Update()
        {
            ChangedStates = false;

            if (_updates[_state] != null)
            {
                State = _updates[_state]();
            }

            if (_currentCoroutine.Active)
            {
                _currentCoroutine.Update();
                if (!ChangedStates && Log && _currentCoroutine.Finished)
                {
                    Calc.Log($"Finished Coroutine {_state}");
                }
            }
        }

        public static implicit operator int(StateMachine s)
        {
            return s._state;
        }

        public void LogAllStates()
        {
            for (int i = 0; i < _updates.Length; i++)
            {
                LogState(i);
            }
        }

        public void LogState(int index)
        {
            Calc.Log("State " + index + ": "
                + (_updates[index] != null ? "U" : "")
                + (_begins[index] != null ? "B" : "")
                + (_ends[index] != null ? "E" : "")
                + (_coroutines[index] != null ? "C" : ""));
        }


    }
}
