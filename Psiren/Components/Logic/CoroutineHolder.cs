﻿//--------------------------------------------------------------------------------
//  CoroutineHolder
//
//  Needs <summary> documentation added
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using System.Collections;
using System.Collections.Generic;

namespace Psiren.Components.Logic
{
    public class CoroutineHolder : Component
    {
        private List<CoroutineData> coroutineList;
        private HashSet<CoroutineData> toRemove;
        private int nextID;
        private bool isRunning;

        public CoroutineHolder()
            : base(true, false)
        {
            coroutineList = new List<CoroutineData>();
            toRemove = new HashSet<CoroutineData>();
        }

        public override void Update()
        {
            isRunning = true;
            for (int i = 0; i < coroutineList.Count; i++)
            {
                var now = coroutineList[i].Data.Peek();

                if (now.MoveNext())
                {
                    if (now.Current is IEnumerator)
                        coroutineList[i].Data.Push(now.Current as IEnumerator);
                }
                else
                {
                    coroutineList[i].Data.Pop();
                    if (coroutineList[i].Data.Count == 0)
                        toRemove.Add(coroutineList[i]);
                }
            }
            isRunning = false;

            if (toRemove.Count > 0)
            {
                foreach (var r in toRemove)
                    coroutineList.Remove(r);
                toRemove.Clear();
            }
        }

        public void EndCoroutine(int id)
        {
            foreach (var c in coroutineList)
            {
                if (c.ID == id)
                {
                    if (isRunning)
                        toRemove.Add(c);
                    else
                        coroutineList.Remove(c);
                    break;
                }
            }
        }

        public int StartCoroutine(IEnumerator functionCall)
        {
            var data = new CoroutineData(nextID++, functionCall);
            coroutineList.Add(data);
            return data.ID;
        }

        public static IEnumerator WaitForFrames(int frames)
        {
            for (int i = 0; i < frames; i++)
                yield return 0;
        }

        private class CoroutineData
        {
            public int ID;
            public Stack<IEnumerator> Data;

            public CoroutineData(int id, IEnumerator functionCall)
            {
                ID = id;
                Data = new Stack<IEnumerator>();
                Data.Push(functionCall);
            }
        }
    }
}