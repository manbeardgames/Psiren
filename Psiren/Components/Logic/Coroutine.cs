﻿//--------------------------------------------------------------------------------
//  Coroutine
//
//  Component used to create coroutines
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Psiren.Utils;
using System.Collections;
using System.Collections.Generic;

namespace Psiren.Components.Logic
{
    [Tracked]
    public class Coroutine : Component
    {
        /// <summary>
        ///     Has this <see cref="Coroutine"/> finished running
        /// </summary>
        public bool Finished { get; private set; }

        /// <summary>
        ///     Should this <see cref="Coroutine"/> remove itself from the  <see cref="Entity"/> it
        ///     is attached to when it finishes
        /// </summary>
        public bool RemoveOnComplete = true;

        /// <summary>
        ///     Shoud this <see cref="Coroutine"/> use raw delta time values from
        ///     the engine for timing, or the variable delta time
        /// </summary>
        public bool UseRawDeltaTime = false;

        //  The IEnumerators for this coroutine
        private Stack<IEnumerator> enumerators;

        //  Timer used for waiting
        private float waitTimer;

        //  Has the coroutine ended
        private bool ended;

        /// <summary>
        ///     Creates a new <see cref="Coroutine"/> instance
        /// </summary>
        /// <param name="functionCall">The function to call</param>
        /// <param name="removeOnComplete">Should this remove itself from the parent <see cref="Entity"/> when completed</param>
        public Coroutine(IEnumerator functionCall, bool removeOnComplete = true)
            : base(true, false)
        {
            enumerators = new Stack<IEnumerator>();
            enumerators.Push(functionCall);
            RemoveOnComplete = removeOnComplete;
        }

        /// <summary>
        ///     Creates a new <see cref="Coroutine"/> instance
        /// </summary>
        /// <param name="removeOnComplete">Should this remove itself from the parent <see cref="Entity"/> when completed</param>
        public Coroutine(bool removeOnComplete = true)
            : base(false, false)
        {
            RemoveOnComplete = removeOnComplete;
            enumerators = new Stack<IEnumerator>();
        }

        /// <summary>
        ///     Updates the coroutine
        /// </summary>
        public override void Update()
        {
            ended = false;

            if (waitTimer > 0)
                waitTimer -= (UseRawDeltaTime ? Engine.RawDeltaTime : Engine.DeltaTime);
            else if (enumerators.Count > 0)
            {
                IEnumerator now = enumerators.Peek();
                if (now.MoveNext() && !ended)
                {
                    if (now.Current is int)
                        waitTimer = (int)now.Current;
                    if (now.Current is float)
                        waitTimer = (float)now.Current;
                    else if (now.Current is IEnumerator)
                        enumerators.Push(now.Current as IEnumerator);
                }
                else if (!ended)
                {
                    enumerators.Pop();
                    if (enumerators.Count == 0)
                    {
                        Finished = true;
                        Active = false;
                        if (RemoveOnComplete)
                            RemoveSelf();
                    }
                }
            }
        }

        /// <summary>
        ///     Cancels the coroutine
        /// </summary>
        public void Cancel()
        {
            Active = false;
            Finished = true;
            waitTimer = 0;
            enumerators.Clear();

            ended = true;
        }

        /// <summary>
        ///     Replaces the the function this uses with another
        /// </summary>
        /// <param name="functionCall">The new function to use</param>
        public void Replace(IEnumerator functionCall)
        {
            Active = true;
            Finished = false;
            waitTimer = 0;
            enumerators.Clear();
            enumerators.Push(functionCall);

            ended = true;
        }
    }
}