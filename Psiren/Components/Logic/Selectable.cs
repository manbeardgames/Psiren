﻿using Microsoft.Xna.Framework;
using Psiren.Components.Graphics;
using System;
using System.Collections.Generic;

namespace Psiren.Components.Logic
{
    public class Selectable : Component
    {
        //-------------------------------------------------
        //  Colors
        //-------------------------------------------------
        public Color NormalColor { get => _normalColor; set => _normalColor = value; }
        private Color _normalColor = Color.White;

        public Color HighlightColor { get => _highlightColor; set => _highlightColor = value; }
        private Color _highlightColor = Color.Orange;

        public Color PressedColor { get => _pressedColor; set => _pressedColor = value; }
        private Color _pressedColor = Color.DarkSlateGray;

        public Color DisabledColor { get => _disabledColor; set => _disabledColor = value; }
        private Color _disabledColor = Color.DarkSlateGray;

        //-------------------------------------------------
        //  Selected
        //-------------------------------------------------
        public bool IsSelected { get => _isSelected; set => _isSelected = value; }
        private bool _isSelected;

        //-------------------------------------------------
        //  Disabled
        //-------------------------------------------------
        public bool Disabled { get => _disabled; set => _disabled = value; }
        private bool _disabled;


        //-------------------------------------------------
        //  Selection Targets
        //-------------------------------------------------
        public List<GraphicsComponent> SelectionTargets { get => _selectionTargets; set => _selectionTargets = value; }
        private List<GraphicsComponent> _selectionTargets = new List<GraphicsComponent>();

        //-------------------------------------------------
        //  Actions
        //-------------------------------------------------
        public Action OnUpPressed { get => _onUpPressed; set => _onUpPressed = value; }
        private Action _onUpPressed;

        public Action OnDownPressed { get => _onDownPressed; set => _onDownPressed = value; }
        private Action _onDownPressed;

        public Action OnLeftPressed { get => _onLeftPressed; set => _onLeftPressed = value; }
        private Action _onLeftPressed;

        public Action OnRightPressed { get => _onRightPressed; set => _onRightPressed = value; }
        private Action _onRightPressed;

        public Action OnConfirmPressed { get => _onConfirmPressed; set => _onConfirmPressed = value; }
        private Action _onConfirmPressed;

        public Action OnBackPressed { get => _onBackPressed; set => _onBackPressed = value; }
        private Action _onBackPressed;





        public Selectable() : base(true, false)
        {

        }

        public override void Update()
        {
            base.Update();
            foreach (GraphicsComponent component in SelectionTargets)
            {
                if (_disabled)
                {
                    component.Color = _disabledColor;
                }
                else
                {
                    if (_isSelected)
                    {
                        component.Color = _highlightColor;
                    }
                    else
                    {
                        component.Color = _normalColor;
                    }
                }
            }
        }

        public void AddTarget(GraphicsComponent component)
        {
            SelectionTargets.Add(component);
        }
    }
}
