﻿using System.Collections.Generic;

namespace Psiren.Components.Graphics.Aseprite
{
    public struct AsepriteDefinition
    {
        public List<AsepriteAnimationDefinition> Animations { get; set; }
        public List<AsepriteFrameDefinition> Frames { get; set; }

        public AsepriteDefinition(List<AsepriteAnimationDefinition> animations, List<AsepriteFrameDefinition> frames)
        {
            Animations = animations;
            Frames = frames;
        }
    }
}
