﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Components.Graphics.Aseprite
{
    public enum Direction
    {
        forward = 1,
        reverse = -1
    }

    public enum BlendMode
    {
        normal
    }

}
