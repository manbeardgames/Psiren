﻿namespace Psiren.Components.Graphics.Aseprite
{
    public struct AsepriteAnimationDefinition
    {

        public static readonly AsepriteAnimationDefinition Empty =
            new AsepriteAnimationDefinition(string.Empty, 0, 0, Direction.forward, false);

        public bool IsEmpty
        {
            get
            {
                return this.Equals(Empty);
            }
        }


        public string Name { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public Direction Direction { get; set; }
        public bool Loop { get; set; }

        public AsepriteAnimationDefinition(string name, int from, int to, Direction direction, bool loop)
        {
            this.Name = name;
            this.From = from;
            this.To = to;
            this.Direction = direction;
            this.Loop = loop;
        }

        /// <summary>
        ///     Tests whether two <see cref="AsepriteAnimationDefinition"/> structures are equal.
        /// </summary>
        /// <param name="def1">The <see cref="AsepriteAnimationDefinition"/> structure on the left side of the equality operator.</param>
        /// <param name="def2">The <see cref="AsepriteAnimationDefinition"/> structure on the right of the equality operator.</param>
        /// <returns>
        ///     true if def1 and def2 have equal properties; otherwise, false.
        /// </returns>
        public static bool operator ==(AsepriteAnimationDefinition def1, AsepriteAnimationDefinition def2)
        {
            return def1.Equals(def2);
        }

        /// <summary>
        ///     Tests whether two <see cref="AsepriteAnimationDefinition"/> structures are different.
        /// </summary>
        /// <param name="def1">The <see cref="AsepriteAnimationDefinition"/> structure on the left of the inequality operator.</param>
        /// <param name="def2">The <see cref="AsepriteAnimationDefinition"/> structure on the right of the inequality operator.</param>
        /// <returns>
        ///     true if def1 and def2 differ in any properties; false if sz1 and sz2 are equal.
        /// </returns>
        public static bool operator !=(AsepriteAnimationDefinition def1, AsepriteAnimationDefinition def2)
        {
            return !def2.Equals(def2);
        }


        /// <summary>
        ///     Tests to see whether the specified object is a <see cref="AsepriteAnimationDefinition"/> structure
        ///     with the same dimensions as this <see cref="AsepriteAnimationDefinition"/> strcture
        /// </summary>
        /// <param name="obj">The object to test</param>
        /// <returns>
        ///     tru if obj is a <see cref="AsepriteAnimationDefinition"/> structure and has the property values as this
        ///     <see cref="AsepriteAnimationDefinition"/> structure; otherwise false
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is AsepriteAnimationDefinition other)
            {
                return this.Direction == other.Direction &&
                    this.From == other.From &&
                    this.Name == other.Name &&
                    this.To == other.To &&
                    this.Loop == other.Loop;
            }
            return false;
        }


        /// <summary>
        ///     Returns a hash code for this <see cref="AsepriteAnimationDefinition"/> structure
        /// </summary>
        /// <returns>
        ///     An integer value that specifies a hash value for this <see cref="AsepriteAnimationDefinition"/> structure
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
