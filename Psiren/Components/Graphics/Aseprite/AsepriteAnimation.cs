﻿using Microsoft.Xna.Framework;
using Psiren.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Components.Graphics.Aseprite
{
    public class AsepriteAnimation : Image
    {
        private Dictionary<string, AsepriteAnimationDefinition> _animations;
        private AsepriteFrameDefinition[] _frameDefinitions;
        private MTexture[] _frames;
        public bool Animating { get; private set; }

        //------------------------------------------------
        //  Current Frame
        //------------------------------------------------
        public AsepriteFrameDefinition CurrentFrame { get; set; }
        public int CurrentFrameIndex { get; private set; }
        //public int CurrentFrameIndex
        //{
        //    get
        //    {
        //        if (_frames == null) { return -1; }
        //        else { return Array.IndexOf(_frames, CurrentFrame); }
        //    }
        //}

        //------------------------------------------------
        //  Current Animation
        //------------------------------------------------
        public AsepriteAnimationDefinition CurrentAnimation { get; set; }
        public string CurrentAnimationID
        {
            get
            {
                if (CurrentAnimation.Equals(AsepriteAnimationDefinition.Empty)) { return string.Empty; }
                else { return CurrentAnimation.Name; }
            }
        }

        //------------------------------------------------
        //  Width
        //------------------------------------------------
        public override float Width
        {
            get
            {
                if (CurrentFrame.Equals(AsepriteFrameDefinition.Empty)) { return 0.0f; }
                else { return CurrentFrame.SpriteSourceSize.Width; }
            }
        }

        //------------------------------------------------
        //  Height
        //------------------------------------------------
        public override float Height
        {
            get
            {
                if (CurrentFrame.Equals(AsepriteFrameDefinition.Empty)) { return 0.0f; }
                else { return CurrentFrame.SpriteSourceSize.Height; }
            }
        }

        //------------------------------------------------
        //  Size
        //------------------------------------------------
        public override Vector2 Size
        {
            get
            {
                return new Vector2(Width, Height);
            }
        }

        public float Rate { get; set; } = 1;
        public bool UseRawDeltaTime { get; set; } = true;

        public Action<string> OnFinish;
        public Action<string> OnLoop;
        public Action<string> OnAnimate;

        private float _animationTimer;
        private bool _played;




        public AsepriteAnimation(MTexture sheet, AsepriteDefinition definition) : base(sheet, true)
        {
            this._animations = new Dictionary<string, AsepriteAnimationDefinition>();
            foreach (var animation in definition.Animations)
            {
                this._animations.Add(animation.Name, animation);
            }

            this._frameDefinitions = new AsepriteFrameDefinition[definition.Frames.Count];
            this._frames = new MTexture[definition.Frames.Count];
            for (int i = 0; i < definition.Frames.Count; i++)
            {
                AsepriteFrameDefinition framedef = definition.Frames[i];
                this._frameDefinitions[i] = framedef;

                Rectangle frame = definition.Frames[i].Frame;
                this._frames[i] = Texture.GetSubtexture(frame);
            }
        }

        public override void Update()
        {
            base.Update();

            //  Check if we are set to animate
            if(Animating && CurrentFrame.Duration > 0)
            {
                //  Increment timer
                if (UseRawDeltaTime) { _animationTimer += Engine.RawDeltaTime * Rate; }
                else { _animationTimer += Engine.DeltaTime * Rate * (int)CurrentAnimation.Direction; }

                //  Check if we need to advance to the next frame.
                if(Math.Abs(_animationTimer) >= CurrentFrame.Duration)
                {
                    //  Advance the frame in the direction of the Rate property
                    CurrentFrameIndex += Math.Sign((int)CurrentAnimation.Direction);

                    //  Since the delta time can sometimes be 'over' the duration of the frame
                    //  that was playing, reduce the timer by the actual duration of the frame
                    //  instead of just setting it to 0.
                    //_animationTimer -= Math.Sign((int)CurrentAnimation.Direction) * CurrentFrame.Duration;
                    _animationTimer = 0.0f;

                    //  Check if the CurrentFrame is the last frame in the animation
                    if(CurrentFrameIndex < CurrentAnimation.From || CurrentFrameIndex > CurrentAnimation.To)
                    {
                        //  Check if we are supposed to loop
                        if(CurrentAnimation.Loop)
                        {
                            if (CurrentAnimation.Direction == Direction.forward) { CurrentFrameIndex = CurrentAnimation.From; }
                            else { CurrentFrameIndex = CurrentAnimation.To; }

                            CurrentFrame = _frameDefinitions[CurrentFrameIndex];

                            //  Trigger OnAnimate
                            OnAnimate?.Invoke(CurrentAnimationID);

                            //  Trigger OnLoop
                            OnLoop?.Invoke(CurrentAnimationID);
                        }
                        else
                        {
                            //  Since not looping, ensure the frame index is on the last frame
                            if (CurrentAnimation.Direction == Direction.forward) { CurrentFrameIndex = CurrentAnimation.To; }
                            else { CurrentFrameIndex = CurrentAnimation.From; }

                            //  We've ended so we are no lonager animating
                            Animating = false;

                            //  Zero out the timer
                            _animationTimer = 0;

                            //  Trigger OnFinish
                            OnFinish?.Invoke(CurrentAnimationID);
                        }
                    }
                    else
                    {
                        //  Not the last frame of the animation, so continue
                        CurrentFrame = _frameDefinitions[CurrentFrameIndex];

                        //  Trigger OnAnimate
                        OnAnimate?.Invoke(CurrentAnimationID);

                    }
                }
            }
        }

        public override void Render()
        {
            Texture = _frames[CurrentFrameIndex];
            base.Render();
        }


        public bool IsPlaying(string id)
        {
            if (!_played) { return false; }
            else if (CurrentAnimationID == string.Empty) { return false; }
            else { return CurrentAnimationID == id; }
        }

        public void Play(string id, bool restart = false)
        {
            if(!IsPlaying(id) || restart)
            {
#if DEBUG
                if (!_animations.ContainsKey(id))
                {
                    throw new Exception($"No Animation defined for ID : {id}");
                }
#endif
                CurrentAnimation = _animations[id];
                _animationTimer = 0.0f;
                CurrentFrameIndex = CurrentAnimation.From;
                _played = true;

                Animating = CurrentAnimation.To - CurrentAnimation.From > 1;
                CurrentFrame = _frameDefinitions[CurrentFrameIndex];
            }
        }


        public void Stop()
        {
            Animating = false;
            _played = false;
        }




    }
}
