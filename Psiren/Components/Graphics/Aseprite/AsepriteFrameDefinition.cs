﻿using Microsoft.Xna.Framework;
using Psiren.Util;
using System.Diagnostics;

namespace Psiren.Components.Graphics.Aseprite
{
    public struct AsepriteFrameDefinition
    {
        public static readonly AsepriteFrameDefinition Empty = new AsepriteFrameDefinition("", Rectangle.Empty, false, false, Rectangle.Empty, Size.Empty, 0);


        public bool IsEmpty
        {
            get
            {
                return this.Equals(Empty);
            }
        }

        public string FileName { get; set; }
        public Rectangle Frame { get; set; }
        public bool Rotated { get; set; }
        public bool Trimmed { get; set; }
        public Rectangle SpriteSourceSize { get; set; }
        public Size SourceSize { get; set; }
        public float Duration { get; set; }

        public AsepriteFrameDefinition(string fileName, Rectangle frame, bool rotated, bool trimmed, Rectangle spriteSourceSize, Size sourceSize, int duration)
        {
            FileName = fileName;
            Frame = frame;
            Rotated = rotated;
            Trimmed = trimmed;
            SpriteSourceSize = spriteSourceSize;
            SourceSize = sourceSize;
            Duration = duration / 1000.0f;
            Debug.WriteLine($"duration: {duration} -- DURATION: {Duration}");
        }

        /// <summary>
        ///     Tests whether two <see cref="AsepriteFrameDefinition"/> structures are equal.
        /// </summary>
        /// <param name="def1">The <see cref="AsepriteFrameDefinition"/> structure on the left side of the equality operator.</param>
        /// <param name="def2">The <see cref="AsepriteFrameDefinition"/> structure on the right of the equality operator.</param>
        /// <returns>
        ///     true if def1 and def2 have equal properties; otherwise, false.
        /// </returns>
        public static bool operator ==(AsepriteFrameDefinition def1, AsepriteFrameDefinition def2)
        {
            return def1.Equals(def2);
        }

        /// <summary>
        ///     Tests whether two <see cref="AsepriteFrameDefinition"/> structures are different.
        /// </summary>
        /// <param name="def1">The <see cref="AsepriteFrameDefinition"/> structure on the left of the inequality operator.</param>
        /// <param name="def2">The <see cref="AsepriteFrameDefinition"/> structure on the right of the inequality operator.</param>
        /// <returns>
        ///     true if def1 and def2 differ in any properties; false if sz1 and sz2 are equal.
        /// </returns>
        public static bool operator !=(AsepriteFrameDefinition def1, AsepriteFrameDefinition def2)
        {
            return !def2.Equals(def2);
        }


        /// <summary>
        ///     Tests to see whether the specified object is a <see cref="AsepriteFrameDefinition"/> structure
        ///     with the same dimensions as this <see cref="AsepriteFrameDefinition"/> strcture
        /// </summary>
        /// <param name="obj">The object to test</param>
        /// <returns>
        ///     tru if obj is a <see cref="AsepriteFrameDefinition"/> structure and has the property values as this
        ///     <see cref="AsepriteFrameDefinition"/> structure; otherwise false
        /// </returns>
        public override bool Equals(object obj)
        {
            if(obj is AsepriteFrameDefinition other)
            {
                return this.FileName == other.FileName &&
                    this.Frame == other.Frame &&
                    this.Rotated == other.Rotated &&
                    this.Trimmed == other.Trimmed &&
                    this.SpriteSourceSize == other.SpriteSourceSize &&
                    this.SourceSize == other.SourceSize &&
                    this.Duration == other.Duration;
            }
            return false;
        }


        /// <summary>
        ///     Returns a hash code for this <see cref="AsepriteFrameDefinition"/> structure
        /// </summary>
        /// <returns>
        ///     An integer value that specifies a hash value for this <see cref="AsepriteFrameDefinition"/> structure
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
