﻿//--------------------------------------------------------------------------------
//  NineSlice
//
//  This allows you to add a texture to an entity and nineslice it with defined 
//  padding.  Useful for UI things.
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.Graphics;
using Psiren.Utils;
using System.Diagnostics;

namespace Psiren.Components.Graphics
{
    [Tracked]
    public class NineSlice : GraphicsComponent
    {


        /// <summary>
        ///     The <see cref="Padding"/> definition for the slice areas
        /// </summary>
        public Padding Padding { get => _padding; }
        private Padding _padding;

        /// <summary>
        ///     The total width that the nine slices should cover (in pixels)
        /// </summary>
        public int Width
        {
            get => _width;
            set
            {
                if (_width == value) { return; }
                _defineSlices = true;
                _width = value;
            }
        }
        private int _width;

        /// <summary>
        ///     The total hieght that the nine slices should cover (in pixels)
        /// </summary>
        public int Height
        {
            get => _height;
            set
            {
                if (_height == value) { return; }
                _defineSlices = true;
                _height = value;
            }
        }
        private int _height;

        //  This is the main texture that all nineslice regions will derive from
        private MTexture _mainTexture;

        //  Array of subtextures for each of the slices
        private MTexture[,] _textures;

        //  Array of scale values for each of the slices
        private Vector2[,] _scales;

        //  Array of the xy-coordinate positions for each of the slices releative the the 
        //  components position
        private Vector2[,] _positions;

        //  Used to determine if the nineslice areas need to be defined
        bool _defineSlices = true;

        /// <summary>
        /// Creates a new <see cref="NineSlice"/> component
        /// </summary>
        /// <param name="position">The relative xy-coordinate position of this component to the parent entity</param>
        /// <param name="texture">The <see cref="MTexture"/> texture to use as the main texture</param>
        /// <param name="padding">The <see cref="Padding"/> definition of the slice areas</param>
        /// <param name="width">The total width (in pixels) that this should fill</param>
        /// <param name="height">The total height (in pixels) that this should fill</param>
        /// <param name="active">Is this component active (updatable)</param>
        public NineSlice(Vector2 position, MTexture texture, Padding padding, int width, int height, bool active = true) : base(active)
        {
            _mainTexture = texture;
            _position = position;
            _padding = padding;
            _width = width;
            _height = height;
            DefineSlices();
            
        }

        /// <summary>
        ///     Defines the subtextures, scales, and position array values for
        ///     each of the sliced areas
        /// </summary>
        private void DefineSlices()
        {
            _textures = new MTexture[3, 3];
            _scales = new Vector2[3, 3];
            _positions = new Vector2[3, 3];

            //-------------------------- TOP LEFT ----------------------------------//;
            _textures[0, 0] = _mainTexture.GetSubtexture(0, 0, _padding.Left, _padding.Top);
            _positions[0, 0] = new Vector2(0.0f, 0.0f);
            _scales[0, 0] = new Vector2(1.0f, 1.0f);


            //-------------------------- TOP MID ----------------------------------//

            _textures[0, 1] = _mainTexture.GetSubtexture(_padding.Left, 0, _mainTexture.Width - _padding.Right - _padding.Left, _padding.Top);
            _positions[0, 1] = new Vector2(_padding.Left, 0.0f);
            _scales[0, 1] = new Vector2(
                x: (_width - _padding.Left - _padding.Right) / (float)_textures[0, 1].Width,
                y: 1.0f);

            //-------------------------- TOP RIGHT --------------------------------//
            _textures[0, 2] = _mainTexture.GetSubtexture(_mainTexture.Width - _padding.Right, 0, _padding.Right, _padding.Top);
            _positions[0, 2] = new Vector2(_width - _padding.Right, 0);
            _scales[0, 2] = new Vector2(1.0f, 1.0f);


            //-------------------------- CENTER LEFT ------------------------------//
            _textures[1, 0] = _mainTexture.GetSubtexture(0, _padding.Top, _padding.Left, _mainTexture.Height - _padding.Top - _padding.Bottom);
            _positions[1, 0] = new Vector2(0.0f, _padding.Top);
            _scales[1, 0] = new Vector2(
                x: 1.0f,
                y: (_height - _padding.Top - _padding.Bottom) / (float)_textures[1, 0].Height);


            //-------------------------- CENTER MID -------------------------------//
            _textures[1, 1] = _mainTexture.GetSubtexture(_padding.Left, _padding.Top, _mainTexture.Width - _padding.Left - _padding.Right, _mainTexture.Height - _padding.Top - _padding.Bottom);
            _positions[1, 1] = new Vector2(_padding.Left, _padding.Top);
            _scales[1, 1] = new Vector2(
                x: (_width - _padding.Left - _padding.Right) / (float)_textures[1, 1].Width,
                y: (_height - _padding.Top - _padding.Bottom) / (float)_textures[1, 1].Height);


            //-------------------------- CENTER RIGHT -----------------------------//
            _textures[1, 2] = _mainTexture.GetSubtexture(_mainTexture.Width - _padding.Right, _padding.Top, _padding.Right, _mainTexture.Height - _padding.Top - _padding.Bottom);
            _positions[1, 2] = new Vector2(_width - _padding.Right, _padding.Top);
            _scales[1, 2] = new Vector2(
                x: 1.0f,
                y: (_height - _padding.Top - _padding.Bottom) / (float)_textures[1, 2].Height);


            //-------------------------- BOTTOM LEFT ------------------------------//
            _textures[2, 0] = _mainTexture.GetSubtexture(0, _mainTexture.Height - _padding.Bottom, _padding.Left, _padding.Bottom);
            _positions[2, 0] = new Vector2(0.0f, _height - _padding.Bottom);
            _scales[2, 0] = new Vector2(1.0f, 1.0f);


            //-------------------------- BOTTOM MID -------------------------------//
            _textures[2, 1] = _mainTexture.GetSubtexture(_padding.Left, _mainTexture.Height - _padding.Bottom, _mainTexture.Width - _padding.Right - _padding.Left, _padding.Bottom);
            _positions[2, 1] = new Vector2(_padding.Left, _height - _padding.Bottom);
            _scales[2, 1] = new Vector2(
                x: (_width - _padding.Left - _padding.Right) / (float)_textures[2, 1].Width,
                y: 1.0f);

            //-------------------------- BOTTOM RIGHT -----------------------------//
            _textures[2, 2] = _mainTexture.GetSubtexture(_mainTexture.Width - _padding.Right, _mainTexture.Height - _padding.Bottom, _padding.Right, _padding.Bottom);
            _positions[2, 2] = new Vector2(_width - _padding.Right, _height - _padding.Bottom);
            _scales[2, 2] = new Vector2(1.0f, 1.0f);

            _defineSlices = false;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Debug.WriteLine($"Position: {_positions[i, j]} -- Scale: {_scales[i, j]}");
                }
            }

        }

        /// <summary>
        ///     Renders the component to the screen
        /// </summary>
        public override void Render()
        {
            if (_defineSlices)
            {
                DefineSlices();
            }

            


            if (_textures != null && _scales != null && _positions != null)
            {
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {

                        // MGBTODO:  Why is layerdepth 0.5f not working in ECHO
                        _textures[i, j].Draw(this.Entity.Position + _position + _positions[i, j], Origin, Color * Alpha, _scales[i, j], Rotation, Effects, 0.1f);
                    }
                }
            }
        }
    }
}
