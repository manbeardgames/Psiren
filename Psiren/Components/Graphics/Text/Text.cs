﻿//--------------------------------------------------------------------------------
//  Text
//
//  Component to draw text to the screen
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Utils;

namespace Psiren.Components.Graphics.Text
{
    public class Text : GraphicsComponent
    {
        public enum HorizontalAlign { Left, Center, Right };
        public enum VerticalAlign { Top, Center, Bottom };

        /// <summary>
        ///     The font used
        /// </summary>
        private SpriteFont _font;
        public SpriteFont Font
        {
            get => _font;
            set
            {
                _font = value;
                UpdateSize();
            }
        }

        /// <summary>
        ///     The text displayed
        /// </summary>
        public string DrawText
        {
            get => _drawText;
            set
            {
                _drawText = value;
                UpdateSize();
            }
        }
        private string _drawText;

        /// <summary>
        ///     The horizonatl origin alignment of the text
        /// </summary>
        private HorizontalAlign _horizontalOrigin;
        public HorizontalAlign HorizontalOrigin
        {
            get => _horizontalOrigin;
            set
            {
                _horizontalOrigin = value;
                UpdateCentering();
            }
        }

        /// <summary>
        ///     The vertical original alignment of the text
        /// </summary>
        private VerticalAlign _verticalOrigin;
        public VerticalAlign VerticalOrigin
        {
            get => _verticalOrigin;
            set
            {
                _verticalOrigin = value;
                UpdateCentering();
            }
        }

        /// <summary>
        ///     The width measurement of the text (in pixels) 
        /// </summary>
        public float Width => _size.X;

        /// <summary>
        ///     The height measurement of the text (in pixels)
        /// </summary>
        public float Height => _size.Y;

        //  The size measurement of the text written in the font
        private Vector2 _size;

        /// <summary>
        ///     Creates a new <see cref="Text"/> instance
        /// </summary>
        /// <param name="font">The font to use</param>
        /// <param name="text">The text to display</param>
        /// <param name="position">The xy-coordiante position relative to the parent entity position</param>
        /// <param name="color">The color of the text</param>
        /// <param name="horizontalAlign">The horizonatal origin alignment</param>
        /// <param name="verticalAlign">The vertical origin alignment</param>
        public Text(SpriteFont font, string text, Vector2 position, Color color, HorizontalAlign horizontalAlign = HorizontalAlign.Center, VerticalAlign verticalAlign = VerticalAlign.Center)
            : base(false)
        {
            _font = font;
            _drawText = text;
            Position = position;
            Color = color;
            _horizontalOrigin = horizontalAlign;
            _verticalOrigin = verticalAlign;
            UpdateSize();
        }

        /// <summary>
        ///     Creates a new <see cref="Text"/> instance
        /// </summary>
        /// <param name="font">The font to use</param>
        /// <param name="text">The text to display</param>
        /// <param name="position">The xy-coordiante position relative to the parent entity position</param>
        /// <param name="horizontalAlign">The horizonatal origin alignment</param>
        /// <param name="verticalAlign">The vertical origin alignment</param>
        public Text(SpriteFont font, string text, Vector2 position, HorizontalAlign horizontalAlign = HorizontalAlign.Center, VerticalAlign verticalAlign = VerticalAlign.Center)
            : this(font, text, position, Color.White, horizontalAlign, verticalAlign) { }


        /// <summary>
        ///     Updates the size property and alignment positioning
        /// </summary>
        public void UpdateSize()
        {
            _size = _font.MeasureString(_drawText);
            UpdateCentering();
        }

        /// <summary>
        ///     Updates the origin position
        /// </summary>
        public void UpdateCentering()
        {
            if (_horizontalOrigin == HorizontalAlign.Left)
            {
                Origin.X = 0;
            }
            else if (_horizontalOrigin == HorizontalAlign.Center)
            {
                Origin.X = _size.X / 2;
            }
            else
            {
                Origin.X = _size.X;
            }

            if (_verticalOrigin == VerticalAlign.Top)
            {
                Origin.Y = 0;
            }
            else if (_verticalOrigin == VerticalAlign.Center)
            {
                Origin.Y = _size.Y / 2;
            }
            else
            {
                Origin.Y = _size.Y;
            }

            Origin = Origin.Floor();
        }


        /// <summary>
        ///     Draws the component to the screen
        /// </summary>
        public override void Render()
        {
            Draw.SpriteBatch.DrawString(_font, _drawText, RenderPosition, Color * Alpha, Rotation, Origin, Scale, Effects, 0.0f);
        }

    }
}
