﻿//--------------------------------------------------------------------------------
//  MultilineTextWithDelayComponent
//  
//  Component used to display lines of text within a set boundry, where each
//  characters is typed out with a delay
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Utils;

namespace Psiren.Components.Graphics.Text
{
    public class MultilineTextWithDelayComponent : GraphicsComponent
    {
        /// <summary>
        ///     The font used
        /// </summary>
        public SpriteFont Font
        {
            get { return _font; }
            set
            {
                if (_font == value) { return; }
                _font = value;
                Initialize();
            }
        }
        SpriteFont _font;


        /// <summary>
        ///     The text to display
        /// </summary>
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;

                Initialize();
            }
        }
        string _text;

        /// <summary>
        ///     The amount of time (in seconds) to delay between writing
        ///     out each character
        /// </summary>
        public float CharacterDelay { get => _characterDelay; set => _characterDelay = value; }
        float _characterDelay;

        /// <summary>
        ///     Is this <see cref="MultilineTextWithDelayComponent"/> still typing
        ///     out text.  True if so, false if it is finished
        /// </summary>
        public bool WritingText { get { return _writingText; } }
        private bool _writingText = true;

        /// <summary>
        ///     The maximum width (in pixels) that a line of characters
        ///     can fit within
        /// </summary>
        public int MaxLineWidth
        {
            get => _maxLineWidth;
            set
            {
                _maxLineWidth = value;
                Initialize();
            }
        }
        int _maxLineWidth;

        ///     The text broken down into discrete lines
        private string[] _lines;

        public int LineCount
        {
            get
            {
                if (_lines == null) { return 0; }
                else { return _lines.Length; }
            }
        }

        //  The index of the current line that is being typed out
        private int _lineIndex = 0;

        //  The index of the current character that is being typed out
        private int _charIndex = 0;

        //  Timer used to time typing out the each character
        private float _timer = 0.0f;

        public bool IsCenter = false;

        private float _width;
        private float _height;



        /// <summary>
        ///     Creates a new <see cref="MultilineTextWithDelayComponent"/> instance
        /// </summary>
        /// <param name="font">The font to use</param>
        /// <param name="text">The text to display</param>
        /// <param name="position">The relative position to the parent entities position</param>
        /// <param name="color">The color of the text</param>
        /// <param name="maxLineWidth">The maximum width (in pixels) that each line of text can fit</param>
        /// <param name="characterDelay">The amount of time (in seconds) to delay between typing out each character</param>
        public MultilineTextWithDelayComponent(SpriteFont font, string text, Vector2 position, Vector2 size, Color color, int maxLineWidth, float characterDelay = 0.25f) : base(true)
        {
            _font = font;
            _text = text;
            _width = size.X;
            _height = size.Y;
            Position = position;
            Color = color;
            _characterDelay = characterDelay;
            _maxLineWidth = maxLineWidth;

            //  Split the text into lines
            _lines = Calc.SplitLines(text, font, maxLineWidth);
        }


        /// <summary>
        ///     Initializes the component by splitting the text into discrete lines
        ///     and resetting the indexes
        /// </summary>
        private void Initialize()
        {
            _lines = Calc.SplitLines(_text, _font, _maxLineWidth);
            _lineIndex = 0;
            _charIndex = 0;
            _writingText = true;
            this.Active = true;
        }


        /// <summary>
        ///     Updates the component
        /// </summary>
        public override void Update()
        {
            //  Check if have finished writing text. If we have the set this component
            //  as non active so it doesn't update anymore
            if (!_writingText)
            {
                this.Active = false;
                return;
            }

            _timer += Engine.DeltaTime;
            if (_timer > _characterDelay)
            {
                _timer = 0.0f;
                _charIndex++;
                if (_charIndex >= _lines[_lineIndex].Length)
                {
                    _charIndex = 0;
                    _lineIndex++;
                    if (_lineIndex >= _lines.Length)
                    {

                        _writingText = false;
                    }
                }
            }
        }

        /// <summary>
        ///     Draws this component to the screen
        /// </summary>
        public override void Render()
        {
            if (_characterDelay <= 0.0f)
            {
                for (int i = 0; i < _lines.Length; i++)
                {
                    var text = _lines[i];
                    if (IsCenter)
                    {
                        Vector2 size = _font.MeasureString(text);
                        Vector2 pos = (RenderPosition + new Vector2(0, _font.MeasureString(text).Y * i) + new Vector2(_width * 0.5f, (_height - (_lines.Length * _font.MeasureString(text).Y)) * 0.5f));
                        Vector2 origin = size * 0.5f;
                        Draw.SpriteBatch.DrawString(_font, text, pos, Color * Alpha, Rotation, origin, Scale, Effects, 0.0f);
                    }
                    else
                    {
                        Draw.SpriteBatch.DrawString(_font, text, RenderPosition + new Vector2(0, _font.MeasureString(text).Y * i), Color * Alpha, Rotation, Origin, Scale, Effects, 0.0f);
                    }

                }
            }
            else
            {

                for (int i = 0; i < _lines.Length; i++)
                {
                    if (i > _lineIndex) { continue; }

                    string text = "";
                    if (i == _lineIndex)
                    {
                        text = _lines[i].Substring(0, _charIndex);

                    }
                    else
                    {
                        text = _lines[i];
                    }
                    Draw.SpriteBatch.DrawString(_font, text, RenderPosition + new Vector2(0, _font.MeasureString(text).Y * i), Color * Alpha, Rotation, Origin, Scale, Effects, 0.0f);

                }
            }
        }
    }
}
