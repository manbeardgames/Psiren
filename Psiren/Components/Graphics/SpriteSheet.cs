﻿//--------------------------------------------------------------------------------
//  SpriteSheet
//
//  Component used to animate spritesheets
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Psiren.Graphics;
using System;
using System.Collections.Generic;

namespace Psiren.Components.Graphics
{
    public class SpriteSheet<T> : Image
    {
        public MTexture[] Frames { get; private set; }
        public bool Animating { get; private set; }
        public T CurrentAnimationID { get; private set; }
        public int CurrentAnimationFrame { get; private set; }

        public override float Width
        {
            get
            {
                if (Frames.Length > 0)
                {
                    return Frames[0].Width;
                }
                else
                {
                    return 0;
                }
            }
        }

        public override float Height
        {
            get
            {
                if (Frames.Length > 0)
                {
                    return Frames[0].Height;
                }
                else
                {
                    return 0;
                }
            }
        }



        public int CurrentFrame;
        public float Rate = 1;
        public bool UseRawDeltaTime;
        public Action<T> OnFinish;
        public Action<T> OnLoop;
        public Action<T> OnAnimate;

        private Dictionary<T, Animation> _animations;
        private Animation _currentAnimation;
        private float _animationTimer;
        private bool _played;

        public SpriteSheet(MTexture texture, int frameWidth, int frameHeight, int frameSep = 0)
            : base(texture, true)
        {
            SetFrames(texture, frameWidth, frameHeight, frameSep);
            _animations = new Dictionary<T, Animation>();
        }

        public void SetFrames(MTexture texture, int frameWidth, int frameHeight, int frameSep = 0)
        {
            List<MTexture> frames = new List<MTexture>();
            int x = 0;
            int y = 0;

            while (y <= texture.Height - frameHeight)
            {
                while (x <= texture.Width - frameWidth)
                {
                    frames.Add(texture.GetSubtexture(x, y, frameWidth, frameHeight));
                    x += frameWidth + frameSep;
                }

                y += frameHeight + frameSep;
                x = 0;
            }

            Frames = frames.ToArray();
        }

        public override void Update()
        {
            base.Update();
            {
                if (Animating && _currentAnimation.Delay > 0)
                {
                    //  Timer
                    if (UseRawDeltaTime)
                    {
                        _animationTimer += Engine.RawDeltaTime * Rate;
                    }
                    else
                    {
                        _animationTimer += Engine.DeltaTime * Rate;
                    }

                    //  Next Frame
                    if (Math.Abs(_animationTimer) >= _currentAnimation.Delay)
                    {
                        CurrentAnimationFrame += Math.Sign(_animationTimer);
                        _animationTimer -= Math.Sign(_animationTimer) * _currentAnimation.Delay;

                        //  End of Animation
                        if (CurrentAnimationFrame < 0 || CurrentAnimationFrame >= _currentAnimation.Frames.Length)
                        {
                            //  Looped
                            if (_currentAnimation.Loop)
                            {
                                CurrentAnimationFrame -= Math.Sign(CurrentAnimationFrame) * _currentAnimation.Frames.Length;
                                CurrentFrame = _currentAnimation.Frames[CurrentAnimationFrame];

                                if (OnAnimate != null)
                                {
                                    OnAnimate(CurrentAnimationID);
                                }

                                if (OnLoop != null)
                                {
                                    OnLoop(CurrentAnimationID);
                                }
                            }
                            else
                            {
                                //  Ended
                                if (CurrentAnimationFrame < 0)
                                {
                                    CurrentAnimationFrame = 0;
                                }
                                else
                                {
                                    CurrentAnimationFrame = _currentAnimation.Frames.Length - 1;
                                }

                                Animating = false;
                                _animationTimer = 0;
                                if (OnFinish != null)
                                {
                                    OnFinish(CurrentAnimationID);
                                }
                            }
                        }
                        else
                        {
                            //  Continue Animation
                            CurrentFrame = _currentAnimation.Frames[CurrentAnimationFrame];
                            if (OnAnimate != null)
                            {
                                OnAnimate(CurrentAnimationID);
                            }
                        }
                    }
                }
            }
        }

        public override void Render()
        {
            Texture = Frames[CurrentFrame];
            base.Render();
        }

        #region Animation Definition

        public void Add(T id, bool loop, float delay, params int[] frames)
        {
#if DEBUG
            foreach (var i in frames)
            {
                if (i >= Frames.Length)
                {
                    throw new IndexOutOfRangeException("Specified frames is out of max range for this Spritesheet");
                }
            }
#endif

            _animations[id] = new Animation()
            {
                Delay = delay,
                Frames = frames,
                Loop = loop
            };
        }

        public void Add(T id, float delay, params int[] frames)
        {
            Add(id, true, delay, frames);
        }

        public void Add(T id, int frame)
        {
            Add(id, false, 0, frame);
        }

        public void ClearAnimations()
        {
            _animations.Clear();
        }

        #endregion Animation Definition

        #region Animation Playback

        public bool IsPlaying(T id)
        {
            if (!_played)
            {
                return false;
            }
            else if (CurrentAnimationID == null)
            {
                return id == null;
            }
            else
            {
                return CurrentAnimationID.Equals(id);
            }
        }

        public void Play(T id, bool restart = false)
        {
            if (!IsPlaying(id) || restart)
            {
#if DEBUG
                if (!_animations.ContainsKey(id))
                {
                    throw new Exception("No Animation defined for ID: " + id.ToString());
                }
#endif
                CurrentAnimationID = id;
                _currentAnimation = _animations[id];
                _animationTimer = 0;
                CurrentAnimationFrame = 0;
                _played = true;

                Animating = _currentAnimation.Frames.Length > 1;
                CurrentFrame = _currentAnimation.Frames[0];
            }
        }

        public void Reverse(T id, bool restart = false)
        {
            Play(id, restart);
            if (Rate > 0)
            {
                Rate *= -1;
            }
        }

        public void Stop()
        {
            Animating = false;
            _played = false;
        }

        #endregion Animation Playback

        private struct Animation
        {
            public float Delay;
            public int[] Frames;
            public bool Loop;
        }
    }

}
