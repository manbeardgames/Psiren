﻿using Psiren.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Psiren.Utils;

namespace Psiren.Components.Graphics
{
    public class BasicGridComponent : GraphicsComponent
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int CellWidth { get; set; }
        public int CellHeight { get; set; }
        public int Spacing { get; set; }
        public int TotalWidthInPixels => Width * CellWidth;
        public int TotalHeightInPixels => Height * CellHeight;

        MTexture _cellTexture;

        /// <summary>
        ///     Creates a new <see cref="BasicGridComponent"/>
        /// </summary>
        /// <param name="width">The number of cells across</param>
        /// <param name="height">The number of cells down</param>
        /// <param name="cellWidth">The width of a single cell</param>
        /// <param name="cellHeight">The height of a single cell</param>
        /// <param name="spacing">The amoutn of space (px) between each cell</param>
        public BasicGridComponent(int width, int height, int cellWidth, int cellHeight, int spacing) : base(true)
        {
            this.Width = width;
            this.Height = height;
            this.CellWidth = cellWidth;
            this.CellHeight = cellHeight;
            this.Spacing = spacing;

            _cellTexture = new MTexture(cellWidth, cellHeight, Color.White);
        }

        /// <summary>
        ///     Creates a new <see cref="BasicGridComponent"/> with cells of equal width and heigth
        /// </summary>
        /// <param name="width">The number of cells across</param>
        /// <param name="height">The number of cells down</param>
        /// <param name="cellSizeWidthAndHeight">The width and height of a single cell</param>
        /// <param name="spacing">The amount of space (px) between each cell</param>
        public BasicGridComponent(int width, int height, int cellSizeWidthAndHeight, int spacing)
            :this(width, height, cellSizeWidthAndHeight, cellSizeWidthAndHeight, spacing) { }



        public override void Render()
        {
            for(int row = 0; row < this.Height; row++)
            {
                for (int column = 0; column < this.Width; column++)
                {
                    Draw.Rect(new Rectangle(
                        x: column * CellWidth,
                        y: row * CellHeight,
                        width: CellWidth - Spacing,
                        height: CellHeight - Spacing), this.Color);
                }
            }
        }
    }
}
