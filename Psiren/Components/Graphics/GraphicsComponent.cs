﻿//--------------------------------------------------------------------------------
//  GraphicsComponnet
//
//  Abstract class that all custom components that render to the screen
//  should inherit from
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Psiren.Components.Graphics
{
    public abstract class GraphicsComponent : Component
    {
        /// <summary>
        ///     The xy-coordinate position of the component relative to the parent entity
        /// </summary>
        public Vector2 Position;
        protected Vector2 _position;

        /// <summary>
        ///     The x-coordinate position of the component relative to the parent entity
        /// </summary>
        public float X { get => Position.X; set => Position.X = value; }

        /// <summary>
        ///     The y-coordinate position of the component relative to the parent entity
        /// </summary>
        public float Y { get => Position.Y; set => Position.Y = value; }

        /// <summary>
        ///     The xy-coordinate origin point used when rendering
        /// </summary>
        public Vector2 Origin;

        /// <summary>
        ///     The xy-scale to render at
        /// </summary>
        public Vector2 Scale = Vector2.One;

        /// <summary>
        ///     The rotation to render at
        /// </summary>
        public float Rotation;

        /// <summary>
        ///     The color it is rendered at
        /// </summary>
        public Color Color = Color.White;

        /// <summary>
        ///     The alpha (opacity) to use when rendering
        /// </summary>
        public float Alpha = 1.0f;

        /// <summary>
        ///     SpriteEffects to use when rendering the component
        /// </summary>
        public SpriteEffects Effects = SpriteEffects.None;

        /// <summary>
        ///     Should the component be flipped on the x axis when rendered
        /// </summary>
        public bool FlipX
        {
            get
            {
                return (Effects & SpriteEffects.FlipHorizontally) == SpriteEffects.FlipHorizontally;
            }
            set
            {
                Effects = value ? (Effects | SpriteEffects.FlipHorizontally) : (Effects & ~SpriteEffects.FlipHorizontally);
            }
        }

        /// <summary>
        ///     Shoudl the component be flipped ont he y axis when rendered
        /// </summary>
        public bool FlipY
        {
            get
            {
                return (Effects & SpriteEffects.FlipVertically) == SpriteEffects.FlipVertically;
            }
            set
            {
                Effects = value ? (Effects | SpriteEffects.FlipVertically) : (Effects & ~SpriteEffects.FlipVertically);
            }
        }

        /// <summary>
        ///     The actual render position of the component. (Entity.Position + this.Position)
        /// </summary>
        public Vector2 RenderPosition
        {
            get
            {
                return (Entity == null ? Vector2.Zero : Entity.Position) + Position;
            }
            set
            {
                Position = value - (Entity == null ? Vector2.Zero : Entity.Position);
            }
        }


        /// <summary>
        ///     Creates a new <see cref="GraphicsComponent"/> instance
        /// </summary>
        /// <param name="active"></param>
        public GraphicsComponent(bool active) : base(active, true) { }

        /// <summary>
        ///     Draws an outline of this component with an xy-coordinate offset
        /// </summary>
        /// <param name="offset"></param>
        public void DrawOutline(int offset = 1)
        {
            DrawOutline(Color.Black, offset);
        }

        /// <summary>
        ///     Draws an outline of this component at the color specified with an xy-coordinate offset
        /// </summary>
        /// <param name="color"></param>
        /// <param name="offset"></param>
        public void DrawOutline(Color color, int offset = 1)
        {
            Vector2 pos = Position;
            Color was = Color;
            Color = color;

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i != 0 || j != 0)
                    {
                        Position = pos + new Vector2(i * offset, j * offset);
                        Render();
                    }
                }
            }

            Position = pos;
            Color = was;
        }
    }
}
