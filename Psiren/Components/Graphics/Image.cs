﻿//--------------------------------------------------------------------------------
//  Image
//
//  Component used to draw a texture
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Psiren.Graphics;

namespace Psiren.Components.Graphics
{
    public class Image : GraphicsComponent
    {
        /// <summary>
        ///     The <see cref="MTexture"/> texture used for this <see cref="Image"/>
        /// </summary>
        public MTexture Texture
        {
            get { return _texture; }
            set { _texture = value; }
        }
        private MTexture _texture;

        /// <summary>
        ///     The width of the <see cref="Image"/>
        /// </summary>
        public virtual float Width => Texture.Width;

        /// <summary>
        ///     The height of this <see cref="Image"/>
        /// </summary>
        public virtual float Height => Texture.Height;

        /// <summary>
        ///     The width and height of the image
        /// </summary>
        public virtual Vector2 Size => new Vector2(Width, Height);


        protected float _layerDepth = 0.0f;
        public float LayerDepth { get => _layerDepth; set => _layerDepth = value; }

        public Rectangle ClipRect { get; set; }


        /// <summary>
        ///     Createa a new <see cref="Image"/> instance
        /// </summary>
        /// <param name="texture">The <see cref="MTexture"/> texture used</param>
        public Image(MTexture texture) : base(false) { Texture = texture; ClipRect = new Rectangle(0, 0, texture.Width, texture.Height); }

        /// <summary>
        ///     (internal use only) Creates a new <see cref="Image"/> instance
        /// </summary>
        /// <param name="texture">The <see cref="MTexture"/> texture used</param>
        /// <param name="active">Is the component active</param>
        internal Image(MTexture texture, bool active) : base(active) { Texture = texture; }

        public override void Render()
        {
            if (Texture != null)
            {
                //Texture.Draw(RenderPosition, Origin, Color * Alpha, Scale, Rotation, Effects, LayerDepth);
                Texture.Draw(RenderPosition, Origin, Color * Alpha, Scale, Rotation, Effects);
            }
        }

        /// <summary>
        ///     Sets the xy-coordinate origin point used for rendering
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Image SetOrigin(float x, float y)
        {
            Origin.X = x;
            Origin.Y = y;
            return this;
        }

        /// <summary>
        ///     Sets the xy-coordinate orgin point to the center of the image
        /// </summary>
        /// <returns></returns>
        public Image CenterOrigin()
        {
            Origin.X = Width / 2.0f;
            Origin.Y = Height / 2.0f;
            return this;
        }

        /// <summary>
        ///     Justifies the xy-coordinate origin point at the value given
        /// </summary>
        /// <param name="at"></param>
        /// <returns></returns>
        public Image JustifyOrigin(Vector2 at)
        {
            Origin.X = Width * at.X;
            Origin.Y = Height * at.Y;
            return this;
        }

        /// <summary>
        ///     Justifies the xy-coordinate origin point at the values given
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Image JustifyOrigin(float x, float y)
        {
            Origin.X = Width * x;
            Origin.Y = Height * y;
            return this;
        }
    }
}
