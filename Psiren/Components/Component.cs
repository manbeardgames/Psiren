﻿//--------------------------------------------------------------------------------
//  Component
//
//  The base component class that all components must inherit from  
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------


using Psiren.Entities;
using Psiren.Scenes;
using Psiren.Utils;

namespace Psiren.Components
{
    public class Component
    {
        /// <summary>
        ///     The <see cref="Entity"/> this <see cref="Component"/> is attached to
        /// </summary>
        public Entity Entity { get; private set; }

        /// <summary>
        ///     Is this active (updatable)
        /// </summary>
        public bool Active;

        /// <summary>
        ///     Is this visible (drawable)
        /// </summary>
        public bool Visible;

        /// <summary>
        ///     Creates a new <see cref="Component"/> instance
        /// </summary>
        /// <param name="active">Is this active (updatable)</param>
        /// <param name="visible">Is this visible (drawable)</param>
        public Component(bool active, bool visible)
        {
            Active = active;
            Visible = visible;
        }

        public virtual void Added(Entity entity)
        {
            Entity = entity;
            if (Scene != null)
            {
                Scene.Tracker.ComponentAdded(this);
            }
        }

        public virtual void Removed(Entity entity)
        {
            if (Scene != null)
                Scene.Tracker.ComponentRemoved(this);
            Entity = null;
        }

        public virtual void EntityAdded(Scene scene)
        {
            scene.Tracker.ComponentAdded(this);
        }

        public virtual void EntityRemoved(Scene scene)
        {
            scene.Tracker.ComponentRemoved(this);
        }

        public virtual void SceneEnd(Scene scene) { }

        public virtual void EntityAwake() { }
        public virtual void Update() { }
        public virtual void Render() { }
        public virtual void DebugRender(Camera camera) { }
        public virtual void HandleGraphicsReset() { }
        public virtual void HandleGraphicsCreate() { }

        /// <summary>
        ///     Removes this <see cref="Component"/> from the <see cref="Entity"/> it is attached to
        /// </summary>
        public void RemoveSelf()
        {
            if (Entity != null)
                Entity.Remove(this);
        }

        /// <summary>
        ///     Gets the <see cref="Scene"/> as type T that the <see cref="Entity"/> this is attached too
        ///     is in
        /// </summary>
        /// <typeparam name="T">The derived <see cref="Scene"/> type</typeparam>
        /// <returns></returns>
        public T SceneAs<T>() where T : Scene
        {
            return Scene as T;
        }

        /// <summary>
        ///     Gets the <see cref="Entity"/> as type T that this is attached too
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T EntityAs<T>() where T : Entity
        {
            return Entity as T;
        }

        /// <summary>
        ///     Returns the <see cref="Scene"/> that the <see cref="Entity"/> this is attached
        ///     to is in
        /// </summary>
        public Scene Scene
        {
            get { return Entity != null ? Entity.Scene : null; }
        }
    }
}
