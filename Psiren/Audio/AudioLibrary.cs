﻿//--------------------------------------------------------------------------------
//  AudioLibrary
//  The AudioLibrary is the library of your audio.  It retains an AudioList
//  which is the store of the AudioDefinitions, and provides utiltiy methods
//  to add and remove SongDefinition and SoundEffectDefinitions, as well as
//  playing them.
//--------------------------------------------------------------------------------
//
//                              License
//  
//    Copyright(c) 2018 Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Psiren.Audio
{
    public class AudioLibrary<T> : IEnumerable<AudioDefinition<T>>, IEnumerable
    {
        /// <summary>
        ///     The master audio list
        /// </summary>
        internal AudioList<T> _audio;






        //-------------------------------------------------------
        //
        //  SongDefinition Stuff
        //
        //-------------------------------------------------------

        /// <summary>
        ///     The current <see cref="SongDefinition"/> that is being played
        /// </summary>
        public SongDefinition<T> CurrentSong { get; private set; }

        /// <summary>
        ///     The volume of songs (0.0 to 1.0)
        /// </summary>
        public float SongVolume
        {
            get { return this._songVolume; }
            set
            {
                if (this._songVolume == value) return;
                this._songVolume = Clamp01(value);
                MediaPlayer.Volume = this._songVolume;
            }
        }
        private float _songVolume;



        //-------------------------------------------------------
        //
        //  SoundEffectDefinition stuff
        //
        //-------------------------------------------------------

        /// <summary>
        ///     The current <see cref="SoundEffectDefinition"/> that is being played
        /// </summary>
        public SoundDefinition<T> CurrentSoundEffect { get; private set; }

        /// <summary>
        ///     The volume of sound effects (0.0 to 1.0)
        /// </summary>
        public float SoundEffectVolume
        {
            get { return this._soundEffectVolume; }
            set
            {
                if (this._soundEffectVolume == value) return;
                this._soundEffectVolume = value;
                SoundEffect.MasterVolume = value;
            }
        }
        private float _soundEffectVolume;


        //-------------------------------------------------------
        //
        //  Transitioning SongDefinition stuff
        //
        //-------------------------------------------------------

        //  Tracks the song transition routines
        private Stack<IEnumerator> _transitionRoutineStack;

        //  The sound to transition too
        private SongDefinition<T> _transitionToSong;

        //  The amount of time to take to transition a song back in
        private float _transitionInTimer = 0.0f;

        //  The amount of time to take to transition a song out
        private float _transitionOutTimer = 0.0f;

        //  The volume during transition
        private float _transitionVolume = 1.0f;

        //  Should the play back position be restored for the song transitioned to
        private bool _restorePlayPosition;

        //  Is a transition currently taking place
        public bool _isTransitioning;

        //  Time passed since last update
        private float _deltaTime;





        //-------------------------------------------------------
        //
        //  Constructors
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Creates a new instance
        /// </summary>
        public AudioLibrary()
        {
            this._audio = new AudioList<T>(this);
            this._transitionRoutineStack = new Stack<IEnumerator>();
            SongVolume = 1.0f;

        }





        //-------------------------------------------------------
        //
        //  Updating
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Update this using the MonoGame GameTime
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            //  Convert gametime to delta time (ms)
            var dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Update(dt);

        }

        /// <summary>
        ///     Update this using converted delta time
        /// </summary>
        /// <param name="dt"></param>
        public void Update(float dt)
        {
            this._deltaTime = dt;

            //  Cache the play position of the current song
            if (CurrentSong != null)
            {
                CurrentSong.PlayPosition = MediaPlayer.PlayPosition;
            }

            //  Check for audio transition fade-out in stuff
            if (this._isTransitioning)
            {
                if (this._transitionRoutineStack.Count > 0)
                {
                    IEnumerator now = this._transitionRoutineStack.Peek();
                    if (now.MoveNext())
                    {
                        if (now.Current is IEnumerator)
                        {
                            this._transitionRoutineStack.Push(now.Current as IEnumerator);
                        }
                    }
                }
            }
        }




        //-------------------------------------------------------
        //
        //  Transition Routines
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Routine that will fade out the volume and swap songs
        /// </summary>
        /// <returns></returns>
        private IEnumerator TransitionBegin()
        {
            float timer = 0.0f;
            float duration = this._transitionOutTimer;

            float from = this._songVolume;
            float to = 0.0f;
            while (timer < duration)
            {
                timer += this._deltaTime;
                MediaPlayer.Volume = Lerp(this._songVolume, to, timer / duration);
                yield return null;
            }

            //  Set the current song to the one we are transitioning to
            CurrentSong = this._transitionToSong;

            //  null the cache
            this._transitionToSong = null;

            //  Play the song. Playposition is set only if _restorePlayPositon is true
            MediaPlayer.Play(CurrentSong.Song, this._restorePlayPosition ? CurrentSong.PlayPosition : TimeSpan.FromTicks(0));

            //  Set the repeating property
            MediaPlayer.IsRepeating = CurrentSong.IsRepeating;

            //  Clear out the stack
            this._transitionRoutineStack.Clear();

            //  Push the transition end to the stack
            this._transitionRoutineStack.Push(TransitionEnd());
        }

        /// <summary>
        ///     Routine that will bring the sound volume back up to configured levels
        ///     after the initial transition
        /// </summary>
        /// <returns></returns>
        private IEnumerator TransitionEnd()
        {
            float timer = 0.0f;
            float duration = this._transitionInTimer;

            float from = 0.0f;
            float to = this._songVolume;
            while (timer < duration)
            {
                timer += this._deltaTime;
                MediaPlayer.Volume = Lerp(from, to, timer / duration);
                yield return null;
            }

            //  Transition of song is completed
            this._isTransitioning = false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="transitionOut"></param>
        /// <param name="transitionIn"></param>
        /// <param name="restorePlayPosition"></param>
        public void PlaySong(T id, float transitionOut = 0.0f, float transitionIn = 0.0f, bool restorePlayPosition = false)
        {
            if (this._audio.Get<SongDefinition<T>>(id) is SongDefinition<T> song)
            {
                //  cache the song as the transition to song
                this._transitionToSong = song;

                //  cache the transition out time
                this._transitionOutTimer = transitionOut;

                //  cache the transition in time
                this._transitionInTimer = transitionIn;

                //  cache the current song volume level
                this._transitionVolume = this._songVolume;

                //  cache the restor play position preference
                this._restorePlayPosition = restorePlayPosition;

                //  clear the transition statck
                this._transitionRoutineStack.Clear();

                //  set that we are now in transition
                this._isTransitioning = true;

                //  push the transition to the stack
                this._transitionRoutineStack.Push(TransitionBegin());
            }
        }


        /// <summary>
        ///     Plays the sound effect with the given id once
        /// </summary>
        /// <param name="id">The id value fo the sound effect to play</param>
        /// <param name="pitch">Pitch adjustment, ranging from -1.0 (down an octave) to 0.0 (no change) to 1.0 (up an octave).</param>
        /// <param name="pan">Panning, ranging from -1.0 (left speaker) to 0.0 (centered), 1.0 (right speaker).</param>
        /// <returns>
        ///     True if a SoundEffectInstance was successfully created and played, false if not.
        /// </returns>
        public bool PlaySoundOneShot(T id, float pitch = 0.0f, float pan = 0.0f)
        {
            var sound = this._audio.Get<SoundDefinition<T>>(id);
            if (sound != null)
            {
                return sound.SoundEffect.Play(SoundEffectVolume, pitch, pan);
            }
            return false;
        }






        //-------------------------------------------------------
        //
        //  Adding Audio
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Adds the given <see cref="AudioDefinition{T}"/> of type T to this library
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition{T}"/> to add/typeparam>
        /// <param name="definition">The audio to add</param>
        /// <returns></returns>
        public X Add<X>(X definition) where X : AudioDefinition<T>
        {
            this._audio.Add(definition);
            return definition as X;
        }

        /// <summary>
        ///     Adds the given collection of <see cref="AudioDefinition{T}"/> to this library
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition"/> to add</typeparam>
        /// <param name="definitions">The collection to of audio to add</param>
        public void Add(IEnumerable<AudioDefinition<T>> definitions)
        {
            this._audio.Add(definitions);
        }

        /// <summary>
        ///     Adds the given <see cref="AudioDefinition{T}"/> to this library
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition{T}"/> to add</typeparam>
        /// <param name="definitions">The audio to add</param>
        public void Add(params AudioDefinition<T>[] definitions)
        {
            this._audio.Add(definitions);
        }

        /// <summary>
        ///     Adds a new Song to the library
        /// </summary>
        /// <param name="id">The id of the song to add</param>
        /// <param name="song">The song to add</param>
        /// <param name="isRepeating">Does the song repeat</param>
        /// <returns></returns>
        public SongDefinition<T> Add(T id, Song song, bool isRepeating = false)
        {
            SongDefinition<T> definition = new SongDefinition<T>(id, song, isRepeating);
            return Add<SongDefinition<T>>(definition);
        }

        /// <summary>
        ///     Adds a new SoundEffect to the library
        /// </summary>
        /// <param name="id">The id of the sound effect to add</param>
        /// <param name="sound">The sound effect to add</param>
        /// <param name="isRepeating">Does the sound effect repeat</param>
        /// <returns></returns>
        public SoundDefinition<T> Add(T id, SoundEffect sound, bool isRepeating = false)
        {
            SoundDefinition<T> definition = new SoundDefinition<T>(id, sound, isRepeating);
            return Add<SoundDefinition<T>>(definition);
        }





        //-------------------------------------------------------
        //
        //  Removing Audio
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Removes the given <see cref="AudioDefinition{T}"/> from this library
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition{T}"/></typeparam>
        /// <param name="definition">The audio to remove</param>
        /// <returns></returns>
        public X Remove<X>(X definition) where X : AudioDefinition<T>
        {
            this._audio.Remove(definition);
            return definition as X;
        }

        /// <summary>
        ///     Removes the given collection of <see cref="AudioDefinition{T}"/> from this library
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition{T}"/></typeparam>
        /// <param name="definitions">The collection of audio to remove</param>
        public void Remove(IEnumerable<AudioDefinition<T>> definitions)
        {
            this._audio.Remove(definitions);
        }


        /// <summary>
        ///     Removes the given <see cref="AudioDefinition{T}"/> from this library
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition{T}"/></typeparam>
        /// <param name="definitions">The audio to remove</param>
        public void Remove(params AudioDefinition<T>[] definitions)
        {
            this._audio.Remove(definitions);
        }



        //-------------------------------------------------------
        //
        //  Retreiving Audio
        //
        //-------------------------------------------------------
        /// <summary>
        ///     Retreives the <see cref="AudioDefinition{T}"/> with the given id
        /// </summary>
        /// <param name="id">The id of the definition to retreive</param>
        /// <returns></returns>
        public AudioDefinition<T> this[T id]
        {
            get
            {
                return Get<AudioDefinition<T>>(id);
            }
        }

        /// <summary>
        ///     Retreives the <see cref="AudioDefinition{T}"/> with the given id
        /// </summary>
        /// <typeparam name="X">Either a <see cref="SoundDefinition{T}"/> or <see cref="SongDefinition{T}"/></typeparam>
        /// <param name="id">The id of the definition</param>
        /// <returns></returns>
        public X Get<X>(T id) where X : AudioDefinition<T>
        {
            return this._audio.Get<X>(id);

        }



        //-------------------------------------------------------
        //
        //  Collection Conversion Utilities
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Returns this as an array of <see cref="AudioDefinition{T}"/> values
        /// </summary>
        /// <returns></returns>
        public AudioDefinition<T>[] ToArray()
        {
            return this._audio.ToArray();
        }



        //-------------------------------------------------------
        //
        //  IEnumerable Interface Implementation
        //
        //-------------------------------------------------------
        public IEnumerator<AudioDefinition<T>> GetEnumerator()
        {
            return this._audio.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //-------------------------------------------------------
        //
        //  Math helpers
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Clamps the given float between 0.0 and 1.0
        /// </summary>
        /// <param name="value">The value to clamp</param>
        /// <returns></returns>
        private float Clamp01(float value)
        {
            if ((double)value < 0.0)
            {
                return 0.0f;
            }
            else if ((double)value > 1.0f)
            {
                return 1.0f;
            }
            return value;
        }


        /// <summary>
        ///     Lerp the between the given float values
        /// </summary>
        /// <param name="from">The starting value</param>
        /// <param name="to">The ending value</param>
        /// <param name="t">The distance of the interpolation between 0.0 and 1.0</param>
        /// <returns></returns>
        private float Lerp(float from, float to, float t)
        {
            return from + (to - from) * Clamp01(t);
        }

    }






}
