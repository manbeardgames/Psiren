# Description
The AudioLibrary and all of it's underlying components are designed to work with how MonoGame plays music and sound effects.  It has a AudioList which stores definitions for all added songs and sound effects to the library.  Playback of songs and soundeffects are done through the library itself.  

For songs, you can either call the ```PlaySong(T id)``` method to immedialty start playback of the song provided, or you can use the ```PlaySong(T id, float transitionOut = 0.0f, float transitionIn = 0.0f, bool restorePlayPosition = false)``` method to allow transitioning/fading out of the current song into the new song.  

For sound effects, currently there is only the ```PlaySoundOneShot(T id, float pitch = 0.0f, float pan = 0.0f)``` method for playing sound effects. I plan to add more extensive functionality for sound effects for repeating them, and managing SoundEffectInstances

# Current Limitations
MonoGame uses the static MediaPlayer class to play Songs (music).  This is limited to only being able to play one song at a time. Due to this, there is no way to do a crossfade between audio tracks.  Keeping this in mind, there is a way built into this AudioLibrary class to allow fade transitions between songs.  

As of right now, playing a SoundEffect from this AudioLibrary, there is only the ```PlaySoundOneShot``` method.  Currently using this, there is no way of setting up repeating or looping sound effects.  I will be adding to this later to allow these things manged through SoundEffectInstances in MonoGame.

# How To Use
Everything with the AudioLibrary takes a generic type paramater.  This allows you to give ID tags to the song and sound effect definitions any way you like, be it enums, int, string, etc.  *For the example code I'm writing below, I'll be doing it use enums for the id tags*, however keep in mind, it's a generic ```<T>``` so you can use what suits you best. 

1. First, create a new AudioLibrary<T> object.

```csharp
//  Add a using statement for Psiren.Audio
using Psiren.Audio;

//  I'm using enums for id tag references in this example
public enum AudioTags
{
    Song_Example_One,
    Song_Example_Two,
    SoundEffect_Example_One
    
}


//  Add an AudioLibrary field. This example is using the AudioTags
//  enum from above as the <T> parameter
public AudioLibrary<AudioTags> Library;



//  In your constructor, or wherever you do your initializations
//  instantiate the AudioLibrary
public MyConstructor()
{
    Library = new AudioLibrary<AudioTags>();
}

//  In your LoadContent method or wherever you are loading content in
//  add the songs and sound effects to the library
public void LoadContent()
{
    //  Add songs
    Library.Add(AudioTag.Song_Example_One, Content.Load<Song>("songs/song-example-one", true);
    Library.Add(AudioTag.Song_Example_Two, Content.Load<Song>("songs/song-example-two", true);
    
    //  Add sound effects the same way
    Library.Add(AudioTag.SoundEffect_Example_One, Content.Load<SoundEffect>("sounds/sondeffect-example-one");
}


//  In your Update method, call the update method for the AudioLibrary
public void Update(GameTime gameTime)
{
    //  Pass the gameTime to the update.  There is also a Update(float dt) method
    //  if you want to pass the dt in as a float instead of the full GameTime object.
    Library.Update(gameTime);
}
```


The above show you how to set it up.  When you want to play a song, you can call  

```csharp
//  This will immediatly start playback of the song
Library.PlaySong(AudioTag.Song_Example_One);
```

or

```csharp
//  This will transition to the song given. The two floats describe
//  the amount of time to take to transition out of the current song
//  and then the amount of time to take transitioning into the new song.
//  The final paremeter tells it if the playback position should be restored
//  for the new song being played.
Library.PlaySong(AudioTag.Song_Example_Two, 3.0f, 3.0f, true);
```

And to play a sound effect  

```csharp
//  This will play the sound effect with no adjustments to pitch or pan
Library.PlaySoundOneShot(AudioTag.SoundEffect_Example_One);
```

or  

```csharp
//  This will play the sound effect with adjustments to pitch and pan.
//  The first float paramter is how much to adjust the pitch
//  The second float paramter is how much to adjust the pan
Library.PlaySoundOneShot(AudioTag.SoundEffect_Example_One, 0.5f, -1.0f);
```

