﻿//--------------------------------------------------------------------------------
//  AudioList
//  The AudioList class acts as collection and collection manager of AudioDefinitions
//--------------------------------------------------------------------------------
//
//                              License
//  
//    Copyright(c) 2018 Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:
//
//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.
//
//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Psiren.Audio
{
    public class AudioList<T> : IEnumerable<AudioDefinition<T>>, IEnumerable
    {
        /// <summary>
        ///     The <see cref="AudioLibrary"/> which this belongs to
        /// </summary>
        public AudioLibrary<T> AudioLibrary { get; internal set; }

        //---------------------------------------------------------
        //  Audio Lists
        //---------------------------------------------------------
        
        //  Collection of all SongDefinitions
        private Dictionary<T, SongDefinition<T>> _songs;

        //  Collection of all SoundDefinitions
        private Dictionary<T, SoundDefinition<T>> _sounds;


        //---------------------------------------------------------
        //  Audio Hash Sets
        //---------------------------------------------------------

        //  Quick lookup referencing for all SongDefinitions
        private HashSet<SongDefinition<T>> _currentSongs;

        //  Quick lookup referencing for all SoundDefinitions
        private HashSet<SoundDefinition<T>> _currentSounds;





        //-------------------------------------------------------
        //
        //  Constructors
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Creates a new instance
        /// </summary>
        /// <param name="library">The <see cref="AudioLibrary{T}"/> this list belongs to</param>
        internal AudioList(AudioLibrary<T> library)
        {
            AudioLibrary = library;

            this._songs = new Dictionary<T, SongDefinition<T>>();
            this._sounds = new Dictionary<T, SoundDefinition<T>>();
            this._currentSongs = new HashSet<SongDefinition<T>>();
            this._currentSounds = new HashSet<SoundDefinition<T>>();
        }





        //-------------------------------------------------------
        //
        //  Adding AudioDefinitions
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Adds the given <see cref="AudioDefinition{T}"/> to the collection
        /// </summary>
        /// <param name="definition">The audio to add</param>
        public void Add<X>(X definition) where X : AudioDefinition<T>
        {

            //  Get the type of x
            Type type = typeof(X);

            if (type == typeof(SongDefinition<T>))
            {
                if (!this._currentSongs.Contains(definition as SongDefinition<T>))
                {
                    this._songs.Add(definition.ID, definition as SongDefinition<T>);
                    this._currentSongs.Add(definition as SongDefinition<T>);
                }
            }
            else if (type == typeof(SoundDefinition<T>))
            {
                if (!this._currentSounds.Contains(definition as SoundDefinition<T>))
                {
                    this._sounds.Add(definition.ID, definition as SoundDefinition<T>);
                    this._currentSounds.Add(definition as SoundDefinition<T>);
                }
            }
        }

        /// <summary>
        ///     Adds a new song to the collection
        /// </summary>
        /// <param name="id">The id of the song</param>
        /// <param name="song">The Song to add</param>
        /// <param name="isRepeating">Should this song loop when played</param>
        public void Add(T id, Song song, bool isRepeating = false)
        {
            SongDefinition<T> definition = new SongDefinition<T>(id, song, isRepeating);
            Add(definition);
        }

        /// <summary>
        ///     Adds a new sound effect to the collection
        /// </summary>
        /// <param name="id">The id of the sound effect</param>
        /// <param name="sound">The sound effect</param>
        /// <param name="isRepeating">Should this sound effect repeat</param>
        public void Add(T id, SoundEffect sound, bool isRepeating = false)
        {
            SoundDefinition<T> definition = new SoundDefinition<T>(id, sound, isRepeating);
            Add(definition);
        }

        /// <summary>
        ///     Adds the given <see cref="AudioDefinition{T}"/> collection to the collection
        /// </summary>
        /// <param name="definitions">The collection of audio to add</param>
        public void Add(IEnumerable<AudioDefinition<T>> definitions)
        {
            foreach (var definition in definitions)
            {
                Add(definition);
            }
        }

        /// <summary>
        ///     Adds the given <see cref="AudioDefinition{T}"/> to the collection
        /// </summary>
        /// <param name="definitions">The audio to add</param>
        public void Add(params AudioDefinition<T>[] definitions)
        {
            foreach (var definition in definitions)
            {
                Add(definition);
            }
        }





        //-------------------------------------------------------
        //
        //  Removing AudioDefinitions
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Removes the given <see cref="AudioDefinition{T}"/> from the colleciton
        /// </summary>
        /// <param name="definition"></param>
        public void Remove<X>(X definition) where X : AudioDefinition<T>
        {
            //  Get the type of X
            Type type = typeof(X);

            if (type == typeof(SongDefinition<T>))
            {
                if (this._currentSongs.Contains(definition as SongDefinition<T>))
                {
                    this._songs.Remove(definition.ID);
                    this._currentSongs.Remove(definition as SongDefinition<T>);
                }
            }
            else if (type == typeof(SoundDefinition<T>))
            {
                if (this._currentSounds.Contains(definition as SoundDefinition<T>))
                {
                    this._sounds.Remove(definition.ID);
                    this._currentSounds.Remove(definition as SoundDefinition<T>);
                }
            }
        }

        /// <summary>
        ///     Removes the given <see cref="AudioDefinition{T}"/> collection from the collection
        /// </summary>
        /// <param name="definitions">The collection of audio to remove</param>
        public void Remove<X>(IEnumerable<X> definitions) where X : AudioDefinition<T>
        {
            foreach (var definition in definitions)
            {
                Remove(definition);
            }
        }

        /// <summary>
        ///     Removes the given <see cref="AudioDefinition{T}"/> from the collection
        /// </summary>
        /// <param name="definitions">The audio to remove</param>
        public void Remove<X>(params X[] definitions) where X : AudioDefinition<T>
        {
            foreach (var definition in definitions)
            {
                Remove(definition);
            }
        }

        /// <summary>
        ///     Removes all <see cref="AudioDefinition{T}"/> of the given type from this collection
        /// </summary>
        /// <typeparam name="T">The type of audio, either <see cref="AudioDefinition"/>, <see cref="SongDefinition"/>, or <see cref="SoundEffectDefinition"/></typeparam>
        public void RemoveAll<X>() where X : AudioDefinition<T>
        {
            Remove<X>(GetAll<X>());
        }





        //-------------------------------------------------------
        //
        //  Get AudioDefinition Utilities
        //
        //-------------------------------------------------------

        /// <summary>
        ///     Gets the <see cref="AudioDefinition{T}"/> of type T with the given name
        /// </summary>
        /// <typeparam name="T">The dervied type of <see cref="AudioDefinition{T}"/></typeparam>
        /// <param name="id">The name of the audio to retrieve</param>
        /// <returns>
        ///     If a matching name is found, the <see cref="AudioDefinition{T}"/> as type T
        ///     is returned, otherwise null is returned
        /// </returns>
        public X Get<X>(T id) where X : AudioDefinition<T>
        {

            Type type = typeof(X);

            if (type == typeof(SongDefinition<T>))
            {
                if (this._songs.ContainsKey(id))
                {
                    return this._songs[id] as X;
                }
            }
            else if (type == typeof(SoundDefinition<T>))
            {
                if (this._sounds.ContainsKey(id))
                {
                    return this._sounds[id] as X;
                }
            }
            return null;
        }

        /// <summary>
        ///     Gets all <see cref="AudioDefinition{T}"/> of type T
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="AudioDefinition{T}"/> to get</typeparam>
        /// <returns></returns>
        public IEnumerable<X> GetAll<X>() where X : AudioDefinition<T>
        {
            //  Get the type of X
            Type type = typeof(X);
            if (type == typeof(SongDefinition<T>))
            {
                foreach (var song in this._songs)
                {
                    yield return song as X;
                }
            }
            else if (type == typeof(SoundDefinition<T>))
            {
                foreach (var sound in this._sounds)
                {
                    yield return sound as X;
                }
            }
            else
            {
                yield return null;
            }
        }





        //-------------------------------------------------------
        //
        //  IEnumerable Interface Implementation
        //
        //-------------------------------------------------------
        public IEnumerator<AudioDefinition<T>> GetEnumerator()
        {
            //return this._audio.GetEnumerator();
            List<AudioDefinition<T>> definitions = new List<AudioDefinition<T>>();
            definitions.AddRange(this._currentSongs.ToList<AudioDefinition<T>>());
            definitions.AddRange(this._currentSounds.ToList<AudioDefinition<T>>());
            return definitions.GetEnumerator();
            //return this._audio.Values.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

    }
}
