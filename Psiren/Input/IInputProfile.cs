﻿//--------------------------------------------------------------------------------
//  IINputProfile
//
//  Interface definition for input profiles
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

//  NOTE ABOUT MAPPING:
//      This is desined so that input mapings can translate to a standardized controller
//      input.  So the naming convention below is assumed to translate to the following
//      controller mappings for an xbox controller and playstation controller.  
//
//      |____IInputProfile_Name___|______XBOX_CONTROLLER______|____PLAYSTATION_CONTROLLER____|
//      |       Action1           |     A Button              |         X Button             |
//      |_________________________|___________________________|______________________________|
//      |       Action2           |     B Button              |         O Button             |
//      |_________________________|___________________________|______________________________|
//      |       Action3           |     X Button              |         Square Button        |
//      |_________________________|___________________________|______________________________|
//      |       Action4           |     Y Button              |         Triange Button       |
//      |_________________________|___________________________|______________________________|
//      |       DpadUp            |     DpadUp                |         DpadUp               |
//      |_________________________|___________________________|______________________________|
//      |       DpadDown          |     DpadDown              |         DpadDown             |
//      |_________________________|___________________________|______________________________|
//      |       DpadLeft          |     DpadLeft              |         DpadLeft             |
//      |_________________________|___________________________|______________________________|
//      |       DpadRight         |     DpadRight             |         DpadRight            |
//      |_________________________|___________________________|______________________________|
//      |       LeftStick         |     L3                    |         L3                   |
//      |_________________________|___________________________|______________________________|
//      |       LeftStickX        |     Left Stick X Axis     |         Left Stick X Axis    |
//      |_________________________|___________________________|______________________________|
//      |       LeftStickY        |     Left Stick Y Axis     |         Left Stick Y Axis    |
//      |_________________________|___________________________|______________________________|
//      |       RightStick        |     R3                    |         R3                   |
//      |_________________________|___________________________|______________________________|
//      |       RightStickX       |     Right Stick X Axis    |         Right Stick X Axis   |
//      |_________________________|___________________________|______________________________|
//      |       RightStickY       |     Right Stick Y Axis    |         Right Stick Y Axis   |
//      |_________________________|___________________________|______________________________|
//      |       LeftBumper        |     Left Bumper           |         L1                   |
//      |_________________________|___________________________|______________________________|
//      |       RightBumper       |     Right Bumper          |         R1                   |
//      |_________________________|___________________________|______________________________|
//      |       LeftTrigger       |     Left Trigger          |         L2                   |
//      |_________________________|___________________________|______________________________|
//      |       RightTrigger      |     Right Trigger         |         R2                   |
//      |_________________________|___________________________|______________________________|

//
//      THE MENU VIRTUALBUTTONS ARE HARDWIRED FOR USE IN THE MENU ENTITY.  BE SURE THOSE
//      ARE SET IN ORDER TO USE THE MENU ENTITY APPROPRIATLY.


namespace Psiren.Input
{
    public interface IInputProfile
    {
        VirtualButton Action1 { get; set; }
        VirtualButton Action2 { get; set; }
        VirtualButton Action3 { get; set; }
        VirtualButton Action4 { get; set; }
        VirtualButton DpadUp { get; set; }
        VirtualButton DpadDown { get; set; }
        VirtualButton DpadLeft { get; set; }
        VirtualButton DpadRight { get; set; }
        VirtualButton LeftStick { get; set; }
        //VirtualIntegerAxis LeftStickX { get; set; }
        VirtualIntegerAxis LeftStickY { get; set; }
        VirtualJoystick LeftStickX { get; set; }
        VirtualButton RightStick { get; set; }
        VirtualIntegerAxis RightStickX { get; set; }
        VirtualIntegerAxis RightStickY { get; set; }
        VirtualButton CommandStart { get; set; }
        VirtualButton CommandSelect { get; set; }
        VirtualButton LeftTrigger { get; set; }
        VirtualButton RightTrigger { get; set; }
        VirtualButton LeftBumper { get; set; }
        VirtualButton RightBumper { get; set; }

        VirtualButton MenuLeft { get; set; }
        VirtualButton MenuRight { get; set; }
        VirtualButton MenuUp { get; set; }
        VirtualButton MenuDown { get; set; }
        VirtualButton MenuConfirm { get; set; }
        VirtualButton MenuCancel { get; set; }
        VirtualButton MenuEsc { get; set; }

        void Initialize();
        void Deregister();


    }
}
