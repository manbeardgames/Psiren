﻿//--------------------------------------------------------------------------------
//  MInput
//
//  Interface definition for input profiles
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System;
using System.Collections.Generic;

namespace Psiren.Input
{
    public static class MInput
    {
        /// <summary>
        ///     Use to get Keyboard information
        /// </summary>
        public static KeyboardData Keyboard { get; private set; }

        /// <summary>
        ///     Use to get mouse informaiton
        /// </summary>
        public static MouseData Mouse { get; private set; }

        /// <summary>
        ///     Used to get Gamepad information
        /// </summary>
        public static GamePadData[] GamePads { get; private set; }

        /// <summary>
        ///     Used to get Touch informaiton
        /// </summary>
        public static Touch Touches { get; private set; }


        //  VirtualInputs that have been created are added to this list.
        internal static List<VirtualInput> VirtualInputs;

        /// <summary>
        ///     Active status determins if <see cref="MInput"/> will update
        /// </summary>
        public static bool Active = true;

        /// <summary>
        ///     Disalbed status determines if checks on buttons, keys can be
        ///     performed
        /// </summary>
        public static bool Disabled = false;

        /// <summary>
        ///     Initilzies the <see cref="MInput"/>
        /// </summary>
        internal static void Initialize()
        {
            //Init devices
            Keyboard = new KeyboardData();
            Mouse = new MouseData();
            GamePads = new GamePadData[4];
            Touches = new Touch();
            for (int i = 0; i < 4; i++)
                GamePads[i] = new GamePadData((PlayerIndex)i);
            VirtualInputs = new List<VirtualInput>();
        }

        /// <summary>
        ///     Shuts down the <see cref="MInput"/>
        /// </summary>
        internal static void Shutdown()
        {
            foreach (var gamepad in GamePads)
                gamepad.StopRumble();
        }


        //---------------------------------------------------
        //  Updates
        //---------------------------------------------------
        /// <summary>
        ///     Updates the <see cref="MInput"/>
        /// </summary>
        internal static void Update()
        {
            if (Engine.Instance.IsActive && Active)
            {
                if (Engine.Commands.Open)
                {
                    Keyboard.UpdateNull();
                    Mouse.UpdateNull();

                }
                else
                {
                    Keyboard.Update();
                    Mouse.Update();
                }

                for (int i = 0; i < 4; i++)
                    GamePads[i].Update();

                Touches.Update();
            }
            else
            {
                Keyboard.UpdateNull();
                Mouse.UpdateNull();
                Touches.UpdateNull();
                for (int i = 0; i < 4; i++)
                    GamePads[i].UpdateNull();
            }

            UpdateVirtualInputs();
        }

        /// <summary>
        ///     Updates the inputs with a null state
        /// </summary>
        public static void UpdateNull()
        {
            Keyboard.UpdateNull();
            Mouse.UpdateNull();
            for (int i = 0; i < 4; i++)
                GamePads[i].UpdateNull();

            UpdateVirtualInputs();
        }

        /// <summary>
        ///     Updates all defined virtual inputs
        /// </summary>
        private static void UpdateVirtualInputs()
        {
            foreach (var virtualInput in VirtualInputs)
                virtualInput.Update();
        }





        //---------------------------------------------------
        //  Keyboard Data
        //---------------------------------------------------
        #region Keyboard

        public class KeyboardData
        {
            /// <summary>
            ///     KeyboardState from the previous frame
            /// </summary>
            public KeyboardState PreviousState;

            /// <summary>
            ///     KeyboardState of the current frame
            /// </summary>
            public KeyboardState CurrentState;

            /// <summary>
            ///     Empty Constructor
            /// </summary>
            internal KeyboardData() { }

            /// <summary>
            ///     Updates the <see cref="KeyboardData"/>
            /// </summary>
            internal void Update()
            {
                PreviousState = CurrentState;
                CurrentState = Microsoft.Xna.Framework.Input.Keyboard.GetState();
            }

            /// <summary>
            ///     Updates the <see cref="KeyboardData"/> with null states
            /// </summary>
            internal void UpdateNull()
            {
                PreviousState = CurrentState;
                CurrentState = new KeyboardState();
            }

            #region Basic Checks
            /// <summary>
            ///     Checks if a key is being held down
            /// </summary>
            /// <param name="key">The key to check</param>
            /// <returns></returns>
            public bool Check(Keys key)
            {
                if (Disabled)
                    return false;

                return CurrentState.IsKeyDown(key);
            }

            /// <summary>
            ///     Checks if a key was pressed
            /// </summary>
            /// <param name="key">The key to check</param>
            /// <returns></returns>
            public bool Pressed(Keys key)
            {
                if (Disabled)
                    return false;

                return CurrentState.IsKeyDown(key) && !PreviousState.IsKeyDown(key);
            }

            /// <summary>
            ///     Checks if a key was released
            /// </summary>
            /// <param name="key"></param>
            /// <returns></returns>
            public bool Released(Keys key)
            {
                if (Disabled)
                    return false;

                return !CurrentState.IsKeyDown(key) && PreviousState.IsKeyDown(key);
            }

            #endregion

            #region Convenience Checks

            /// <summary>
            ///     Checks if one of two keys is being held down
            /// </summary>
            /// <param name="keyA">First key to check</param>
            /// <param name="keyB">Second key to check</param>
            /// <returns></returns>
            public bool Check(Keys keyA, Keys keyB)
            {
                return Check(keyA) || Check(keyB);
            }

            /// <summary>
            ///     Checks if one of two keys was pressed
            /// </summary>
            /// <param name="keyA">First key to check</param>
            /// <param name="keyB">Second key to check</param>
            /// <returns></returns>
            public bool Pressed(Keys keyA, Keys keyB)
            {
                return Pressed(keyA) || Pressed(keyB);
            }

            /// <summary>
            ///     Checks if one of two keys was released
            /// </summary>
            /// <param name="keyA">First key to check</param>
            /// <param name="keyB">Second key to check</param>
            /// <returns></returns>
            public bool Released(Keys keyA, Keys keyB)
            {
                return Released(keyA) || Released(keyB);
            }

            /// <summary>
            ///     Checks if one of three keys is being held down
            /// </summary>
            /// <param name="keyA">First key to check</param>
            /// <param name="keyB">Second key to check</param>
            /// <param name="keyC">Third key to check</param>
            /// <returns></returns>
            public bool Check(Keys keyA, Keys keyB, Keys keyC)
            {
                return Check(keyA) || Check(keyB) || Check(keyC);
            }

            /// <summary>
            ///     Checks if one of three keys was pressed
            /// </summary>
            /// <param name="keyA">First key to check</param>
            /// <param name="keyB">Second key to check</param>
            /// <param name="keyC">Third key to check</param>
            /// <returns></returns>
            public bool Pressed(Keys keyA, Keys keyB, Keys keyC)
            {
                return Pressed(keyA) || Pressed(keyB) || Pressed(keyC);
            }

            /// <summary>
            ///     Checks if one of three keys was released
            /// </summary>
            /// <param name="keyA">First key to check</param>
            /// <param name="keyB">Second key to check</param>
            /// <param name="keyC">Third key to check</param>
            /// <returns></returns>
            public bool Released(Keys keyA, Keys keyB, Keys keyC)
            {
                return Released(keyA) || Released(keyB) || Released(keyC);
            }

            #endregion

            #region Axis
            /// <summary>
            ///     Given the definition of negative and positive for two keys
            ///     to repreasent an axis, check and return the axis value (-1, 0, 1)
            /// </summary>
            /// <param name="negative">The key representing the negaitve of the axis</param>
            /// <param name="positive">The key representing the positive of the axis</param>
            /// <returns></returns>
            public int AxisCheck(Keys negative, Keys positive)
            {
                if (Check(negative))
                {
                    if (Check(positive))
                        return 0;
                    else
                        return -1;
                }
                else if (Check(positive))
                    return 1;
                else
                    return 0;
            }

            /// <summary>
            ///     Givent he definition of negative and positive for two keys
            ///     to represent an axis, check and return the axis value (-1, 0, 1)
            ///     or, if both are held down, return the value of <paramref name="both"/>
            /// </summary>
            /// <param name="negative">The key representing the negative of the axis</param>
            /// <param name="positive">The key representing the positive of the axis</param>
            /// <param name="both">The value to return if both are held down</param>
            /// <returns></returns>
            public int AxisCheck(Keys negative, Keys positive, int both)
            {
                if (Check(negative))
                {
                    if (Check(positive))
                        return both;
                    else
                        return -1;
                }
                else if (Check(positive))
                    return 1;
                else
                    return 0;
            }

            #endregion
        }

        #endregion


        //---------------------------------------------------
        //  Mouse Data
        //---------------------------------------------------
        #region Mouse

        public class MouseData
        {
            /// <summary>
            ///     MouseState on previous frame
            /// </summary>
            public MouseState PreviousState;

            /// <summary>
            ///     MouseState on current frame
            /// </summary>
            public MouseState CurrentState;

            /// <summary>
            ///     Creates a new <see cref="MouseData"/> instance
            /// </summary>
            internal MouseData()
            {
                PreviousState = new MouseState();
                CurrentState = new MouseState();
            }

            /// <summary>
            ///     Updats the <see cref="MouseData"/>
            /// </summary>
            internal void Update()
            {
                PreviousState = CurrentState;
                CurrentState = Microsoft.Xna.Framework.Input.Mouse.GetState();
            }

            /// <summary>
            ///     Updates the <see cref="MouseData"/> with null states
            /// </summary>
            internal void UpdateNull()
            {
                PreviousState = CurrentState;
                CurrentState = new MouseState();
            }

            #region Buttons
            /// <summary>
            ///     Checks if the Left button is held down
            /// </summary>
            public bool CheckLeftButton
            {
                get { return CurrentState.LeftButton == ButtonState.Pressed; }
            }

            /// <summary>
            ///     Checks if the Right button is held down
            /// </summary>
            public bool CheckRightButton
            {
                get { return CurrentState.RightButton == ButtonState.Pressed; }
            }

            /// <summary>
            ///     Checks if the Middle button is held down
            /// </summary>
            public bool CheckMiddleButton
            {
                get { return CurrentState.MiddleButton == ButtonState.Pressed; }
            }

            /// <summary>
            ///     Checks if the Left button was pressed
            /// </summary>
            public bool PressedLeftButton
            {
                get { return CurrentState.LeftButton == ButtonState.Pressed && PreviousState.LeftButton == ButtonState.Released; }
            }

            /// <summary>
            ///     Checks if the Right button was pressed
            /// </summary>
            public bool PressedRightButton
            {
                get { return CurrentState.RightButton == ButtonState.Pressed && PreviousState.RightButton == ButtonState.Released; }
            }

            /// <summary>
            ///     Checks if the Middle button was pressed
            /// </summary>
            public bool PressedMiddleButton
            {
                get { return CurrentState.MiddleButton == ButtonState.Pressed && PreviousState.MiddleButton == ButtonState.Released; }
            }

            /// <summary>
            ///     Checks if the Left button was Released
            /// </summary>
            public bool ReleasedLeftButton
            {
                get { return CurrentState.LeftButton == ButtonState.Released && PreviousState.LeftButton == ButtonState.Pressed; }
            }

            /// <summary>
            ///     Checks if the Right button was Released
            /// </summary>
            public bool ReleasedRightButton
            {
                get { return CurrentState.RightButton == ButtonState.Released && PreviousState.RightButton == ButtonState.Pressed; }
            }

            /// <summary>
            ///     Checks if the Middle button was Released
            /// </summary>
            public bool ReleasedMiddleButton
            {
                get { return CurrentState.MiddleButton == ButtonState.Released && PreviousState.MiddleButton == ButtonState.Pressed; }
            }

            #endregion

            #region Wheel

            /// <summary>
            ///     Gets the current mouse wheel scroll value
            /// </summary>
            public int Wheel
            {
                get { return CurrentState.ScrollWheelValue; }
            }

            /// <summary>
            ///     Gets the delta of the mouse wheel scroll data between current and previous frame
            /// </summary>
            public int WheelDelta
            {
                get { return CurrentState.ScrollWheelValue - PreviousState.ScrollWheelValue; }
            }

            #endregion

            #region Position

            /// <summary>
            ///     Checks if the mouse position has moved between the previous and current frame
            /// </summary>
            public bool WasMoved
            {
                get
                {
                    return CurrentState.X != PreviousState.X
                        || CurrentState.Y != PreviousState.Y;
                }
            }

            /// <summary>
            ///     The x-coordinate of the mouse
            /// </summary>
            public float X
            {
                get { return Position.X; }
                set { Position = new Vector2(value, Position.Y); }
            }

            /// <summary>
            ///     The y-coordinate of the mouse
            /// </summary>
            public float Y
            {
                get { return Position.Y; }
                set { Position = new Vector2(Position.X, value); }
            }

            /// <summary>
            ///     The xy-coordinate position of the mouse
            /// </summary>
            public Vector2 Position
            {
                get
                {
                    return Vector2.Transform(new Vector2(CurrentState.X, CurrentState.Y), Matrix.Invert(Engine.ScreenMatrix));
                }

                set
                {
                    var vector = Vector2.Transform(value, Engine.ScreenMatrix);
                    Microsoft.Xna.Framework.Input.Mouse.SetPosition((int)Math.Round(vector.X), (int)Math.Round(vector.Y));
                }
            }

            #endregion
        }

        #endregion


        //---------------------------------------------------
        //  Gamepad Data
        //---------------------------------------------------
        #region GamePads

        public class GamePadData
        {
            /// <summary>
            ///     The <see cref="PlayerIndex"/> used for the <see cref="GamePadData"/>
            /// </summary>
            public PlayerIndex PlayerIndex { get; private set; }

            /// <summary>
            ///     GamePadState on previous frame 
            /// </summary>
            public GamePadState PreviousState;

            /// <summary>
            ///     GamePadState on current frame
            /// </summary>
            public GamePadState CurrentState;

            /// <summary>
            ///     Is the game pad currently attached
            /// </summary>
            public bool Attached;

            /// <summary>
            ///     Strength to use for rumble
            /// </summary>
            private float rumbleStrength;

            /// <summary>
            ///     Amount of time to rumble
            /// </summary>
            private float rumbleTime;

            /// <summary>
            ///     Creates a new <see cref="GamePadData"/> instance
            /// </summary>
            /// <param name="playerIndex">The <see cref="PlayerIndex"/> of the gamepad</param>
            internal GamePadData(PlayerIndex playerIndex)
            {
                PlayerIndex = playerIndex;
            }

            /// <summary>
            ///     Updates the <see cref="GamePadData"/>
            /// </summary>
            public void Update()
            {
                PreviousState = CurrentState;
                CurrentState = Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex);
                Attached = CurrentState.IsConnected;

                if (rumbleTime > 0)
                {
                    rumbleTime -= Engine.DeltaTime;
                    if (rumbleTime <= 0)
                        GamePad.SetVibration(PlayerIndex, 0, 0);
                }
            }

            /// <summary>
            ///     Updates the <see cref="GamePadData"/> with null states
            /// </summary>
            public void UpdateNull()
            {
                PreviousState = CurrentState;
                CurrentState = new GamePadState();
                Attached = Microsoft.Xna.Framework.Input.GamePad.GetState(PlayerIndex).IsConnected;

                if (rumbleTime > 0)
                    rumbleTime -= Engine.DeltaTime;

                GamePad.SetVibration(PlayerIndex, 0, 0);
            }

            /// <summary>
            ///     Triggers the game pad to rumble
            /// </summary>
            /// <param name="strength">The strength to use for the rumble</param>
            /// <param name="time">The amount of time to rumble (in seconds)</param>
            public void Rumble(float strength, float time)
            {
                if (rumbleTime <= 0 || strength > rumbleStrength || (strength == rumbleStrength && time > rumbleTime))
                {
                    GamePad.SetVibration(PlayerIndex, strength, strength);
                    rumbleStrength = strength;
                    rumbleTime = time;
                }
            }

            /// <summary>
            ///     Stops the rumble
            /// </summary>
            public void StopRumble()
            {
                GamePad.SetVibration(PlayerIndex, 0, 0);
                rumbleTime = 0;
            }

            #region Buttons

            /// <summary>
            ///     Checks if a button is held down
            /// </summary>
            /// <param name="button">The button to check</param>
            /// <returns></returns>
            public bool Check(Buttons button)
            {
                if (Disabled)
                    return false;

                return CurrentState.IsButtonDown(button);
            }

            /// <summary>
            ///     Checks if a button was pressed
            /// </summary>
            /// <param name="button">The button to check</param>
            /// <returns></returns>
            public bool Pressed(Buttons button)
            {
                if (Disabled)
                    return false;

                return CurrentState.IsButtonDown(button) && PreviousState.IsButtonUp(button);
            }

            /// <summary>
            ///     Checks if a button was released
            /// </summary>
            /// <param name="button">The button to check</param>
            /// <returns></returns>
            public bool Released(Buttons button)
            {
                if (Disabled)
                    return false;

                return CurrentState.IsButtonUp(button) && PreviousState.IsButtonDown(button);
            }

            /// <summary>
            ///     Checks if one of two buttons is held down
            /// </summary>
            /// <param name="buttonA">The first button to check</param>
            /// <param name="buttonB">The second button to check</param>
            /// <returns></returns>
            public bool Check(Buttons buttonA, Buttons buttonB)
            {
                return Check(buttonA) || Check(buttonB);
            }

            /// <summary>
            ///     Checks if one of two buttons is pressed
            /// </summary>
            /// <param name="buttonA">The first button to check</param>
            /// <param name="buttonB">The second button to check</param>
            /// <returns></returns>
            public bool Pressed(Buttons buttonA, Buttons buttonB)
            {
                return Pressed(buttonA) || Pressed(buttonB);
            }

            /// <summary>
            ///     Checks if one of two buttons was released
            /// </summary>
            /// <param name="buttonA">The first button to check</param>
            /// <param name="buttonB">The second button to check</param>
            /// <returns></returns>
            public bool Released(Buttons buttonA, Buttons buttonB)
            {
                return Released(buttonA) || Released(buttonB);
            }

            /// <summary>
            ///     Checks if one of three buttons is held down
            /// </summary>
            /// <param name="buttonA">The first button to check</param>
            /// <param name="buttonB">The second button to check</param>
            /// <param name="buttonC">The third button to check</param>
            /// <returns></returns>
            public bool Check(Buttons buttonA, Buttons buttonB, Buttons buttonC)
            {
                return Check(buttonA) || Check(buttonB) || Check(buttonC);
            }

            /// <summary>
            ///     Checks if one of three buttons was pressed
            /// </summary>
            /// <param name="buttonA">The first button to check</param>
            /// <param name="buttonB">The second button to check</param>
            /// <param name="buttonC">The third button to check</param>
            /// <returns></returns>
            public bool Pressed(Buttons buttonA, Buttons buttonB, Buttons buttonC)
            {
                return Pressed(buttonA) || Pressed(buttonB) || Check(buttonC);
            }

            /// <summary>
            ///     Checks if one of three buttons was released
            /// </summary>
            /// <param name="buttonA">The first button to check</param>
            /// <param name="buttonB">The second button to check</param>
            /// <param name="buttonC">The third button to check</param>
            /// <returns></returns>
            public bool Released(Buttons buttonA, Buttons buttonB, Buttons buttonC)
            {
                return Released(buttonA) || Released(buttonB) || Check(buttonC);
            }

            #endregion

            #region Sticks

            /// <summary>
            ///     Gets the left stick x and y axis data
            /// </summary>
            /// <returns></returns>
            public Vector2 GetLeftStick()
            {
                Vector2 ret = CurrentState.ThumbSticks.Left;
                ret.Y = -ret.Y;
                return ret;
            }

            /// <summary>
            ///     Gets the left stick x and y axis data with a deadzone defined
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public Vector2 GetLeftStick(float deadzone)
            {
                Vector2 ret = CurrentState.ThumbSticks.Left;
                if (ret.LengthSquared() < deadzone * deadzone)
                    ret = Vector2.Zero;
                else
                    ret.Y = -ret.Y;
                return ret;
            }

            /// <summary>
            ///     Gets the right stick x and y axis data
            /// </summary>
            /// <returns></returns>
            public Vector2 GetRightStick()
            {
                Vector2 ret = CurrentState.ThumbSticks.Right;
                ret.Y = -ret.Y;
                return ret;
            }

            /// <summary>
            ///     Gets the right stick x and y axis data with a deadzone defined
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public Vector2 GetRightStick(float deadzone)
            {
                Vector2 ret = CurrentState.ThumbSticks.Right;
                if (ret.LengthSquared() < deadzone * deadzone)
                    ret = Vector2.Zero;
                else
                    ret.Y = -ret.Y;
                return ret;
            }

            #region Left Stick Directions

            /// <summary>
            ///     Checks if the left stick is pushed horizontally right 
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickLeftCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.X <= -deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was pushed horizontally left  
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickLeftPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.X <= -deadzone && PreviousState.ThumbSticks.Left.X > -deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was released from being pushed horizontally left  
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickLeftReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.X > -deadzone && PreviousState.ThumbSticks.Left.X <= -deadzone;
            }

            /// <summary>
            ///     Checks if the left stick is pushed horizontally right 
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickRightCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.X >= deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was pushed horizontally right  
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickRightPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.X >= deadzone && PreviousState.ThumbSticks.Left.X < deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was released from being pushed horizontally right 
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickRightReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.X < deadzone && PreviousState.ThumbSticks.Left.X >= deadzone;
            }

            /// <summary>
            ///     Checks if the left stick is pushed vertically down
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickDownCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.Y <= -deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was pushed vertically down
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickDownPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.Y <= -deadzone && PreviousState.ThumbSticks.Left.Y > -deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was released from being pushed vertically down
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickDownReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.Y > -deadzone && PreviousState.ThumbSticks.Left.Y <= -deadzone;
            }

            /// <summary>
            ///     Checks if the left stick is pushed vertically up
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickUpCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.Y >= deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was pushed vertically up
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickUpPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.Y >= deadzone && PreviousState.ThumbSticks.Left.Y < deadzone;
            }

            /// <summary>
            ///     Checks if the left stick was released from being pushed vertically up
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool LeftStickUpReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Left.Y < deadzone && PreviousState.ThumbSticks.Left.Y >= deadzone;
            }

            /// <summary>
            ///     Gets the left stick horizontal axis value
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public float LeftStickHorizontal(float deadzone)
            {
                float h = CurrentState.ThumbSticks.Left.X;
                if (Math.Abs(h) < deadzone)
                    return 0;
                else
                    return h;
            }

            /// <summary>
            ///     Gets the left stick vertical axis valu
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public float LeftStickVertical(float deadzone)
            {
                float v = CurrentState.ThumbSticks.Left.Y;
                if (Math.Abs(v) < deadzone)
                    return 0;
                else
                    return -v;
            }

            #endregion

            #region Right Stick Directions

            /// <summary>
            ///     Checks if the right stick is beign pushed horizontally left
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickLeftCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.X <= -deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was beign pushed horizontally left
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickLeftPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.X <= -deadzone && PreviousState.ThumbSticks.Right.X > -deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was released from beign pushed horizontally left
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickLeftReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.X > -deadzone && PreviousState.ThumbSticks.Right.X <= -deadzone;
            }

            /// <summary>
            ///     Checks if the right stick is beign pushed horizontally left
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickRightCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.X >= deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was beign pushed horizontally right
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickRightPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.X >= deadzone && PreviousState.ThumbSticks.Right.X < deadzone;
            }

            /// <summary>
            ///     Checks if the right stick is released from beign pushed horizontally right
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickRightReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.X < deadzone && PreviousState.ThumbSticks.Right.X >= deadzone;
            }

            /// <summary>
            ///     Checks if the right stick is being pushed vertically up
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickUpCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.Y <= -deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was being pushed vertically up
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickUpPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.Y <= -deadzone && PreviousState.ThumbSticks.Right.Y > -deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was released from being pushed vertically up
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickUpReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.Y > -deadzone && PreviousState.ThumbSticks.Right.Y <= -deadzone;
            }

            /// <summary>
            ///     Checks if the right stick is being pushed vertically down
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickDownCheck(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.Y >= deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was being pushed vertically down
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickDownPressed(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.Y >= deadzone && PreviousState.ThumbSticks.Right.Y < deadzone;
            }

            /// <summary>
            ///     Checks if the right stick was released from being pushed vertically down
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public bool RightStickDownReleased(float deadzone)
            {
                return CurrentState.ThumbSticks.Right.Y < deadzone && PreviousState.ThumbSticks.Right.Y >= deadzone;
            }

            /// <summary>
            ///     Gets the right stick horizontal axis value
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public float RightStickHorizontal(float deadzone)
            {
                float h = CurrentState.ThumbSticks.Right.X;
                if (Math.Abs(h) < deadzone)
                    return 0;
                else
                    return h;
            }

            /// <summary>
            ///     Gets the right stick vertical value
            /// </summary>
            /// <param name="deadzone">The deadzone definition</param>
            /// <returns></returns>
            public float RightStickVertical(float deadzone)
            {
                float v = CurrentState.ThumbSticks.Right.Y;
                if (Math.Abs(v) < deadzone)
                    return 0;
                else
                    return -v;
            }

            #endregion

            #endregion

            #region DPad

            /// <summary>
            ///     Gets -1 if DpadLeft is pressed, 1 for DpadRight, and 0 for none
            /// </summary>
            public int DPadHorizontal
            {
                get
                {
                    return CurrentState.DPad.Right == ButtonState.Pressed ? 1 : (CurrentState.DPad.Left == ButtonState.Pressed ? -1 : 0);
                }
            }

            /// <summary>
            ///     Gets -1 if DpadUp is pressed, 1 for DpadDown, and 0 for none
            /// </summary>
            public int DPadVertical
            {
                get
                {
                    return CurrentState.DPad.Down == ButtonState.Pressed ? 1 : (CurrentState.DPad.Up == ButtonState.Pressed ? -1 : 0);
                }
            }

            /// <summary>
            ///     Gets the dpad axis values
            /// </summary>
            public Vector2 DPad
            {
                get
                {
                    return new Vector2(DPadHorizontal, DPadVertical);
                }
            }

            /// <summary>
            ///     Checks if DpadLeft is being held down
            /// </summary>
            public bool DPadLeftCheck
            {
                get
                {
                    return CurrentState.DPad.Left == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadLeft was pressed
            /// </summary>
            public bool DPadLeftPressed
            {
                get
                {
                    return CurrentState.DPad.Left == ButtonState.Pressed && PreviousState.DPad.Left == ButtonState.Released;
                }
            }

            /// <summary>
            ///     Checks if DpadLeft was released
            /// </summary>
            public bool DPadLeftReleased
            {
                get
                {
                    return CurrentState.DPad.Left == ButtonState.Released && PreviousState.DPad.Left == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadRight is being held down
            /// </summary>
            public bool DPadRightCheck
            {
                get
                {
                    return CurrentState.DPad.Right == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadRight was pressed
            /// </summary>
            public bool DPadRightPressed
            {
                get
                {
                    return CurrentState.DPad.Right == ButtonState.Pressed && PreviousState.DPad.Right == ButtonState.Released;
                }
            }

            /// <summary>
            ///     Checks if DpadRight was released
            /// </summary>
            public bool DPadRightReleased
            {
                get
                {
                    return CurrentState.DPad.Right == ButtonState.Released && PreviousState.DPad.Right == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadUp is being held down
            /// </summary>
            public bool DPadUpCheck
            {
                get
                {
                    return CurrentState.DPad.Up == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadUp was pressed
            /// </summary>
            public bool DPadUpPressed
            {
                get
                {
                    return CurrentState.DPad.Up == ButtonState.Pressed && PreviousState.DPad.Up == ButtonState.Released;
                }
            }

            /// <summary>
            ///     Checks if DpadUp was released
            /// </summary>
            public bool DPadUpReleased
            {
                get
                {
                    return CurrentState.DPad.Up == ButtonState.Released && PreviousState.DPad.Up == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadDown is being held down
            /// </summary>
            public bool DPadDownCheck
            {
                get
                {
                    return CurrentState.DPad.Down == ButtonState.Pressed;
                }
            }

            /// <summary>
            ///     Checks if DpadDown was pressed
            /// </summary>
            public bool DPadDownPressed
            {
                get
                {
                    return CurrentState.DPad.Down == ButtonState.Pressed && PreviousState.DPad.Down == ButtonState.Released;
                }
            }

            /// <summary>
            ///     Checks if DpadDown was released
            /// </summary>
            public bool DPadDownReleased
            {
                get
                {
                    return CurrentState.DPad.Down == ButtonState.Released && PreviousState.DPad.Down == ButtonState.Pressed;
                }
            }

            #endregion

            #region Triggers

            /// <summary>
            ///     Checks if the left trigger is being held down
            /// </summary>
            /// <param name="threshold">The minimum threshold for a postive value</param>
            /// <returns></returns>
            public bool LeftTriggerCheck(float threshold)
            {
                if (Disabled)
                    return false;

                return CurrentState.Triggers.Left >= threshold;
            }

            /// <summary>
            ///     Checks if the left trigger was pressed
            /// </summary>
            /// <param name="threshold">The minimum threshold for a positive value</param>
            /// <returns></returns>
            public bool LeftTriggerPressed(float threshold)
            {
                if (Disabled)
                    return false;

                return CurrentState.Triggers.Left >= threshold && PreviousState.Triggers.Left < threshold;
            }

            /// <summary>
            ///     Checks if the left trigger was released
            /// </summary>
            /// <param name="threshold">The minimum threshold for a postive value</param>
            /// <returns></returns>
            public bool LeftTriggerReleased(float threshold)
            {
                if (Disabled)
                    return false;

                return CurrentState.Triggers.Left < threshold && PreviousState.Triggers.Left >= threshold;
            }

            /// <summary>
            ///     Checks if the Right Trigger is currently held down
            /// </summary>
            /// <param name="threshold">The minimum threshold for a postive value</param>
            /// <returns></returns>
            public bool RightTriggerCheck(float threshold)
            {
                if (Disabled)
                    return false;

                return CurrentState.Triggers.Right >= threshold;
            }

            /// <summary>
            ///     Checks if the Right Trigger was pressed
            /// </summary>
            /// <param name="threshold">The minimum threshold for a postive value</param>
            /// <returns></returns>
            public bool RightTriggerPressed(float threshold)
            {
                if (Disabled)
                    return false;

                return CurrentState.Triggers.Right >= threshold && PreviousState.Triggers.Right < threshold;
            }

            /// <summary>
            ///     Checks if the Right Trigger was released
            /// </summary>
            /// <param name="threshold">The minimum threshold for a postive value</param>
            /// <returns></returns>
            public bool RightTriggerReleased(float threshold)
            {
                if (Disabled)
                    return false;

                return CurrentState.Triggers.Right < threshold && PreviousState.Triggers.Right >= threshold;
            }

            #endregion
        }

        #endregion

        //---------------------------------------------------
        //  Touch Data
        //---------------------------------------------------
        public class Touch
        {
            public TouchCollection TouchCollection { get; private set; }
            public GestureSample Gesture { get; private set; }

            internal Touch()
            {
                TouchPanel.EnableMouseGestures = true;
                TouchPanel.EnabledGestures = GestureType.HorizontalDrag | GestureType.VerticalDrag | GestureType.Tap;

            }

            public void Update()
            {
                TouchCollection = TouchPanel.GetState();
                //Debug.WriteLine($"Touches: {TouchCollection.Count}");
                Gesture = default(GestureSample);

                while (TouchPanel.IsGestureAvailable)
                {
                    Gesture = TouchPanel.ReadGesture();
                }
            }

            public void UpdateNull()
            {
                TouchCollection = new TouchCollection();
            }
        }

        #region Helpers

        /// <summary>
        ///     Rumbles the first available gamepad
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="time"></param>
        public static void RumbleFirst(float strength, float time)
        {
            GamePads[0].Rumble(strength, time);
        }

        /////////// <summary>
        ///////////     Checks for an axis value based on two bools
        /////////// </summary>
        /////////// <param name="negative">The bool that is the negative axis value</param>
        /////////// <param name="positive">The bool that is the positive axis value</param>
        /////////// <param name="bothValue">The value to return if both are true</param>
        /////////// <returns></returns>
        ////////public static int Axis(bool negative, bool positive, int bothValue)
        ////////{
        ////////    if (negative)
        ////////    {
        ////////        if (positive)
        ////////            return bothValue;
        ////////        else
        ////////            return -1;
        ////////    }
        ////////    else if (positive)
        ////////        return 1;
        ////////    else
        ////////        return 0;
        ////////}

        /////////// <summary>
        ///////////     Checks the axis value against a deadzone
        /////////// </summary>
        /////////// <param name="axisValue">The axis value</param>
        /////////// <param name="deadzone">The deadzone definition</param>
        /////////// <returns></returns>
        ////////public static int Axis(float axisValue, float deadzone)
        ////////{
        ////////    if (Math.Abs(axisValue) >= deadzone)
        ////////        return Math.Sign(axisValue);
        ////////    else
        ////////        return 0;
        ////////}

        /////////// <summary>
        ///////////     Checks for an axis value basd on two bools, 
        /////////// </summary>
        /////////// <param name="negative"></param>
        /////////// <param name="positive"></param>
        /////////// <param name="bothValue"></param>
        /////////// <param name="axisValue"></param>
        /////////// <param name="deadzone"></param>
        /////////// <returns></returns>
        ////////public static int Axis(bool negative, bool positive, int bothValue, float axisValue, float deadzone)
        ////////{
        ////////    int ret = Axis(axisValue, deadzone);
        ////////    if (ret == 0)
        ////////        ret = Axis(negative, positive, bothValue);
        ////////    return ret;
        ////////}

        #endregion
    }
}
