﻿//--------------------------------------------------------------------------------
//  Virtual Button
//
//
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Psiren.Input
{
    public class VirtualButton : VirtualInput
    {
        /// <summary>
        ///     List of <see cref="Node"/> that represent this <see cref="VirtualButton"/>
        /// </summary>
        public List<Node> Nodes;

        /// <summary>
        ///     The amount of time to buffer states
        /// </summary>
        public float BufferTime;

        /// <summary>
        ///     Is the button press repeating
        /// </summary>
        public bool Repeating { get; private set; }


        private float firstRepeatTime;
        private float multiRepeatTime;
        private float bufferCounter;
        private float repeatCounter;
        private bool canRepeat;
        private bool consumed;

        /// <summary>
        ///     Creates a new <see cref="VirtualButton"/> instance
        /// </summary>
        /// <param name="bufferTime">The amount of time to buffer the states</param>
        public VirtualButton(float bufferTime) : base()
        {
            Nodes = new List<Node>();
            BufferTime = bufferTime;
        }

        /// <summary>
        ///     Creates a new <see cref="VirtualButton"/> instance
        /// </summary>
        public VirtualButton() : this(0) { }


        /// <summary>
        ///     Creates a new <see cref="VirtualButton"/> instance
        /// </summary>
        /// <param name="bufferTime">The amount of time to buffer the states</param>
        /// <param name="nodes">The <see cref="VirtualButton.Node"/>s</param>
        public VirtualButton(float bufferTime, params Node[] nodes) : base()
        {
            Nodes = new List<Node>(nodes);
            BufferTime = bufferTime;
        }

        /// <summary>
        ///     Creates a new <see cref="VirtualButton"/> instance
        /// </summary>
        /// <param name="nodes">The <see cref="VirtualButton.Node"/>s</param>
        public VirtualButton(params Node[] nodes) : this(0, nodes)
        {

        }

        /// <summary>
        ///     Sets the repeating time
        /// </summary>
        /// <param name="repeatTime">The amount of time to use for repeating</param>
        public void SetRepeat(float repeatTime)
        {
            SetRepeat(repeatTime, repeatTime);
        }

        /// <summary>
        ///     Sets the repeating time
        /// </summary>
        /// <param name="firstRepeatTime">The amount of time for the first repeat</param>
        /// <param name="multiRepeatTime">The amount of time for repeats after the first</param>
        public void SetRepeat(float firstRepeatTime, float multiRepeatTime)
        {
            this.firstRepeatTime = firstRepeatTime;
            this.multiRepeatTime = multiRepeatTime;
            canRepeat = (this.firstRepeatTime > 0);
            if (!canRepeat)
                Repeating = false;
        }

        /// <summary>
        ///     Updates this <see cref="VirtualButton"/>
        /// </summary>
        public override void Update()
        {
            consumed = false;
            bufferCounter -= Engine.DeltaTime;

            bool check = false;
            foreach (var node in Nodes)
            {
                node.Update();
                if (node.Pressed)
                {
                    bufferCounter = BufferTime;
                    check = true;
                }
                else if (node.Check)
                    check = true;
            }

            if (!check)
            {
                Repeating = false;
                repeatCounter = 0;
                bufferCounter = 0;
            }
            else if (canRepeat)
            {
                Repeating = false;
                if (repeatCounter == 0)
                    repeatCounter = firstRepeatTime;
                else
                {
                    repeatCounter -= Engine.DeltaTime;
                    if (repeatCounter <= 0)
                    {
                        Repeating = true;
                        repeatCounter = multiRepeatTime;
                    }
                }
            }
        }

        /// <summary>
        ///     Checks if this <see cref="VirtualButton"/> is beign held down
        /// </summary>
        public bool Check
        {
            get
            {
                if (MInput.Disabled)
                    return false;

                foreach (var node in Nodes)
                    if (node.Check)
                        return true;
                return false;
            }
        }

        /// <summary>
        ///     Checks if this <see cref="VirtualButton"/> was pressed
        /// </summary>
        public bool Pressed
        {
            get
            {
                if (MInput.Disabled)
                    return false;

                if (consumed)
                    return false;

                if (bufferCounter > 0 || Repeating)
                    return true;

                foreach (var node in Nodes)
                    if (node.Pressed)
                        return true;
                return false;
            }
        }

        /// <summary>
        ///     Checks if this <see cref="VirtualButton"/> was released
        /// </summary>
        public bool Released
        {
            get
            {
                if (MInput.Disabled)
                    return false;

                foreach (var node in Nodes)
                    if (node.Released)
                        return true;
                return false;
            }
        }

        /// <summary>
        /// Ends the Press buffer for this button
        /// </summary>
        public void ConsumeBuffer()
        {
            bufferCounter = 0;
        }

        /// <summary>
        /// This button will not register a Press for the rest of the current frame, but otherwise 
        /// continues to function normally. If the player continues to hold the button, next frame 
        /// will not count as a Press. Also ends the Press buffer for this button
        /// </summary>
        public void ConsumePress()
        {
            bufferCounter = 0;
            consumed = true;
        }

        /// <summary>
        ///     Implicietly converts this <see cref="VirtualButton"/> to a bool for its Check() state
        /// </summary>
        /// <param name="button"></param>
        public static implicit operator bool(VirtualButton button)
        {
            return button.Check;
        }

        /// <summary>
        ///     <see cref="VirtualButton.Node"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public abstract class Node : VirtualInputNode
        {
            public abstract bool Check { get; }
            public abstract bool Pressed { get; }
            public abstract bool Released { get; }
        }

        /// <summary>
        ///     <see cref="VirtualButton.KeyboardKey"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class KeyboardKey : Node
        {
            /// <summary>
            ///     The keyboard key
            /// </summary>
            public Keys Key;

            /// <summary>
            ///     Creates a new <see cref="KeyboardKey"/> instance
            /// </summary>
            /// <param name="key"></param>
            public KeyboardKey(Keys key)
            {
                Key = key;
            }

            /// <summary>
            ///     Checks if the <see cref="KeyboardKey"/> is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.Keyboard.Check(Key); }
            }

            /// <summary>
            ///     Checks if the <see cref="KeyboardKey"/> was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.Keyboard.Pressed(Key); }
            }

            /// <summary>
            ///     Checks if the <see cref="KeyboardKey"/> was released
            /// </summary>
            public override bool Released
            {
                get { return MInput.Keyboard.Released(Key); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadButton"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadButton : Node
        {
            /// <summary>
            ///     The gamepad index used for this <see cref="VirtualButton.PadButton"/>
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The button this <see cref="VirtualButton.PadButton"/> represents
            /// </summary>
            public Buttons Button;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadButton"/> instance
            /// </summary>
            /// <param name="gamepadIndex"></param>
            /// <param name="button"></param>
            public PadButton(int gamepadIndex, Buttons button)
            {
                GamepadIndex = gamepadIndex;
                Button = button;
            }

            /// <summary>
            ///     Checks if this <see cref="VirtualButton.PadButton"/> is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].Check(Button); }
            }

            /// <summary>
            ///     Checks if this <see cref="VirtualButton.PadButton"/> was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].Pressed(Button); }
            }

            /// <summary>
            ///     Checks if this <see cref="VirtualButton.PadButton"/> was released
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].Released(Button); }
            }
        }

        #region Pad Left Stick

        /// <summary>
        ///     <see cref="VirtualButton.PadLeftStickRight"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     left stick horizontal right axis to act as a button
        /// </summary>
        public class PadLeftStickRight : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadLeftStickRight"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadLeftStickRight(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickRightCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickRightPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was released
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickRightReleased(Deadzone); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadLeftStickLeft"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     left stick horizontal left axis to act as a button
        /// </summary>
        public class PadLeftStickLeft : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadLeftStickLeft"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadLeftStickLeft(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickLeftCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickLeftPressed(Deadzone); }
            }
            /// <summary>
            ///     Checks if this was released
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickLeftReleased(Deadzone); }
            }
        }


        /// <summary>
        ///     <see cref="VirtualButton.PadLeftStickUp"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     left stick vertical up axis to act as a button
        /// </summary>
        public class PadLeftStickUp : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadLeftStickUp"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadLeftStickUp(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickUpCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickUpPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was released
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickUpReleased(Deadzone); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadLeftStickDown"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     left stick vertical down axis to act as a button
        /// </summary>
        public class PadLeftStickDown : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadLeftStickDown"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadLeftStickDown(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickDownCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickDownPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].LeftStickDownReleased(Deadzone); }
            }
        }

        #endregion

        #region Pad Right Stick

        /// <summary>
        ///     <see cref="VirtualButton.PadRightStickRight"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     right stick horizontal right axis to act as a button
        /// </summary>
        public class PadRightStickRight : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadRightStickRight"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadRightStickRight(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].RightStickRightCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].RightStickRightPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].RightStickRightReleased(Deadzone); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadRightStickLeft"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     right stick horizontal left axis to act as a button
        /// </summary>
        public class PadRightStickLeft : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadRightStickLeft"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadRightStickLeft(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].RightStickLeftCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].RightStickLeftPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].RightStickLeftReleased(Deadzone); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadRightStickUp"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     right stick vertical up axis to act as a button
        /// </summary>
        public class PadRightStickUp : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadRightStickUp"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadRightStickUp(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].RightStickUpCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].RightStickUpPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].RightStickUpReleased(Deadzone); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadRightStickDown"/> for a <see cref="VirtualButton"/> allowing the gamepad
        ///     right stick vertical down axis to act as a button
        /// </summary>
        public class PadRightStickDown : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Createas a new <see cref="VirtualButton.PadRightStickDown"/> instance
            /// </summary>
            /// <param name="gamepadindex">The gamepad index</param>
            /// <param name="deadzone">The amount of dead zone</param>
            public PadRightStickDown(int gamepadindex, float deadzone)
            {
                GamepadIndex = gamepadindex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].RightStickDownCheck(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].RightStickDownPressed(Deadzone); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].RightStickDownReleased(Deadzone); }
            }
        }

        #endregion

        #region Pad Triggers

        /// <summary>
        ///     <see cref="VirtualButton.PadLeftTrigger"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadLeftTrigger : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Amount to trigger a postive 
            /// </summary>
            public float Threshold;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadLeftTrigger"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            /// <param name="threshold">The amount to trigger a postive</param>
            public PadLeftTrigger(int gamepadIndex, float threshold)
            {
                GamepadIndex = gamepadIndex;
                Threshold = threshold;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].LeftTriggerCheck(Threshold); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].LeftTriggerPressed(Threshold); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].LeftTriggerReleased(Threshold); }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadRightTrigger"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadRightTrigger : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Amount to trigger a postive 
            /// </summary>
            public float Threshold;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadRightTrigger"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            /// <param name="threshold">The amount to trigger a postive</param>
            public PadRightTrigger(int gamepadIndex, float threshold)
            {
                GamepadIndex = gamepadIndex;
                Threshold = threshold;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].RightTriggerCheck(Threshold); }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].RightTriggerPressed(Threshold); }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].RightTriggerReleased(Threshold); }
            }
        }

        #endregion

        #region Pad DPad

        /// <summary>
        ///     <see cref="VirtualButton.PadDPadRight"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadDPadRight : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadDPadRight"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            public PadDPadRight(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].DPadRightCheck; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].DPadRightPressed; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].DPadRightReleased; }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadDPadLeft"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadDPadLeft : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadDPadLeft"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            public PadDPadLeft(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].DPadLeftCheck; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].DPadLeftPressed; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].DPadLeftReleased; }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadDPadUp"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadDPadUp : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadDPadUp"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            public PadDPadUp(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].DPadUpCheck; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].DPadUpPressed; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].DPadUpReleased; }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.PadDPadDown"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class PadDPadDown : Node
        {
            /// <summary>
            ///     The gamepad index 
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.PadDPadDown"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            public PadDPadDown(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.GamePads[GamepadIndex].DPadDownCheck; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.GamePads[GamepadIndex].DPadDownPressed; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.GamePads[GamepadIndex].DPadDownReleased; }
            }
        }

        #endregion

        #region Mouse

        /// <summary>
        ///     <see cref="VirtualButton.MouseLeftButton"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class MouseLeftButton : Node
        {
            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.Mouse.CheckLeftButton; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.Mouse.PressedLeftButton; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.Mouse.ReleasedLeftButton; }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.MouseRightButton"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class MouseRightButton : Node
        {
            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.Mouse.CheckRightButton; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.Mouse.PressedRightButton; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.Mouse.ReleasedRightButton; }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.MouseMiddleButton"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class MouseMiddleButton : Node
        {
            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get { return MInput.Mouse.CheckMiddleButton; }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get { return MInput.Mouse.PressedMiddleButton; }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get { return MInput.Mouse.ReleasedMiddleButton; }
            }
        }

        #endregion

        #region Other Virtual Inputs


        /// <summary>
        ///     <see cref="VirtualButton.VirtualAxisTrigger"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class VirtualAxisTrigger : Node
        {
            /// <summary>
            ///     VirtualInput threhsold modes
            /// </summary>
            public enum Modes { LargerThan, LessThan, Equals };

            /// <summary>
            ///     The <see cref="Mode"/> to use
            /// </summary>
            public VirtualInput.ThresholdModes Mode;

            /// <summary>
            ///     The amount to trigger a postive
            /// </summary>
            public float Threshold;

            /// <summary>
            ///     The <see cref="VirtualAxis"/>
            /// </summary>
            private VirtualAxis axis;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxisTrigger"/> instance
            /// </summary>
            /// <param name="axis">The <see cref="VirtualAxis"/> to use as the trigger</param>
            /// <param name="mode">The <see cref="VirtualInput.ThresholdModes"/></param>
            /// <param name="threshold">The amount to trigger a postive</param>
            public VirtualAxisTrigger(VirtualAxis axis, VirtualInput.ThresholdModes mode, float threshold)
            {
                this.axis = axis;
                Mode = mode;
                Threshold = threshold;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return axis.Value >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return axis.Value <= Threshold;
                    else
                        return axis.Value == Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return axis.Value >= Threshold && axis.PreviousValue < Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return axis.Value <= Threshold && axis.PreviousValue > Threshold;
                    else
                        return axis.Value == Threshold && axis.PreviousValue != Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return axis.Value < Threshold && axis.PreviousValue >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return axis.Value > Threshold && axis.PreviousValue <= Threshold;
                    else
                        return axis.Value != Threshold && axis.PreviousValue == Threshold;
                }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.VirtualIntegerAxisTrigger"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class VirtualIntegerAxisTrigger : Node
        {
            /// <summary>
            ///     VirtualInput threhsold modes
            /// </summary>
            public enum Modes { LargerThan, LessThan, Equals };

            /// <summary>
            ///     The <see cref="VirtualInput.ThresholdModes"/> to use
            /// </summary>
            public VirtualInput.ThresholdModes Mode;

            /// <summary>
            ///     The amount to trigger a postive
            /// </summary>
            public int Threshold;

            /// <summary>
            ///     The <see cref="VirtualIntegerAxis"/>
            /// </summary>
            private VirtualIntegerAxis axis;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.VirtualIntegerAxisTrigger"/> instance
            /// </summary>
            /// <param name="axis">The <see cref="VirtualIntegerAxis"/></param>
            /// <param name="mode">The <see cref="VirtualInput.ThresholdModes"/></param>
            /// <param name="threshold">The amount to trigger a postive</param>
            public VirtualIntegerAxisTrigger(VirtualIntegerAxis axis, VirtualInput.ThresholdModes mode, int threshold)
            {
                this.axis = axis;
                Mode = mode;
                Threshold = threshold;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return axis.Value >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return axis.Value <= Threshold;
                    else
                        return axis.Value == Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return axis.Value >= Threshold && axis.PreviousValue < Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return axis.Value <= Threshold && axis.PreviousValue > Threshold;
                    else
                        return axis.Value == Threshold && axis.PreviousValue != Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return axis.Value < Threshold && axis.PreviousValue >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return axis.Value > Threshold && axis.PreviousValue <= Threshold;
                    else
                        return axis.Value != Threshold && axis.PreviousValue == Threshold;
                }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.VirtualJoystickXTrigger"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class VirtualJoystickXTrigger : Node
        {
            /// <summary>
            ///     VirtualInput threhsold modes
            /// </summary>
            public enum Modes { LargerThan, LessThan, Equals };

            /// <summary>
            ///     The <see cref="VirtualInput.ThresholdModes"/> to use
            /// </summary>
            public VirtualInput.ThresholdModes Mode;

            /// <summary>
            ///     The amount to trigger a postive
            /// </summary>
            public float Threshold;

            /// <summary>
            ///     The <see cref="VirtualJoystick"/>
            /// </summary>
            private VirtualJoystick joystick;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.VirtualJoystickXTrigger"/> instance
            /// </summary>
            /// <param name="joystick">The <see cref="VirtualJoystick"/></param>
            /// <param name="mode">The <see cref="VirtualInput.ThresholdModes"/></param>
            /// <param name="threshold">The amount to trigger a postive</param>
            public VirtualJoystickXTrigger(VirtualJoystick joystick, VirtualInput.ThresholdModes mode, float threshold)
            {
                this.joystick = joystick;
                Mode = mode;
                Threshold = threshold;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold;
                    else
                        return joystick.Value.X == Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold && joystick.PreviousValue.X < Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold && joystick.PreviousValue.X > Threshold;
                    else
                        return joystick.Value.X == Threshold && joystick.PreviousValue.X != Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return joystick.Value.X < Threshold && joystick.PreviousValue.X >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return joystick.Value.X > Threshold && joystick.PreviousValue.X <= Threshold;
                    else
                        return joystick.Value.X != Threshold && joystick.PreviousValue.X == Threshold;
                }
            }
        }

        /// <summary>
        ///     <see cref="VirtualButton.VirtualJoystickYTrigger"/> for a <see cref="VirtualButton"/>
        /// </summary>
        public class VirtualJoystickYTrigger : Node
        {
            /// <summary>
            ///     The <see cref="VirtualInput.ThresholdModes"/> to use
            /// </summary>
            public VirtualInput.ThresholdModes Mode;

            /// <summary>
            ///     The amount to trigger a postive
            /// </summary>
            public float Threshold;

            /// <summary>
            ///     The <see cref="VirtualJoystick"/>
            /// </summary>
            private VirtualJoystick joystick;

            /// <summary>
            ///     Creates a new <see cref="VirtualButton.VirtualJoystickYTrigger"/> instance
            /// </summary>
            /// <param name="joystick">The <see cref="VirtualJoystick"/></param>
            /// <param name="mode">The <see cref="VirtualInput.ThresholdModes"/></param>
            /// <param name="threshold">The amount to trigger a postive</param>
            public VirtualJoystickYTrigger(VirtualJoystick joystick, VirtualInput.ThresholdModes mode, float threshold)
            {
                this.joystick = joystick;
                Mode = mode;
                Threshold = threshold;
            }

            /// <summary>
            ///     Checks if this is held down
            /// </summary>
            public override bool Check
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold;
                    else
                        return joystick.Value.X == Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was pressed
            /// </summary>
            public override bool Pressed
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return joystick.Value.X >= Threshold && joystick.PreviousValue.X < Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return joystick.Value.X <= Threshold && joystick.PreviousValue.X > Threshold;
                    else
                        return joystick.Value.X == Threshold && joystick.PreviousValue.X != Threshold;
                }
            }

            /// <summary>
            ///     Checks if this was release
            /// </summary>
            public override bool Released
            {
                get
                {
                    if (Mode == VirtualInput.ThresholdModes.LargerThan)
                        return joystick.Value.X < Threshold && joystick.PreviousValue.X >= Threshold;
                    else if (Mode == VirtualInput.ThresholdModes.LessThan)
                        return joystick.Value.X > Threshold && joystick.PreviousValue.X <= Threshold;
                    else
                        return joystick.Value.X != Threshold && joystick.PreviousValue.X == Threshold;
                }
            }
        }

        #endregion
    }
}
