﻿//--------------------------------------------------------------------------------
//  VitualAxis
//
// A virtual input represented as a float between -1 and 1
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using System.Collections.Generic;

namespace Psiren.Input
{
    public class VirtualAxis : VirtualInput
    {
        /// <summary>
        ///     List of <see cref="Node"/> that represent this <see cref="VirtualAxis"/>
        /// </summary>
        public List<Node> Nodes;

        /// <summary>
        ///     The current value
        /// </summary>
        public float Value { get; private set; }

        /// <summary>
        ///     The previous value
        /// </summary>
        public float PreviousValue { get; private set; }

        /// <summary>
        ///     Creates a new <see cref="VirtualAxis"/> instance
        /// </summary>
        public VirtualAxis() : base()
        {
            Nodes = new List<Node>();
        }

        /// <summary>
        ///     Creates a new <see cref="VirtualAxis"/> instance
        /// </summary>
        /// <param name="nodes">The <see cref="Node"/>s to give to this</param>
        public VirtualAxis(params Node[] nodes) : base()
        {
            Nodes = new List<Node>(nodes);
        }

        /// <summary>
        ///     Updates the <see cref="VirtualAxis"/>
        /// </summary>
        public override void Update()
        {
            foreach (var node in Nodes)
                node.Update();

            PreviousValue = Value;
            Value = 0;
            foreach (var node in Nodes)
            {
                float value = node.Value;
                if (value != 0)
                {
                    Value = value;
                    break;
                }
            }
        }

        /// <summary>
        ///     allows implicit converstion of the <see cref="VirtualAxis"/> to a float
        /// </summary>
        /// <param name="axis"></param>
        public static implicit operator float(VirtualAxis axis)
        {
            return axis.Value;
        }

        /// <summary>
        ///     <see cref="VirtualAxis.Node"/> for a <see cref="VirtualAxis"/>
        /// </summary>
        public abstract class Node : VirtualInputNode
        {
            public abstract float Value { get; }
        }


        public class TouchSwipeVerticle : Node
        {
            public TouchSwipeVerticle()
            {

            }

            public override float Value
            {
                get
                {
                    if ((MInput.Touches.Gesture.GestureType & GestureType.VerticalDrag) == GestureType.VerticalDrag)
                    {
                        return MInput.Touches.Gesture.Delta.Y < 0 ? -1 :
                            MInput.Touches.Gesture.Delta.Y > 0 ? 1 : 0;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }


        public class TouchSwipHorizontal : Node
        {
            public TouchSwipHorizontal() { }
            public override float Value
            {
                get
                {
                    if ((MInput.Touches.Gesture.GestureType & GestureType.HorizontalDrag) == GestureType.HorizontalDrag)
                    {
                        return MInput.Touches.Gesture.Delta.X < 0 ? -1 :
                            MInput.Touches.Gesture.Delta.X > 0 ? 1 : 0;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }



        /// <summary>
        ///    <see cref="VirtualAxis.PadLeftStickX"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class PadLeftStickX : Node
        {
            /// <summary>
            ///     The index of the gamepad this is for
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.PadLeftStickX"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The index of the gamepad</param>
            /// <param name="deadzone">The amount of deadzone</param>
            public PadLeftStickX(int gamepadIndex, float deadzone)
            {
                GamepadIndex = gamepadIndex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get
                {
                    return Calc.SignThreshold(MInput.GamePads[GamepadIndex].GetLeftStick().X, Deadzone);
                }
            }
        }

        /// <summary>
        ///    <see cref="VirtualAxis.PadLeftStickY"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class PadLeftStickY : Node
        {
            /// <summary>
            ///     The index of the gamepad this is for
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.PadLeftStickY"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The index of the gamepad</param>
            /// <param name="deadzone">The amount of deadzone</param>
            public PadLeftStickY(int gamepadIndex, float deadzone)
            {
                GamepadIndex = gamepadIndex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get
                {
                    return Calc.SignThreshold(MInput.GamePads[GamepadIndex].GetLeftStick().Y, Deadzone);
                }
            }
        }

        /// <summary>
        ///    <see cref="VirtualAxis.PadRightStickX"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class PadRightStickX : Node
        {
            /// <summary>
            ///     The index of the gamepad this is for
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.PadRightStickX"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The index of the gamepad</param>
            /// <param name="deadzone">The amount of deadzone</param>
            public PadRightStickX(int gamepadIndex, float deadzone)
            {
                GamepadIndex = gamepadIndex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get
                {
                    return Calc.SignThreshold(MInput.GamePads[GamepadIndex].GetRightStick().X, Deadzone);
                }
            }
        }

        /// <summary>
        ///    <see cref="VirtualAxis.PadRightStickY"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class PadRightStickY : Node
        {
            /// <summary>
            ///     The index of the gamepad this is for
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.PadRightStickY"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The index of the gamepad</param>
            /// <param name="deadzone">The amount of deadzone</param>
            public PadRightStickY(int gamepadIndex, float deadzone)
            {
                GamepadIndex = gamepadIndex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get
                {
                    return Calc.SignThreshold(MInput.GamePads[GamepadIndex].GetRightStick().Y, Deadzone);
                }
            }
        }

        /// <summary>
        ///    <see cref="VirtualAxis.PadDpadLeftRight"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class PadDpadLeftRight : Node
        {
            /// <summary>
            ///     The index of the gamepad this is for
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.PadDpadLeftRight"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The index of the gamepad</param>
            public PadDpadLeftRight(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get
                {
                    if (MInput.GamePads[GamepadIndex].DPadRightCheck)
                        return 1f;
                    else if (MInput.GamePads[GamepadIndex].DPadLeftCheck)
                        return -1f;
                    else
                        return 0;
                }
            }
        }

        /// <summary>
        ///    <see cref="VirtualAxis.PadDpadUpDown"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class PadDpadUpDown : Node
        {
            /// <summary>
            ///     The index of the gamepad this is for
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.PadDpadUpDown"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The index of the gamepad</param>
            public PadDpadUpDown(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get
                {
                    if (MInput.GamePads[GamepadIndex].DPadDownCheck)
                        return 1f;
                    else if (MInput.GamePads[GamepadIndex].DPadUpCheck)
                        return -1f;
                    else
                        return 0;
                }
            }
        }

        /// <summary>
        ///    <see cref="VirtualAxis.KeyboardKeys"/>  for a <see cref="VirtualAxis"/>
        /// </summary>
        public class KeyboardKeys : Node
        {
            /// <summary>
            ///     The <see cref="OverlapBehavior"/> to use if <see cref="Positive"/> and <see cref="Negative"/> overlap
            /// </summary>
            public OverlapBehaviors OverlapBehavior;

            /// <summary>
            ///     The key that represents a postive axis
            /// </summary>
            public Keys Positive;

            /// <summary>
            ///     The key that represents a negative axis
            /// </summary>
            public Keys Negative;

            // The current value
            private float value;


            private bool turned;

            /// <summary>
            ///     Creates a new <see cref="VirtualAxis.KeyboardKeys"/> instance
            /// </summary>
            /// <param name="overlapBehavior">The <see cref="OverlapBehavior"/> to use if the <paramref name="positive"/> and <paramref name="negative"/> overlap</param>
            /// <param name="negative">The key that represents a postive on the axis</param>
            /// <param name="positive">The key that represents a negative on the axis</param>
            public KeyboardKeys(OverlapBehaviors overlapBehavior, Keys negative, Keys positive)
            {
                OverlapBehavior = overlapBehavior;
                Negative = negative;
                Positive = positive;
            }

            /// <summary>
            ///     Udpates this <see cref="VirtualAxis.KeyboardKeys"/>
            /// </summary>
            public override void Update()
            {
                if (MInput.Keyboard.Check(Positive))
                {
                    if (MInput.Keyboard.Check(Negative))
                    {
                        switch (OverlapBehavior)
                        {
                            default:
                            case OverlapBehaviors.CancelOut:
                                value = 0;
                                break;

                            case OverlapBehaviors.TakeNewer:
                                if (!turned)
                                {
                                    value *= -1;
                                    turned = true;
                                }
                                break;

                            case OverlapBehaviors.TakeOlder:
                                //value stays the same
                                break;
                        }
                    }
                    else
                    {
                        turned = false;
                        value = 1;
                    }
                }
                else if (MInput.Keyboard.Check(Negative))
                {
                    turned = false;
                    value = -1;
                }
                else
                {
                    turned = false;
                    value = 0;
                }
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override float Value
            {
                get { return value; }
            }
        }
    }
}
