﻿//--------------------------------------------------------------------------------
//  VirtualJoystick
//
//  A virtual input that is represented as a Vector2, with both X and Y as values between -1 and 1
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Psiren.Input
{
    public class VirtualJoystick : VirtualInput
    {
        /// <summary>
        ///     List of <see cref="Node"/> represented by this <see cref="VirtualJoystick"/>
        /// </summary>
        public List<Node> Nodes;

        /// <summary>
        ///     Is it normalizd
        /// </summary>
        public bool Normalized;


        public float? SnapSlices;

        /// <summary>
        ///     The current value
        /// </summary>
        public Vector2 Value { get; private set; }

        /// <summary>
        ///     The previous value
        /// </summary>
        public Vector2 PreviousValue { get; private set; }

        /// <summary>
        ///     Creates a new <see cref="VirtualJoystick"/> instance
        /// </summary>
        /// <param name="normalized">Should the values be normalized</param>
        public VirtualJoystick(bool normalized) : base()
        {
            Nodes = new List<Node>();
            Normalized = normalized;
        }

        /// <summary>
        ///     Creates a new <see cref="VirtualJoystick"/> instance
        /// </summary>
        /// <param name="normalized">Should the values be normalized</param>
        /// <param name="nodes">The <see cref="Node"/>s to represent</param>
        public VirtualJoystick(bool normalized, params Node[] nodes) : base()
        {
            Nodes = new List<Node>(nodes);
            Normalized = normalized;
        }

        /// <summary>
        ///     Updates this
        /// </summary>
        public override void Update()
        {
            foreach (var node in Nodes)
                node.Update();

            PreviousValue = Value;
            Value = Vector2.Zero;
            foreach (var node in Nodes)
            {
                Vector2 value = node.Value;
                if (value != Vector2.Zero)
                {
                    if (Normalized)
                    {
                        if (SnapSlices.HasValue)
                            value = value.SnappedNormal(SnapSlices.Value);
                        else
                            value.Normalize();
                    }
                    else if (SnapSlices.HasValue)
                        value = value.Snapped(SnapSlices.Value);

                    Value = value;
                    break;
                }
            }
        }

        /// <summary>
        ///     Implicitly converts this <see cref="VirtualJoystick"/> to a Vector2 based on <see cref="Value"/>
        /// </summary>
        /// <param name="joystick"></param>
        public static implicit operator Vector2(VirtualJoystick joystick)
        {
            return joystick.Value;
        }

        /// <summary>
        ///     The thing that is represented
        /// </summary>
        public abstract class Node : VirtualInputNode
        {
            /// <summary>
            ///     The value of the node
            /// </summary>
            public abstract Vector2 Value { get; }
        }


        /// <summary>
        ///     <see cref="VirtualJoystick.PadLeftStick"/> for a <see cref="VirtualJoystick"/>
        /// </summary>
        public class PadLeftStick : Node
        {
            /// <summary>
            ///     The gamepad index
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Creates a new <see cref="VirtualJoystick.PadLeftStick"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            /// <param name="deadzone">The amount of deadzone</param>
            public PadLeftStick(int gamepadIndex, float deadzone)
            {
                GamepadIndex = gamepadIndex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override Vector2 Value
            {
                get
                {
                    return MInput.GamePads[GamepadIndex].GetLeftStick(Deadzone);
                }
            }
        }

        /// <summary>
        ///     <see cref="VirtualJoystick.PadRightStick"/> for a <see cref="VirtualJoystick"/>
        /// </summary>
        public class PadRightStick : Node
        {
            /// <summary>
            ///     The gamepad index
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     The amount of deadzone
            /// </summary>
            public float Deadzone;

            /// <summary>
            ///     Creates a new <see cref="VirtualJoystick.PadRightStick"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            /// <param name="deadzone">The amount of deadzone</param>
            public PadRightStick(int gamepadIndex, float deadzone)
            {
                GamepadIndex = gamepadIndex;
                Deadzone = deadzone;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override Vector2 Value
            {
                get
                {
                    return MInput.GamePads[GamepadIndex].GetRightStick(Deadzone);
                }
            }
        }

        /// <summary>
        ///     <see cref="VirtualJoystick.PadDpad"/> for a <see cref="VirtualJoystick"/>
        /// </summary>
        public class PadDpad : Node
        {
            /// <summary>
            ///     The gamepad index
            /// </summary>
            public int GamepadIndex;

            /// <summary>
            ///     Creates a new <see cref="VirtualJoystick.PadDpad"/> instance
            /// </summary>
            /// <param name="gamepadIndex">The gamepad index</param>
            public PadDpad(int gamepadIndex)
            {
                GamepadIndex = gamepadIndex;
            }

            /// <summary>
            ///     The current value
            /// </summary>
            public override Vector2 Value
            {
                get
                {
                    Vector2 value = Vector2.Zero;

                    if (MInput.GamePads[GamepadIndex].DPadRightCheck)
                        value.X = 1f;
                    else if (MInput.GamePads[GamepadIndex].DPadLeftCheck)
                        value.X = -1f;

                    if (MInput.GamePads[GamepadIndex].DPadDownCheck)
                        value.Y = 1f;
                    else if (MInput.GamePads[GamepadIndex].DPadUpCheck)
                        value.Y = -1f;

                    return value;
                }
            }
        }

        /// <summary>
        ///     <see cref="VirtualJoystick.KeyboardKeys"/> for a <see cref="VirtualJoystick"/>
        /// </summary>
        public class KeyboardKeys : Node
        {
            /// <summary>
            ///     The <see cref="OverlapBehavior"/> to use when more than one check passes
            /// </summary>
            public OverlapBehaviors OverlapBehavior;

            /// <summary>
            ///     The key to represent left
            /// </summary>
            public Keys Left;

            /// <summary>
            ///     The key to represent right
            /// </summary>
            public Keys Right;

            /// <summary>
            ///     The key to represent up
            /// </summary>
            public Keys Up;

            /// <summary>
            ///     The key to represent down
            /// </summary>
            public Keys Down;

            private bool turnedX;
            private bool turnedY;

            /// <summary>
            ///     The current value
            /// </summary>
            private Vector2 value;

            /// <summary>
            ///     Creates a new <see cref="VirtualJoystick.KeyboardKeys"/> instance
            /// </summary>
            /// <param name="overlapBehavior">The <see cref="OverlapBehavior"/> to use when more than one check passes</param>
            /// <param name="left">The key to represent left</param>
            /// <param name="right">The key to represent right</param>
            /// <param name="up">The key to represent up</param>
            /// <param name="down">The key to represent down</param>
            public KeyboardKeys(OverlapBehaviors overlapBehavior, Keys left, Keys right, Keys up, Keys down)
            {
                OverlapBehavior = overlapBehavior;
                Left = left;
                Right = right;
                Up = up;
                Down = down;
            }

            /// <summary>
            ///     Updates this
            /// </summary>
            public override void Update()
            {
                //X Axis
                if (MInput.Keyboard.Check(Left))
                {
                    if (MInput.Keyboard.Check(Right))
                    {
                        switch (OverlapBehavior)
                        {
                            default:
                            case OverlapBehaviors.CancelOut:
                                value.X = 0;
                                break;

                            case OverlapBehaviors.TakeNewer:
                                if (!turnedX)
                                {
                                    value.X *= -1;
                                    turnedX = true;
                                }
                                break;

                            case OverlapBehaviors.TakeOlder:
                                //X stays the same
                                break;
                        }
                    }
                    else
                    {
                        turnedX = false;
                        value.X = -1;
                    }
                }
                else if (MInput.Keyboard.Check(Right))
                {
                    turnedX = false;
                    value.X = 1;
                }
                else
                {
                    turnedX = false;
                    value.X = 0;
                }

                //Y Axis
                if (MInput.Keyboard.Check(Up))
                {
                    if (MInput.Keyboard.Check(Down))
                    {
                        switch (OverlapBehavior)
                        {
                            default:
                            case OverlapBehaviors.CancelOut:
                                value.Y = 0;
                                break;

                            case OverlapBehaviors.TakeNewer:
                                if (!turnedY)
                                {
                                    value.Y *= -1;
                                    turnedY = true;
                                }
                                break;

                            case OverlapBehaviors.TakeOlder:
                                //Y stays the same
                                break;
                        }
                    }
                    else
                    {
                        turnedY = false;
                        value.Y = -1;
                    }
                }
                else if (MInput.Keyboard.Check(Down))
                {
                    turnedY = false;
                    value.Y = 1;
                }
                else
                {
                    turnedY = false;
                    value.Y = 0;
                }
            }


            /// <summary>
            ///     The current value
            /// </summary>
            public override Vector2 Value
            {
                get { return value; }
            }
        }
    }


}
