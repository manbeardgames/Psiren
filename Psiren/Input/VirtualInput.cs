﻿//--------------------------------------------------------------------------------
//  Virtual Input
//
// Represents a virtual button, axis or joystick whose state is determined by the 
// state of its VirtualInputNodes
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

namespace Psiren.Input
{
    public abstract class VirtualInput
    {
        /// <summary>
        ///     Definitions of behaviors when inputs overlap
        /// </summary>
        public enum OverlapBehaviors { CancelOut, TakeOlder, TakeNewer };

        /// <summary>
        ///     Definitions of modes to use with thresholds
        /// </summary>
        public enum ThresholdModes { LargerThan, LessThan, EqualTo };

        /// <summary>
        ///     Creates a new <see cref="VirtualInput"/> instance
        /// </summary>
        public VirtualInput()
        {
            MInput.VirtualInputs.Add(this);
        }

        /// <summary>
        ///     Deregisters the <see cref="VirtualInput"/> from <see cref="MInput"/>
        /// </summary>
        public void Deregister()
        {
            MInput.VirtualInputs.Remove(this);
        }

        /// <summary>
        ///     Udpates the <see cref="VirtualInput"/>
        /// </summary>
        public abstract void Update();
    }

    /// <summary>
    /// Add these to your VirtualInput to define how it determines its current input state. 
    /// For example, if you want to check whether a keyboard key is pressed, create a VirtualButton and add to it a VirtualButton.KeyboardKey
    /// </summary>
    public abstract class VirtualInputNode
    {
        public virtual void Update()
        {

        }
    }
}
