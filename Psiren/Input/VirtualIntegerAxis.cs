﻿//--------------------------------------------------------------------------------
//  VirtualIntegerAxis
//
//  A virtual input that is represented as a int that is either -1, 0, or 1
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

namespace Psiren.Input
{
    public class VirtualIntegerAxis : VirtualInput
    {
        /// <summary>
        ///     List of <see cref="VirtualAxis.Node"/>s this <see cref="VirtualIntegerAxis"/> represents
        /// </summary>
        public List<VirtualAxis.Node> Nodes;

        /// <summary>
        ///     The current value
        /// </summary>
        public int Value;

        /// <summary>
        ///     The previous value
        /// </summary>
        public int PreviousValue { get; private set; }

        /// <summary>
        ///     Creates a new <see cref="VirtualIntegerAxis"/> instance
        /// </summary>
        public VirtualIntegerAxis() : base()
        {
            Nodes = new List<VirtualAxis.Node>();
        }

        /// <summary>
        ///     Creates a new <see cref="VirtualIntegerAxis"/> instances
        /// </summary>
        /// <param name="nodes">The <see cref="VirtualAxis.Node"/>s to use</param>
        public VirtualIntegerAxis(params VirtualAxis.Node[] nodes) : base()
        {
            Nodes = new List<VirtualAxis.Node>(nodes);
        }

        /// <summary>
        ///     Updates this
        /// </summary>
        public override void Update()
        {
            foreach (var node in Nodes)
                node.Update();

            PreviousValue = Value;
            Value = 0;
            foreach (var node in Nodes)
            {
                float value = node.Value;
                if (value != 0)
                {
                    Value = Math.Sign(value);
                    break;
                }
            }
        }

        /// <summary>
        ///     Implicitly converts this <see cref="VirtualIntegerAxis"/> to an int value based on <see cref="Value"/>
        /// </summary>
        /// <param name="axis"></param>
        public static implicit operator int(VirtualIntegerAxis axis)
        {
            return axis.Value;
        }
    }
}
