﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Components.Graphics;
using Psiren.Components.Graphics.Text;
using Psiren.Components.Logic;
using Psiren.Graphics;
using System;
using System.Collections;

namespace Psiren.Entities
{

    public abstract class SelectableButton : Entity
    {
        protected enum States
        {
            Idle,
            Selected,
            Deselected
        }

        public SpriteFont Font
        {
            get { return _font; }
            set { _font = value; }
        }
        protected SpriteFont _font;

        public float FontScale
        {
            get { return _fontScale; }
            set { _fontScale = value; }
        }
        protected float _fontScale;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        protected string _label;

        protected Selectable _selectable;
        protected StateMachine _stateMachine;

        protected Vector2 _startPosition;

        public float AnimationDuration { get => _animationDuration; set => _animationDuration = value; }
        protected float _animationDuration;

        public Vector2 AnimateToOffset { get => _animateToOffset; set => _animateToOffset = value; }
        protected Vector2 _animateToOffset;

        public Vector2 AnimateBackOffset { get => _animateBackOffset; set => _animateBackOffset = value; }
        protected Vector2 _animateBackOffset;

        protected bool _canAcceptInput;

        public Action OnSelected { get => _onSelected; set => _onSelected = value; }
        private Action _onSelected;

        public Action OnDeselect { get => _onDeselect; set => _onDeselect = value; }
        private Action _onDeselect;

        public Action OnMenuBack { get => _onMenuBack; set => _onMenuBack = value; }
        private Action _onMenuBack;


        public new float Width
        {
            get { return _width; }
            set
            {
                if (_width == value) { return; }
                _width = value;
                if (Get<NineSlice>() is NineSlice nineSlice)
                {
                    nineSlice.Width = (int)value;
                }
            }
        }
        protected float _width;

        public new float Height
        {
            get { return _height; }
            set
            {
                if (_height == value) { return; }
                _height = value;
                if (Get<NineSlice>() is NineSlice nineSlice)
                {
                    nineSlice.Height = (int)value;
                }
            }
        }
        protected float _height;


        public float LabelWidth { get { return _font.MeasureString(_label).X; } }
        public float LabelHeight { get { return _font.MeasureString(_label).Y; } }





        protected void Initialize()
        {
            //  Create Text component
            Text text = new Text(
                font: _font,
                text: _label,
                position: new Vector2(
                    x: _width / 2.0f - _font.MeasureString(_label).X / 2.0f,
                    y: _height / 2.0f - _font.MeasureString(_label).Y / 2.0f),
                color: Color.White,
                horizontalAlign: Text.HorizontalAlign.Left,
                verticalAlign: Text.VerticalAlign.Top);
            this.Add(text);

            //  Create selectable component
            _selectable = new Selectable();
            _selectable.AddTarget(text);
            this.Add(_selectable);

            //  Create StateMachine
            _stateMachine = new StateMachine(3);
            _stateMachine.SetCallbacks((int)States.Idle, IdelUpdate, null, null, null);
            _stateMachine.SetCallbacks((int)States.Selected, SelectedUpdate, SelectedRoutine, null, null);
            _stateMachine.SetCallbacks((int)States.Deselected, DeselectedUpdate, DeselectedRoutine, null, null);
            this.Add(_stateMachine);
        }


        public override void Update()
        {
            base.Update();
            //if (Get<Text>() is Text text)
            //{
            //    text.Position = new Vector2(
            //        x: _width / 2.0f - _font.MeasureString(_label).X / 2.0f,
            //        y: _height / 2.0f - _font.MeasureString(_label).Y / 2.0f);
            //}

            if (_canAcceptInput == false)
            {
                _canAcceptInput = true;
            }
            else
            {
                if (_selectable.IsSelected)
                {
                    if (Engine.InputProfiles[PlayerNumber.One].MenuUp.Pressed)
                    {
                        _selectable.OnUpPressed?.Invoke();
                    }
                    else if (Engine.InputProfiles[PlayerNumber.One].MenuDown.Pressed)
                    {
                        _selectable.OnDownPressed?.Invoke();
                    }
                    else if (Engine.InputProfiles[PlayerNumber.One].MenuLeft.Pressed)
                    {
                        _selectable.OnLeftPressed?.Invoke();
                    }
                    else if (Engine.InputProfiles[PlayerNumber.One].MenuRight.Pressed)
                    {
                        _selectable.OnRightPressed?.Invoke();
                    }
                }

                if (_canAcceptInput && _selectable.IsSelected)
                {
                    if (Engine.InputProfiles[PlayerNumber.One].MenuConfirm.Pressed)
                    {
                        _selectable.OnConfirmPressed?.Invoke();
                    }

                    if (Engine.InputProfiles[PlayerNumber.One].MenuCancel.Pressed || Engine.InputProfiles[PlayerNumber.One].MenuEsc.Pressed)
                    {
                        _onMenuBack?.Invoke();
                    }
                }


            }
        }

        protected virtual int IdelUpdate() { return (int)States.Idle; }
        protected virtual int SelectedUpdate() { return (int)States.Selected; }
        protected virtual int DeselectedUpdate() { return (int)States.Deselected; }
        protected abstract IEnumerator SelectedRoutine();
        protected abstract IEnumerator DeselectedRoutine();

        public virtual void Select()
        {
            _canAcceptInput = false;
            _selectable.IsSelected = true;
            _onSelected?.Invoke();
            _stateMachine.State = (int)States.Selected;
        }

        public virtual void Deselect()
        {
            _selectable.IsSelected = false;
            _onDeselect?.Invoke();
            _stateMachine.State = (int)States.Deselected;
        }

        public bool Disabled { get => _selectable.Disabled; set => _selectable.Disabled = value; }


        public void Enable()
        {
            _selectable.Disabled = false;
        }

        public void Disable()
        {
            _selectable.Disabled = true;
        }

        public SelectableButton UpPressed(Action onUpPressed)
        {
            _selectable.OnUpPressed = onUpPressed;
            return this;
        }



        public SelectableButton DownPressed(Action onDownPressed)
        {
            _selectable.OnDownPressed = onDownPressed;
            return this;
        }

        public SelectableButton LeftPressed(Action onLeftPressed)
        {
            _selectable.OnLeftPressed = onLeftPressed;
            return this;
        }

        public SelectableButton RightPressed(Action onRightPressed)
        {
            _selectable.OnRightPressed = onRightPressed;
            return this;
        }

        public SelectableButton ConfirmPressed(Action onConfirmPressed)
        {
            _selectable.OnConfirmPressed = onConfirmPressed;
            return this;
        }


        /// <summary>
        ///     Adds a <see cref="NineSlice"/> component to this menu for the background
        /// </summary>
        /// <param name="texture">The Texture to use</param>
        /// <param name="padding">The <see cref="Padding"/> definition for the texure</param>
        /// <returns></returns>
        public NineSlice AddNineSlice(MTexture texture, Padding padding)
        {
            NineSlice slice = new NineSlice(Vector2.Zero, texture, padding, (int)this.Width, (int)this.Height);
            this.Add(slice);
            //_selectable.AddTarget(slice);
            return slice;

        }


    }
}
