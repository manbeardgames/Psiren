﻿//--------------------------------------------------------------------------------
//  MenuSlider
//
//  A MenuOption of type int that allows moving between a range of values
//
//  Note: 
//      These are specifically designed to be used with the Menu entity only
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework.Graphics;
using System;

namespace Psiren.Entities.Menu.Items
{
    public class MenuSlider : MenuOption<int>
    {
        /// <summary>
        ///     Creates a new <see cref="MenuSlider"/> instance
        /// </summary>
        /// <param name="font">The font to use</param>
        /// <param name="label">The label to draw</param>
        /// <param name="values">The values</param>
        /// <param name="min">The minimum value</param>
        /// <param name="max">The maximum value</param>
        /// <param name="value">The value to set as the default selected</param>
        public MenuSlider(SpriteFont font, string label, Func<int, string> values, int min, int max, int value = -1)
            : base(font, label)
        {
            for (int index = min; index <= max; index++)
            {
                this.Add(values(index), index, value == index);
            }
        }

    }
}
