﻿//--------------------------------------------------------------------------------
//  MenuItem
//
//  The base class for all MenuItems to derive from.
//
//  Note: 
//      These are specifically designed to be used with the Menu entity only
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using System;

namespace Psiren.Entities.Menu.Items
{
    public abstract class MenuItem
    {
        /// <summary>
        ///     Is the item visible
        /// </summary>
        public bool Visible { get => _visible; set => _visible = value; }
        protected bool _visible = true;

        /// <summary>
        ///     Shoudl the width of this item be included when the 
        ///     container <see cref="Menu"/> measures its width
        /// </summary>
        public bool IncludeWidthInMeasurement { get => _includeWidthInMeasurement; set => _includeWidthInMeasurement = value; }
        protected bool _includeWidthInMeasurement = true;


        /// <summary>
        ///     Is this item selectable
        /// </summary>
        public bool Selectable { get => _selectable; set => _selectable = value; }
        protected bool _selectable;

        /// <summary>
        ///     Is this item currently disabled
        /// </summary>
        public bool Disabled { get => _disabled; set => _disabled = value; }
        protected bool _disabled;

        /// <summary>
        ///     Is this item hoverable
        /// </summary>
        public bool Hoverable
        {
            get
            {
                if (_selectable && _visible)
                {
                    return !_disabled;
                }
                return false;
            }
        }

        /// <summary>
        ///     The <see cref="Menu"/> that is the container of this item
        /// </summary>
        public Menu Container { get => _container; set => _container = value; }
        protected Menu _container;

        //----------------------------------------------------------
        //  Actions
        //----------------------------------------------------------
        public Action OnEnter { get => _onEnter; set => _onEnter = value; }
        protected Action _onEnter;

        public Action OnLeave { get => _onLeave; set => _onLeave = value; }
        protected Action _onLeave;

        public Action OnPressed { get => _onPressed; set => _onPressed = value; }
        protected Action _onPressed;

        public Action OnUpdate { get => _onUpdate; set => _onUpdate = value; }
        protected Action _onUpdate;

        public Action OnDown { get => _onDown; set => _onDown = value; }
        protected Action _onDown;

        public Action OnUp { get => _onUp; set => _onUp = value; }
        protected Action _onUp;



        //----------------------------------------------------------
        //  Setting the actions via methods
        //  This allows chaining the setting such as
        //  item.Enter().Leave().Pressed()
        //----------------------------------------------------------
        public MenuItem Enter(Action onEnter)
        {
            _onEnter = onEnter;
            return this;
        }

        public MenuItem Leave(Action onLeave)
        {
            _onLeave = onLeave;
            return this;
        }

        public MenuItem Pressed(Action onPressed)
        {
            _onPressed = onPressed;
            return this;
        }

        public MenuItem Down(Action onDown)
        {
            _onDown = onDown;
            return this;
        }


        public MenuItem Up(Action onUp)
        {
            _onUp = onUp;
            return this;
        }

        public float Width
        {
            get
            {
                return LeftWidth() + RightWidth();
            }
        }

        public virtual void ConfirmPressed() { }
        public virtual void UpPressed() { }
        public virtual void DownPressed() { }
        public virtual void LeftPressed() { }
        public virtual void RightPressed() { }
        public virtual void Added() { }
        public virtual void Update() { }

        public virtual float LeftWidth() { return 0.0f; }
        public virtual float RightWidth() { return 0.0f; }

        public virtual float Height() { return 0.0f; }

        public virtual void Render(Vector2 position, bool highlighted) { }
    }
}
