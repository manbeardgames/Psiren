﻿//--------------------------------------------------------------------------------
//  MenuOption
//
//  A menu item with selectable options of type T
//
//  Note: 
//      These are specifically designed to be used with the Menu entity only
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Utils;
using System;
using System.Collections.Generic;

namespace Psiren.Entities.Menu.Items
{
    public class MenuOption<T> : MenuItem
    {
        /// <summary>
        ///     List of the values
        /// </summary>
        public List<Tuple<string, T>> Values = new List<Tuple<string, T>>();

        /// <summary>
        ///     The label to display
        /// </summary>
        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        private string _label;

        /// <summary>
        ///     The current selected value index
        /// </summary>
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }
        private int _index;


        /// <summary>
        ///     Action to perform when the value changes
        /// </summary>
        public Action<T> OnValueChange;


        //  The font to use
        private SpriteFont _font;

        //  Sine value used to make the < > move in and out
        private float _sine;

        //  The last direction we used to move values (-1 or 1) (left or right)
        private int _lastDir;

        /// <summary>
        ///     Creates a new <see cref="MenuOption{T}"/> instance
        /// </summary>
        /// <param name="font">The font to use</param>
        /// <param name="label">The label to display</param>
        public MenuOption(SpriteFont font, string label)
        {
            this._font = font;
            this.Label = label;
            this.Selectable = true;
        }

        /// <summary>
        ///     Adds a new value option
        /// </summary>
        /// <param name="label">The label of the option </param>
        /// <param name="value">The value of the option</param>
        /// <param name="selected">Shoudl it be selected after adding</param>
        /// <returns></returns>
        public MenuOption<T> Add(string label, T value, bool selected = false)
        {
            this.Values.Add(new Tuple<string, T>(label, value));
            if (selected)
            {
                this.Index = this.Values.Count - 1;
            }
            return this;
        }


        //public MenuOption<T> Change(Action<T> action)
        //{
        //    this.OnValueChange = action;
        //    return this;
        //}

        //public override void Added()
        //{
        //    //this.Container.InnerContent = TextMenu.InnerContentMode.TwoColum;
        //}

        /// <summary>
        ///     Adjusts the selected value to the left
        /// </summary>
        public override void LeftPressed()
        {
            if (_index <= 0) { return; }

            //  TODO: Add audio event

            _index--;
            _lastDir = -1;
            OnValueChange?.Invoke(this.Values[this.Index].Item2);
        }

        /// <summary>
        ///     Adjusts the selected value to the right
        /// </summary>
        public override void RightPressed()
        {
            if (this.Index >= this.Values.Count - 1) { return; }

            //  TODO: Add audio event
            _index++;
            _lastDir = 1;
            OnValueChange?.Invoke(this.Values[this.Index].Item2);
        }

        /// <summary>
        ///     Things to do when it's confirmed this has been pressed
        /// </summary>
        public override void ConfirmPressed()
        {
            if (this.Values.Count != 2) { return; }
            if (this.Index == 0)
            {
                //  TODO: Add audio event
            }
            else
            {
                //  TODO: Add audio event
            }
            this.Index = 1 - this.Index;
            this._lastDir = this.Index == 1 ? 1 : -1;
            this.OnValueChange?.Invoke(this.Values[this.Index].Item2);
        }

        /// <summary>
        ///     Updates the <see cref="MenuOption{T}"/>
        /// </summary>
        public override void Update()
        {
            this._sine += Engine.RawDeltaTime * 2.0f;
        }

        /// <summary>
        ///     The width (in pixles) of this for the left side
        ///     of the <see cref="Menu"/> as measured by the label.
        /// </summary>
        /// <returns></returns>
        public override float LeftWidth()
        {
            return _font.MeasureString(this.Label).X;
        }

        /// <summary>
        ///     The width (in pixles) of this for the right side
        ///     of the <see cref="Menu"/> as measured by the value label and value.
        /// </summary>
        /// <returns></returns>
        public override float RightWidth()
        {
            float val1 = 0.0f;
            foreach (Tuple<string, T> tuple in this.Values)
            {
                val1 = Math.Max(val1, _font.MeasureString(tuple.Item1).X);
            }
            return val1 + (_font.MeasureString("<").X + _font.MeasureString(">").X) + 20.0f;
        }

        /// <summary>
        ///     The height of the this as measured by the label
        /// </summary>
        /// <returns></returns>
        public override float Height()
        {
            return _font.MeasureString(this.Label).Y;
        }

        /// <summary>
        ///     Renders to the screen
        /// </summary>
        /// <param name="position">The xy-coordinate position relative to the container position</param>
        /// <param name="highlighted">Is this highlighted</param>
        public override void Render(Vector2 position, bool highlighted)
        {
            float alpha = _container.Alpha;

            Color labelColor = _disabled ? Color.DarkSlateGray : (highlighted ? _container.HightlightColor : Color.White) * alpha;
            Draw.SpriteBatch.DrawString(_font, Label, position, labelColor);

            if (Values.Count <= 0) return;

            float rightColumn = RightWidth();
            float itemWidth = _font.MeasureString(Values[_index].Item1).X;

            

            Draw.SpriteBatch.DrawString(
                spriteFont: _font,
                text: Values[_index].Item1,
                position: position + new Vector2((_container.InnerWidth - (rightColumn * 0.5f) - (itemWidth * 0.5f)), 0.0f),
                color: labelColor,
                scale: 1.0f,
                origin: Vector2.Zero,
                rotation: 0.0f,
                effects: SpriteEffects.None,
                layerDepth: 0.0f);

            Vector2 vector2 = Vector2.UnitX * (highlighted ? (float)(Math.Sin(_sine * 4.0f) * 4.0f) : 0.0f);

            bool leftBracketFlag = _index > 0;
            Color leftBracketColor = leftBracketFlag ? labelColor : Color.DarkSlateBlue * alpha;

            Draw.SpriteBatch.DrawString(
                spriteFont: _font,
                text: "<",
                position: position + new Vector2((_container.InnerWidth - rightColumn), 0.0f) - (leftBracketFlag ? vector2 : Vector2.Zero),
                color: leftBracketColor);

            bool rightBracketFlag = _index < Values.Count - 1;
            Color rightBracketColor = rightBracketFlag ? labelColor : Color.DarkSlateGray * alpha;

            Draw.SpriteBatch.DrawString(
                spriteFont: _font,
                text: ">",
                position: position + new Vector2((_container.InnerWidth - _font.MeasureString(">").X), 0.0f) + (rightBracketFlag ? vector2 : Vector2.Zero),
                color: rightBracketColor);
        }
    }
}
