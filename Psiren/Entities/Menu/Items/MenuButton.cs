﻿//--------------------------------------------------------------------------------
//  MenuButton
//
//  A button for a menu
//
//  Note: 
//      These are specifically designed to be used with the Menu entity only
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Utils;

namespace Psiren.Entities.Menu.Items
{
    public class MenuButton : MenuItem
    {
        /// <summary>
        ///     The font to use
        /// </summary>
        public SpriteFont Font
        {
            get { return _font; }
            set { _font = value; }
        }
        private SpriteFont _font;

        public float FontScale { get => _fontScale; set => _fontScale = value; }
        private float _fontScale = 1.0f;

        /// <summary>
        ///     The label (string) to display
        /// </summary>
        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
        private string _label;

        /// <summary>
        ///     Should the button always be in the center of the menu
        /// </summary>
        public bool AlwaysCenter
        {
            get { return _alwaysCenter; }
            set { _alwaysCenter = value; }
        }
        private bool _alwaysCenter;


        /// <summary>
        ///     Creates a new <see cref="MenuButton"/> instance
        /// </summary>
        /// <param name="font">The font to use</param>
        /// <param name="label">The label to use</param>
        public MenuButton(SpriteFont font, string label)
        {
            _font = font;
            _label = label;
            _selectable = true;
        }

        /// <summary>
        ///     Confirm that the button is pressed.
        /// </summary>
        public override void ConfirmPressed()
        {
            //  TODO: Add audio event here before base is called

            base.ConfirmPressed();
        }

        /// <summary>
        ///     The width (in pixles) of this button for the left side
        ///     of the <see cref="Menu"/> as measured by the label.
        ///     Buttons do not define a right width
        /// </summary>
        /// <returns></returns>
        public override float LeftWidth()
        {
            return _font.MeasureString(_label).X * _fontScale;
        }

        /// <summary>
        ///     The height of the button as measured by the label
        /// </summary>
        /// <returns></returns>
        public override float Height()
        {
            return _font.MeasureString(_label).Y * _fontScale;
        }



        /// <summary>
        ///     Renders the button to the screen
        /// </summary>
        /// <param name="position">The xy-coordinate position to render at relative to the top-left of the container</param>
        /// <param name="highlighted">Should it be rendered as highlighted</param>
        public override void Render(Vector2 position, bool highlighted)
        {
            float alpha = _container.Alpha;
            Color color = _disabled ? Color.DarkSlateGray : (highlighted ? _container.HightlightColor : Color.White) * alpha;
            Color fontColor = color * (alpha * alpha * alpha);
            bool flag = !this.AlwaysCenter;
            //Draw.SpriteBatch.DrawString(_font, _label, position + (flag ? Vector2.Zero : new Vector2(_container.Width * 0.5f, 0.0f)), fontColor);
            Draw.SpriteBatch.DrawString(_font, _label, Calc.Floor(position + (flag ? Vector2.Zero : new Vector2(_container.Width * 0.5f, 0.0f))), fontColor, 0.0f, Vector2.One, _fontScale, SpriteEffects.None, 0.0f);
        }

    }
}
