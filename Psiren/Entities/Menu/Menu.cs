﻿//--------------------------------------------------------------------------------
//  Menu
//
//  Entity that allows creating menus with selectable buttons and options
//
//  Note: 
//      This is designed to be used specifically with the MenuOption and MenuItem classes
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.Components.Graphics;
using Psiren.Entities;
using Psiren.Entities.Menu.Items;
using Psiren.Graphics;
using System;
using System.Collections.Generic;

namespace Psiren.Entities.Menu
{
    public class Menu : Entity
    {
        /// <summary>
        ///     Is this menu the current focused menu
        /// </summary>
        public bool Focused
        {
            get => _focused;
            set
            {
                if (_focused == value) { return; }
                _focused = value;
                if (value)
                {
                    _canAcceptInput = false;
                }
            }
        }
        private bool _focused = true;

        //  Can this menu accept input
        bool _canAcceptInput = true;



        //----------------------------------------------------------
        //  Item management
        //----------------------------------------------------------
        /// <summary>
        ///     Index of the currently selected item
        /// </summary>
        public int Selection { get => _selection; set => _selection = value; }
        private int _selection = -1;

        //  List of the MenuItems
        private List<MenuItem> _items = new List<MenuItem>();


        /// <summary>
        ///     Gets or Sets the currently selected item
        /// </summary>
        public MenuItem Current
        {
            get
            {
                if (_items.Count <= 0 || _selection < 0) { return null; }
                return _items[_selection];
            }
            set
            {
                _selection = _items.IndexOf(value);
            }
        }

        //----------------------------------------------------------
        //  Measurement stuff
        //----------------------------------------------------------
        /// <summary>
        ///     The amount of vertical space between items
        /// </summary>
        public float ItemSpacing { get => _itemSpacing; set => _itemSpacing = value; }
        private float _itemSpacing = 4.0f;

        /// <summary>
        ///     The width of the menu
        /// </summary>
        public new float Width { get => _width; private set => _width = value; }
        private float _width;

        /// <summary>
        ///     The width (in pixels) of the inner space of this menu
        ///     as defined by the width minus the left and right padding
        /// </summary>
        public float InnerWidth
        {
            get
            {
                return _width - _padding.Left - _padding.Right;
            }
        }
        /// <summary>
        ///     The minimum width of the menu. 
        /// </summary>
        public float MinWidth { get => _minWidth; set => _minWidth = value; }
        private float _minWidth;

        /// <summary>
        ///     The width of the left column of the menu
        /// </summary>
        public float LeftColumnWidth { get => _leftColumnWidth; set => _leftColumnWidth = value; }
        private float _leftColumnWidth;

        /// <summary>
        ///     The widht of the right column of the menu
        /// </summary>
        public float RightColumnWidth { get => _rightColumnWidth; set => _rightColumnWidth = value; }
        private float _rightColumnWidth;

        /// <summary>
        ///     The height of the menu
        /// </summary>
        public new float Height { get => _height; private set => _height = value; }
        private float _height;

        /// <summary>
        ///     The minimum height allowed for the menu
        /// </summary>
        public float MinHeight { get => _minHeight; set => _minHeight = value; }
        private float _minHeight;

        /// <summary>
        ///     The height (in pixels) of the inner space of this menu
        ///     as defined by the height minus the top and bottom padding
        /// </summary>
        public float InnerHeight { get => _height - _padding.Top - _padding.Bottom; }

        /// <summary>
        ///     The color to use when an item in the menu is highlighted (selected)
        /// </summary>
        public Color HightlightColor { get => _highlightColor; set => _highlightColor = value; }
        private Color _highlightColor = Color.Orange;

        /// <summary>
        ///     The alpha (transparency) of the rendering of the menu and
        ///     its items
        /// </summary>
        public float Alpha { get => _alpha; set => _alpha = value; }
        private float _alpha = 1.0f;

        /// <summary>
        ///     The <see cref="Padding"/> of the inside area of the this menu
        /// </summary>
        public Padding Padding { get => _padding; }
        private Padding _padding = Padding.Ten;






        //----------------------------------------------------------
        //  Actions
        //----------------------------------------------------------
        public Action OnEsc { get => _onEsc; set => _onEsc = value; }
        public Action _onEsc;

        public Action OnCancel { get => _onCancel; set => _onCancel = value; }
        public Action _onCancel;

        public Action OnUpdate { get => _onUpdate; set => _onUpdate = value; }
        public Action _onUpdate;

        public Action OnPause { get => _onPause; set => _onPause = value; }
        public Action _onPause;

        public Action OnClose { get => _onClose; set => _onClose = value; }
        public Action _onClose;





        //----------------------------------------------------------
        //  Initializations
        //----------------------------------------------------------
        /// <summary>
        ///     Creates a new <see cref="Menu"/> instance
        /// </summary>
        /// <param name="position"></param>
        /// <param name="padding"></param>
        public Menu(Vector2 position, Padding padding) : base(position) { _padding = padding; }


        //----------------------------------------------------------
        //  Adding / Removing Items
        //----------------------------------------------------------
        /// <summary>
        ///     Add a new Item to the menu
        /// </summary>
        /// <param name="item">The Menu.Item item to add</param>
        /// <returns>This menu</returns>
        public Menu Add(MenuItem item)
        {
            _items.Add(item);
            item.Container = this;
            if (_selection == -1)
            {
                this.FirstSelection();
            }
            this.RecalculateSize();
            item.Added();
            return this;
        }

        /// <summary>
        ///     Clears all items from the menu
        /// </summary>
        public void Clear()
        {
            this._items = new List<MenuItem>();
        }


        //----------------------------------------------------------
        //  Update and Render
        //----------------------------------------------------------
        /// <summary>
        ///     Updates the menu and all of its Menu.Item items
        /// </summary>
        public override void Update()
        {
            base.Update();

            _onUpdate?.Invoke();
            if (_focused)
            {
                if (!_canAcceptInput)
                {
                    _canAcceptInput = true;
                }
                else
                {
                    if (Engine.InputProfiles[PlayerNumber.One].MenuDown.Pressed)
                    {
                        Current.OnDown?.Invoke();
                        MoveSelection(1);
                    }
                    else if (Engine.InputProfiles[PlayerNumber.One].MenuUp.Pressed)
                    {
                        Current.OnUp?.Invoke();
                        MoveSelection(-1);
                    }

                    if (Current != null)
                    {
                        if (Engine.InputProfiles[PlayerNumber.One].MenuLeft.Pressed)
                        {
                            Current.LeftPressed();
                        }

                        if (Engine.InputProfiles[PlayerNumber.One].MenuRight.Pressed)
                        {
                            Current.RightPressed();
                        }

                        if (Engine.InputProfiles[PlayerNumber.One].MenuConfirm.Pressed)
                        {
                            Current.ConfirmPressed();
                            Current.OnPressed?.Invoke();
                        }
                    }

                    if (!Engine.InputProfiles[PlayerNumber.One].MenuConfirm.Pressed)
                    {
                        if (Engine.InputProfiles[PlayerNumber.One].MenuCancel.Pressed && _onCancel != null)
                        {
                            _onCancel();
                        }
                        if (Engine.InputProfiles[PlayerNumber.One].MenuEsc.Pressed && _onEsc != null)
                        {
                            _onEsc();
                        }
                    }
                }
            }

            foreach (MenuItem item in _items)
            {
                if (item.OnUpdate != null)
                {
                    item.OnUpdate();
                }
                item.Update();
            }

        }

        /// <summary>
        ///     Renders the Menu and its Menu.Item items to the screen
        /// </summary>
        public override void Render()
        {
            RecalculateSize();
            base.Render();
            Vector2 itemPosition = Position + new Vector2(_padding.Left, _padding.Top);
            foreach (MenuItem item in _items)
            {
                if (item.Visible)
                {
                    float height = item.Height();
                    item.Render(itemPosition, this._focused && this.Current == item);
                    itemPosition.Y += height + _itemSpacing;
                }
            }
        }


        //----------------------------------------------------------
        //  Helper Methods
        //----------------------------------------------------------
        /// <summary>
        ///     Gets the index of the given <see cref="MenuItem"/> item
        /// </summary>
        /// <param name="item">The <see cref="MenuItem"/>  item to find the index of</param>
        /// <returns>The (int) index of the <see cref="MenuItem"/>  in this menu's item list</returns>
        public int IndexOf(MenuItem item)
        {
            return this._items.IndexOf(item);
        }

        /// <summary>
        ///     Sets the first <see cref="MenuItem"/>  item in this menus' item as selected
        /// </summary>
        public void FirstSelection()
        {
            this._selection = -1;
            this.MoveSelection(1);
        }

        /// <summary>
        ///     Moves the selection of the <see cref="MenuItem"/>  items in the
        ///     direction given
        /// </summary>
        /// <param name="direction">A negative value will move up, and a postive value will move down</param>
        public void MoveSelection(int direction)
        {
            int selection = this._selection;
            direction = Math.Sign(direction);
            this._selection = Calc.Clamp(_selection + direction, 0, this._items.Count - 1);

            while (!Current.Hoverable && (_selection > 0 && direction < 0 || _selection < _items.Count - 1 && direction > 0))
            {
                _selection += direction;
            }

            if (!Current.Hoverable)
            {
                _selection = selection;
            }

            if (_selection == selection || Current == null)
            {
                return;
            }

            if (selection >= 0 && _items[selection] != null && _items[selection].OnLeave != null)
            {
                _items[selection].OnLeave();
            }

            if (Current.OnEnter != null)
            {
                Current.OnEnter();
            }



        }

        /// <summary>
        ///     Recalculates the size of the menu based on the <see cref="MenuItem"/>  items
        ///     within the menu
        /// </summary>
        public void RecalculateSize()
        {
            _leftColumnWidth = _rightColumnWidth = _height = 0.0f;

            foreach (MenuItem item in _items)
            {
                if (item.IncludeWidthInMeasurement)
                {
                    _leftColumnWidth = Math.Max(_leftColumnWidth, item.LeftWidth());
                }
            }
            foreach (MenuItem item in _items)
            {
                if (item.IncludeWidthInMeasurement)
                {
                    _rightColumnWidth = Math.Max(_rightColumnWidth, item.RightWidth());
                }
            }
            foreach (MenuItem obj in _items)
            {
                if (obj.Visible)
                    _height += obj.Height() + _itemSpacing;
            }
            _height -= _itemSpacing;
            _height = Math.Max(_minHeight, _height + _padding.Top + _padding.Bottom);
            _width = Math.Max(_minWidth, _leftColumnWidth + _rightColumnWidth + _padding.Right + _padding.Left);

            if (this.Get<NineSlice>() is NineSlice nineSlice)
            {
                nineSlice.Width = (int)_width;
                nineSlice.Height = (int)_height;
            }
        }

        public void MakeCenter()
        {
            var centerOfScreen = new Vector2(Engine.Width, Engine.Height) * 0.5f;

            var newPos = centerOfScreen - (new Vector2(Width, Height) * 0.5f);
            Position = newPos;
        }

        /// <summary>
        ///     Retrieves the Y offset of the <see cref="MenuItem"/> item given
        /// </summary>
        /// <param name="item">The Menu.Item item to get the Y offset of</param>
        /// <returns>The Y offset of the Menu.Item item given</returns>
        public float GetYOffsetOf(MenuItem item)
        {
            if (item == null) { return 0.0f; }
            float num = 0.0f;
            foreach (MenuItem obj in _items)
            {
                if (item.Visible)
                {
                    num += obj.Height() + _itemSpacing;
                }
                if (obj == item) { break; }
            }
            return num - item.Height() * 0.5f - _itemSpacing;
        }

        /// <summary>
        ///     Closes the menu
        /// </summary>
        public void Close()
        {
            //if (Current != null)
            //{
            Current?.OnLeave?.Invoke();
            //}

            _onClose?.Invoke();
            this.RemoveSelf();
        }

        /// <summary>
        ///     Adds a <see cref="NineSlice"/> component to this menu for the background
        /// </summary>
        /// <param name="texture">The Texture to use</param>
        /// <param name="padding">The <see cref="Padding"/> definition for the texure</param>
        /// <returns></returns>
        public NineSlice AddNineSlice(MTexture texture, Padding padding)
        {
            return this.Add(new NineSlice(Position, texture, padding, (int)this.Width, (int)this.Height));
        }

    }

}




