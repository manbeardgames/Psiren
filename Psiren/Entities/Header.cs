﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Components.Graphics;
using Psiren.Components.Graphics.Text;
using Psiren.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Entities
{
    public class Header : Entity
    {
        public new float Width { get => _width; set => _width = value; }
        private float _width;

        public new float Height { get => _height; set => _height = value; }
        private float _height;

        SpriteFont _font;
        string _label;

        public Header(SpriteFont font, string label, Text.HorizontalAlign horizontalAlign, Text.VerticalAlign verticalAlign, Vector2 position, float width, float height) : base(position)
        {

            _width = width;
            _height = height;
            _label = label;
            _font = font;

            Text text = new Text(
                font: font,
                text: label,
                position: new Vector2(_width, height) * 0.5f,
                horizontalAlign: horizontalAlign,
                verticalAlign: verticalAlign);
            this.Add(text);

        }


        public void AddNineslice(MTexture texture, Padding padding)
        {
            NineSlice nineSlice = new NineSlice(
                position: Vector2.Zero,
                texture: texture,
                padding: padding,
                width: (int)this.Width,
                height: (int)this.Height);

            this.Add(nineSlice);
        }
    }
}
