﻿//--------------------------------------------------------------------------------
//  TextArea
//  
//  Entity used to draw text within an area across multiple lines
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Components.Graphics;
using Psiren.Components.Graphics.Text;
using Psiren.Graphics;
using Psiren.Utils;
using System.Diagnostics;

namespace Psiren.Entities
{
    public class TextArea : Entity
    {
        /// <summary>
        ///     The width (in pixels)
        /// </summary>
        public new float Width
        {
            get => _width;
            set
            {
                if (_width == value) { return; }
                _width = value;
                if (Get<NineSlice>() is NineSlice nineslice)
                {
                    nineslice.Width = (int)value;
                }
            }
        }
        private float _width;

        /// <summary>
        ///     The Height (in pixels)
        /// </summary>
        public new float Height
        {
            get => _height;
            set
            {
                if (_height == value) { return; }
                _height = value;
                if (Get<NineSlice>() is NineSlice nineSlice)
                {
                    nineSlice.Height = (int)value;
                }
            }
        }
        private float _height;

        /// <summary>
        ///     The inner width as defined by the width minus the left and right <see cref="Padding"/>
        /// </summary>
        public float InnerWidth
        {
            get
            {
                if (Get<NineSlice>() is NineSlice nineSlice)
                {
                    return _width - nineSlice.Padding.Left - nineSlice.Padding.Right;
                }
                else
                {
                    return _width;
                }
            }

        }

        /// <summary>
        ///     The inner height as defined by the height minus the top and bottom <see cref="Padding"/>
        /// </summary>
        public float InnerHeight
        {
            get
            {
                if (Get<NineSlice>() is NineSlice nineSlice)
                {
                    return _height - nineSlice.Padding.Top - nineSlice.Padding.Bottom;
                }
                else
                {
                    return _height;
                }
            }

        }

        //  The multiline text component
        protected MultilineTextWithDelayComponent _messageBoxText;

        public MultilineTextWithDelayComponent MessageBoxText => _messageBoxText;


        protected TextArea() { }


        /// <summary>
        ///     Creates a new <see cref="TextArea"/> instance
        /// </summary>
        /// <param name="position">The xy-coordinate position</param>
        /// <param name="font">The font to use</param>
        /// <param name="width">The total width (in pixels)</param>
        /// <param name="height">The total height (in pixels)</param>
        public TextArea(Vector2 position, SpriteFont font, float width, float height)
        {

            _messageBoxText = new MultilineTextWithDelayComponent(
                font: font,
                text: "",
                size: new Vector2(width, height),
                position: Vector2.Zero,
                color: Color.White,
                maxLineWidth: (int)width,
                characterDelay: 0.0f);

            _width = width;
            _height = height;
            Debug.WriteLine($"W/H {Width} / {Height}");

            _position = position;
            this.Add(_messageBoxText);
        }

        /// <summary>
        ///     Creates a new <see cref="TextArea"/> instance
        /// </summary>
        /// <param name="position">The xy-coordinate position</param>
        /// <param name="font">The font to use</param>
        /// <param name="width">The total width (in pixels)</param>
        /// <param name="height">The total height (in pixels)</param>
        /// <param name="tag">The <see cref="BitTag"/> tag</param>
        public TextArea(Vector2 position, SpriteFont font, float width, float height, BitTag tag)
            : this(position, font, width, height)
        {
            this.Tag = tag;
        }

        public void SetCenterOrigin(bool to)
        {
            _messageBoxText.IsCenter = to;
        }


        /// <summary>
        ///     Set the text for the message box
        /// </summary>
        /// <param name="to">The text to set it to</param>
        public void SetText(string to)
        {
            _messageBoxText.Text = to;
        }

        public void SetColor(Color color)
        {
            _messageBoxText.Color = color;
        }

        /// <summary>
        ///     Start text redrawing
        /// </summary>
        private void ResetText()
        {
            _messageBoxText.Text = _messageBoxText.Text;
        }

        /// <summary>
        ///     Adds a <see cref="NineSlice"/> component 
        /// </summary>
        /// <param name="texture">The texture</param>
        /// <param name="padding">The <see cref="Padding"/> definition of the nine slice</param>
        /// <returns></returns>
        public NineSlice AddNineSlice(MTexture texture, Padding padding)
        {
            NineSlice result = this.Add(new NineSlice(Vector2.Zero, texture, padding, (int)this.Width, (int)this.Height));
            _messageBoxText.Position = new Vector2(result.Padding.Left, result.Padding.Top);
            _messageBoxText.MaxLineWidth = (int)InnerWidth;
            return result;
        }


        public void SetAlpha(float to)
        {
            NineSlice nineSlice = this.Get<NineSlice>();
            if(nineSlice != null)
            {
                nineSlice.Alpha = to;
            }

            _messageBoxText.Alpha = to;

        }



        public override void Render()
        {
            base.Render();
        }
    }
}
