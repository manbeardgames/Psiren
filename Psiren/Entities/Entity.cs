﻿//--------------------------------------------------------------------------------
//  Entity
//
//  Entities make up the items within a Scene
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.Colliders;
using Psiren.Components;
using Psiren.InternalUtilities;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Psiren.Entities
{
    [Tracked]
    public class Entity : IEnumerable<Component>, IEnumerable
    {
        /// <summary>
        ///     Is this active (updatable)
        /// </summary>
        public bool Active { get => this._active; set => this._active = value; }
        private bool _active = true;

        /// <summary>
        ///     Is this visible (drawable)
        /// </summary>
        public bool Visible { get => this._visible; set => this._visible = value; }
        private bool _visible = true;

        protected string _name = "";




        //----------------------------------------------------------
        //  Position
        //----------------------------------------------------------
        #region Position
        protected Vector2 _position;
        /// <summary>
        ///     The xy-coordinate position
        /// </summary>
        public Vector2 Position
        {
            get => _position;
            set
            {
                _position = value;
            }
        }

        /// <summary>
        ///     The x-coordinate
        /// </summary>
        public float X { get => _position.X; set => _position.X = value; }
        /// <summary>
        ///     The y-coordinate
        /// </summary>
        public float Y { get => _position.Y; set => _position.Y = value; }
        #endregion Position

        //----------------------------------------------------------
        //  Collider
        //----------------------------------------------------------
        #region Collider
        private bool _collidable = true;
        public bool Collidable { get => this._collidable; set => this._collidable = value; }
        private Collider _collider;
        public Collider Collider
        {
            get => _collider;
            set
            {
                if (_collider == value) { return; }
#if DEBUG
                if (value.Entity != null)
                {
                    throw new Exception("Setting an Entity's Collider to a Collider already in use by another Entity");
                }
#endif
                if (_collider != null)
                {
                    _collider.Removed();
                }
                _collider = value;
                if (_collider != null)
                {
                    _collider.Added(this);
                }
            }
        }

        /// <summary>
        ///     The collider width. 0.0f if no collider
        /// </summary>
        public float Width
        {
            get
            {
                if (_collider == null) { return 0.0f; }
                else { return _collider.Width; }
            }
        }

        /// <summary>
        ///     The collider height. 0.0f if no collider
        /// </summary>
        public float Height
        {
            get
            {
                if (_collider == null) { return 0.0f; }
                else { return _collider.Height; }
            }
        }

        /// <summary>
        ///     The x-coordinate of the left edge of the collider
        /// </summary>
        public float Left
        {
            get
            {
                if (_collider == null) { return X; }
                else { return Position.X + _collider.Left; }
            }
            set
            {
                if (_collider == null) { _position.X = value; }
                else { _position.X = value - _collider.Left; }
            }
        }

        /// <summary>
        ///     The x-coordinate of the right edge of the collider
        /// </summary>
        public float Right
        {
            get
            {
                if (_collider == null) { return Position.X; }
                else { return _position.X + _collider.Right; }
            }

            set
            {
                if (_collider == null) { _position.X = value; }
                else { _position.X = value - _collider.Right; }
            }
        }

        /// <summary>
        ///     The y-coordinate of the top edge of the collider
        /// </summary>
        public float Top
        {
            get
            {
                if (_collider == null) { return _position.Y; }
                else { return _position.Y + _collider.Top; }
            }

            set
            {
                if (_collider == null) { _position.Y = value; }
                else { _position.Y = value - _collider.Top; }
            }
        }

        /// <summary>
        ///     The y-coordinate of the bottom edge of the collider
        /// </summary>
        public float Bottom
        {
            get
            {
                if (_collider == null) { return _position.Y; }
                else { return _position.Y + _collider.Bottom; }
            }

            set
            {
                if (_collider == null) { _position.Y = value; }
                else { _position.Y = value - _collider.Bottom; }
            }
        }

        /// <summary>
        ///     The center x-coordinate of the collider
        /// </summary>
        public float CenterX
        {
            get
            {
                if (_collider == null) { return _position.X; }
                else { return _position.X + _collider.CenterX; }
            }

            set
            {
                if (_collider == null) { _position.X = value; }
                else { _position.X = value - _collider.CenterX; }
            }
        }

        /// <summary>
        ///     The center y-coordinate of the collider
        /// </summary>
        public float CenterY
        {
            get
            {
                if (_collider == null) { return _position.Y; }
                else { return _position.Y + _collider.CenterY; }
            }

            set
            {
                if (_collider == null) { _position.Y = value; }
                else { _position.Y = value - _collider.CenterY; }
            }
        }

        /// <summary>
        ///     The top left xy-coordinate of the collider
        /// </summary>
        public Vector2 TopLeft
        {
            get => new Vector2(Left, Top);
            set
            {
                Left = value.X;
                Top = value.Y;
            }
        }

        /// <summary>
        ///     The top right xy-coordinate of the collider
        /// </summary>
        public Vector2 TopRight
        {
            get => new Vector2(Right, Top);
            set
            {
                Right = value.X;
                Top = value.Y;
            }
        }

        /// <summary>
        ///     The bottom left xy-coordinate of the collider
        /// </summary>
        public Vector2 BottomLeft
        {
            get => new Vector2(Left, Bottom);
            set
            {
                Left = value.X;
                Bottom = value.Y;
            }
        }

        /// <summary>
        ///     The bottom right xy-coordinate of the collider
        /// </summary>
        public Vector2 BottomRight
        {
            get => new Vector2(Right, Bottom);
            set
            {
                Right = value.X;
                Bottom = value.Y;
            }
        }

        /// <summary>
        ///     The center xy-coordinate of the collider
        /// </summary>
        public Vector2 Center
        {
            get => new Vector2(CenterX, CenterY);
            set
            {
                CenterX = value.X;
                CenterY = value.Y;
            }
        }

        /// <summary>
        ///     The center y-coordinate along the left edge of the collider
        /// </summary>
        public Vector2 CenterLeft
        {
            get => new Vector2(Left, CenterY);
            set
            {
                Left = value.X;
                CenterY = value.Y;
            }
        }

        /// <summary>
        ///     The center y-coordinate along the right edge of the collider
        /// </summary>
        public Vector2 CenterRight
        {
            get => new Vector2(Right, CenterY);
            set
            {
                Right = value.X;
                CenterY = value.Y;
            }
        }

        /// <summary>
        ///     The center x-coordinate along the top edge of the collider
        /// </summary>
        public Vector2 TopCenter
        {
            get => new Vector2(CenterX, Top);
            set
            {
                CenterX = value.X;
                Top = value.Y;
            }
        }

        /// <summary>
        ///     The center x-coordinagte along the bottom edge of the collider
        /// </summary>
        public Vector2 BottomCenter
        {
            get => new Vector2(CenterX, Bottom);
            set
            {
                CenterX = value.X;
                Bottom = value.Y;
            }
        }
        #endregion Collider

        /// <summary>
        ///     The <see cref="Scene"/> this is in
        /// </summary>
        public Scene Scene { get => this._scene; private set => this._scene = value; }
        private Scene _scene;

        /// <summary>
        ///     The list of <see cref="Component"/>s attached to this
        /// </summary>
        public ComponentList Components { get => this._components; set => this._components = value; }
        private ComponentList _components;

        //----------------------------------------------------------
        //  Tags
        //----------------------------------------------------------
        #region Tags
        private int _tag;
        public int Tag
        {
            get => _tag;
            set
            {
                if (_tag != value)
                {
                    if (Scene != null)
                    {
                        for (int i = 0; i < BitTag.TotalTags; i++)
                        {
                            int check = 1 << i;
                            bool add = (value & check) != 0;
                            bool has = (Tag & check) != 0;

                            if (has != add)
                            {
                                if (add)
                                {
                                    Scene.TagLists[i].Add(this);
                                }
                                else
                                {
                                    Scene.TagLists[i].Remove(this);
                                }
                            }
                        }
                    }

                    _tag = value;
                }
            }
        }

        public bool TagFullCheck(int tag) => (_tag & tag) == tag;
        public bool TagCheck(int tag) => (_tag & tag) != 0;
        public void AddTag(int tag) => Tag |= tag;
        public void RemoveTag(int tag) => Tag &= ~tag;
        #endregion Tags


        internal int _depth = 0;
        public int Depth
        {
            get => _depth;
            set
            {
                if (_depth == value) { return; }
                _depth = value;
                if (_scene != null)
                {
                    _scene.SetActualDepth(this);
                }
            }
        }
        internal double _actualDepth = 0;



        /// <summary>
        ///     Creates a new <see cref="Entity"/> instance
        /// </summary>
        /// <param name="position">The xy-coordinate position</param>
        public Entity(Vector2 position)
        {
            _position = position;
            _components = new ComponentList(this);
        }

        /// <summary>
        ///     Creates a new <see cref="Entity"/> instance at xy-coordinate (0, 0)
        /// </summary>
        public Entity() : this(Vector2.Zero) { }

        /// <summary>
        ///     Call when a <see cref="Scene"/> is begining
        /// </summary>
        /// <param name="scene"></param>
        public virtual void SceneBegin(Scene scene) { }

        /// <summary>
        ///     Call when a <see cref="Scene"/> is ending
        /// </summary>
        /// <param name="scene"></param>
        public virtual void SceneEnd(Scene scene)
        {
            if (_components != null)
            {
                foreach (var c in _components)
                {
                    c.SceneEnd(scene);
                }
            }
        }

        /// <summary>
        ///     Called only once, on the first frame, this is added to the <see cref="Scene"/> or when it becomes active
        ///     for the first time
        /// </summary>
        /// <param name="scene"></param>
        public virtual void Awake(Scene scene)
        {
            if (_components != null)
            {
                foreach (var c in _components)
                {
                    c.EntityAwake();
                }
            }
        }

        /// <summary>
        ///     Called when this is added to a <see cref="Scene"/>
        /// </summary>
        /// <param name="scene"></param>
        public virtual void Added(Scene scene)
        {
            _scene = scene;
            if (_components != null)
            {
                foreach (var c in _components)
                {
                    c.EntityAdded(scene);
                }
            }
            _scene.SetActualDepth(this);
        }

        /// <summary>
        ///     Called when this is removed from a  <see cref="Scene"/>
        /// </summary>
        /// <param name="scene"></param>
        public virtual void Removed(Scene scene)
        {
            if (_components != null)
            {
                foreach (var c in _components)
                {
                    c.EntityRemoved(scene);
                }
            }
            Scene = null;
        }

        /// <summary>
        ///     Updates the <see cref="Component"/>s
        /// </summary>
        public virtual void Update()
        {
            _components.Update();
        }


        /// <summary>
        ///     Draws the <see cref="Component"/>s to the screen
        /// </summary>
        public virtual void Render()
        {
            _components.Render();
        }

        /// <summary>
        ///     Renders the <see cref="Collidable"/> if there is one
        ///     then calls the debug render method of all <see cref="Component"/>s 
        ///     attached to this
        /// </summary>
        /// <param name="camera"></param>
        public virtual void DebugRender(Camera camera)
        {
            if (_collider != null)
            {
                _collider.Render(camera, _collidable ? Color.Red : Color.DarkRed);

            }
            _components.DebugRender(camera);
        }

        /// <summary>
        ///     Called when the <see cref="Engine"/> detects a graphic reset
        /// </summary>
        public virtual void HandleGraphicsReset()
        {
            _components.HandleGraphicsReset();
        }

        /// <summary>
        ///     Called when the <see cref="Engine"/> detects graphics have been created
        /// </summary>
        public virtual void HandleGraphicsCreate()
        {
            _components.HandleGraphicsCreate();
        }

        /// <summary>
        ///     Removes this from the <see cref="Scene"/>
        /// </summary>
        public void RemoveSelf()
        {
            if (_scene != null)
            {
                _scene.Entities.Remove(this);
            }
        }

        //----------------------------------------------------------
        //  Helper Methods
        //----------------------------------------------------------
        #region Collision Shortcuts
        #region Collide Check
        public bool CollideCheck(Entity other) => Collide.Check(this, other);
        public bool CollideCheck(Entity other, Vector2 at) => Collide.Check(this, other, at);
        public bool CollideCheck(BitTag tag)
        {
#if DEBUG
            if (Scene == null)
            {
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            }
#endif
            return Collide.Check(this, Scene[tag]);
        }

        public bool CollideCheck(BitTag tag, Vector2 at)
        {
#if DEBUG
            if (Scene == null)
            {
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            }
#endif
            return Collide.Check(this, Scene[tag], at);
        }

        /// <summary>
        ///     Checks the collision of this entity against any entity within
        ///     the scene that has the given <see cref="BitTag"/> given at the 
        ///     position given
        /// </summary>
        /// <param name="tag">The <see cref="BitTag"/> of <see cref="Entity"/> objects within the scene to check against</param>
        /// <param name="at">The position to check for collision at</param>
        /// <param name="collidedWith">The <see cref="Entity"/> that was collided with, null if none</param>
        /// <returns>True if collided with an <see cref="Entity"/>, otherwise false</returns>
        /// <remarks>Psiren</remarks>
        public bool CollideCheck(BitTag tag, Vector2 at, out Entity collidedWith)
        {
#if DEBUG
            if(Scene == null)
            {
                throw new Exception("Can't collide check an Entity against a tag list when it is not a member of a Scene");
            }
#endif
            return Collide.Check(this, Scene[tag], at, out collidedWith);
        }

        public bool CollideCheck<T>() where T : Entity
        {
#if DEBUG
            if (Scene == null)
            {
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            }
#endif
            return Collide.Check(this, Scene.Tracker.Entities[typeof(T)]);

        }

        public bool CollideCheck<T>(Vector2 at) where T : Entity
        {
            return Collide.Check(this, Scene.Tracker.Entities[typeof(T)], at);
        }

        public bool CollideCheck<T, Exclude>() where T : Entity where Exclude : Entity
        {
#if DEBUG
            if (Scene == null)
            {
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            }
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(T)))
            {
                throw new Exception("Can't collide check an Entity against an untracked Entity type");
            }
            else if (!Scene.Tracker.Entities.ContainsKey(typeof(Exclude)))
            {
                throw new Exception("Excluded type is an untracked Entity type");
            }
#endif
            var exclude = Scene.Tracker.Entities[typeof(Exclude)];
            foreach (var e in Scene.Tracker.Entities[typeof(T)])
            {
                if (!exclude.Contains(e))
                {
                    if (Collide.Check(this, e))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool CollideCheck<T, Exclude>(Vector2 at) where T : Entity where Exclude : Entity
        {
            var was = Position;
            Position = at;
            var ret = CollideCheck<T, Exclude>();
            Position = was;
            return ret;
        }

        public bool CollideCheckByComponent<T>() where T : Component
        {
#if DEBUG
            if (Scene == null)
            {
                throw new Exception("Can't collide check an Entity against tracked Entities when it is not a member of a Scene");
            }
            else if (!Scene.Tracker.Components.ContainsKey(typeof(T)))
            {
                throw new Exception("Can't collide check an Entity against an untracked Component type");
            }
#endif
            foreach (var c in Scene.Tracker.Components[typeof(T)])
            {
                if (Collide.Check(this, c.Entity))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheckByComponent<T>(Vector2 at) where T : Component
        {
            Vector2 old = Position;
            Position = at;
            bool ret = CollideCheckByComponent<T>();
            Position = old;
            return ret;
        }
        #endregion Collide Check

        #region Collide Geometry
        public bool CollidePoint(Vector2 point)
        {
            return Collide.CheckPoint(this, point);
        }
        public bool CollidePoint(Vector2 point, Vector2 at)
        {
            return Collide.CheckPoint(this, point, at);
        }
        public bool CollideLine(Vector2 from, Vector2 to)
        {
            return Collide.CheckLine(this, from, to);
        }
        public bool CollideLine(Vector2 from, Vector2 to, Vector2 at)
        {
            return Collide.CheckLine(this, from, to, at);
        }
        public bool CollideRect(Rectangle rect)
        {
            return Collide.CheckRect(this, rect);
        }
        public bool CollideRect(Rectangle rect, Vector2 at)
        {
            return Collide.CheckRect(this, rect, at);
        }
        #endregion Collide Geometry
        #endregion Collision Shortcuts

        #region Components Shortcuts
        /// <summary>
        /// Adds a new component of type T to the entity and returns the component back
        /// as type T
        /// </summary>
        /// <typeparam name="T">The type of the component adding</typeparam>
        /// <param name="component">The component to add</param>
        /// <returns>The component add as type T</returns>
        /// <remarks>
        ///     Psiren
        /// </remarks>
        public T Add<T>(T component) where T : Component
        {
            _components.Add(component);
            return component as T;
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/> to this <see cref="Entity"/>
        /// </summary>
        /// <param name="component"></param>
        public void Add(Component component)
        {
            _components.Add(component);
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/>s to this <see cref="Entity"/>
        /// </summary>
        /// <param name="components"></param>
        public void Add(params Component[] components)
        {
            _components.Add(components);
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/> from this <see cref="Entity"/>
        /// </summary>
        /// <param name="component"></param>
        public void Remove(Component component)
        {
            _components.Remove(component);
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/>s from this <see cref="Entity"/>
        /// </summary>
        /// <param name="components"></param>
        public void Remove(params Component[] components)
        {
            _components.Remove(components);
        }

        public T Get<T>() where T : Component
        {
            return _components.Get<T>();
        }


        public IEnumerator<Component> GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion Components Shortcuts


        /// <summary>
        ///     Ges the <see cref="Scene"/> as type T that this <see cref="Entity"/>
        ///     is a part of
        /// </summary>
        /// <typeparam name="T">The derived type of <see cref="Scene"/></typeparam>
        /// <returns></returns>
        public T SceneAs<T>() where T : Scene
        {
            return Scene as T;
        }


    }
}
