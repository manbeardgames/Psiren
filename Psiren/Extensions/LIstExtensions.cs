﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Extensions
{
    public static class ListExtensions
    {

        /// <summary>
        ///     Turns a list of type T into a Queue of type T......it's magic
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static Queue<T> ToQueue<T>(this List<T> list)
        {
            Queue<T> queue = new Queue<T>();
            for (int i = 0; i < list.Count; i++)
            {
                queue.Enqueue(list[i]);
            }

            return queue;
        }


    }
}
