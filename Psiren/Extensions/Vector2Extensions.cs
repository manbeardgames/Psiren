﻿//------------------------------------------------------------
//  Vector2Extensions
//  Extension methods for the Vector2 class 
//
//-----------------------------Psiren Engine License-----------------------------

//    Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------

using Microsoft.Xna.Framework;

namespace Psiren.Extensions
{
    public static class Vector2Extenions
    {
        /// <summary>
        /// Checks if the Vector2 is a Up Vector2 (0.0f, -1.0f)
        /// </summary>
        /// <param name="vector">The Vector2 object to check</param>
        /// <returns>True if a Up Vector2, flase otherwise</returns>
        public static bool IsUp(this Vector2 vector) => vector == new Vector2(0, -1);

        /// <summary>
        /// Checks if the Vector2 is a Down Vector2 (0.0f, 1.0f)
        /// </summary>
        /// <param name="vector">The Vector2 object to check</param>
        /// <returns>True if a Down Vector2, flase otherwise</returns>
        public static bool IsDown(this Vector2 vector) => vector == new Vector2(0, 1);

        /// <summary>
        /// Checks if the Vector2 is a Left Vecto2 (-1.0f, 0.0f)
        /// </summary>
        /// <param name="vector">The Vector2 object to check</param>
        /// <returns>True if a Left Vector2, false otherwise</returns>
        public static bool IsLeft(this Vector2 vector) => vector == new Vector2(-1, 0);

        /// <summary>
        /// Checks if the Vector2 is a Right Vector2 (1.0f, 0.0f)
        /// </summary>
        /// <param name="vector">The Vector2 object to check</param>
        /// <returns>True if a Right Vector2, false otherwise</returns>
        public static bool IsRight(this Vector2 vector) => vector == new Vector2(1, 0);

    }
}
