﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Psiren.Components.Graphics.Aseprite;
using Psiren.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.ContentReaders
{
    public class AsepritePackageReader : ContentTypeReader<AsepriteDefinition>
    {
        protected override AsepriteDefinition Read(ContentReader input, AsepriteDefinition existingInstance)
        {
            //  Read how many frames there are in total
            int frameCount = input.ReadInt32();

            //  Initilize a new list of frame definitions
            List<AsepriteFrameDefinition> frameDefinitions = new List<AsepriteFrameDefinition>();

            //  Read all the data about the frames
            for(int i = 0; i < frameCount; i++)
            {
                //  filename
                string fileName = input.ReadString();

                //  frame-x
                int frameX = input.ReadInt32();

                //  frame-y
                int frameY = input.ReadInt32();

                //  frame-width
                int frameWidth = input.ReadInt32();

                //  frame-height
                int frameHeight = input.ReadInt32();

                //  rotated
                bool rotated = input.ReadBoolean();

                //  trimmed
                bool trimmed = input.ReadBoolean();

                //  spriteSourceSize-x
                int spriteSourceSizeX = input.ReadInt32();

                //  sprieSourceSize-y
                int spriteSourceSizeY = input.ReadInt32();

                //  spriteSourceSize-width
                int spriteSourceSizeW = input.ReadInt32();

                //  spriteSourceSize-height
                int spriteSourceSizeH = input.ReadInt32();

                //  sourceSize-width
                int sourceSizeW = input.ReadInt32();

                //  sourceSize-height
                int sourceSizeH = input.ReadInt32();

                //  duration 
                int duration = input.ReadInt32();


                //  create a new frame definition
                AsepriteFrameDefinition definition = new AsepriteFrameDefinition()
                {
                    FileName = fileName,
                    Frame = new Rectangle(frameX, frameY, frameWidth, frameHeight),
                    Rotated = rotated,
                    Trimmed = trimmed,
                    SpriteSourceSize = new Rectangle(spriteSourceSizeX, spriteSourceSizeY, spriteSourceSizeW, spriteSourceSizeH),
                    SourceSize = new Size(sourceSizeW, sourceSizeH),
                    Duration = duration / 1000.0f
                };

                //  Add the definition to the list
                frameDefinitions.Add(definition);
            }

            //  Read how many animations there are in total
            int animationCount = input.ReadInt32();

            //  Initilize a new list of animation definitions
            List<AsepriteAnimationDefinition> animationDefinitions = new List<AsepriteAnimationDefinition>();

            //  Read all the data about animations
            for(int i = 0; i < animationCount; i++)
            {
                //  name
                string name = input.ReadString();

                //  from
                int from = input.ReadInt32();

                //  to
                int to = input.ReadInt32();

                //  direction
                string direction = input.ReadString();

                //  create a new animation definition
                AsepriteAnimationDefinition definition = new AsepriteAnimationDefinition()
                {
                    Name = name,
                    From = from,
                    To = to,
                    Direction = direction == "forward" ? Direction.forward : Direction.reverse,
                    Loop = true
                };

                //  Add the definition to the list
                animationDefinitions.Add(definition);
            }

            //  Creat an AsepriteDefinition
            AsepriteDefinition asepriteDefinition = new AsepriteDefinition()
            {
                Animations = animationDefinitions,
                Frames = frameDefinitions
            };

            //  Returnt the definition
            return asepriteDefinition;
        }
    }
}
