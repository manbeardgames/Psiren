﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Core.Components
{
    public class GraphicalComponent : Component
    {
        public bool Visible { get; set; }

        public GraphicalComponent(bool active, bool visible) : base(active)
        {
            this.Visible = visible;
        }

        public virtual void HandleGraphicsReset() { }
        public virtual void HandleGraphicsCreate() { }
        public virtual void Render() { }
        public virtual void DebugRender(Camera camera) { }
    }
}
