﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Core.Components
{
    public class Transform2DComponent : Component
    {

        //-------------------------------------------------------------
        //  Position
        //-------------------------------------------------------------

        /// <summary>
        ///     The xy-coordinate position
        /// </summary>
        public Vector2 Position { get; set; }
        public float X => this.Position.X;
        public float Y => this.Position.Y;


        //-------------------------------------------------------------
        //  Scale
        //-------------------------------------------------------------
        public Vector2 Scale { get; set; }
        public float ScaleX => this.Scale.X;
        public float ScaleY => this.Scale.Y;

        //-------------------------------------------------------------
        //  Rotation
        //-------------------------------------------------------------
        public Vector2 Rotation { get; set; }
        public float RotationX => this.Rotation.X;
        public float RotationY => this.Rotation.Y;

        public Transform2DComponent():base()
        {
            this.Position = Vector2.Zero;
            this.Scale = Vector2.One;
            this.Rotation = Vector2.Zero;
        }

        public Transform2DComponent(Vector2 position):this()
        {
            this.Position = position;
        }
    }
}
