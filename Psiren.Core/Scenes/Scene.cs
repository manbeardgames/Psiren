﻿//--------------------------------------------------------------------------------
//  Scene
//
//  A Scene
//
//  TODO: Needs <summary> documtation for collision seciton
//
//-----------------------------Psiren Engine License-----------------------------

//    Original Work Copyright(c) 2012-2014 Matt Thorson
//    Modified Work Copyright(c) 2018   Chris Whitley
//
//    Permission is hereby granted, free of charge, to any person obtaining a copy
//    of this software and associated documentation files (the "Software"), to deal
//    in the Software without restriction, including without limitation the rights
//    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the Software is
//    furnished to do so, subject to the following conditions:

//    The above copyright notice and this permission notice shall be included in
//    all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//    THE SOFTWARE.
//--------------------------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.Core.Components;
using Psiren.Core.Entities;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Psiren.Core.Scenes
{
    public class Scene : IEnumerable<Entity>, IEnumerable
    {
        /// <summary>
        ///     Is the <see cref="Scene"/> paused
        /// </summary>
        public bool Paused { get; set; }

        /// <summary>
        ///     How long the <see cref="Scene"/> has been active
        /// </summary>
        public float TimeActive { get; set; }

        /// <summary>
        ///     Raw time for how long the <see cref="Scene"/> has been active
        /// </summary>
        public float RawTimeActive { get; set; }

        /// <summary>
        ///     Does the <see cref="Scene"/> have focus
        /// </summary>
        public bool Focused { get; private set; }

        /// <summary>
        ///     Collection of <see cref="Entity"/> using in the <see cref="Scene"/>
        /// </summary>
        public EntityList Entities { get; private set; }

        /// <summary>
        ///     Collection of <see cref="BitTag"/> used in the <see cref="Scene"/>
        /// </summary>
        public TagLists TagLists { get; private set; }

        /// <summary>
        ///     Collection of <see cref="Renderer"/> used in the <see cref="Scene"/>
        /// </summary>
        public RendererList RendererList { get; private set; }

        /// <summary>
        ///     <see cref="Tracker"/> used for the <see cref="Scene"/>
        /// </summary>
        public Tracker Tracker { get; private set; }

        //  Lookup dictionary for depths
        private Dictionary<int, double> _actualDepthLookup;

        /// <summary>
        ///     Action to perform at the end of each frame
        /// </summary>
        public event Action OnEndOfFrame;

        public bool Initialized { get; private set; }

        /// <summary>
        ///     Creates a new <see cref="Scene"/> instance
        /// </summary>
        public Scene()
        {
            Tracker = new Tracker();
            Entities = new EntityList(this);
            TagLists = new TagLists();
            RendererList = new RendererList(this);

            _actualDepthLookup = new Dictionary<int, double>();

            Entities.Add(HelperEntity);


        }

        /// <summary>
        ///     Called once at the beginning of the <see cref="Scene"/>
        /// </summary>
        public virtual void Begin()
        {
            this.Focused = true;
            foreach (var entity in this.Entities)
            {
                entity.SceneBegin(this);
            }
        }

        /// <summary>
        ///     Called at the end fo the <see cref="Scene"/>
        /// </summary>
        public virtual void End()
        {
            this.Focused = false;
            foreach (var entity in this.Entities)
            {
                entity.SceneEnd(this);
            }
        }

        //------------------------------------------------------------
        //  Updates
        //------------------------------------------------------------
        #region Updates
        /// <summary>
        ///     Updates various properties before the actual update occurs
        /// </summary>
        public virtual void BeforeUpdate()
        {
            if (!this.Paused)
            {
                this.TimeActive += Engine.DeltaTime;
            }
            this.RawTimeActive += Engine.RawDeltaTime;

            this.Entities.UpdateLists();
            this.TagLists.UpdateLists();
            this.RendererList.UpdateLists();
        }

        /// <summary>
        ///     Udpates the entities and renderers for the scene
        /// </summary>
        public virtual void Update()
        {
            if (!this.Paused)
            {
                this.Entities.Update();
                this.RendererList.Update();
            }
        }

        /// <summary>
        ///     Called after update, if an end of frame action is defined, calls that
        /// </summary>
        public virtual void AfterUpdate()
        {
            this.OnEndOfFrame?.Invoke();
            this.OnEndOfFrame = null;
        }
        #endregion Updates

        //------------------------------------------------------------
        //  Render
        //------------------------------------------------------------
        #region Render

        /// <summary>
        ///     Calls the BeforeRender of the renderers for this <see cref="Scenen"/>
        /// </summary>
        public virtual void BeforeRender()
        {
            this.RendererList.BeforeRender();
        }


        /// <summary>
        ///     Calls the Renderer of the renderers for this <see cref="Scene"/>
        /// </summary>
        public virtual void Render()
        {
            this.RendererList.Render();
        }

        /// <summary>
        ///     Calls the AfterRender for the renderers for this <see cref="Scene"/>
        /// </summary>
        public virtual void AfterRender()
        {
            this.RendererList.AfterRender();
        }
        #endregion Render

        //------------------------------------------------------------
        //  Graphics Reset and Create
        //------------------------------------------------------------
        #region Graphics Reset and Create
        /// <summary>
        ///     Calls HandleGraphicsReset for all entities in this <see cref="Scene"/>
        /// </summary>
        public virtual void HandleGraphicsReset()
        {
            this.Entities.HandleGraphicsReset();
        }

        /// <summary>
        ///     Calls HandleGraphicsCreatea for all entities in this <see cref="Scene"/>
        /// </summary>
        public virtual void HandleGraphicsCreate()
        {
            this.Entities.HandleGraphicsCreate();
        }
        #endregion Graphics Reset and Create


        //------------------------------------------------------------
        //  Focus
        //------------------------------------------------------------
        #region Focus
        /// <summary>
        ///     Called when this <see cref="Scene"/> gains focus
        /// </summary>
        public virtual void GainFocus() { }

        /// <summary>
        ///     Called when this <see cref="Scene"/> loses focus
        /// </summary>
        public virtual void LostFocus() { }
        #endregion Focus

        //------------------------------------------------------------
        //  Interval
        //------------------------------------------------------------
        #region Interval

        /// <summary>
        /// Returns whether the <see cref="Scene"/> timer has passed the given time interval since the last frame. Ex: given 2.0f, this will return true once every 2 seconds
        /// </summary>
        /// <param name="interval">The time interval to check for</param>
        /// <returns></returns>
        public bool OnInterval(float interval)
        {
            return (int)((this.TimeActive - Engine.DeltaTime) / interval) < (int)(this.TimeActive / interval);
        }

        /// <summary>
        /// Returns whether the <see cref="Scene"/> timer has passed the given time interval since the last frame. Ex: given 2.0f, this will return true once every 2 seconds
        /// </summary>
        /// <param name="interval">The time interval to check for</param>
        /// <returns></returns>
        public bool OnInterval(float interval, float offset)
        {
            return Math.Floor((this.TimeActive - offset - Engine.DeltaTime) / interval) < Math.Floor((this.TimeActive - offset) / interval);
        }

        /// <summary>
        ///     Returns whether the <see cref="Scene"/> is between interval
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public bool BetweenInterval(float interval)
        {
            return Calc.BetweenInterval(this.TimeActive, interval);
        }

        /// <summary>
        ///     Returns whether the <see cref="Scene"/> timer has passed the given timer interval since last frame, using the Raw time
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public bool OnRawInterval(float interval)
        {
            return (int)((this.RawTimeActive - Engine.RawDeltaTime) / interval) < (int)(this.RawTimeActive / interval);
        }

        /// <summary>
        ///     Returns whether the <see cref="Scene"/> timer has passed the given timer interval since last frame, using Raw time
        /// </summary>
        /// <param name="interval"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool OnRawInterval(float interval, float offset)
        {
            return Math.Floor((this.RawTimeActive - offset - Engine.RawDeltaTime) / interval) < Math.Floor((this.RawTimeActive - offset) / interval);
        }

        /// <summary>
        ///     Returns whether the <see cref="Scene"/> timer is between intervals, using Raw time
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public bool BetweenRawInterval(float interval)
        {
            return Calc.BetweenInterval(this.RawTimeActive, interval);
        }

        #endregion


        //------------------------------------------------------------
        //  Collision v Tags
        //------------------------------------------------------------
        #region Collisions v Tags

        public bool CollideCheck(Vector2 point, int tag)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheck(Vector2 from, Vector2 to, int tag)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheck(Rectangle rect, int tag)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheck(Rectangle rect, Entity entity)
        {
            return (entity.Collidable && entity.CollideRect(rect));
        }

        public Entity CollideFirst(Vector2 point, int tag)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    return list[i];
                }
            }
            return null;
        }

        public Entity CollideFirst(Vector2 from, Vector2 to, int tag)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    return list[i];
                }
            }
            return null;
        }

        public Entity CollideFirst(Rectangle rect, int tag)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    return list[i];
                }
            }
            return null;
        }

        public void CollideInto(Vector2 point, int tag, List<Entity> hits)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    hits.Add(list[i]);
                }
            }
        }

        public void CollideInto(Vector2 from, Vector2 to, int tag, List<Entity> hits)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    hits.Add(list[i]);
                }
            }
        }

        public void CollideInto(Rectangle rect, int tag, List<Entity> hits)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    list.Add(list[i]);
                }
            }
        }

        public List<Entity> CollideAll(Vector2 point, int tag)
        {
            List<Entity> list = new List<Entity>();
            CollideInto(point, tag, list);
            return list;
        }

        public List<Entity> CollideAll(Vector2 from, Vector2 to, int tag)
        {
            List<Entity> list = new List<Entity>();
            CollideInto(from, to, tag, list);
            return list;
        }

        public List<Entity> CollideAll(Rectangle rect, int tag)
        {
            List<Entity> list = new List<Entity>();
            CollideInto(rect, tag, list);
            return list;
        }

        public void CollideDo(Vector2 point, int tag, Action<Entity> action)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    action(list[i]);
                }
            }
        }

        public void CollideDo(Vector2 from, Vector2 to, int tag, Action<Entity> action)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    action(list[i]);
                }
            }
        }

        public void CollideDo(Rectangle rect, int tag, Action<Entity> action)
        {
            var list = this.TagLists[(int)tag];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    action(list[i]);
                }
            }
        }

        public Vector2 LineWalkCheck(Vector2 from, Vector2 to, int tag, float precision)
        {
            Vector2 add = to - from;
            add.Normalize();
            add *= precision;

            int amount = (int)Math.Floor((from - to).Length() / precision);
            Vector2 prev = from;
            Vector2 at = from + add;

            for (int i = 0; i <= amount; i++)
            {
                if (CollideCheck(at, tag))
                {
                    return prev;
                }
                prev = at;
                at += add;
            }

            return to;
        }

        #endregion


        //------------------------------------------------------------
        //  Collisions v Tracked List Entities
        //------------------------------------------------------------
        #region Collisions v Tracked List Entities

        public bool CollideCheck<T>(Vector2 point) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheck<T>(Vector2 from, Vector2 to) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheck<T>(Rectangle rect) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    return true;
                }
            }
            return false;
        }

        public T CollideFirst<T>(Vector2 point) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    return list[i] as T;
                }
            }
            return null;
        }

        public T CollideFirst<T>(Vector2 from, Vector2 to) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    return list[i] as T;
                }
            }
            return null;
        }

        public T CollideFirst<T>(Rectangle rect) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    return list[i] as T;
                }
            }
            return null;
        }

        public void CollideInto<T>(Vector2 point, List<Entity> hits) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    hits.Add(list[i]);
                }
            }
        }

        public void CollideInto<T>(Vector2 from, Vector2 to, List<Entity> hits) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    hits.Add(list[i]);
                }
            }
        }

        public void CollideInto<T>(Rectangle rect, List<Entity> hits) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    list.Add(list[i]);
                }
            }
        }

        public void CollideInto<T>(Vector2 point, List<T> hits) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    hits.Add(list[i] as T);
                }
            }
        }

        public void CollideInto<T>(Vector2 from, Vector2 to, List<T> hits) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    hits.Add(list[i] as T);
                }
            }
        }

        public void CollideInto<T>(Rectangle rect, List<T> hits) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    hits.Add(list[i] as T);
                }
            }
        }

        public List<T> CollideAll<T>(Vector2 point) where T : Entity
        {
            List<T> list = new List<T>();
            CollideInto<T>(point, list);
            return list;
        }

        public List<T> CollideAll<T>(Vector2 from, Vector2 to) where T : Entity
        {
            List<T> list = new List<T>();
            CollideInto<T>(from, to, list);
            return list;
        }

        public List<T> CollideAll<T>(Rectangle rect) where T : Entity
        {
            List<T> list = new List<T>();
            CollideInto<T>(rect, list);
            return list;
        }

        public void CollideDo<T>(Vector2 point, Action<T> action) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollidePoint(point))
                {
                    action(list[i] as T);
                }
            }
        }

        public void CollideDo<T>(Vector2 from, Vector2 to, Action<T> action) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideLine(from, to))
                {
                    action(list[i] as T);
                }
            }
        }

        public void CollideDo<T>(Rectangle rect, Action<T> action) where T : Entity
        {
            var list = this.Tracker.Entities[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Collidable && list[i].CollideRect(rect))
                {
                    action(list[i] as T);
                }
            }
        }

        public Vector2 LineWalkCheck<T>(Vector2 from, Vector2 to, float precision) where T : Entity
        {
            Vector2 add = to - from;
            add.Normalize();
            add *= precision;

            int amount = (int)Math.Floor((from - to).Length() / precision);
            Vector2 prev = from;
            Vector2 at = from + add;

            for (int i = 0; i <= amount; i++)
            {
                if (CollideCheck<T>(at))
                    return prev;
                prev = at;
                at += add;
            }

            return to;
        }

        #endregion


        //------------------------------------------------------------
        //  Collisions v Tracked List Components
        //------------------------------------------------------------
        #region Collisions v Tracked List Components

        public bool CollideCheckByComponent<T>(Vector2 point) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollidePoint(point))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheckByComponent<T>(Vector2 from, Vector2 to) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideLine(from, to))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CollideCheckByComponent<T>(Rectangle rect) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideRect(rect))
                {
                    return true;
                }
            }
            return false;
        }

        public T CollideFirstByComponent<T>(Vector2 point) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollidePoint(point))
                {
                    return list[i] as T;
                }
            }
            return null;
        }

        public T CollideFirstByComponent<T>(Vector2 from, Vector2 to) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideLine(from, to))
                {
                    return list[i] as T;
                }
            }
            return null;
        }

        public T CollideFirstByComponent<T>(Rectangle rect) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideRect(rect))
                {
                    return list[i] as T;
                }
            }
            return null;
        }

        public void CollideIntoByComponent<T>(Vector2 point, List<Component> hits) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollidePoint(point))
                {
                    hits.Add(list[i]);
                }
            }
        }

        public void CollideIntoByComponent<T>(Vector2 from, Vector2 to, List<Component> hits) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideLine(from, to))
                {
                    hits.Add(list[i]);
                }
            }
        }

        public void CollideIntoByComponent<T>(Rectangle rect, List<Component> hits) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideRect(rect))
                {
                    list.Add(list[i]);
                }
            }
        }

        public void CollideIntoByComponent<T>(Vector2 point, List<T> hits) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollidePoint(point))
                {
                    hits.Add(list[i] as T);
                }
            }
        }

        public void CollideIntoByComponent<T>(Vector2 from, Vector2 to, List<T> hits) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideLine(from, to))
                {
                    hits.Add(list[i] as T);
                }
            }
        }

        public void CollideIntoByComponent<T>(Rectangle rect, List<T> hits) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideRect(rect))
                {
                    list.Add(list[i] as T);
                }
            }
        }

        public List<T> CollideAllByComponent<T>(Vector2 point) where T : Component
        {
            List<T> list = new List<T>();
            CollideIntoByComponent<T>(point, list);
            return list;
        }

        public List<T> CollideAllByComponent<T>(Vector2 from, Vector2 to) where T : Component
        {
            List<T> list = new List<T>();
            CollideIntoByComponent<T>(from, to, list);
            return list;
        }

        public List<T> CollideAllByComponent<T>(Rectangle rect) where T : Component
        {
            List<T> list = new List<T>();
            CollideIntoByComponent<T>(rect, list);
            return list;
        }

        public void CollideDoByComponent<T>(Vector2 point, Action<T> action) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollidePoint(point))
                {
                    action(list[i] as T);
                }
            }
        }

        public void CollideDoByComponent<T>(Vector2 from, Vector2 to, Action<T> action) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideLine(from, to))
                {
                    action(list[i] as T);
                }
            }
        }

        public void CollideDoByComponent<T>(Rectangle rect, Action<T> action) where T : Component
        {
            var list = this.Tracker.Components[typeof(T)];

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Entity.Collidable && list[i].Entity.CollideRect(rect))
                {
                    action(list[i] as T);
                }
            }
        }

        public Vector2 LineWalkCheckByComponent<T>(Vector2 from, Vector2 to, float precision) where T : Component
        {
            Vector2 add = to - from;
            add.Normalize();
            add *= precision;

            int amount = (int)Math.Floor((from - to).Length() / precision);
            Vector2 prev = from;
            Vector2 at = from + add;

            for (int i = 0; i <= amount; i++)
            {
                if (CollideCheckByComponent<T>(at))
                    return prev;
                prev = at;
                at += add;
            }

            return to;
        }

        #endregion


        //------------------------------------------------------------
        //  Utils
        //------------------------------------------------------------
        #region Utils

        internal void SetActualDepth(Entity entity)
        {
            const double theta = .000001f;

            double add = 0;
            if (_actualDepthLookup.TryGetValue(entity._depth, out add))
            {
                _actualDepthLookup[entity._depth] += theta;
            }
            else
            {
                _actualDepthLookup.Add(entity._depth, theta);
            }
            entity._actualDepth = entity._depth - add;

            //Mark lists unsorted
            Entities.MarkUnsorted();
            for (int i = 0; i < BitTag.TotalTags; i++)
            {
                if (entity.TagCheck(1 << i))
                {
                    this.TagLists.MarkUnsorted(i);
                }
            }
        }

        #endregion

        #region Entity Shortcuts

        /// <summary>
        /// Shortcut to call Engine.Pooler.Create, add the Entity to this Scene, and return it. Entity type must be marked as Pooled
        /// </summary>
        /// <typeparam name="T">Pooled Entity type to create</typeparam>
        /// <returns></returns>
        public T CreateAndAdd<T>() where T : Entity, new()
        {
            var entity = Engine.Pooler.Create<T>();
            Add(entity);
            return entity;
        }

        /// <summary>
        /// Quick access to entire tag lists of Entities. Result will never be null
        /// </summary>
        /// <param name="tag">The tag list to fetch</param>
        /// <returns></returns>
        public List<Entity> this[BitTag tag]
        {
            get
            {
                return this.TagLists[tag.ID];
            }
        }

        public T Add<T>(T entity) where T : Entity
        {
            this.Entities.Add(entity);
            return entity as T;
        }

        /// <summary>
        /// Shortcut function for adding an Entity to the Scene's Entities list
        /// </summary>
        /// <param name="entity">The Entity to add</param>
        public Entity Add(Entity entity)
        {
            this.Entities.Add(entity);
            return entity;
        }

        /// <summary>
        /// Shortcut function for removing an Entity from the Scene's Entities list
        /// </summary>
        /// <param name="entity">The Entity to remove</param>
        public void Remove(Entity entity)
        {
            this.Entities.Remove(entity);
        }

        /// <summary>
        /// Shortcut function for adding a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to add</param>
        public void Add(IEnumerable<Entity> entities)
        {
            this.Entities.Add(entities);
        }

        /// <summary>
        /// Shortcut function for removing a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to remove</param>
        public void Remove(IEnumerable<Entity> entities)
        {
            this.Entities.Remove(entities);
        }

        /// <summary>
        /// Shortcut function for adding a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to add</param>
        public void Add(params Entity[] entities)
        {
            this.Entities.Add(entities);
        }

        /// <summary>
        /// Shortcut function for removing a set of Entities from the Scene's Entities list
        /// </summary>
        /// <param name="entities">The Entities to remove</param>
        public void Remove(params Entity[] entities)
        {
            this.Entities.Remove(entities);
        }

        /// <summary>
        /// Allows you to iterate through all Entities in the Scene
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Entity> GetEnumerator()
        {
            return Entities.GetEnumerator();
        }

        /// <summary>
        /// Allows you to iterate through all Entities in the Scene
        /// </summary>
        /// <returns></returns>
        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public List<Entity> GetEntitiesByTagMask(int mask)
        {
            List<Entity> list = new List<Entity>();
            foreach (var entity in this.Entities)
            {
                if ((entity.Tag & mask) != 0)
                {
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<Entity> GetEntitiesExcludingTagMask(int mask)
        {
            List<Entity> list = new List<Entity>();
            foreach (var entity in Entities)
            {
                if ((entity.Tag & mask) == 0)
                {
                    list.Add(entity);
                }
            }
            return list;
        }

        public List<T> GetEntitiesOfType<T>() where T : Entity
        {
            List<T> list = new List<T>();
            foreach (var entity in this.Entities)
            {
                if (entity is T t)
                {
                    list.Add(t as T);
                }
            }
            return list;
        }

        #endregion

        #region Renderer Shortcuts

        /// <summary>
        /// Shortcut function to add a Renderer to the Renderer list
        /// </summary>
        /// <param name="renderer">The Renderer to add</param>
        public void Add(Renderer renderer)
        {
            this.RendererList.Add(renderer);
        }

        /// <summary>
        /// Shortcut function to remove a Renderer from the Renderer list
        /// </summary>
        /// <param name="renderer">The Renderer to remove</param>
        public void Remove(Renderer renderer)
        {
            this.RendererList.Remove(renderer);
        }

        #endregion

    }
}
