﻿using Psiren.Core.Components;
using Psiren.Core.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Core.InternalUtilities
{
    public class ComponentList : IEnumerable<Component>, IEnumerable
    {
        /// <summary>
        ///     Defines the lock mode when working with the list of components
        /// </summary>
        public enum LockModes { Open, Locked, Error };

        /// <summary>
        ///     The <see cref="Entity"/> which uses this component list
        /// </summary>
        public Entity Entity { get; internal set; }

        /// <summary>
        ///     Collection of all components;
        /// </summary>
        private List<Component> _components;

        /// <summary>
        ///     Colelction of components that need to be added
        /// </summary>
        private List<Component> _toAdd;

        /// <summary>
        ///     Collection of components that need to be removed
        /// </summary>
        private List<Component> _toRemove;

        /// <summary>
        ///     HashSet collection of current components
        /// </summary>
        private HashSet<Component> _current;

        /// <summary>
        ///     HashSet collection of components that need to be added
        /// </summary>
        private HashSet<Component> _adding;

        /// <summary>
        ///     HashSet collectin of components that need to be removed
        /// </summary>
        private HashSet<Component> _removing;

        /// <summary>
        ///     The number of components
        /// </summary>
        public int Count { get => _components.Count; }

        /// <summary>
        /// The current <see cref="LockModes"/>
        /// </summary>
        private LockModes _lockMode;

        internal LockModes LockMode
        {
            get { return _lockMode; }
            set
            {
                _lockMode = value;
                if(_toAdd.Count > 0)
                {
                    foreach(var component in _toAdd)
                    {
                        if(!_current.Contains(component))
                        {
                            _current.Add(component);
                            _components.Add(component);
                            component.Added(Entity);
                        }
                    }

                    _adding.Clear();
                    _toAdd.Clear();
                }

                if(_toRemove.Count > 0)
                {
                    foreach(var component in _toRemove)
                    {
                        if(_current.Contains(component))
                        {
                            _current.Remove(component);
                            _components.Remove(component);
                            component.Removed(Entity);
                        }
                    }

                    _removing.Clear();
                    _toRemove.Clear();
                }
            }
        }


        /// <summary>
        ///     Creates a new instance
        /// </summary>
        /// <param name="entity"></param>
        internal ComponentList(Entity entity)
        {
            this.Entity = entity;

            _components = new List<Component>();
            _toAdd = new List<Component>();
            _toRemove = new List<Component>();

            _current = new HashSet<Component>();
            _adding = new HashSet<Component>();
            _removing = new HashSet<Component>();
        }

        /// <summary>
        ///    
        /// </summary>
        /// <remarks>
        ///     Chris TODO:
        ///         I added this an don't know why, it does the same thing
        ///         as the normal add. Most likely i ment this to add the 
        ///         component to the beginning of each collection, but that's
        ///         not what it's doing
        /// </remarks>
        /// <param name="component"></param>
        public void AddAtBeginning(Component component)
        {
            switch(_lockMode)
            {
                case LockModes.Open:
                    if(!_current.Contains(component))
                    {
                        _current.Add(component);
                        _components.Add(component);
                        component.Added(this.Entity);
                    }
                    break;
                case LockModes.Locked:
                    if(!_current.Contains(component) && !_adding.Contains(component))
                    {
                        _adding.Add(component);
                        _toAdd.Add(component);
                    }
                    break;
                case LockModes.Error:
                    throw new Exception("Cannot add or remove components at this time");
            }
        }

        public void Add(Component component)
        {
            switch(_lockMode)
            {
                case LockModes.Open:
                    if(!_current.Contains(component))
                    {
                        _current.Add(component);
                        _components.Add(component);
                        component.Added(this.Entity);
                    }
                    break;
                case LockModes.Locked:
                    if(!_current.Contains(component) && !_adding.Contains(component))
                    {
                        _adding.Add(component);
                        _toAdd.Add(component);
                    }
                    break;

                case LockModes.Error:
                    throw new Exception("Cannot add or remove Components at this time");
            }
        }

        /// <summary>
        ///     Adds the given collection of <see cref="Component"/>s to this.
        /// </summary>
        /// <param name="components"></param>
        public void Add(IEnumerable<Component> components)
        {
            foreach (var component in components)
            {
                this.Add(component);
            }
        }

        /// <summary>
        ///     Adds the given <see cref="Component"/>s to this
        /// </summary>
        /// <param name="components"></param>
        public void Add(params Component[] components)
        {
            foreach (var component in components)
            {
                this.Add(component);
            }
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/> from the collection
        /// </summary>
        /// <param name="component"></param>
        public void Remove(Component component)
        {
            switch (_lockMode)
            {
                case LockModes.Open:
                    if(_current.Contains(component))
                    {
                        _current.Remove(component);
                        _components.Remove(component);
                        component.Removed(this.Entity);
                    }
                    break;
                case LockModes.Locked:
                    if(_current.Contains(component) && !_removing.Contains(component))
                    {
                        _removing.Add(component);
                        _toRemove.Add(component);
                    }
                    break;
                case LockModes.Error:
                    throw new Exception("Cannot add or remove Components at this time");
            }
        }

        ///<summary>
        ///     Removes the given <see cref="Component"/> collection from this collection
        ///</summary>
        ///<param name="components"></param>
        public void Remove(IEnumerable<Component> components)
        {
            foreach (var component in components)
            {
                Remove(component);
            }
        }

        /// <summary>
        ///     Removes the given <see cref="Component"/>s from this colleciton
        /// </summary>
        /// <param name="components"></param>
        public void Remove(params Component[] components)
        {
            foreach (var component in components)
            {
                Remove(component);
            }
        }

        /// <summary>
        ///     Removes all <see cref="Component"/>s from this collection of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void RemoveAll<T>() where T : Component
        {
            Remove(GetAll<T>());
        }


        public IEnumerator<Component> GetEnumerator()
        {
            return _components.GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Component[] ToArray()
        {
            return _components.ToArray<Component>();
        }

        internal void Update()
        {
            LockMode = LockModes.Locked;
            foreach (var component in _components)
            {
                if(component.Active)
                {
                    component.Update();
                }
            }
            LockMode = LockModes.Open;
        }

        /// <summary>
        ///     Renders each <see cref="GraphicalComponent"/> if it is visible
        /// </summary>
        internal void Render()
        {
            LockMode = LockModes.Error;
            foreach (GraphicalComponent component in _components.Where(c => c is GraphicalComponent))
            {
                if(component.Visible)
                {
                    component.Render();
                }
            }
            LockMode = LockModes.Open;
        }

        internal void DebugRender(Camera camera)
        {
            LockMode = LockModes.Error;
            foreach (GraphicalComponent component in _components.Where(c => c is GraphicalComponent))
            {
                component.DebugRender(camera);
            }
        }

        internal void HandleGraphicsReset()
        {
            LockMode = LockModes.Error;
            foreach (GraphicalComponent component in _components.Where(c => c is GraphicalComponent))
            {
                component.HandleGraphicsReset();
            }
            LockMode = LockModes.Open;
        }

        internal void HandleGraphicsCreate()
        {
            LockMode = LockModes.Error;
            foreach (GraphicalComponent component in _components.Where(c => c is GraphicalComponent))
            {
                component.HandleGraphicsCreate();
            }
            LockMode = LockModes.Open;
        }

        public T Get<T>() where T : Component
        {
            foreach (var component in _components)
            {
                if(component is T)
                {
                    return component as T;
                }
            }
            return null;
        }

        public IEnumerable<T> GetAll<T>() where T : Component
        {
            foreach (var component in _components)
            {
                if(component is T)
                {
                    yield return component as T;
                }
            }
        }









    }
}
