﻿using Microsoft.Xna.Framework;

namespace Psiren.Pathfinding
{
    public static class LocationExtensions
    {
        public static Vector2 ToVector2(this Location location)
        {
            return new Vector2(location.x, location.y);
        }
    }
}
