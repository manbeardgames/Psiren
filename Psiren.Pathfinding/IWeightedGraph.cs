﻿using System.Collections.Generic;

namespace Psiren.Pathfinding
{
    public interface IWeightedGraph<L>
    {
        double Cost(Location a, Location b);
        IEnumerable<Location> Neighbors(Location id);
    }
}
