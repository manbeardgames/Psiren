﻿using System;

namespace Psiren.Pathfinding
{
    public class Test
    {
        public static void DrawGride(SquareGrid grid, AstarSearch aStar)
        {


            //  Print out the cameFrom array
            for (var y = 0; y < grid.height; y++)
            {
                for (var x = 0; x < grid.width; x++)
                {
                    Location id = new Location(x, y);
                    Location ptr = id;
                    if (!aStar.cameFrom.TryGetValue(id, out ptr))
                    {
                        ptr = id;
                    }

                    if (grid.walls.Contains(id))
                    {
                        Console.Write("# ");
                    }
                    else if (ptr.x == x + 1)
                    {
                        Console.Write("R ");
                        //Console.Write("{0} ", char.ConvertFromUtf32(0x2192));
                        //Console.Write("\u2192 ");
                    }
                    else if (ptr.x == x - 1)
                    {
                        Console.Write("L ");
                        //Console.Write("{0} ", char.ConvertFromUtf32(0x2190));
                        //Console.Write("\u2190 ");
                    }
                    else if (ptr.y == y + 1)
                    {
                        Console.Write("D ");
                        //Console.Write("{0} ", char.ConvertFromUtf32(0x2193));
                        //Console.Write("\u2193 ");
                    }
                    else if (ptr.y == y - 1)
                    {
                        Console.Write("U ");
                        //Console.Write("{0} ", char.ConvertFromUtf32(0x2191));
                        //Console.Write("\u2191 ");
                    }
                    else
                    {
                        Console.Write("* ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
