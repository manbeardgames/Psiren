﻿namespace Psiren.Pathfinding
{
    public struct Location
    {
        //  Impelmentation notes: I am using hte default Equals but it can
        //  be slow. You'll probably want to override both Equals and
        //  GetHashCode in a real project

        public readonly int x, y;
        public Location(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", x, y);
        }

        public static bool operator ==(Location l1, Location l2)
        {
            return l1.Equals(l2);
        }

        public static bool operator !=(Location l1, Location l2)
        {
            return !l1.Equals(l2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
