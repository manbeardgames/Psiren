﻿//----------------------------------------------------------
//  DemoInputProfile
//  Your game needs to define an implemntation of the 
//  IInputProfile interface.  This gets assigned to the 
//  Engine.InputProfile in the Engine.Initialize() method
//
//
//  InputProfiles act as a translation between a standardized
//  set of input actions, and the controls for your game, be it
//  from keyboard, mouse, or gamepad.
//
//  For our game, the player moves left or right using
//  either the arrow keys or A and D on the keyboard, or 
//  dpad and/or left joystick on a gamepad
//  and fires shots using Enter and/or Space on keyboard
//  Left mouse click or the A button on a gamepad.
//----------------------------------------------------------
using Microsoft.Xna.Framework.Input;
using Psiren.Input;

namespace Psiren.SceneDemo
{
    class DemoInputProfile : IInputProfile
    {
        //---------------------------------------------------
        //  Standard Mappings
        //---------------------------------------------------
        public VirtualButton Action1 { get => _action1; set => _action1 = value; }
        private VirtualButton _action1;

        public VirtualButton Action2 { get => _action2; set => _action2 = value; }
        private VirtualButton _action2;

        public VirtualButton Action3 { get => _action3; set => _action3 = value; }
        private VirtualButton _action3;

        public VirtualButton Action4 { get => _action4; set => _action4 = value; }
        private VirtualButton _action4;

        public VirtualButton DpadUp { get => _dpadUp; set => _dpadUp = value; }
        private VirtualButton _dpadUp;

        public VirtualButton DpadDown { get => _dpadDown; set => _dpadDown = value; }
        private VirtualButton _dpadDown;

        public VirtualButton DpadLeft { get => _dpadLeft; set => _dpadLeft = value; }
        private VirtualButton _dpadLeft;

        public VirtualButton DpadRight { get => _dpadRight; set => _dpadRight = value; }
        private VirtualButton _dpadRight;

        public VirtualButton LeftStick { get => _leftStick; set => _leftStick = value; }
        private VirtualButton _leftStick;

        public VirtualJoystick LeftStickX { get => _leftStickX; set => _leftStickX = value; }
        private VirtualJoystick _leftStickX;

        public VirtualIntegerAxis LeftStickY { get => _leftStickY; set => _leftStickY = value; }
        private VirtualIntegerAxis _leftStickY;

        public VirtualButton RightStick { get => _rightStick; set => _rightStick = value; }
        private VirtualButton _rightStick;

        public VirtualIntegerAxis RightStickX { get => _rightStickX; set => _rightStickX = value; }
        private VirtualIntegerAxis _rightStickX;

        public VirtualIntegerAxis RightStickY { get => _rightStickY; set => _rightStickY = value; }
        private VirtualIntegerAxis _rightStickY;

        public VirtualButton CommandStart { get => _commandStart; set => _commandStart = value; }
        private VirtualButton _commandStart;

        public VirtualButton CommandSelect { get => _commandSelect; set => _commandSelect = value; }
        private VirtualButton _commandSelect;

        public VirtualButton LeftTrigger { get => _leftTrigger; set => _leftTrigger = value; }
        private VirtualButton _leftTrigger;

        public VirtualButton RightTrigger { get => _rightTrigger; set => _rightTrigger = value; }
        private VirtualButton _rightTrigger;

        public VirtualButton LeftBumper { get => _leftBumper; set => _leftBumper = value; }
        private VirtualButton _leftBumper;

        public VirtualButton RightBumper { get => _rightBumper; set => _rightBumper = value; }
        private VirtualButton _rightBumper;

        //---------------------------------------------------
        //  Menu Mappings
        //--------------------------------------------------
        public VirtualButton MenuUp { get => _menuUp; set => _menuUp = value; }
        private VirtualButton _menuUp;

        public VirtualButton MenuDown { get => _menuDown; set => _menuDown = value; }
        private VirtualButton _menuDown;

        public VirtualButton MenuLeft { get => _menuLeft; set => _menuLeft = value; }
        private VirtualButton _menuLeft;

        public VirtualButton MenuRight { get => _menuRight; set => _menuRight = value; }
        private VirtualButton _menuRight;

        public VirtualButton MenuConfirm { get => _menuSelect; set => _menuSelect = value; }
        private VirtualButton _menuSelect;

        public VirtualButton MenuCancel { get => _menuBack; set => _menuBack = value; }
        private VirtualButton _menuBack;

        public VirtualButton MenuEsc { get => _menuEsc; set => _menuEsc = value; }
        private VirtualButton _menuEsc;

        /// <summary>
        ///     Here is where we will define the actual mappings
        ///     from a real input to the virtual inputs in the profile
        /// </summary>
        public void Initialize()
        {
            //------------------------------------------------------------------
            //  Action 1 is ment to represent the "A" button on a xbox gamepad.
            //  We also want to use Action1 as the "Fire" button in our game
            //  so we'll map the real inputs (Nodes) needed for this to Action1
            //------------------------------------------------------------------
            _action1 = new VirtualButton();

            //  First node is the A button on the first gamepad (index 0 gamepad)
            _action1.Nodes.Add(new VirtualButton.PadButton(0, Buttons.A));

            //  Second Node is the Enter key on the keyboard
            _action1.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Enter));

            //  Third node is the left click on the mouse
            _action1.Nodes.Add(new VirtualButton.MouseLeftButton());

            //------------------------------------------------------------------
            //  For our game, the player can move left or right.  We don't care about
            //  variable speed left/right like you would get in measuring a joystick 
            //  axis, we just care about "is left, or is right".  So for this
            //  we'll use the Dpad virtual inputs in the InputProfile
            //------------------------------------------------------------------
            _dpadLeft = new VirtualButton();
            //  Left can be the keyboard left arrow key
            _dpadLeft.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Left));
            //  Left can also be the gamepad dpad left
            _dpadLeft.Nodes.Add(new VirtualButton.PadButton(0, Buttons.DPadLeft));
            //  Left can also be from the gamepad joystick left
            _dpadLeft.Nodes.Add(new VirtualButton.PadButton(0, Buttons.LeftThumbstickLeft));

            //  We can also define the nodes using one of the overloads for the VirtualButton
            //  constructor.  We'll do this as an example for the mapping of the 
            //  dpad right virtual input
            _dpadRight = new VirtualButton(new VirtualButton.KeyboardKey(Keys.Right),
                new VirtualButton.PadButton(0, Buttons.DPadRight),
                new VirtualButton.PadButton(0, Buttons.LeftThumbstickRight));




            //_leftStickX = new VirtualJoystick();
            //_leftStickX.Nodes.Add(new VirtualAxis.PadDpadLeftRight(0));
            //_leftStickX.Nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehaviors.TakeNewer, Keys.Left, Keys.Right));
            //_leftStickX.Nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehaviors.TakeNewer, Keys.A, Keys.D));

            _leftStickY = new VirtualIntegerAxis();
            _leftStickY.Nodes.Add(new VirtualAxis.PadDpadUpDown(0));
            _leftStickY.Nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehaviors.TakeNewer, Keys.Up, Keys.Down));
            _leftStickY.Nodes.Add(new VirtualAxis.KeyboardKeys(VirtualInput.OverlapBehaviors.TakeNewer, Keys.W, Keys.S));

            //------------------------------------------------------------------
            //  As a special note, the Engine has an inheritable entity call
            //  Menu.  The Menu entity is hardwired to use the Engine.InputProfile
            //  menu inputs.  If you plan to use the Menu entity, be sure to set
            //  these as well
            //------------------------------------------------------------------
            _menuUp = new VirtualButton();
            _menuUp.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Up));
            _menuUp.Nodes.Add(new VirtualButton.PadButton(0, Buttons.DPadUp));
            _menuUp.Nodes.Add(new VirtualButton.PadButton(0, Buttons.LeftThumbstickUp));

            _menuDown = new VirtualButton();
            _menuDown.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Down));
            _menuDown.Nodes.Add(new VirtualButton.PadButton(0, Buttons.DPadDown));
            _menuDown.Nodes.Add(new VirtualButton.PadButton(0, Buttons.LeftThumbstickDown));

            _menuLeft = new VirtualButton();
            _menuLeft.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Left));
            _menuLeft.Nodes.Add(new VirtualButton.PadButton(0, Buttons.DPadLeft));
            _menuLeft.Nodes.Add(new VirtualButton.PadButton(0, Buttons.LeftThumbstickLeft));

            _menuRight = new VirtualButton();
            _menuRight.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Right));
            _menuRight.Nodes.Add(new VirtualButton.PadButton(0, Buttons.DPadRight));
            _menuRight.Nodes.Add(new VirtualButton.PadButton(0, Buttons.LeftThumbstickRight));

            _menuSelect = new VirtualButton();
            _menuSelect.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Enter));
            _menuSelect.Nodes.Add(new VirtualButton.PadButton(0, Buttons.A));

            _menuBack = new VirtualButton();
            _menuBack.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Escape));
            _menuBack.Nodes.Add(new VirtualButton.PadButton(0, Buttons.B));

            _menuEsc = new VirtualButton();
            _menuEsc.Nodes.Add(new VirtualButton.KeyboardKey(Keys.Escape));
            _menuEsc.Nodes.Add(new VirtualButton.PadButton(0, Buttons.B));

        }

        /// <summary>
        ///     This allows us to unregister any virtual inputs defined
        ///     in the input profile
        /// </summary>
        public void Deregister()
        {
            //  Actions
            if (_action1 != null) { _action1.Deregister(); }
            if (_action2 != null) { _action2.Deregister(); }
            if (_action3 != null) { _action3.Deregister(); }
            if (_action4 != null) { _action4.Deregister(); }

            //  Dpads
            if (_dpadUp != null) { _dpadUp.Deregister(); }
            if (_dpadDown != null) { _action4.Deregister(); }
            if (_dpadLeft != null) { _dpadLeft.Deregister(); }
            if (_dpadRight != null) { _dpadRight.Deregister(); }

            //  Left Stick
            if (_leftStick != null) { _leftStick.Deregister(); }
            if (_leftStickX != null) { _leftStickX.Deregister(); }
            if (_leftStickY != null) { _leftStickY.Deregister(); }

            //  Right Stick
            if (_rightStick != null) { _rightStick.Deregister(); }
            if (_rightStickX != null) { _rightStickX.Deregister(); }
            if (_rightStickY != null) { _rightStickY.Deregister(); }

            //  Triggers
            if (_leftTrigger != null) { _leftTrigger.Deregister(); }
            if (_rightTrigger != null) { _rightTrigger.Deregister(); }

            //  Bumpers
            if (_leftBumper != null) { _leftBumper.Deregister(); }
            if (_rightBumper != null) { _rightBumper.Deregister(); }

            //  Commands
            if (_commandStart != null) { _commandStart.Deregister(); }
            if (_commandSelect != null) { _commandSelect.Deregister(); }

            //  Menus
            if (_menuUp != null) { _menuUp.Deregister(); }
            if (_menuDown != null) { _menuDown.Deregister(); }
            if (_menuLeft != null) { _menuLeft.Deregister(); }
            if (_menuRight != null) { _menuRight.Deregister(); }
            if (_menuSelect != null) { _menuSelect.Deregister(); }
            if (_menuBack != null) { _menuBack.Deregister(); }

        }

    }
}
