﻿//----------------------------------------------------------
//  Tags
//  Tags are a way of creating tags for entities used within
//  a Scene. For this demo, we create three tags. One for the
//  player, one for the bullets, and one for the asteroids
//----------------------------------------------------------

using Psiren.Utils;

namespace Psiren.SceneDemo
{
    //  Make it a static class for easy access
    public static class Tags
    {
        //  Create a BitTag property for each tag.
        public static BitTag Player;
        public static BitTag Bullet;
        public static BitTag Asteroid;
        
        //  Add an Initialize method to use to initialize the tags
        //  This will be called in the GameBase Initialize()
        public static void Initialize()
        {
            Player = new BitTag("player");
            Bullet = new BitTag("bullet");
            Asteroid = new BitTag("asteroid");
            
        }
    }
}
