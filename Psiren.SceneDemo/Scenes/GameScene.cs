﻿////----------------------------------------------------------
////  GameScene
////  Our GameScene is where the game is actually played.
////  Multiple entities are used here, including a player
////  and the asteroieds that spawn.  Most of the logic 
////  here is just general game dev logic, but it will show
////  adding a custom entity to the scene and spawning
////  additional entities and managing them throughout
////  the scene life
////
////  We also show using a different renderer, the SingleTagRenderer
////----------------------------------------------------------
//using Microsoft.Xna.Framework;
//using Psiren.Renderers;
//using Psiren.SceneDemo.Entities;
//using Psiren.Scenes;

//namespace Psiren.SceneDemo.Scenes
//{
//    public class GameScene : Scene
//    {
//        SingleTagRenderer _playerRenderer;
//        SingleTagRenderer _asteroidRenderer;
//        SingleTagRenderer _bulletRenderer;

//        public GameScene() : base()
//        {
//            _playerRenderer = new SingleTagRenderer(Tags.Player);
//            this.RendererList.Add(_playerRenderer);
//            Player player = new Player(Vector2.Zero);
//            player.Position = new Vector2(Engine.Width * 0.5f, Engine.Height - player.Height);
//            this.Add(player);
//        }

//        public override void Update()
//        {
//            base.Update();
//        }
//    }
//}
