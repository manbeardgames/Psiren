﻿//----------------------------------------------------------
//  TitleScene
//  Our title scene will show the title of the game
//  and prompt the user to press a button to start the
//  actual game.  Doing this will actually swap the Engine.Scene
//  to the game scene
//
//  There are override methods you can use within a Scene
//  such as Update and Render. We do not use them here
//  as they are not needed, but you can check them
//  out in the documentation on the wiki
//----------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Components.Graphics.Text;
using Psiren.Components.Logic;
using Psiren.Entities;
using Psiren.Renderers;
using Psiren.Scenes;
using System;
using System.Collections;

namespace Psiren.SceneDemo.Scenes
{
    public class TitleScene : Scene
    {
        //  An everything renderer will render all entities
        //  regardless of the tag they have set. 
        EverythingRenderer _renderer;


        public TitleScene():base()
        {
            //  First thing would be to setup the renderers and add them
            //  to the Scene's RendererList.  This scene however just has
            //  the EverythingRenderer renderer
            _renderer = new EverythingRenderer();
            this.RendererList.Add(_renderer);

            //  Now that we have the renderer setup, we can add the initial 
            //  Entities for the scene.  For the title Scene, we need some
            //  Text to show the title of the game, and some text that
            //  will blink in and out telling the player to press a
            //  button to start

            //  For the text, let's create an entity and give it a Text
            //  component.  We'll put the entity position at the center of
            //  the screen at the top and tell the Text component to 
            //  use center justification.
            //  Note that a component's position is RELATIVE to the position of the
            //  entity it is attached to
            Entity textEntity = new Entity(new Vector2(Engine.Width * 0.5f, 10.0f));
            Text titleText = new Text(
               font: Engine.Instance.Content.Load<SpriteFont>("fonts/default"),
               text: "Space Shooter",
               position: Vector2.Zero,
               horizontalAlign: Text.HorizontalAlign.Center,
               verticalAlign: Text.VerticalAlign.Center);
            textEntity.Add(titleText);
            this.Add(textEntity);


            //  Now we'll add the text that will tell the player to press 
            //  a button to start the game.  
            Entity startEntity = new Entity(new Vector2(Engine.Width, Engine.Height) * 0.5f);
            startEntity.Add(new Text(
                    font: Engine.Instance.Content.Load<SpriteFont>("fonts/default"),
                    text: "Press A on Gamepad or Enter on Keyboard to start",
                    position: Vector2.Zero,
                    horizontalAlign: Text.HorizontalAlign.Center,
                    verticalAlign: Text.VerticalAlign.Center));
            this.Add(startEntity);

            //  Now, I'm doing this for an example, but notice above we didn't
            //  store the Text component for use later. No worries, we can
            //  just retrive it with some get methods
            Text startText = startEntity.Get<Text>();

            //  Now to get it blinking, we can use a Coroutine.  Coroutines are
            //  just a component, so we add it to an entity.  It would make sense
            //  here to add it to the startEntity, however, to show that this exists
            //  we'll use the HelperEntity. This is an entity that every scene has
            //  by default to use for various things like this.
            //  The second paramter for the Coroutine constructor tells it that it
            //  should remove itself from the entity when it finishes
            this.HelperEntity.Add(new Coroutine(BlinkTextRoutine(startText), true));
        }

        IEnumerator BlinkTextRoutine(Text text)
        {
            //  Loop adjusting the text alpha until action1 is pressed
            while(Engine.InputProfiles[PlayerNumber.One].Action1.Pressed == false)
            { 
                text.Alpha = Math.Abs((float)Math.Sin(Engine.GameTime.TotalGameTime.TotalSeconds));
                yield return null;
            }

            //  Switch to the GameScene by just setting the Engine.Scene to it
            //Engine.Scene = new GameScene();
        }

    }
}
