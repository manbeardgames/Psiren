﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Components.Graphics;
using Psiren.Entities;
using Psiren.Graphics;

namespace Psiren.SceneDemo.Entities
{
    public class AsteroidEntity :Entity
    {
        public AsteroidEntity(Vector2 position): base(position)
        {
            
            this.Add(new Image(new MTexture(Engine.Instance.Content.Load<Texture2D>("images/asteroid"))));
        }
    }
}
