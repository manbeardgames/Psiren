﻿////----------------------------------------------------------
////  Player
////  The player inherits from Entity, so it acts just like
////  and Entity but we can add some custom logic to it.
////
////  The player is represented by a ship sprite on the screen
////  so we'll need the Image component to draw that.
////  And the player can move left and right, so we'll need
////  to overrid the Update to add the input logic.
////
////  Finally the player will need to be able to fire bullets
////  so well have a way to add a new bullet entity to the scene
////  from within the player entity
////----------------------------------------------------------
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;
//using Psiren.Colliders;
//using Psiren.Components.Graphics;
//using Psiren.Entities;
//using Psiren.Graphics;
//using System;

//namespace Psiren.SceneDemo.Entities
//{
//    public class Player : Entity
//    {
//        const float FULLSPEED = 320.0f;
//        const float DRAGPERSECOND = 0.9f;
//        const float ROTATIONRADIANSPERSECOND = 6f;
//        const float VELOCITYMAXIMUM = 320f;
//        private Vector2 _velocity = Vector2.Zero;


//        //  When inheriting from the base entity class
//        //  you can set the position of the entity by calling
//        //  the base() constructor with the position overload
//        public Player(Vector2 position) : base(position)
//        {
//            //  Set the tag of the player
//            this.Tag = Tags.Player;

//            //  Add a new image component
//            this.Add(new Image(new MTexture(Engine.Instance.Content.Load<Texture2D>("images/player"))));

//            //  Now the texture we are using has the ship facing down. We need to flip it
//            Get<Image>().FlipY = true;
//            Get<Image>().CenterOrigin();

//            //  Setup the collider for the player based on the width/height of
//            //  the image
//            var size = Get<Image>().Size;
//            this.Collider = new Hitbox(size.X, size.Y);
//        }

//        public override void Update()
//        {
//            //  base.Update is when components of this entity are updated
//            base.Update();
//            var image = Get<Image>();

//            //   Calculate the current forward vector
//            Vector2 forward = new Vector2((float)Math.Sin(image.Rotation), -(float)Math.Cos(image.Rotation));
//            Vector2 right = new Vector2(-forward.Y, forward.X);

//            //  Calculate the new foward vector with the movement
//            if(new Vector2(Engine.InputProfiles[PlayerNumber.One].LeftStickX, Engine.InputProfiles[PlayerNumber.One].LeftStickY).LengthSquared() > 0f)
//            {
//                //  Change the direction
//                Vector2 wantedForward = Vector2.Normalize(new Vector2(Engine.InputProfiles[PlayerNumber.One].LeftStickX.Value, Engine.InputProfiles[PlayerNumber.One].LeftStickY.Value));
//                float angleDiff = (float)Math.Acos(Vector2.Dot(wantedForward, forward));
//                float facing = (Vector2.Dot(wantedForward, right) > 0f) ? 1f : -1f;
//                if(angleDiff > 0.001f)
//                {
                    
//                    image.Rotation += Math.Min(angleDiff, facing * Engine.DeltaTime * ROTATIONRADIANSPERSECOND);
//                }

//                //  Add velocity
//                _velocity += new Vector2(Engine.InputProfiles[PlayerNumber.One].LeftStickX, Engine.InputProfiles[PlayerNumber.One].LeftStickY) * (Engine.DeltaTime * FULLSPEED);
//                if(_velocity.Length() > VELOCITYMAXIMUM)
//                {
//                    _velocity = Vector2.Normalize(_velocity) * VELOCITYMAXIMUM;
//                }

//                //  Apply drag tot he velocity
//                _velocity -= _velocity * (Engine.DeltaTime * DRAGPERSECOND);
//                if(_velocity.LengthSquared() <- 0f)
//                {
//                    _velocity = Vector2.Zero;
//                }

//                this.Position += _velocity * Engine.DeltaTime;
//            }



//            this.Position = Vector2.Clamp(this.Position, Vector2.Zero, new Vector2(Engine.Width - this.Width, Engine.Height - this.Height));
//        }



//    }
//}
