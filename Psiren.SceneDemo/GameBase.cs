﻿//----------------------------------------------------------
//  GameBase
//  This is the Game1.cs file (I rename mine to GameBase, you
//  call yours whatever works for you).  Normally in MonoGame
//  This would inherit from the Game base class. Instead
//  We'll inherit from Engine now.
//
//
//  For the constructor, call the :base() constructor as well
//  to set the width, height, windowWidth, windowHeight,
//  windowTitle, and fullScreen paramters
//
//  The other requirement is going to be overriding the 
//  Initialize() method.  Override this and be sure to
//  include the following:
//      Tag initialization
//      Assign an InputProfile to the Engine
//      Initialize the InputProfile
//      Set the initial Scene to use
//
//  There are other overloads that you can use here, so
//  feel free to explore.  This is just limited to those
//  used for this demo. You can find more information about
//  the others on the wiki
//----------------------------------------------------------
using Psiren.SceneDemo.Scenes;
using Psiren;

namespace Psiren.SceneDemo
{
    public class GameBase : Engine
    {
        /// <summary>
        ///     Default constructor.  Calls the base() constructor
        ///     to set initial values needed
        /// </summary>
        public GameBase()
            :base(1280, 720, 1280, 720, "Psiren.SceneDemo", false)
        {

        }

        /// <summary>
        ///     Need to override Initialize so that you can
        ///     setup the needed initializations for your game
        ///     that work with the Engine
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();

            //  At minimum you should perform the following
            //  in the initialize method.  

            //  Initialize your tags
            Tags.Initialize();

            //  Assign and initialize the input profile to use
            Engine.InputProfiles = new System.Collections.Generic.Dictionary<PlayerNumber, Input.IInputProfile>();
            Engine.InputProfiles.Add(PlayerNumber.One, new DemoInputProfile());
            Engine.InputProfiles[PlayerNumber.One].Initialize();

            //  Set the initial scene
            Scene = new TitleScene();
        }

    }
}
