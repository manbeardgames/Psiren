﻿using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Tests.ContentPipelineExtensions
{
    public static class Tags
    {
        public static BitTag EmptyTag;
        public static BitTag TestTag;

        public static void Initialize()
        {
            TestTag= new BitTag("TestTag");
            EmptyTag = new BitTag("empty");
        }
    }
}
