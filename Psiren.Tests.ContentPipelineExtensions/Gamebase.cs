﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Psiren.Tests.ContentPipelineExtensions.Scenes;

namespace Psiren.Tests.ContentPipelineExtensions
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Gamebase : Engine
    {
        
        public Gamebase():base(width: 1280, height: 704, windowWidth: 1280, windowHeight: 704, windowTitle:"Content Pipeline Extension Testing", fullscreen: false)
        {
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            Tags.Initialize();

            Scene = new OgmoImportTestScene();

        }
    }
}
