﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Colliders;
using Psiren.Components.Graphics;
using Psiren.Components.Graphics.Text;
using Psiren.Entities;
using Psiren.Graphics;
using Psiren.Input;
using Psiren.Ogmo;
using Psiren.Renderers;
using Psiren.Scenes;
using Psiren.Utils;
using System;
using System.Collections.Generic;

namespace Psiren.Tests.ContentPipelineExtensions.Scenes
{
    public class OgmoImportTestScene : Scene
    {
        SingleTagRenderer _tagRenderer;
        OgmoLevel level;


        public OgmoImportTestScene():base()
        {
            //  Setup Renderer
            _tagRenderer = new SingleTagRenderer(Tags.TestTag);
            this.RendererList.Add(_tagRenderer);


            level = Engine.Instance.Content.Load<OgmoLevel>("maps/testlevel");

            Entity testEntity = new Entity(new Vector2(Engine.Width, Engine.Height) * 0.5f);
            testEntity.Collider = new Hitbox(100, 100);
            testEntity.Add(new Image(new MTexture(100, 100, Color.White)));
            testEntity.Tag = Tags.TestTag;

            this.Add(testEntity);

        }

        public override void Update()
        {
            base.Update();

            //if(MInput.Mouse.WheelDelta != 0)
            //{
            //    _renderer.Camera.Zoom += 0.1f * Math.Sign(MInput.Mouse.WheelDelta);
            //}
        }

        public override void Render()
        {
            //level.Render(_renderer.Camera);

            
            base.Render();
        }
    }
}
