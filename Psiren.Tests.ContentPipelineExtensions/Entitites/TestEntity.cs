﻿using Psiren.Colliders;
using Psiren.Entities;
using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Tests.ContentPipelineExtensions.Entitites
{
    [Tracked]
    public class TestEntity : Entity
    {

        public TestEntity()
        {
            Collider = new Hitbox(10, 10);
            this.Position = new Microsoft.Xna.Framework.Vector2(Engine.Width, Engine.Height) * 0.5f;
        }
    }
}
