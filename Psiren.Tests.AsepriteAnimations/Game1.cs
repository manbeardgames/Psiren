﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Psiren.Tests.AsepriteAnimations.Scenes;

namespace Psiren.Tests.AsepriteAnimations
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Engine
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        public Game1()
            :base(1280, 720, 1280, 720, "Test Aseprite Animation", false)
        {
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();

            Engine.Scene = new TestScene();
        }
    }
}
