﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Colliders;
using Psiren.Components.Graphics.Aseprite;
using Psiren.Entities;
using Psiren.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Tests.AsepriteAnimations.Entities
{
    public class TestEntity : Entity
    {
        AsepriteAnimation _animation;

        public TestEntity(Vector2 position, string aid) : base(position)
        {
            //Position = new Vector2(Engine.Width * 0.5f, Engine.Height);

            MTexture texture = new MTexture(Engine.Instance.Content.Load<Texture2D>("skeleton"));
            AsepriteDefinition definition = Engine.Instance.Content.Load<AsepriteDefinition>("skeleton-animation");
            this._animation = new AsepriteAnimation(texture, definition);
            this._animation.Play(aid);
            this._animation.SetOrigin(0, this._animation.Height);
            this._animation.Scale = new Vector2(3.0f, 3.0f);
            Add(this._animation);

            this.Collider = new Hitbox(_animation.Width*_animation.Scale.X, _animation.Height * _animation.Scale.Y, 0, -_animation.Height * _animation.Scale.Y);
        }

        public override void Update()
        {
            base.Update();
            this._animation.SetOrigin(0, this._animation.Height);
            this.Collider.Width = _animation.Width * _animation.Scale.X;
            this.Collider.Height = _animation.Height * _animation.Scale.Y;
            this.Collider.Position.Y = -_animation.Height * _animation.Scale.Y;
        }
    }
}
