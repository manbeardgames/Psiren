﻿using Microsoft.Xna.Framework;
using Psiren.Colliders;
using Psiren.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Tests.AsepriteAnimations.Entities
{
    public class ColliderEntity :Entity
    {
        public ColliderEntity():base(Vector2.Zero)
        {
            this.Collider = new Hitbox(100, 100);
        }
    }
}
