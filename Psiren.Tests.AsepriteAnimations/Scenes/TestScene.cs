﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Components.Graphics.Aseprite;
using Psiren.Entities;
using Psiren.Renderers;
using Psiren.Scenes;
using Psiren.Tests.AsepriteAnimations.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Tests.AsepriteAnimations.Scenes
{
    public class TestScene : Scene
    {
        TestEntity _idleEntity;
        TestEntity _walkEntity;
        TestEntity _attackEntity;
        TestEntity _returnEntity;
        TestEntity _hitEntity;
        TestEntity _deadEntity;


        EverythingRenderer _renderer;

        public TestScene() : base()
        {
            this._renderer = new EverythingRenderer();
            this._renderer.SamplerState = SamplerState.PointClamp;
            RendererList.Add(this._renderer);

            float baseX = 100.0f;
            float size = 100.0f;
            float margin = 100.0f;
            this._idleEntity = new TestEntity(new Vector2(baseX, Engine.Height * 0.5f), "idle");

            this._walkEntity = new TestEntity(new Vector2(_idleEntity.Position.X + _idleEntity.Width + margin, Engine.Height * 0.5f), "walk");
            this._attackEntity = new TestEntity(new Vector2(_walkEntity.Position.X + _walkEntity.Width + margin, Engine.Height * 0.5f), "attack");
            this._returnEntity = new TestEntity(new Vector2(_attackEntity.Position.X + _attackEntity.Width + margin, Engine.Height * 0.5f), "return");
            this._hitEntity = new TestEntity(new Vector2(_returnEntity.Position.X + _returnEntity.Width + margin, Engine.Height * 0.5f), "hit");
            this._deadEntity = new TestEntity(new Vector2(_hitEntity.Position.X + _hitEntity.Width + margin, Engine.Height * 0.5f), "dead");
            Add(this._idleEntity);
            Add(this._walkEntity);
            Add(this._attackEntity);
            Add(this._returnEntity);
            Add(this._hitEntity);
            Add(this._deadEntity);


        }
    }
}
