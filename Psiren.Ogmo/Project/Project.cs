﻿//////////--------------------------------------------------------------------------------
//////////  Project
//////////
//////////  Project is the root for the OGMO project oep files
//////////
//////////-----------------------------Psiren Engine License-----------------------------

//////////    Copyright(c) 2018   Chris Whitley
//////////
//////////    Permission is hereby granted, free of charge, to any person obtaining a copy
//////////    of this software and associated documentation files (the "Software"), to deal
//////////    in the Software without restriction, including without limitation the rights
//////////    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//////////    copies of the Software, and to permit persons to whom the Software is
//////////    furnished to do so, subject to the following conditions:

//////////    The above copyright notice and this permission notice shall be included in
//////////    all copies or substantial portions of the Software.

//////////    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//////////    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//////////    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//////////    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//////////    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//////////    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//////////    THE SOFTWARE.
//////////--------------------------------------------------------------------------------
////////using Microsoft.Xna.Framework;
////////using System;
////////using System.Collections.Generic;
////////using System.Xml;


////////namespace Psiren.Ogmo.Project
////////{
////////    public class OgmoProject
////////    {
////////        /// <summary>
////////        ///     The version of OGMO used to create the project file
////////        /// </summary>
////////        public string Version { get; set; }

////////        /// <summary>
////////        ///     The name of the project
////////        /// </summary>
////////        public string Name { get; set; }

////////        /// <summary>
////////        ///     The background color used in the project editor
////////        /// </summary>
////////        public OColor BackgroundColor { get; set; }

////////        /// <summary>
////////        ///     The color of hte grid used in the project editor
////////        /// </summary>
////////        public OColor GridColor { get; set; }

////////        /// <summary>
////////        ///     The default size specified when creating new levels
////////        /// </summary>
////////        public Size LevelDefaultSize { get; set; }

////////        /// <summary>
////////        ///     The minimum size of a level
////////        /// </summary>
////////        public Size LevelMinimumSize { get; set; }

////////        /// <summary>
////////        ///     The maximum size of level
////////        /// </summary>
////////        public Size LevelMaximumSize { get; set; }

////////        /// <summary>
////////        ///     The fully qualified file name of the level
////////        /// </summary>
////////        public string FileName { get; set; }

////////        /// <summary>
////////        ///     The <see cref="AngleMode"/> used for defining angles
////////        /// </summary>
////////        public AngleMode AngleMode { get; set; }

////////        /// <summary>
////////        ///     Is the camera enabled in OGMO
////////        /// </summary>
////////        public bool CameraEnabled { get; set; }

////////        /// <summary>
////////        ///     The size of the camera in OGMO
////////        /// </summary>
////////        public Size CameraSize { get; set; }

////////        /// <summary>
////////        ///     Should the camera's position be exported
////////        /// </summary>
////////        public bool ExportCameraPosition { get; set; }

////////        /// <summary>
////////        ///     Int type Level Value Definitions
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<IntValueDefinition> IntValueDefinitions { get; set; }

////////        /// <summary>
////////        ///     Bool type Level Value Definitions
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<BoolValueDefinition> BoolValueDefinitions { get; set; }

////////        /// <summary>
////////        ///     Float type Level Value Definitions
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<FloatValueDefinition> FloatValueDefinitions { get; set; }

////////        /// <summary>
////////        ///     String type Level Value Definitions
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<StringValueDefinition> StringValueDefinitions { get; set; }

////////        /// <summary>
////////        ///     Enum type Level Value Definitions
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<EnumValueDefinition> EnumValueDefinitions { get; set; }

////////        /// <summary>
////////        ///     Color type Level Value Definitions
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<ColorValueDefinition> ColorValueDefinitions { get; set; }

////////        /// <summary>
////////        ///     Collection of <see cref="TileLayerDefinition"/>
////////        /// </summary>
////////        public List<TileLayerDefinition> TileLayerDefinitions { get; set; }

////////        /// <summary>
////////        ///     Collection of <see cref="GridLayerDefinition"/>
////////        /// </summary>
////////        public List<GridLayerDefinition> GridLayerDefinitions { get; set; }

////////        /// <summary>
////////        ///     Collection of <see cref="EntityLayerDefinition"/>
////////        /// </summary>
////////        public List<EntityLayerDefinition> EntityLayerDefinitions { get; set; }

////////        /// <summary>
////////        ///     Collection of <see cref="TileSet"/>
////////        /// </summary>
////////        public List<TileSet> TileSets { get; set; }


////////        /// <summary>
////////        ///     Collection of <see cref="EntityDefinition"/>
////////        /// </summary>
////////        /// <remarks>
////////        ///     This is currently not used
////////        /// </remarks>
////////        [Obsolete("Currently not in use", true)]
////////        public List<EntityDefinition> EntityDefinitions { get; set; }

////////        public static OgmoProject FromXmlFile(string filePath)
////////        {
////////            XmlDocument doc = new XmlDocument();
////////            doc.Load(filePath);
////////            return OgmoProject.FromXmlDocument(doc);
////////        }

////////        public static OgmoProject FromXmlString(string xml)
////////        {
////////            XmlDocument doc = new XmlDocument();
////////            doc.LoadXml(xml);
////////            return OgmoProject.FromXmlDocument(doc);
////////        }

////////        public static OgmoProject FromXmlDocument(XmlDocument doc)
////////        {
////////            OgmoProject project = new OgmoProject();
////////            //  Create a new OgmoProject

////////            var projectElement = doc.DocumentElement;

////////            //--------------------------------------
////////            //  Version
////////            //--------------------------------------
////////            XmlNode versionNode = doc.SelectSingleNode("/project/OgmoVersion");
////////            project.Version = versionNode.InnerText;


////////            //--------------------------------------
////////            //  Name
////////            //--------------------------------------
////////            XmlNode nameNode = doc.SelectSingleNode("/project/Name");
////////            project.Name = nameNode.InnerText;


////////            //--------------------------------------
////////            //  Background Color
////////            //--------------------------------------
////////            XmlNode backgroundColorNode = doc.SelectSingleNode("/project/BackgroundColor");
////////            project.BackgroundColor = new OColor()
////////            {
////////                R = Int32.Parse(backgroundColorNode.Attributes["R"].Value),
////////                G = Int32.Parse(backgroundColorNode.Attributes["G"].Value),
////////                B = Int32.Parse(backgroundColorNode.Attributes["B"].Value),
////////                A = Int32.Parse(backgroundColorNode.Attributes["A"].Value)
////////            };

////////            //--------------------------------------
////////            //  Grid Color
////////            //--------------------------------------
////////            XmlNode gridColorNode = doc.SelectSingleNode("/project/GridColor");
////////            project.GridColor = new OColor()
////////            {
////////                R = Int32.Parse(gridColorNode.Attributes["R"].Value),
////////                G = Int32.Parse(gridColorNode.Attributes["G"].Value),
////////                B = Int32.Parse(gridColorNode.Attributes["B"].Value),
////////                A = Int32.Parse(gridColorNode.Attributes["A"].Value)
////////            };



////////            //--------------------------------------
////////            //  Level Default Size
////////            //--------------------------------------
////////            XmlNode levelDefaultSizeWidthNode = doc.SelectSingleNode("/project/LevelDefaultSize/Width");
////////            XmlNode levelDefaultSizeHeightNode = doc.SelectSingleNode("/project/LevelDefaultSize/Height");
////////            project.LevelDefaultSize = new Size()
////////            {
////////                Width = Int32.Parse(levelDefaultSizeWidthNode.InnerText),
////////                Height = Int32.Parse(levelDefaultSizeHeightNode.InnerText)
////////            };

////////            //--------------------------------------
////////            //  Level Minimum Size
////////            //--------------------------------------
////////            XmlNode levelMinimumSizeWidthNode = doc.SelectSingleNode("/project/LevelMinimumSize/Width");
////////            XmlNode levelMinimumSizeHeightNode = doc.SelectSingleNode("/project/LevelMinimumSize/Height");
////////            project.LevelMinimumSize = new Size()
////////            {
////////                Width = Int32.Parse(levelMinimumSizeWidthNode.InnerText),
////////                Height = Int32.Parse(levelMinimumSizeHeightNode.InnerText)
////////            };


////////            //--------------------------------------
////////            //  Level Maximum Size
////////            //--------------------------------------
////////            XmlNode levelMaximumSizeWidthNode = doc.SelectSingleNode("/project/LevelMaximumSize/Width");
////////            XmlNode levelMaximumSizeHeightNode = doc.SelectSingleNode("/project/LevelMaximumSize/Height");
////////            project.LevelMaximumSize = new Size()
////////            {
////////                Width = Int32.Parse(levelMaximumSizeWidthNode.InnerText),
////////                Height = Int32.Parse(levelMaximumSizeHeightNode.InnerText)
////////            };

////////            //--------------------------------------
////////            //  Filename
////////            //--------------------------------------
////////            XmlNode fileNameNode = doc.SelectSingleNode("/project/Filename");
////////            project.FileName = fileNameNode.InnerText;


////////            //--------------------------------------
////////            //  Angle Mode
////////            //--------------------------------------
////////            XmlNode angleModeNode = doc.SelectSingleNode("/project/AngleMode");
////////            Enum.TryParse<AngleMode>(angleModeNode.InnerText, out AngleMode angleMode);
////////            project.AngleMode = angleMode;


////////            //--------------------------------------
////////            //  Camera Enabled
////////            //--------------------------------------
////////            XmlNode camerEnabledNode = doc.SelectSingleNode("/project/CameraEnabled");
////////            Boolean.TryParse(camerEnabledNode.InnerText, out bool cameraEnabled);
////////            project.CameraEnabled = cameraEnabled;


////////            //--------------------------------------
////////            //  Camera Size
////////            //--------------------------------------
////////            XmlNode cameraSizeWidthNode = doc.SelectSingleNode("/project/CameraSize/Width");
////////            XmlNode cameraSizeHeightNode = doc.SelectSingleNode("/project/CameraSize/Height");
////////            Int32.TryParse(cameraSizeWidthNode.InnerText, out int cameraWidth);
////////            Int32.TryParse(cameraSizeHeightNode.InnerText, out int cameraHeight);
////////            project.CameraSize = new Size()
////////            {
////////                Width = cameraWidth,
////////                Height = cameraHeight
////////            };


////////            //--------------------------------------
////////            //  Export Camera Position
////////            //--------------------------------------
////////            XmlNode exportCameraPositionNode = doc.SelectSingleNode("/project/ExportCameraPosition");
////////            Boolean.TryParse(exportCameraPositionNode.InnerText, out bool exportCameraPosition);
////////            project.ExportCameraPosition = exportCameraPosition;

////////            //--------------------------------------
////////            //  Level Value Definitions
////////            //--------------------------------------
////////            //  LevelValueDefinitions nodes would be next, but skipping for now

////////            //--------------------------------------
////////            //  Layer Definitions
////////            //--------------------------------------
////////            project.TileLayerDefinitions = new List<TileLayerDefinition>();
////////            project.GridLayerDefinitions = new List<GridLayerDefinition>();
////////            project.EntityLayerDefinitions = new List<EntityLayerDefinition>();
////////            XmlNode layerDefinitionsNode = doc.SelectSingleNode("/project/LayerDefinitions");
////////            foreach (XmlNode child in layerDefinitionsNode)
////////            {
////////                XmlElement layerNameElement = child["Name"];

////////                XmlElement layerGridElement = child["Grid"];
////////                XmlElement layerGridWidthElement = layerGridElement["Width"];
////////                Int32.TryParse(layerGridWidthElement.InnerText, out int layerGridWidth);

////////                XmlElement layerGridHeightElement = layerGridElement["Height"];
////////                Int32.TryParse(layerGridHeightElement.InnerText, out int layerGridHeight);

////////                XmlElement layerScrollFactorElement = child["ScrollFactor"];

////////                XmlElement layerScrollFactorXElement = layerScrollFactorElement["X"];
////////                Int32.TryParse(layerScrollFactorXElement.InnerText, out int layerScrollFactorX);

////////                XmlElement layerScrollFactorYElement = layerScrollFactorElement["Y"];
////////                Int32.TryParse(layerScrollFactorYElement.InnerText, out int layerScrollFactorY);

////////                //  Get the type
////////                string type = child.Attributes["xsi:type"].Value;

////////                if (type == "TileLayerDefinition")
////////                {
////////                    XmlElement tileLayerExportModeElement = child["ExportMode"];
////////                    Enum.TryParse<TileLayerExportMode>(tileLayerExportModeElement.InnerText, out TileLayerExportMode tileLayerExportMode);

////////                    project.TileLayerDefinitions.Add(new TileLayerDefinition()
////////                    {
////////                        Name = layerNameElement.InnerText,
////////                        Grid = new Size()
////////                        {
////////                            Width = layerGridWidth,
////////                            Height = layerGridHeight
////////                        },
////////                        ScrollFactor = new Vector2()
////////                        {
////////                            X = layerScrollFactorX,
////////                            Y = layerScrollFactorY
////////                        },
////////                        ExportMode = tileLayerExportMode
////////                    });
////////                }
////////                else if (type == "EntityLayerDefinition")
////////                {
////////                    project.EntityLayerDefinitions.Add(new EntityLayerDefinition()
////////                    {
////////                        Name = layerNameElement.InnerText,
////////                        Grid = new Size()
////////                        {
////////                            Width = layerGridWidth,
////////                            Height = layerGridHeight
////////                        },
////////                        ScrollFactor = new Vector2()
////////                        {
////////                            X = layerScrollFactorX,
////////                            Y = layerScrollFactorY
////////                        }
////////                    });
////////                }
////////                else if (type == "GridLayerDefinition")
////////                {
////////                    XmlElement layerColorElement = child["Color"];

////////                    Int32.TryParse(layerColorElement.Attributes["A"].Value, out int layerColorA);
////////                    Int32.TryParse(layerColorElement.Attributes["R"].Value, out int layerColorR);
////////                    Int32.TryParse(layerColorElement.Attributes["G"].Value, out int layerColorG);
////////                    Int32.TryParse(layerColorElement.Attributes["B"].Value, out int layerColorB);

////////                    XmlElement layerGridExportModeElement = child["ExportMode"];
////////                    Enum.TryParse<GridLayerExportMode>(layerGridExportModeElement.InnerText, out GridLayerExportMode gridLayerExportMode);

////////                    project.GridLayerDefinitions.Add(new GridLayerDefinition()
////////                    {
////////                        Name = layerNameElement.InnerText,
////////                        Grid = new Size()
////////                        {
////////                            Width = layerGridWidth,
////////                            Height = layerGridHeight
////////                        },
////////                        ScrollFactor = new Vector2()
////////                        {
////////                            X = layerScrollFactorX,
////////                            Y = layerScrollFactorY
////////                        },
////////                        Color = new OColor()
////////                        {

////////                            R= layerColorR,
////////                            G= layerColorG,
////////                            B= layerColorB,
////////                            A= layerColorA
////////                        },
////////                        ExportMode = gridLayerExportMode
////////                    });
////////                }

////////            }


////////            //--------------------------------------
////////            //  TileSets
////////            //--------------------------------------
////////            XmlNode tileSetsNode = doc.SelectSingleNode("/project/Tilesets");
////////            if (tileSetsNode != null)
////////            {
////////                project.TileSets = new List<TileSet>();

////////                foreach (XmlNode child in tileSetsNode)
////////                {
////////                    XmlElement tileSetNameElement = child["Name"];
////////                    XmlElement tileSetFilePathElement = child["FilePath"];
////////                    XmlElement tileSetSizeElement = child["TileSize"];

////////                    XmlElement tileSetSizeWidthElement = tileSetSizeElement["Width"];
////////                    Int32.TryParse(tileSetSizeWidthElement.InnerText, out int tileSetSizeWidth);

////////                    XmlElement tileSetSizeHeightElement = tileSetSizeElement["Height"];
////////                    Int32.TryParse(tileSetSizeHeightElement.InnerText, out int tileSetSizeHeight);

////////                    XmlElement tileSetTileSepElement = child["TileSep"];
////////                    Int32.TryParse(tileSetTileSepElement.InnerText, out int tileSetSizeSep);

////////                    project.TileSets.Add(new TileSet()
////////                    {
////////                        Name = tileSetNameElement.InnerText,
////////                        FileName = System.IO.Path.GetFileName(tileSetFilePathElement.InnerText),
////////                        FileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(tileSetFilePathElement.InnerText),
////////                        TileSize = new Size()
////////                        {
////////                            Width = tileSetSizeWidth,
////////                            Height = tileSetSizeHeight
////////                        },
////////                        TileSep = tileSetSizeSep
////////                    });

////////                }
////////            }

////////            //--------------------------------------
////////            //  Entity Definitions
////////            //--------------------------------------
////////            //  Skipping these for now

////////            return project;

////////        }

////////        public static OgmoProject FromJson(string json)
////////        {
////////            return Newtonsoft.Json.JsonConvert.DeserializeObject<OgmoProject>(json);
////////        }

////////        public int[] GetTileCount(LayerDefinition layerDefinition, int width, int height)
////////        {
////////            int[] tileCount = new int[2];

////////            tileCount[0] = width / layerDefinition.Grid.Width;
////////            tileCount[1] = height / layerDefinition.Grid.Height;

////////            return tileCount;
////////        }
        
////////    }
////////}
