﻿////////////--------------------------------------------------------------------------------
////////////  ProjectReader
////////////
////////////  This is the ContentReader for Level objects
////////////
////////////-----------------------------Psiren Engine License-----------------------------

////////////    Copyright(c) 2018   Chris Whitley
////////////
////////////    Permission is hereby granted, free of charge, to any person obtaining a copy
////////////    of this software and associated documentation files (the "Software"), to deal
////////////    in the Software without restriction, including without limitation the rights
////////////    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
////////////    copies of the Software, and to permit persons to whom the Software is
////////////    furnished to do so, subject to the following conditions:

////////////    The above copyright notice and this permission notice shall be included in
////////////    all copies or substantial portions of the Software.

////////////    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
////////////    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
////////////    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
////////////    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
////////////    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
////////////    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
////////////    THE SOFTWARE.
////////////--------------------------------------------------------------------------------
//////////using Microsoft.Xna.Framework.Content;
//////////using Microsoft.Xna.Framework.Graphics;
//////////using System.Collections.Generic;
//////////using TRead = Psiren.Ogmo.Level.OgmoLevel;

//////////namespace Psiren.Ogmo.Level
//////////{
//////////    public class LevelReader : ContentTypeReader<TRead>
//////////    {
//////////        protected override TRead Read(ContentReader input, TRead existingInstance)
//////////        {
//////////            //  First read level string in
//////////            string json = input.ReadString();

//////////            //  Next read the number of tilesets as int
//////////            int tileSetCount = input.ReadInt32();


//////////            Dictionary<string, byte[]> tileSets = new Dictionary<string, byte[]>();

//////////            //  Now read all the tilesets
//////////            for (int i = 0; i < tileSetCount; i++)
//////////            {
//////////                //  Read the name of the tileset as a string
//////////                string tilesetName = input.ReadString();

//////////                //  Read the number of bytes
//////////                int byteCount = input.ReadInt32();

//////////                //  Read the tileset byte[]
//////////                byte[] tileSetBytes = input.ReadBytes(byteCount);

//////////                tileSets.Add(tilesetName, tileSetBytes);
//////////            }




//////////            //  Create Level
//////////            OgmoLevel level = Newtonsoft.Json.JsonConvert.DeserializeObject<OgmoLevel>(json);
//////////            level.TileSetsAsBytes = tileSets;




//////////            //  Return project
//////////            return level;
//////////        }
//////////    }
//////////}
