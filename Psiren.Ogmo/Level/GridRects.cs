﻿////////namespace Psiren.Ogmo.Level
////////{
////////    public class GridRect
////////    {
////////        /// <summary>
////////        ///     The x-coordinate top-left position
////////        /// </summary>
////////        public int X { get; set; }

////////        /// <summary>
////////        ///     The y-coordinate top-left position
////////        /// </summary>
////////        public int Y { get; set; }

////////        /// <summary>
////////        ///     The width of the grid rectangle
////////        /// </summary>
////////        public int Width { get; set; }

////////        /// <summary>
////////        ///     The height of the grid rectangle
////////        /// </summary>
////////        public int Height { get; set; }
////////    }
////////}
