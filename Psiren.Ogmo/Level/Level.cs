﻿////////using Microsoft.Xna.Framework.Graphics;
////////using Psiren.Graphics;
////////using Psiren.Ogmo.Project;
////////using System;
////////using System.Collections.Generic;
////////using System.Xml;


////////namespace Psiren.Ogmo.Level
////////{
////////    public class OgmoLevel
////////    {
////////        public string Name { get; set; }
////////        public int Width { get; set; }
////////        public int Height { get; set; }

////////        public List<TileLayer> TileLayers { get; set; }
////////        public List<GridLayer> GridLayers { get; set; }
////////        public List<EntityLayer> EntityLayers { get; set; }

////////        public OgmoProject OgmoProject;


////////        public Dictionary<string, byte[]> TileSetsAsBytes { get; set; }
////////        public Dictionary<string, MTexture> TileSets { get; set; }



////////        public void InitilizeTileSets(GraphicsDevice gd)
////////        {
////////            TileSets = new Dictionary<string, MTexture>();

////////            foreach(var kvp in TileSetsAsBytes)
////////            {
////////                //  Create memory stream
////////                var memoryStream = new System.IO.MemoryStream(kvp.Value);

////////                //  Convert byte[] into Texture2D
////////                Texture2D texture = Texture2D.FromStream(gd, memoryStream);

////////                MTexture mTexture = new MTexture(texture);

////////                //  Add to dictionary
////////                TileSets.Add(kvp.Key, mTexture);

////////            }
////////        }
////////    }
////////}
