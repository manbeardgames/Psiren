﻿////////namespace Psiren.Ogmo.Level
////////{
////////    public class OgmoEntity
////////    {
////////        /// <summary>
////////        ///     The name of the entitiy
////////        /// </summary>
////////        public string Name { get; set; }

////////        /// <summary>
////////        ///     The id of the entity
////////        /// </summary>
////////        public int ID { get; set; }

////////        /// <summary>
////////        ///     The x-coordinate position of the entity
////////        /// </summary>
////////        public int X { get; set; }

////////        /// <summary>
////////        ///     The y-coordinate position of the entity
////////        /// </summary>
////////        public int Y { get; set; }
////////    }
////////}
