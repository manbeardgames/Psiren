﻿////////using System.Collections.Generic;

////////namespace Psiren.Ogmo.Level
////////{
////////    public class TileLayer : Layer
////////    {
////////        /// <summary>
////////        ///     The name of the <see cref="Project.TileSet"/> used for this layer
////////        /// </summary>
////////        public string TileSet { get; set; }

////////        /// <summary>
////////        ///     Collection of the <see cref="Tile"/>s defined for this layer
////////        /// </summary>
////////        public List<Tile> Tiles { get; set; }
////////    }
////////}
