﻿////////namespace Psiren.Ogmo.Level
////////{
////////    public class Layer
////////    {
////////        /// <summary>
////////        ///     The name of the layer
////////        /// </summary>
////////        public string Name { get; set; }

////////        /// <summary>
////////        ///     Value representing the order of this layer as defined
////////        ///     in the OGMO Editor when the level was created. 
////////        ///     This is defined back-to-front, meaning that lower valued
////////        ///     order is behind those that have a higher value
////////        /// </summary>
////////        public int Order { get; set; }
////////    }
////////}
