﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Psiren.Ogmo.Converters;

namespace Psiren.Ogmo
{
    public class OgmoTile
    {
        public Vector2 Size { get; set; }
        public Vector2 Position { get; set; }

        [JsonConverter(typeof(ColorConverter))]
        public Color Color { get; set; }        

        public string TileSetName { get; set; }
        public Vector2 TileSheetPosition { get; set; }

        public Vector2 TileSheetTileSize { get; set; }
        public int TileSheetSep { get; set; }

        public Vector2 Scale { get; set; }
    }
}
