﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.Graphics;
using Psiren.Renderers;
using Psiren.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Psiren.Ogmo
{
    public class OgmoLevel
    {
        public string Name { get; set; }
        public Vector2 Size { get; set; }
        public Dictionary<string, string> LevelValues { get; set; }
        public Dictionary<string, OgmoLayerDefinition> LayerDefinitions { get; set; }
        public List<OgmoLayer> Layers { get; set; }
        public Dictionary<string, byte[]> TileSetBytes { get; set; }
        public Dictionary<string, MTexture> TileSets { get; set; }

        public void Render(Camera camera)
        {
            Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, camera.Matrix * Engine.ScreenMatrix);
            foreach (var layer in this.Layers)
            {
                foreach (var tile in layer.Tiles)
                {
                    if (string.IsNullOrEmpty(tile.TileSetName)) { break; }

                    TileSets[tile.TileSetName].GetSubtexture((int)tile.TileSheetPosition.X, (int)tile.TileSheetPosition.Y, (int)tile.TileSheetTileSize.X, (int)tile.TileSheetTileSize.Y)
                        .Draw(tile.Position, Vector2.Zero, Color.White, tile.Scale.X);
                }
            }
            Draw.SpriteBatch.End();


            if (Engine.Commands.Open) { DebugRender(camera); }
        }

        public void DebugRender(Camera camera)
        {
            Draw.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone, null, camera.Matrix * Engine.ScreenMatrix);
            foreach (var layer in this.Layers)
            {
                foreach (var tile in layer.Tiles)
                {
                    if (!string.IsNullOrEmpty(tile.TileSetName)) { break; }

                    Draw.HollowRect(tile.Position.X, tile.Position.Y, tile.Size.X, tile.Size.Y, tile.Color);
                    //Draw.Rect(tile.Position.X, tile.Position.Y, tile.Size.X, tile.Size.Y, tile.Color * 0.15f);
                    Draw.Rect(tile.Position.X, tile.Position.Y, tile.Size.X, tile.Size.Y, Color.Blue);


                }
            }
            Draw.SpriteBatch.End();
        }
    }

}
