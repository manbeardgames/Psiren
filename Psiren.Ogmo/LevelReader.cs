﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using Psiren.Graphics;
using System.Collections.Generic;
using System.IO;

namespace Psiren.Ogmo
{
    public class LevelReader : ContentTypeReader<OgmoLevel>
    {
        protected override OgmoLevel Read(ContentReader input, OgmoLevel existingInstance)
        {
            //  First read the level string in
            string json = input.ReadString();

            //  Now deseralize the json 
            OgmoLevel level = JsonConvert.DeserializeObject<OgmoLevel>(json);

            //  Initilize the tilesets as MTextures
            level.TileSets = new Dictionary<string, Graphics.MTexture>();
            foreach (var tilesetAsBytes in level.TileSetBytes)
            {
                //  Create a memory stream
                var memoryStream = new MemoryStream(tilesetAsBytes.Value);

                //  Convert byte[] into Texture2D
                Texture2D texture = Texture2D.FromStream(Engine.Graphics.GraphicsDevice, memoryStream);

                //  Convert texture to MTexture
                MTexture mTexture = new MTexture(texture);

                //  Add to the dictionary
                level.TileSets.Add(tilesetAsBytes.Key, mTexture);
            }

            level.TileSetBytes = null;

            return level;
        }
    }
}
