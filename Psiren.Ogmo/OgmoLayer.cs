﻿using System.Collections.Generic;

namespace Psiren.Ogmo
{
    public class OgmoLayer
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public List<OgmoTile> Tiles { get; set; }
    }
}
