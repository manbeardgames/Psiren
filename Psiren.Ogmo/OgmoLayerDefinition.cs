﻿using Microsoft.Xna.Framework;

namespace Psiren.Ogmo
{
    public class OgmoLayerDefinition
    {
        public string Name { get; set; }
        public Vector2 Grid { get; set; }
        public Vector2 ScrollFactor { get; set; }
    }
}
